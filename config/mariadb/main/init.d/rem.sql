CREATE DATABASE rem_api;
USE rem_api;

CREATE TABLE `DistributedCSRFToken` (
  `token` varchar(100) NOT NULL,
  `ipAddress` varchar(64) NOT NULL,
  `userAgent` varchar(255) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `user` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`token`)
);

CREATE TABLE `Sessions` (
  `id` SERIAL,
  `username` varchar(63) NOT NULL,
  `time` datetime NOT NULL,
  `organization` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;