### REQUIRED FOR REPLICATION - DO NOT REMOVE ###
CREATE USER 'repluser'@'%' IDENTIFIED BY 'replsecret';
GRANT REPLICATION SLAVE ON *.* TO 'repluser'@'%';

## Generate base membership DB
CREATE DATABASE membership_api;
USE membership_api;
CREATE TABLE `MembershipForm` (
  `id` varchar(255) NOT NULL,
  `dateCreated` bigint(20) DEFAULT NULL,
  `dateUpdated` bigint(20) DEFAULT NULL,
  `dateSubmitted` bigint(20) DEFAULT NULL,
  `membershipLevel` varchar(255) DEFAULT NULL,
  `purchaseOrderRequired` varchar(255) DEFAULT NULL,
  `registrationCountry` varchar(255) DEFAULT NULL,
  `signingAuthority` bit(1) NOT NULL,
  `userID` varchar(255) DEFAULT NULL,
  `vatNumber` varchar(255) DEFAULT NULL,
  `applicationGroup` varchar(255) DEFAULT NULL,
  `state` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `Contact` (
  `id` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fName` varchar(255) DEFAULT NULL,
  `lName` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `form_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2g2hji3xsvakv5blqilnol5ad` (`form_id`),
  CONSTRAINT `FK2g2hji3xsvakv5blqilnol5ad` FOREIGN KEY (`form_id`) REFERENCES `MembershipForm` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `FormOrganization` (
  `id` varchar(255) NOT NULL,
  `legalName` varchar(255) DEFAULT NULL,
  `twitterHandle` varchar(255) DEFAULT NULL,
  `aggregateRevenue` varchar(255) DEFAULT NULL,
  `organizationType` varchar(255) DEFAULT NULL,
  `employeeCount` varchar(255) DEFAULT NULL,
  `form_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_27vg3uhbmy3ev7ote4fjd4evl` (`form_id`),
  CONSTRAINT `FKib65ho89l5rvfgqql7wq15oxv` FOREIGN KEY (`form_id`) REFERENCES `MembershipForm` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `FormWorkingGroup` (
  `id` varchar(255) NOT NULL,
  `effectiveDate` datetime(6) DEFAULT NULL,
  `participationLevel` varchar(255) DEFAULT NULL,
  `workingGroupID` varchar(255) DEFAULT NULL,
  `contact_id` varchar(255) DEFAULT NULL,
  `form_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_1gt3qh8yu1jpky54o38igvfrq` (`contact_id`),
  KEY `UK_2qrha8ti59jcxnvi2h8b9d2u8` (`form_id`),
  CONSTRAINT `FK8mhoi37sufequy7nnn19811rv` FOREIGN KEY (`form_id`) REFERENCES `MembershipForm` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FKmnoygtwlsvh31vb4volo8odct` FOREIGN KEY (`contact_id`) REFERENCES `Contact` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `Address` (
  `id` varchar(255) NOT NULL,
  `locality` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `postalCode` varchar(255) DEFAULT NULL,
  `administrativeArea` varchar(255) DEFAULT NULL,
  `addressLine1` varchar(255) DEFAULT NULL,
  `addressLine2` varchar(255) DEFAULT NULL,
  `organization_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_i4vgutrsl3ve37hc8xx7vvslf` (`organization_id`),
  CONSTRAINT `FKok1ylu26qjwvmfwgxlllbkj8l` FOREIGN KEY (`organization_id`) REFERENCES `FormOrganization` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `DistributedCSRFToken` (
  `token` varchar(100) NOT NULL,
  `ipAddress` varchar(64) NOT NULL,
  `userAgent` varchar(255) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `user` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE VIEW FormStatusReport AS
  SELECT
  f.id as formId,
  f.userID as userId,
  f.membershipLevel,
  o.legalName as organizationName,
  COALESCE(wg.wgCount, 0) as workingGroupCount,
  f.dateCreated,
  f.dateUpdated,
  f.dateSubmitted,
  f.registrationCountry,
  (CASE
    WHEN o.id IS NULL THEN 2
    WHEN (f.membershipLevel IS NULL
    or f.membershipLevel = '') THEN 3
    WHEN (wg.wgCount IS NULL
    or wg.wgCount = 0) THEN 4
    WHEN c.id is not null then 5
    else 6
  END) as formCompletionStage,
  f.state as formState
FROM
  MembershipForm f
LEFT JOIN (
  SELECT
    *,
    COUNT(*) as wgCount
  FROM
    FormWorkingGroup wg
  GROUP BY
    wg.form_id) wg on
  f.id = wg.form_id
LEFT JOIN FormOrganization o on
  f.id = o.form_id
LEFT JOIN Contact c on
  f.id = c.form_id
  AND c.`type` = 'SIGNING'
GROUP BY
  f.id; 
  

DELIMITER //
CREATE PROCEDURE csrfTokenClean(maxAge DATETIME) 
  BEGIN
    DELETE FROM DistributedCSRFToken WHERE `timestamp` < maxAge; 
  END;
  //
DELIMITER ;
CREATE EVENT scheduledCsrfTokenClean
  ON SCHEDULE EVERY 1 HOUR 
    DO CALL csrfTokenClean(DATE_SUB(NOW(), INTERVAL 1 hour));

CREATE TABLE IF NOT EXISTS `SecureTransaction` (
  `id` SERIAL NOT NULL,
  `token` varchar(255) UNIQUE NOT NULL,
  `associatedEntityId` varchar(255) NOT NULL,
  `associatedEntityType` varchar(63) NOT NULL,
  `is_active` BOOLEAN NOT NULL,
  PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_520_ci;
CREATE INDEX IDX_SecureTransaction_associatedEntityId ON SecureTransaction (associatedEntityId(25));
CREATE INDEX IDX_SecureTransaction_associatedEntityType ON SecureTransaction (associatedEntityType(25));
CREATE INDEX IDX_SecureTransaction_is_active ON SecureTransaction (is_active);

CREATE TABLE IF NOT EXISTS `EmailOptOut` (
  `id` SERIAL NOT NULL,
  `dateReceived` datetime NOT NULL,
  `email` varchar(127) UNIQUE NOT NULL,
  `transaction_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`transaction_id`) REFERENCES `SecureTransaction` (`id`) ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_520_ci;
CREATE INDEX IDX_EmailOptOut_email ON EmailOptOut (email(15));


CREATE TABLE IF NOT EXISTS `WorkingGroupApplication` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user` varchar(100) NOT NULL,
  `organizationId` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `updated` datetime NOT NULL DEFAULT current_timestamp(),
  `state` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `WorkingGroupApplicationContact` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fName` varchar(100) DEFAULT NULL,
  `lName` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `user` varchar(100) DEFAULT NULL,
  `workingGroupAlias` varchar(100) NOT NULL,
  `workingGroupLevel` varchar(100) NOT NULL,
  `isSigningAuthority` boolean NOT NULL DEFAULT FALSE,
  `application_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `WorkingGroupApplicationContact_WorkingGroupApplication_FK` (`application_id`),
  CONSTRAINT `WorkingGroupApplicationContact_WorkingGroupApplication_FK` FOREIGN KEY (`application_id`) REFERENCES `WorkingGroupApplication` (`id`)
);
CREATE TABLE `Sessions` (
  `id` SERIAL,
  `username` varchar(63) NOT NULL,
  `time` datetime NOT NULL,
  `organization` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
