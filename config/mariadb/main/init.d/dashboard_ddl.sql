CREATE DATABASE dashboard;
USE dashboard;
DROP TABLE IF EXISTS Committer, CommitterAffiliation, CommitterProjectActivity, Project CASCADE;

CREATE TABLE `Committer` (
  `id` varchar(128) NOT NULL,
  `first` varchar(128) NOT NULL,
  `last` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `country` varchar(2) DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `CommitterAffiliation` (
  `id` varchar(128) NOT NULL,
  `orgId` int(11) DEFAULT NULL,
  `orgName` varchar(128) NOT NULL,
  `activeDate` date DEFAULT NULL,
  `inactiveDate` date DEFAULT NULL,
  `ts` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  UNIQUE KEY `id_2` (`id`,`orgId`,`activeDate`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `CommitterProjectActivity` (
  `login` varchar(128) NOT NULL,
  `project` varchar(128) NOT NULL,
  `period` varchar(6) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `count` bigint(21) NOT NULL DEFAULT 0,
  PRIMARY KEY (`login`,`project`,`period`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Project` (
  `id` varchar(128) NOT NULL,
  `parentId` varchar(128) DEFAULT NULL,
  `short` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `license` varchar(128) DEFAULT NULL,
  `specification` varchar(32) DEFAULT NULL,
  `url` varchar(256) NOT NULL,
  `start` date DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- dashboard.ProjectCompanyActivity definition

CREATE TABLE `ProjectCompanyActivity` (
  `project` varchar(128) NOT NULL,
  `company` varchar(128) DEFAULT NULL,
  `orgId` int(11) DEFAULT NULL,
  `count` bigint(21) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
