import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

export default defineConfig({
  base: '/',
  build: {
    outDir: './build',
    target: 'es2015',
  },
  server: {
    port: 3000,
  },
  plugins: [react()],
});
