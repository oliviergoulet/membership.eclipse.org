/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { useContext } from "react"
import PortalContext from "../Context/PortalContext"
import { User } from "../types";

interface AuthData {
    user: User | null;
}

const useAuth = (): AuthData => {
    const { currentUserPortal } = useContext(PortalContext);

    return {
        user: currentUserPortal,
    }
}

export default useAuth;
