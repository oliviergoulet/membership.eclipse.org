/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { useState } from 'react';

export interface ImageMetadata { 
  /** Width of the image in pixels. */
  width: number;
  /** Height of the image in pixels. */
  height: number;
}

const supportedFileTypesForMetadata = ['image/png', 'image/jpeg', 'image/svg+xml'];

/**
 * Loads the metadata of an image. Returns a promise that resolves with the
 * metadata or an error. 
 */
const loadMetadata = async (url: string): Promise<[ImageMetadata | null, Error | null]> => {
  return new Promise((resolve) => {
    let result: [ImageMetadata | null, Error | null] = [null, null];
    let tempImage = new Image();
    tempImage.src = url;
    
    tempImage.onload = () => {
      result[0] = {
        width: tempImage.width || 0,
        height: tempImage.height || 0,
      };
      resolve(result);
    };

    tempImage.onerror = () => {
      result[1] = new Error('An error occured while loading the image');
      resolve(result);
    }
  });
}

/**
  * Handles local image file uploads and processing. 
  *
  * Returns a stateful image value which can contain the URL of an uploaded
  * image. Also contains an uploader function to locally upload an image. Also
  * includes metadata as well for JPG, PNG and SVGs.
  */
const useImageFile = () => {
  const [image, setImage] = useState<string | ArrayBuffer | null>(null);
  const [metadata, setMetadata] = useState<ImageMetadata | null>(null);
  
  /**
    * Handles the upload of an image file. This image is only uploaded locally.
    *
    * @param file - The image file to upload.
    */
  const uploader = (file: File) => {
    const reader = new FileReader();
  
    // When the file is loaded, set the image and metadata.
    reader.addEventListener('load', async (event) => {
      if (event.target?.result) {
        let metadata;
        let error;
        
        // If the file is a supported type, load the metadata.
        if (supportedFileTypesForMetadata.includes(file.type)) {
          [metadata, error] = await loadMetadata(event.target.result as string);

          if (error) return;
          setMetadata(metadata);
        }
        
        // Set the image URL as state when the file is loaded.
        setImage(event.target.result);
      }
    });

    // This will assign the file to a data URL. In other words, the image will
    // now have a URL.
    reader.readAsDataURL(file);
  }

  return { image, metadata, uploader };
};

export default useImageFile;
