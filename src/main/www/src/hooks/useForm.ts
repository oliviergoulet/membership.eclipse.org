/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { useFormikContext } from 'formik';

// An adapter for useFormikContext

/**
 * Hook which exposes form state and functions to update it
 */ 
const useForm = <T>() => {
  const formik = useFormikContext<T>();

  return { 
    formValues: formik.values, 
    errors: formik.errors,
    dirty: formik.dirty,
    resetForm: formik.resetForm,
    setFieldValue: formik.setFieldValue,
    handleFieldChange: formik.handleChange,
    handleFieldBlur: formik.handleBlur,
    handleSubmit: formik.handleSubmit,
  };
};

export default useForm;
