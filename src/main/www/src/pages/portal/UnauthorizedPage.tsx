/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Container, createStyles, makeStyles, Theme, Typography } from "@material-ui/core";
import FullLayout from "../../layouts/FullLayout";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            textAlign: 'center'
        },
        backgroundText: {
            opacity: '25%',
            lineHeight: 1,
            userSelect: 'none',
            fontSize: '10rem',
            [theme.breakpoints.up('md')]: {
                fontSize: '24rem'
            }
        },
        text: {
            marginBottom: '1rem'
        }
    })
);

export const UnauthorizedPage = () => {
    const classes = useStyles();

    return (
        <FullLayout>
          <Container className={ classes.container } maxWidth='md'>
              <Typography className={ classes.backgroundText } component="h1">401</Typography>
              <Typography 
                  className={ classes.text } 
                  component="h1" 
                  variant="h4"
              >
                  Unauthorized
              </Typography>
              <Typography className={ classes.text }>
                  You are not authorized for this action.
              </Typography>
          </Container>
        </FullLayout>
    );
}
