/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { iconGray, brightOrange } from '../../Constants/Constants';
import { makeStyles, createStyles, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Typography, Theme } from '@material-ui/core';
import BuildIcon from '@material-ui/icons/Build';
import AdminEditOrganizationForm from '../../modules/AdminEditOrganizationForm';
import FullLayout from '../../layouts/FullLayout';
import { usePreviewMode } from '../../Context/PreviewModeContext';
import { useHistory } from 'react-router';

// @todo: Use portal page layout component instead of duplicating styles and
// JSX.

const useStyle = makeStyles((theme: Theme) =>
  createStyles({
    headerIcon: {
      fontSize: 80,
      color: iconGray,
      borderBottom: `6px ${brightOrange} solid`,
    },
    pageTitle: {
      margin: theme.spacing(0.5, 0, 4),
    },
  })
);

export const AdminPage = () => {
  const classes = useStyle();
  const { inPreviewMode, disablePreviewMode } = usePreviewMode();
  const history = useHistory();
  

  const handleDisablePreviewClick = () => disablePreviewMode({ redirect: false });
  const handleKeepPreviewingClick = () => {
    history.push('/portal/dashboard');
  }

  return (
    <FullLayout>
      <BuildIcon className={classes.headerIcon} />
      <Typography className={classes.pageTitle} variant="h4" component="h1">
        Admin Panel
      </Typography>
      <AdminEditOrganizationForm />
      <Dialog open={inPreviewMode}>
        <DialogTitle>You are currently in preview mode!</DialogTitle>
        <DialogContent>
          <DialogContentText>
            We noticed that you are currently in preview mode. To continue
            using the admin panel, please disable preview mode.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button color="secondary" onClick={handleKeepPreviewingClick}>Keep Previewing</Button>
          <Button color="primary" onClick={handleDisablePreviewClick}>Disable Preview</Button>
        </DialogActions>
      </Dialog>
    </FullLayout>
  );
};
