/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { useContext, useEffect } from 'react';
import AccountMenu from '../../modules/AccountMenu';
import BackgroundBoxLayout from '../../layouts/BackgroundBoxLayout';
import { Box, Button, Typography } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import permissions from '../../Constants/permissions';
import PortalContext from '../../Context/PortalContext';
import PreviewAutocomplete from '../../modules/PreviewAutocomplete';
import Protected from '../../components/Protected';
import Spacer from '../../components/Spacer';
import { useHistory } from 'react-router';
import { usePreviewMode } from '../../Context/PreviewModeContext';

const StandbyPage = () => {
  const { currentUserPortal } = useContext(PortalContext);
  const { inPreviewMode, disablePreviewMode } = usePreviewMode();
  const history = useHistory();

  const handleAdminButtonClick = () => {
    history.push('/portal/admin');
  }
  
  // Disable preview mode when user returns to this page.
  // If the user returns to the standby page, they probably mean to preview
  // someone else. This will improve the experience as the autocomplete will be
  // cleared automatically.
  useEffect(() => {
    if (inPreviewMode) disablePreviewMode();
  }, [inPreviewMode, disablePreviewMode]);
  
  // In the case that the user has not provided their given name, we will use
  // their username instead.
  const name = currentUserPortal!.givenName || currentUserPortal!.name;

  return (
    <BackgroundBoxLayout>
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <Box>
          <Typography variant="h5" component="h1">
            Hi {name}!
          </Typography>
        </Box>
        <Box>
          <AccountMenu icon={<MoreVertIcon/>} />
        </Box>
      </Box>

      {/* Preview */} 
      <Spacer y={2} />
      <Typography variant="h5" component="h2">
        Preview a Member Organization
      </Typography>
      <Typography>
        Select an organization from the field below to preview a member
        organization's dashboard.
      </Typography>
      <Spacer y={2} />
      <PreviewAutocomplete />

      {/* Admin */} 
      <Protected allowedRoles={permissions.accessAdmin} strict>
        <Spacer y={6} />
        <Typography variant="h5" component="h2">
          Admin Panel
        </Typography>
        <Typography>
          Need to edit a member organization? Click the button below to go to
          the Admin Panel.
        </Typography>
        <Spacer y={1} />
        <Button variant="contained" color="primary" onClick={handleAdminButtonClick}>
          Admin Panel
        </Button>
      </Protected>
    </BackgroundBoxLayout>
  );
};

export default StandbyPage;
