/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';
import PortalFooter from '../components/Portal/PortalFooter';
import backgroundImage from '../assets/images/placeholderImg.jpg';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    background: {
      position: 'fixed',
      top: '-30px',
      left: '-30px',
      right: '-30px',
      bottom: '-30px',
      zIndex: -10,
      backgroundImage: `url(${backgroundImage})`,
      backgroundPosition: 'center',
      backgroundSize: 'cover',
      filter: 'blur(20px)',
    },
    contentContainer: {
      margin: theme.spacing(2),
      borderRadius: '5px',
      backgroundColor: '#fff',
      boxShadow: '1px 1px 14px rgba(0, 0, 0, 0.15)',
      [theme.breakpoints.up('md')]: {
        width: '60%',
        maxWidth: '768px',
      },
    }
  })
);

interface Props extends React.PropsWithChildren<{}> {}

/**
  * The BackgroundBoxLayout component contains a background image with a
  * centered modal-like box with content.
  */
export const BackgroundBoxLayout: React.FC<Props> = ({ children }) => {
  const classes = useStyles();

  return (
    <Box width="100vw" height="100vh">
      <Box 
        display="flex" 
        justifyContent="center" 
        alignItems="center" 
        width="100%" 
        height="100%"
      >
        <Box className={classes.background}></Box>
        <Box className={classes.contentContainer} component="main" padding={4}>
          {children}
        </Box>
      </Box>
      <PortalFooter textColor="#fff" textShadow="0px 1px 6px #000" />
    </Box>
  );
};

export default BackgroundBoxLayout;


