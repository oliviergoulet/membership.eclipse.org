/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React, { useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppTopBar from '../components/Portal/NavBar/AppTopBar';
import LeftNavBar from '../components/Portal/NavBar/LeftNavBar';
import PortalFooter from '../components/Portal/PortalFooter';
import { mainContentBGColor } from '../Constants/Constants';
import { Box } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    content: {
      position: 'relative',
      minHeight: 'calc(100vh - 65px)',
      flexGrow: 1,
      backgroundColor: mainContentBGColor,
      padding: theme.spacing(3),
      [theme.breakpoints.up('sm')]: {
        padding: theme.spacing(6),
      },
      [theme.breakpoints.up('md')]: {
        padding: theme.spacing(8),
      },
      [theme.breakpoints.up('lg')]: {
        padding: theme.spacing(10),
      },
    },
  })
);

interface Props extends React.PropsWithChildren<{}> {}

/**
  * The FullLayout component is the main layout of the portal. It includes the
  * top bar, the left navigation bar, the main content and the footer.
  */
export const FullLayout: React.FC<Props> = ({ children }) => {
  const classes = useStyles();
  const [mobileOpen, setMobileOpen] = useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  return (
    <Box display="flex">
      <LeftNavBar mobileOpen={mobileOpen} handleDrawerToggle={handleDrawerToggle} />
      <Box flexGrow={1} maxWidth={1}>
        <AppTopBar handleDrawerToggle={handleDrawerToggle} />
        <Box component="main" className={classes.content}>
          {children}
        </Box>
        <PortalFooter />
      </Box>
    </Box>
  );
};

export default FullLayout;

