/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import * as yup from 'yup';

const maximumLengthError = (max: number) => `The value exceeds max length ${max} characters`;
const invalidUrlError = 'Please enter a valid URL, the format should be https://example.com';

// Basic Organization Profile
export const basicOrganizationProfileSchema = yup.object().shape({
  description: yup
    .string()
    .max(700, maximumLengthError(700)),
  url: yup
    .string()
    .max(255, maximumLengthError(255))
    .url(invalidUrlError),
});
