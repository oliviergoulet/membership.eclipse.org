/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import { useEffect, useState } from 'react';
import './App.css';
import AppTemplate from './components/UIComponents/Templates/AppTemplate';
import MembershipContext from './Context/MembershipContext';
import { HashRouter, BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Application from './components/Application/Application';
import Portal from './components/Portal/Portal';
import NotFound404 from './components/ErrorPages/NotFound404';
import InternalError50x from './components/ErrorPages/InternalError50x';
import {
  api_prefix,
  FETCH_HEADER,
  getCurrentMode,
  LOGIN_FROM_KEY,
  MODE_REACT_ONLY,
  ORIGINAL_PATH_KEY,
  ROUTE_SIGN_IN,
} from './Constants/Constants';
import PortalContext from './Context/PortalContext';
import GlobalContext from './Context/GlobalContext';
import { ThemeProvider } from './Context/theme';
import TopSlideMsg from './components/UIComponents/Notifications/TopSlideMsg';
import Loading from './components/UIComponents/Loading/Loading';

const App = () => {
  const [currentUserForm, setCurrentUserForm] = useState(null);
  const [currentUserPortal, setCurrentUserPortal] = useState(null);
  const [currentFormId, setCurrentFormId] = useState('');
  const [furthestPage, setFurthestPage] = useState({
    index: 0,
    pathName: ROUTE_SIGN_IN,
  });
  const [needLoadingSignIn, setNeedLoadingSignIn] = useState(true);
  const [currentStepIndex, setCurrentStepIndex] = useState(0);
  const [orgId, setOrgId] = useState(0);
  const [contactFilterRole, setContactFilterRole] = useState('');
  const [contactData, setContactData] = useState(null);
  const [gotCSRF, setGotCSRF] = useState(false);
  const [isFetchingUser, setIsFetchingUser] = useState(true);
  const [shouldShowMsg, setShouldShowMsg] = useState(false);
  const [displayPeriod, setDisplayPeriod] = useState(4000);
  const [msgContent, setMsgContent] = useState('');
  const [isError, setIsError] = useState(false);
  const [orgInfo, setOrgInfo] = useState(null);
  const [isFetchingOrgInfo, setIsFetchingOrgInfo] = useState(true);
  const [allRelations, setAllRelations] = useState(null);
  const [currentLinks, setCurrentLinks] = useState(null);
  const [orgRepData, setOrgRepData] = useState(null);
  const [resourcesData, setResourcesData] = useState(null);
  const [committers, setCommitters] = useState(null);
  const [contributors, setContributors] = useState(null);
  const [cbiData, setCBIData] = useState(null);
  const [chartCommits, setChartCommits] = useState(null);
  const [chartCommitters, setChartCommitters] = useState(null);
  const [interestedProjectsData, setInterestedProjectsData] = useState(null);
  const [yourProjectsData, setYourProjectsData] = useState(null);
  const [allYourProjectsData, setAllYourProjectsData] = useState(null);
  const [yourWGData, setYourWGData] = useState(null);
  const [allYourWGData, setAllYourWGData] = useState(null);
  const [interestedWGData, setInterestedWGData] = useState(null);
  const [chartMonths, setChartMonths] = useState(null);

  const failedToExecute = (msg, displayPeriod) => {
    setDisplayPeriod(displayPeriod);
    setShouldShowMsg(true);
    setMsgContent(msg || 'Something went wrong, please try again later.');
    setIsError(true);
  };
  const succeededToExecute = (msg) => {
    setIsError(false);
    setShouldShowMsg(true);
    setMsgContent(msg || 'Saved successfully!');
  };

  const globalContext = {
    gotCSRF,
    setGotCSRF,
    failedToExecute,
    succeededToExecute,
  };
  const membershipContextValue = {
    currentFormId,
    setCurrentFormId,
    furthestPage,
    setFurthestPage,
    needLoadingSignIn,
    setNeedLoadingSignIn,
    currentStepIndex,
    setCurrentStepIndex,
    orgId,
    setOrgId,
    contactFilterRole,
    setContactFilterRole,
    currentUserForm,
    setCurrentUserForm,
  };
  const PortalContextValue = {
    orgId,
    setOrgId,
    contactFilterRole,
    setContactFilterRole,
    contactData,
    setContactData,
    orgInfo,
    setOrgInfo,
    isFetchingOrgInfo,
    setIsFetchingOrgInfo,
    allRelations,
    setAllRelations,
    currentLinks,
    setCurrentLinks,
    orgRepData,
    setOrgRepData,
    resourcesData,
    setResourcesData,
    committers,
    setCommitters,
    contributors,
    setContributors,
    cbiData,
    setCBIData,
    chartCommits,
    setChartCommits,
    chartCommitters,
    setChartCommitters,
    interestedProjectsData,
    setInterestedProjectsData,
    yourProjectsData,
    setYourProjectsData,
    allYourProjectsData,
    setAllYourProjectsData,
    yourWGData,
    setYourWGData,
    allYourWGData,
    setAllYourWGData,
    interestedWGData,
    setInterestedWGData,
    chartMonths,
    setChartMonths,
    currentUserPortal,
    setCurrentUserPortal,
  };

  useEffect(() => {
    if (getCurrentMode() === MODE_REACT_ONLY) {
      setGotCSRF(true);
      return;
    }

    fetch(`${api_prefix()}/csrf`, { headers: FETCH_HEADER })
      .then((res) => {
        FETCH_HEADER['x-csrf-token'] = res.headers.get('x-csrf-token');
        FETCH_HEADER['x-csrf-token'] && setGotCSRF(true);
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <div className="App">
      <ThemeProvider>
        <GlobalContext.Provider value={globalContext}>
          <BrowserRouter>
            <Switch>
              <Route exact path="/">
                {localStorage.getItem(LOGIN_FROM_KEY) === 'Portal' ? (
                  <Redirect to={sessionStorage.getItem(ORIGINAL_PATH_KEY) || '/portal'} />
                ) : (
                  <Redirect to="/application" />
                )}
              </Route>

              <Route exact path="/application">
                <MembershipContext.Provider value={membershipContextValue}>
                  <AppTemplate>
                    <HashRouter hashType="noslash">{gotCSRF ? <Application /> : <Loading />}</HashRouter>
                  </AppTemplate>
                </MembershipContext.Provider>
              </Route>

              <Route exact path="/404">
                <AppTemplate>
                  <NotFound404 />
                </AppTemplate>
              </Route>

              <Route exact path="/50x">
                <AppTemplate>
                  <InternalError50x />
                </AppTemplate>
              </Route>

              <Route path="/portal">
                <PortalContext.Provider value={PortalContextValue}>
                  <BrowserRouter hashType="noslash">
                    <Portal isFetchingUser={isFetchingUser} setIsFetchingUser={setIsFetchingUser} />
                  </BrowserRouter>
                </PortalContext.Provider>
              </Route>

              {/* Redirect user to 404 page for all the unknown pathnames/urls */}
              <Redirect to="404" />
            </Switch>
          </BrowserRouter>

          <TopSlideMsg
            shouldShowUp={shouldShowMsg}
            setShouldShowUp={setShouldShowMsg}
            isError={isError}
            msgContent={msgContent}
            displayPeriod={displayPeriod}
          />
        </GlobalContext.Provider>
      </ThemeProvider>
    </div>
  );
};

export default App;
