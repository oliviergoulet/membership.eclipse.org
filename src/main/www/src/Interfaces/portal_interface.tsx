/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
export enum OrganizationRoles {
  CR = 'Member Representative',
  CRA = 'Alternate Member Representative',
  MA = 'Marketing Representative',
  DE = 'Delegate',
  CM = 'Committer',
  EMPLY = 'Employee',
}

// Casting the keys' type will stop ts complaining type string can't be used as index signature for enum
export const OrganizationRolesKeys = Object.keys(OrganizationRoles) as (keyof typeof OrganizationRoles)[];
export type ContactRelation = keyof typeof OrganizationRoles;

export interface ContactBackend {
  id: string;
  relations: Array<string>;
  email: string;
  first_name: string;
  last_name: string;
}

export interface ContactFrontend {
  id: string;
  name: string;
  relation: Array<ContactRelation>;
  role: Array<string>;
  email: string;
  requestRemove: string;
}

export interface OrgRep {
  name: string;
  type: string;
}
export interface ProjectsAndWGItem {
  name: string;
  url: string;
}

export interface SelectedItemArray {
  title: string;
  data: Array<ProjectsAndWGItem>;
}

export interface DashboardProjectsAndWGProps {
  setSelectedItemArray: (data: SelectedItemArray) => void;
  setOpen: (data: boolean) => void;
}

export interface WGPA {
  description: null | string;
  document_id: string;
  level: string;
  working_group: string;
}

export interface OrgInfoBackend {
  description: null | { long: string };
  levels: Array<{ level: string; description: string; sort_order: string }>;
  logos: { print: null | string; web: null | string };
  name: string;
  organization_id: number;
  website: null | string;
  wgpas: Array<WGPA>;
}

export interface AllRelationsFrontend {
  [key: string]: string;
}

export interface CurrentLink {
  id: number;
  title: string;
  description: string;
  url: string;
}

export interface ResourcesListItem {
  name: string;
  url: string;
}
export interface ResourcesData {
  subtitle: string;
  listItems: Array<ResourcesListItem> | null;
}

export interface CommitterOrContributor {
  name: string;
  url: string;
}

export interface CBIData {
  inUse: number;
  allocated: number;
}

export interface ProjectsDataBackend {
  active: boolean;
  level: number;
  project_id: string;
  name: string;
  url_index: string;
  url_download: string;
}

export interface ProjectsDataFrontend {
  status: string;
  id: string;
  name: string;
  url: string;
}

export interface AllProjectsBackend {
  name: string;
  url: string;
  state: 'Regular' | 'Incubating' | 'Archived';
}

export interface CustomToolbarProps {
  fileName: string;
}
