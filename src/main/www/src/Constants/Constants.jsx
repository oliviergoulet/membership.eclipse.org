/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
/**
 * list all constants here
 *
 * The purpose of this file is try to avoid using strings directly everywhere,
 * just hope to use consistent variables for strings.
 */

import {
  ArrowBack as ArrowBackIcon,
  Assessment as AssessmentIcon,
  Business as BusinessIcon,
  BusinessCenter as BusinessCenterIcon,
  PeopleAlt as PeopleAltIcon,
  Description as DescriptionIcon,
  Help as HelpIcon,
  RecentActors as RecentActorsIcon,
  Build as BuildIcon,
} from '@material-ui/icons';
import permissions  from './permissions';

export const api_prefix = () => {
  return '//' + window.location.host + '/api';
};

export const application_api_prefix = () => {
  return '//' + window.location.host + '/application_api';
};

export const LOGIN_FROM_KEY = 'logInFrom';
export const ORIGINAL_PATH_KEY = 'originalPath';
export const API_PREFIX_FORM = application_api_prefix() + '/form';
export const API_FORM_PARAM = '?sort=dateCreated&order=desc';

export const SIGN_IN = 'Sign In';
export const COMPANY_INFORMATION = 'Company Information';
export const MEMBERSHIP_LEVEL = 'Membership Level';
export const WORKING_GROUPS = 'Working Groups';
export const SIGNING_AUTHORITY = 'Signing Authority';
export const REVIEW = 'Review';
export const HAS_TOKEN_EXPIRED = 'HAS_TOKEN_EXPIRED';

export const LOGIN_EXPIRED_MSG = 'Your session has expired, please sign in again.';
export const MAX_LENGTH_HELPER_TEXT = 'The value exceeds max length 255 characters';
export const MAX_LENGTH_HELPER_TEXT_SEVEN_HUNDRED = 'The value exceeds max length 700 characters';
export const URL_HELPER_TEXT = 'Please enter a valid URL, the format should be https://example.com';

export const ROUTE_SIGN_IN = '/sign-in';
export const ROUTE_COMPANY = '/company-info';
export const ROUTE_MEMBERSHIP = '/membership-level';
export const ROUTE_WGS = '/working-groups';
export const ROUTE_SIGNING = '/signing-authority';
export const ROUTE_REVIEW = '/review';
export const ROUTE_SUBMITTED = '/submitted';

export const CURRENT_STEP = 'currentStep';
export const COMPANY_INFO_STEP = 'company info step';
export const MEMBERSHIP_LEVEL_STEP = 'membership level step';
export const WORKING_GROUP_STEP = 'working group step';
export const SIGNING_AUTHORITY_STEP = 'signing authority step';

export const FETCH_METHOD = {
  POST: 'POST',
  GET: 'GET',
  PUT: 'PUT',
  DELETE: 'DELETE',
};

export const FETCH_HEADER = {
  'x-csrf-token': '',
  'Content-Type': 'application/json',
  Accept: 'application/json',
};

export const FETCH_HEADER_WITHOUT_CSRF = {
  'Content-Type': 'application/json',
  Accept: 'application/json',
};

export const MEMBERSHIP_LEVELS = [
  { label: 'Strategic Member', value: 'Strategic Member' },
  {
    label: 'Contributing Member',
    value: 'Contributing Member',
  },
  { label: 'Associate Member', value: 'Associate Member' },
];

export const PAGE_STEP = [
  { label: SIGN_IN, pathName: ROUTE_SIGN_IN },
  { label: COMPANY_INFORMATION, pathName: ROUTE_COMPANY },
  { label: MEMBERSHIP_LEVEL, pathName: ROUTE_MEMBERSHIP },
  { label: WORKING_GROUPS, pathName: ROUTE_WGS },
  { label: SIGNING_AUTHORITY, pathName: ROUTE_SIGNING },
  { label: REVIEW, pathName: ROUTE_REVIEW },
];

export const CONTACT_TYPE = {
  COMPANY: 'COMPANY',
  MARKETING: 'MARKETING',
  ACCOUNTING: 'ACCOUNTING',
  WORKING_GROUP: 'WORKING_GROUP',
  SIGNING: 'SIGNING',
};

export const OPTIONS_FOR_PURCHASING_PROCESS = [
  { label: 'Yes', value: 'yes' },
  { label: 'No', value: 'no' },
  { label: 'Not Applicable', value: 'na' },
];

export const OPTIONS_FOR_REVENUE = [
  { label: '> €1 billion', value: '> €1 billion' },
  { label: '€250 million - €1 billion', value: '€250 million - €1 billion' },
  { label: '€100 million - €250 million', value: '€100 million - €250 million' },
  { label: '€50 million - €100 million', value: '€50 million - €100 million' },
  { label: '€10 million - €50 million', value: '€10 million - €50 million' },
  { label: '€1 million - €10 million', value: '€1 million - €10 million' },
  { label: '< €1 million', value: '< €1 million' },
  { label: 'Not Applicable', value: 'Not Applicable' },
];

export const HELPERTEXT_FOR_REVENUE = (
  <>
    Choose Not Applicable if your organization is: <br />
    Govt, Govt agencies, Research Organizations, NGOs, etc. <br />
    Academic, Publishing Organizations, User Groups, etc.
  </>
);

export const OPTIONS_FOR_EMPLOYEE_COUNT = [
  { label: '1 - 10', value: '1 - 10' },
  { label: '10 - 100', value: '10 - 100' },
  { label: '100 - 1000', value: '100 - 1000' },
  { label: '1000 - 10,000', value: '1000 - 10,000' },
  { label: '> 10,000', value: '> 10,000' },
];

export const OPTIONS_FOR_ORG_TYPE = [
  {
    label: 'Non-Profit Open Source Organization/User Group',
    value: 'NON_PROFIT_OPEN_SOURCE',
  },
  { label: 'Academic Organization', value: 'ACADEMIC' },
  { label: 'Standards Organization', value: 'STANDARDS' },
  {
    label: 'Government Organization, Government Agency, or NGO',
    value: 'GOVERNMENT_ORGANIZATION_AGENCY_NGO',
  },
  { label: 'Publishing/Media Organization', value: 'MEDIA_ORGANIZATION' },
  { label: 'Research Institute', value: 'RESEARCH' },
  { label: 'All others', value: 'OTHER' },
];

export const END_POINT = {
  organizations: 'organizations',
  committers: 'committers',
  projects: 'projects',
  committers_yearly: 'committers/activity/yearly',
  contributors: 'contributors',
  activity_yearly: 'activity/yearly',
  contacts: 'contacts',
  working_groups: 'working_groups',
  working_groups_eclipse: 'working_groups?parent_organization=eclipse&status=proposed&status=active',
  userinfo: 'userinfo',
  complete: 'complete',
  resources: 'https://newsroom.eclipse.org/api/resources?pagesize=60&page=',
};

// const for workingGroups string
export const workingGroups = 'workingGroups';
// const for companies string
export const companies = 'companies';

export const newForm_tempId = 'new';

export const MODE_REACT_ONLY = 'MODE_REACT_ONLY';
export const MODE_REACT_API = 'MODE_REACT_API';

export function getCurrentMode() {
  const validApiDomain = [
    '//membership-staging.eclipse.org',
    '//membership.eclipse.org/',
    '//www.rem.docker/',
    '//nginx.rem.docker/',
  ].some((value) => {
    return window.location.href.indexOf(value) !== -1;
  });

  if (validApiDomain) {
    return MODE_REACT_API;
  }

  return MODE_REACT_ONLY;
}

export const FULL_WORKING_GROUP_LIST_FOR_REACT_ONLY = [
  {
    label: 'openMobility Working Group',
    value: 'openMobility Working Group',
    participation_levels: [
      { description: 'Participant Member', relation: 'WGAPS' },
      { description: 'Committer Member', relation: 'WGFHA' },
    ],
    charter: 'https://www.eclipse.org/org/workinggroups/openmobility_charter.php',
  },
  {
    label: 'Jakarta EE Working Group',
    value: 'Jakarta EE Working Group',
    participation_levels: [
      { description: 'Strategic Member', relation: 'WGSD' },
      { description: 'Enterprise Member', relation: 'WGDSA' },
      { description: 'Participant Member', relation: 'WGAPS' },
      { description: 'Guest Member', relation: 'WGSAP' },
    ],
    charter: 'https://www.eclipse.org/org/workinggroups/jakarta_ee_charter.php',
  },
];

export const NAV_PATHS = {
  dashboard: '/portal/dashboard',
  orgProfile: '/portal/org-profile',
  dashboardProjectsWG: '/portal/dashboard#projects-wg',
  dashboardCommittersAndContributors: '/portal/dashboard#committers-contributors',
  dashboardResources: '/portal/dashboard#resources',
  dashboardFAQs: '/portal/dashboard#faqs',
  contactManagement: '/portal/contact-management',
  yourProjects: '/portal/your-projects',
};

export const NAV_OPTIONS_DATA = [
  {
    name: 'Return to Standby',
    path: '/portal/standby',
    icon: <ArrowBackIcon />,
    allowedRoles: permissions.accessStandby,
    strict: true,
  },
  {
    name: 'Admin Panel',
    path: '/portal/admin',
    icon: <BuildIcon />,
    allowedRoles: permissions.accessAdmin,
    strict: true,
  },
  {
    name: 'standby-divider',
    path: 'standby-divider',
    type: 'divider',
    icon: <hr />,
  },
  {
    name: 'Dashboard',
    path: NAV_PATHS.dashboard,
    icon: <AssessmentIcon />,
    allowedRoles: permissions.accessDashboard,
  },
  {
    name: 'Projects and Working Groups',
    path: NAV_PATHS.dashboardProjectsWG,
    type: 'submenu',
    icon: <BusinessCenterIcon />,
    allowedRoles: permissions.accessDashboard,
  },
  {
    name: 'Committers and Contributors',
    path: NAV_PATHS.dashboardCommittersAndContributors,
    type: 'submenu',
    icon: <PeopleAltIcon />,
    allowedRoles: permissions.accessDashboard,
  },
  {
    name: 'Resources',
    path: NAV_PATHS.dashboardResources,
    type: 'submenu',
    icon: <DescriptionIcon />,
    allowedRoles: permissions.accessDashboard,
  },
  {
    name: 'FAQs',
    path: NAV_PATHS.dashboardFAQs,
    type: 'submenu',
    icon: <HelpIcon />,
    allowedRoles: permissions.accessDashboard,
  },
  {
    name: 'divider',
    path: 'divider',
    type: 'divider',
    icon: <hr />,
    allowedRoles: permissions.accessDashboard,
  },
  {
    name: 'Contact Management',
    path: NAV_PATHS.contactManagement,
    icon: <RecentActorsIcon />,
    allowedRoles: permissions.accessContacts,
  },
  {
    name: 'Your Projects',
    path: NAV_PATHS.yourProjects,
    icon: <BusinessCenterIcon />,
    allowedRoles: permissions.accessDashboard,
  },
  {
    name: 'Your Organization Profile',
    path: NAV_PATHS.orgProfile,
    icon: <BusinessIcon />,
    allowedRoles: permissions.accessOrgProfile,
  },
];

export const PERMISSIONS_BASED_ON_ROLES = {
  accessContacts: ['CR', 'CRA', 'MA', 'DE'],
  accessOrgProfile: ['CR', 'CRA', 'MA', 'DE'],
  accessAdminPanel: ['admin'],
  editDelegates: ['CR', 'CRA'],
  editCRA: ['CR'],
  editContactRoles: ['CR', 'CRA', 'DE'],
  requestRemovalOfContacts: ['CR', 'CRA', 'DE'],
  viewContributors: ['CR', 'CRA', 'MA', 'DE'],
  viewCommitters: ['CR', 'CRA', 'MA', 'DE'],
};

export const ADDITIONAL_ROLES = {
  ELEVATED_ACCESS: 'eclipsefdn_membership_portal_elevated_access',
};

export const UPDATE_DENYLISTED_RELATIONS = ['emply', 'cm', 'cr'];

export const ROLE_DESCRIPTION = {
  CR: 'The Member Representative is the primary point of contact between your organization and Eclipse Foundation. We may reach out to Member Representatives from time to time with various business related activities, in particular in relation to your benefits as a member of the General Assembly. The Member Representative also has the authority for updating company information stored in the Member Portal.',
  CRA: 'Alternate Member Representative shall be a secondary point of contact between your organization and Eclipse Foundation. In the absence of the Member Representative, we may reach out to the Alternate Member Representative for all communications and business related activities.  The Alternate Member Representative also has the authority for updating company information stored in the Member Portal.',
  DE: 'Member Representatives may delegate some or all of their responsibilities related to this Member Portal to Delegates.  That is, Delegates are granted by the Member Representative the authority for updating company information.',
  MA: `Marketing Representative shall be the point of contact for all marketing initiatives in relation to the Foundation. Our marketing team may reach out to the Marketing Representative from time to time for all marketing related activities, and will be included in the Foundation's marketing campaigns.`,
};

export const errMsgForGetRequest = 'Could not get the data, please try again later.';

export const errMsgForSessionExpired =
  'Unable to authorize, refresh to try again. Please back up any unsaved changes as they will be lost after refresh.';

export const CONFIG_FOR_BAR_LINE_CHART = {
  responsive: true,
  aspectRatio: 1.8,
};

export const LOGO_DIMENSIONS_LIMITATION = 2000; // pixels
export const LOGO_SIZE_LIMITATION_WEB = 1048576; // bytes = 1MB
export const LOGO_SIZE_LIMITATION_PRINT = 10485760; // bytes = 10MB

export const MEMBER_LEVELS = {
  strategicMember: { code: 'SD', description: 'Strategic Member' },
  contributingMember: { code: 'AP, OHAP', description: 'Contributing Member' },
  associateMember: { code: 'AS', description: 'Associate Member' },
};

// Constants for styles
export const drawerWidth = 280;
export const themeBlack = '#0B0B0B';
export const darkOrange = '#DD730A';
export const brightOrange = '#FC9D05';
export const iconGray = '#9B9BAE';
export const lightGray = '#dfdfe2';
export const darkGray = '#807C7C';
export const borderRadiusSize = 5;
export const mainContentBGColor = '#fff';
export const brightBlue = '#08BDF4';
export const errorRed = '#cf2020';
export const successGreen = '#358C54';
