/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { roles } from './roles';

const permissions = {
  accessAdmin: [
    roles.admin
  ],
  accessContacts: [
    roles.companyRep, 
    roles.companyRepAlternate, 
    roles.marketing, 
    roles.delegate
  ],
  accessDashboard: [
    roles.employee,
    roles.companyRep,
    roles.companyRepAlternate,
    roles.marketing,
    roles.delegate
  ],
  accessOrgProfile: [
    roles.companyRep, 
    roles.companyRepAlternate, 
    roles.marketing, 
    roles.delegate
  ],
  accessPortal: [
    roles.admin,
    roles.elevated,
    roles.employee,
  ],
  accessStandby: [
    roles.admin,
    roles.elevated,
  ],
  editDelegates: [
    roles.companyRep,
    roles.companyRepAlternate
  ],
  editCRA: [
    roles.companyRep
  ],
  editContactRoles: [
    roles.companyRep, 
    roles.companyRepAlternate, 
    roles.delegate
  ],
  requestRemovalOfContacts: [
    roles.companyRep, 
    roles.companyRepAlternate, 
    roles.delegate
  ],
  viewContributors: [
    roles.companyRep, 
    roles.companyRepAlternate,  
    roles.marketing, 
    roles.delegate
  ],
  viewCommitters: [
    roles.companyRep, 
    roles.companyRepAlternate,  
    roles.marketing, 
    roles.delegate
  ],
};

type Permissions = typeof permissions;

export default permissions as Permissions;
