/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React, { useState } from 'react';
import { Box, Typography } from '@material-ui/core';
import { AutocompleteChangeReason } from '@material-ui/lab/Autocomplete';
import EditOrganization from './adminEditOrganizationForm/EditOrganization';
import OrganizationAutocomplete from '../components/OrganizationAutocomplete';

interface AutocompleteOption {
  value: number;
  label: string;
}

/** A module component which allows the admin user to select an organization
  * from an autocomplete and make changes to the organization.
  */
const AdminEditOrganizationForm = () => {
  const [selectedOrganization, setSelectedOrganization] = useState<AutocompleteOption | null>(null);

  // We update the selected organization state whenever the user selects a new
  // organization from the autocomplete.
  const handleAutocompleteChange = (
    _: React.ChangeEvent<{}>,
    value: AutocompleteOption | null,
    __?: AutocompleteChangeReason
  ) => {
    setSelectedOrganization(value);
  };

  return (
    <>
      <Typography>Select a member organization to manage.</Typography>
      <Box my="1rem">
        <OrganizationAutocomplete value={selectedOrganization} onChange={handleAutocompleteChange} />
      </Box>
      <hr />
      {selectedOrganization ? (
        <EditOrganization id={selectedOrganization.value} name={selectedOrganization.label} />
      ) : null}
    </>
  );
};

export default AdminEditOrganizationForm;
