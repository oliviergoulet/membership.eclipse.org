/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { useContext, useEffect, useState } from 'react';
import CustomCard from '../components/UIComponents/CustomCard/CustomCard';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import PortalContext from '../Context/PortalContext';
import { 
  api_prefix, 
  brightBlue, 
  getCurrentMode, 
  END_POINT, 
  FETCH_METHOD, 
  MODE_REACT_ONLY,
  NAV_PATHS,
} from '../Constants/Constants';
import { pickRandomItems, renderItemList } from '../Utils/portalFunctionHelpers';
import { fetchWrapper } from '../Utils/formFunctionHelpers';
import { useHistory } from 'react-router';

const isReactOnlyMode = getCurrentMode() === MODE_REACT_ONLY;

/**
  * A component that displays a card with a list of 5 random committers. 
  */
export const CommitterCard = () => {
  const { isFetching, committers, viewAll } = useCommitterCard();

  return (
      <CustomCard
        isFetching={isFetching}
        subtitle="Your Committers"
        color={brightBlue}
        icon={<PeopleAltIcon />}
        listItems={renderItemList(committers, isFetching, 'No committers yet', '')}
        urlText="View All"
        callBackFunc={viewAll}
      />
  );
};

export default CommitterCard;

const useCommitterCard = () => {
  const { orgId, committers, setCommitters, setContactFilterRole } = useContext(PortalContext);
  const [isFetching, setIsFetching] = useState(true);
  const history = useHistory();

  /**
    * Navigate to the contact management page with the contributor filter
    * applied. This will show all contributors.
    */
  const viewAll = () => {
    setContactFilterRole('committer');
    history.push(NAV_PATHS.contactManagement);
  }

  useEffect(() => {
    // If committers is already set, don't fetch again.
    if (!isFetching && committers !== null) return;

    // Use the proper API endpoint depending on the environment.
    const urlForCommitters = isReactOnlyMode
      ? '/membership_data/test_committers_and_contributors.json'
      : api_prefix() + `/${END_POINT.organizations}/${orgId}/${END_POINT.committers}`;
    
    // Fetch the committers data and set it as state in the PortalContext.
    const saveCommittersData = (data: Array<any>) => {
      const committersData = data.map((committer) => ({
        name: `${committer.fname} ${committer.lname}`,
        url: `https://accounts.eclipse.org/users/${committer.person_id}`,
      }));

      // Pick 5 random committers from the list.
      const fiveRandomCommitters =
        committersData?.length > 0
          ? pickRandomItems(committersData, 5)
          : [
              {
                name: 'No committers yet',
                url: '',
              },
            ];

      setCommitters(fiveRandomCommitters);
      setIsFetching(false);
    }
    
    fetchWrapper(urlForCommitters, FETCH_METHOD.GET, saveCommittersData, '', () => setIsFetching(false));
  }, [orgId, committers, setCommitters, isFetching, setIsFetching]);

  return { isFetching, committers, viewAll };
};
