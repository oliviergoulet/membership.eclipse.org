/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React, { useContext, useEffect, useState } from 'react';
import { 
  Box, 
  CircularProgress, 
  Grid, 
  Typography 
} from '@material-ui/core';
import GlobalContext from '../../Context/GlobalContext';
import InfoForm from './editOrganization/InfoForm';
import WebLogoForm from './editOrganization/WebLogoForm';
import PrintLogoForm from './editOrganization/PrintLogoForm';
import { Organization } from '../../types';
import { getOrganization } from '../../api/membership';
import { getCurrentMode } from '../../Constants/Constants';
import { showErrMsg } from '../../Utils/portalFunctionHelpers';
import { transformSnakeToCamel } from '../../api/shared';

interface EditOrganizationProps {
  id: number;
  name: string;
}

const EditOrganization = ({ id, name }: EditOrganizationProps) => {
  const [organization, setOrganization] = useState<Organization | undefined>();
  const { failedToExecute } = useContext(GlobalContext);

  const organizationLoaded = organization !== undefined;

  useEffect(() => {
    if (getCurrentMode() === 'MODE_REACT_ONLY') {
        fetch('/membership_data/test_org_info_data.json')
          .then((response) => response.json())
          .then((org) => setOrganization(transformSnakeToCamel(org) as Organization))
    } else {
      const loadOrganization = async () => {
        const [org, error] = await getOrganization(id, { cache: 'no-cache' });
        if (error) {
          error.code && showErrMsg(error.code, failedToExecute);
          return;
        }

        setOrganization(org!);
      };

      loadOrganization();
    }
  }, [failedToExecute, id]);

  const infoFormInitialValues = {
    description: organization?.description?.long || '',
    url: organization?.website || '',
  };

  return (
    <Box my="1rem">
      <Typography component="h2" variant="h5">
        {name}
      </Typography>
      { !organizationLoaded ? <CircularProgress /> : (
        <Box my="1rem">
          <InfoForm id={id} initialValues={infoFormInitialValues} />
          <Grid container spacing={3}>
            <Grid item xl={3} xs={6}>
              <WebLogoForm id={id} initialLogo={organization!.logos.web || ''} />
            </Grid>
            <Grid item xl={3} xs={6}>
              <PrintLogoForm id={id} />
            </Grid>
            <Grid item xl={6}></Grid>
          </Grid>
        </Box>
      )}
    </Box>
  );
}

export default EditOrganization;

