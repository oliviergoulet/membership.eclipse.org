/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React, { useEffect } from 'react';
import { useFormikContext, FormikHelpers } from 'formik';
import { Typography } from '@material-ui/core';
import { LOGO_DIMENSIONS_LIMITATION, LOGO_SIZE_LIMITATION_WEB } from '../../../Constants/Constants';
import { showErrMsg } from '../../../Utils/portalFunctionHelpers';
import { updateOrganizationLogo } from '../../../api/membership';
import useImageFile, { ImageMetadata } from '../../../hooks/useImageFile';
import GlobalContext from '../../../Context/GlobalContext';
import FormContainer from '../../../components/FormContainer';
import UploadPreviewer from '../../../components/UploadPreviewer';

interface WebLogoFormValues {
  /** The logo file. */
  logo?: File;
  /** The logo image metadata. */
  metadata?: ImageMetadata;
}

interface Props {
  /** The organization ID. This is required to know which organization we are
   * updating the logo for. */
  id: number;
  /** The initial logo URL. This is required to display the current logo before
   * the user uploads a new one. */
  initialLogo?: string;
}

/** The presentational component for a web logo form. */
const WebLogoForm: React.FC<Omit<Props, 'id'>> = ({ initialLogo }) => {
  const { setFieldValue, handleSubmit } = useWebLogoForm();
  const { metadata, uploader } = useImageFile();

  const handleSave = (files: File[]) => {
    // When the user uploads a new logo, update the form value.
    const logo = files[0];
    setFieldValue('logo', logo);
    // We upload the logo to retrieve its metadata. This metadata will be
    // required to validate whether or not the image does not fit the sizing
    // constraints.
    uploader(logo);
  };

  useEffect(() => {
    // When `metadata` changes, update the metadata form value.
    setFieldValue('metadata', metadata);
  }, [metadata, setFieldValue]);

  return (
    <form onSubmit={handleSubmit}>
      <Typography component="h3" variant="h6">
        Logo for Web
      </Typography>
      <UploadPreviewer
        title="Upload Web Logo"
        initialImage={initialLogo}
        maxFileSize={LOGO_SIZE_LIMITATION_WEB}
        onSave={handleSave}
      />
      <Typography>
        The supported formats for uploading your logo include: PNG, or JPG file under 1 MB in size.
      </Typography>
      <Typography>The logo dimension cannot exceed 2000 by 2000 pixels.</Typography>
      <Typography>
        To better display the logo across Eclipse Foundation websites, please upload a version with minimal margins.
        Padding will be added to the display.
      </Typography>
    </form>
  );
};

interface UseWebLogoFormReturnType {
  /** The current form values. */
  formValues: WebLogoFormValues;
  /** A function to set a form value. */
  setFieldValue: (field: string, value: any, shouldValidate?: boolean | undefined) => void;
  /** The current form status. */
  status: any;
  /** Whether the form is valid. */
  isValid: boolean;
  /** Whether the form is submitting. */
  isSubmitting: boolean;
  /** A function to handle form submission. */
  handleSubmit: (event?: React.FormEvent<HTMLFormElement> | undefined) => void;
}

/**
 * A custom hook for managing core functionality of a web logo form.
 */
const useWebLogoForm = (): UseWebLogoFormReturnType => {
  const formik = useFormikContext<WebLogoFormValues>();

  return {
    formValues: formik.values,
    setFieldValue: formik.setFieldValue,
    status: formik.status,
    isValid: formik.isValid,
    isSubmitting: formik.isSubmitting,
    handleSubmit: formik.handleSubmit,
  };
};

/**
 * A form for managing a web logo.
 */
const WrappedWebLogoForm: React.FC<Props> = ({ id, initialLogo }) => {
  const { succeededToExecute, failedToExecute } = React.useContext(GlobalContext);

  const handleSubmit = async (formValues: WebLogoFormValues, helpers: FormikHelpers<WebLogoFormValues>) => {
    helpers.setSubmitting(true);

    // If the user did not upload a logo, do not submit the form.
    // We also require metadata to do validation.
    if (!formValues.logo || !formValues.metadata) {
      helpers.setSubmitting(false);
      return;
    }
  
    // Validate the image dimensions.
    if (
      formValues.metadata.width > LOGO_DIMENSIONS_LIMITATION ||
      formValues.metadata.height > LOGO_DIMENSIONS_LIMITATION
    ) {
      failedToExecute(`Image width and height must be ${LOGO_DIMENSIONS_LIMITATION}px or smaller`);
      helpers.setStatus({ error: `Image width and height must be ${LOGO_DIMENSIONS_LIMITATION}px or smaller` });
      helpers.setSubmitting(false);
      return;
    }

    const [, error] = await updateOrganizationLogo(id, formValues.logo, { format: 'WEB' });

    if (error) {
      error.code && showErrMsg(error.code, failedToExecute);
      helpers.setStatus({ error: error.message });
      helpers.setSubmitting(false);
      return;
    }

    succeededToExecute();
    helpers.setStatus({ success: 'Form has been successfully submitted' });
    helpers.setSubmitting(false);
  };

  return (
    <FormContainer initialValues={{}} onSubmit={handleSubmit}>
      <WebLogoForm initialLogo={initialLogo} />
    </FormContainer>
  );
};

export default WrappedWebLogoForm;
