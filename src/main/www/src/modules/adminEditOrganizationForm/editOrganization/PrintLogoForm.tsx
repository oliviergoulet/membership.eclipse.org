/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React from 'react';
import { useFormikContext, FormikHelpers } from 'formik';
import { Typography } from '@material-ui/core';
import { LOGO_SIZE_LIMITATION_PRINT } from '../../../Constants/Constants';
import { showErrMsg } from '../../../Utils/portalFunctionHelpers';
import { updateOrganizationLogo } from '../../../api/membership';
import GlobalContext from '../../../Context/GlobalContext';
import FormContainer from '../../../components/FormContainer';
import UploadPreviewer from '../../../components/UploadPreviewer';

interface Props {
  /** The organization ID. This is required to know which organization we are
   * updating the logo for. */
  id: number;
}

interface PrintLogoFormValues {
  /** The logo file. */
  logo?: File;
}

const PrintLogoForm: React.FC = () => {
  const { setFieldValue, handleSubmit } = usePrintLogoForm();

  const handleSave = (files: File[]) => {
    const logo = files[0];
    setFieldValue('logo', logo);
  };

  return (
    <form onSubmit={handleSubmit}>
      <Typography component="h3" variant="h6">
        Logo for Print
      </Typography>
      <UploadPreviewer
        title="Upload Print Logo"
        placeholder="No preview available"
        acceptedFiles={['.eps']}
        maxFileSize={LOGO_SIZE_LIMITATION_PRINT}
        onSave={handleSave}
      />
      <Typography>
        If available please include the .eps file of your company logo and it should be under 10 MB in size.
      </Typography>
    </form>
  );
};

/**
 * A custom hook for managing core functionality of a print logo form.
 */
const usePrintLogoForm = () => {
  const formik = useFormikContext<PrintLogoFormValues>();

  return {
    formValues: formik.values,
    setFieldValue: formik.setFieldValue,
    status: formik.status,
    isValid: formik.isValid,
    isSubmitting: formik.isSubmitting,
    handleSubmit: formik.handleSubmit,
  };
};

/**
 * A form for managing a print logo.
 */
const WrappedPrintLogoForm = ({ id }: Props) => {
  const { succeededToExecute, failedToExecute } = React.useContext(GlobalContext);

  const handleSubmit = async (formValues: PrintLogoFormValues, helpers: FormikHelpers<PrintLogoFormValues>) => {
    helpers.setSubmitting(true);

    // If no logo was uploaded, we don't submit the form.
    if (!formValues.logo) {
      helpers.setSubmitting(false);
      return;
    }

    const [, error] = await updateOrganizationLogo(id, formValues.logo, { format: 'PRINT' });

    if (error) {
      error.code && showErrMsg(error.code, failedToExecute);
      helpers.setStatus({ error: error.message });
      helpers.setSubmitting(false);
      return;
    }

    succeededToExecute();
    helpers.setStatus({ success: 'Form has been successfully submitted' });
    helpers.setSubmitting(false);
  };

  return (
    <FormContainer initialValues={{}} onSubmit={handleSubmit}>
      <PrintLogoForm />
    </FormContainer>
  );
};

export default WrappedPrintLogoForm;
