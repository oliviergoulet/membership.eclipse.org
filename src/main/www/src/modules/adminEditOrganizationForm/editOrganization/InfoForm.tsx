/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React, { useContext } from 'react';
import { Box, Grid, Button } from '@material-ui/core';
import { FormikHelpers } from 'formik';
import { useForm } from '../../../hooks';
import FormContainer from '../../../components/FormContainer';
import TextField from '../../../components/TextField';
import GlobalContext from '../../../Context/GlobalContext';
import { updateOrganizationInfo } from '../../../api/membership';
import { basicOrganizationProfileSchema } from '../../../validation';
import { showErrMsg } from '../../../Utils/portalFunctionHelpers';

interface EditInfoFormValues {
  /** The organization's description. */
  description?: string;
  /** The organization's website URL. */
  url?: string;
}

interface Props {
  /** The organization ID. This is required to know which organization we are
   * updating the logo for. */
  id: number;
  /** The initial form values. These values would be existing values which
   * exist in a database. For example, their current description. */
  initialValues: EditInfoFormValues
}

/** The presentational component for an edit info form. */
const EditInfoForm = () => {
  const { 
    formValues, 
    errors, 
    dirty,
    handleFieldChange, 
    handleFieldBlur, 
    handleSubmit, 
    handleKeyDown,
    handleCancelButtonClick,
  } = useEditInfoForm();

  const hasErrors = errors && Object.keys(errors).length > 0;

  return (
    <form onSubmit={handleSubmit} onKeyDown={handleKeyDown}>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <TextField 
            label="Description"
            name="description"
            value={formValues.description ?? ''}
            helperText="700 character limit"
            rows={8}
            inputProps={{maxLength: 700}}
            onChange={handleFieldChange}
            onBlur={handleFieldBlur}
            multiline 
            fullWidth
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            label="Company URL"
            name="url"
            value={formValues.url ?? ''}
            onChange={handleFieldChange}
            onBlur={handleFieldBlur}
            fullWidth
          />
        </Grid>
        <Grid item xs={12}>
          <Box component="span" mr={1.5}>
            <Button 
              type="submit"
              variant="contained"
              color="primary"
              disabled={hasErrors || !dirty}
            >
              Save Changes
            </Button>
          </Box>
          <Button 
            variant="contained"
            color="secondary"
            onClick={handleCancelButtonClick}
          >
            Cancel
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

/**
 * A custom hook for managing core functionality of an edit info form.
 */
const useEditInfoForm = () => {
  const formHelpers = useForm<EditInfoFormValues>();
  
  const handleKeyDown = (event: React.KeyboardEvent) => {
    // Prevents the form from being submitted by pressing ENTER.
    if (event.key === 'enter') {
      event.preventDefault();
    }
  };

  const handleCancelButtonClick = () => {
    formHelpers.resetForm();
  };

  return { 
    ...formHelpers,
    handleKeyDown,
    handleCancelButtonClick,
  };
};

/**
 * A form for managing an organization's basic information such as description
 * or website url.
 */
const WrappedEditInfoForm = ({ id, initialValues }: Props) => {
  const { succeededToExecute, failedToExecute } = useContext(GlobalContext);

  const handleSubmit = async (formValues: EditInfoFormValues, helpers: FormikHelpers<EditInfoFormValues>): Promise<void> => {
    helpers.setSubmitting(true);

    const [, error] = await updateOrganizationInfo(id, formValues); 
    
    if (error) {
      error.code && showErrMsg(error.code, failedToExecute);
      helpers.setStatus({ error: error.message });
      helpers.setSubmitting(false);
      return;
    } 
    
    succeededToExecute('');
    helpers.setStatus({ success: 'Form has been successfully submitted' });
    helpers.setSubmitting(false);
  };

  return (
    <FormContainer 
      initialValues={initialValues}
      validationSchema={basicOrganizationProfileSchema}
      onSubmit={handleSubmit}
    >
      <EditInfoForm />
    </FormContainer>
  );
};

export default WrappedEditInfoForm;
