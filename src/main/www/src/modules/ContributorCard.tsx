/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { useContext, useEffect, useState } from 'react';
import CustomCard from '../components/UIComponents/CustomCard/CustomCard';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import PortalContext from '../Context/PortalContext';
import { 
  api_prefix, 
  brightBlue, 
  getCurrentMode, 
  END_POINT, 
  FETCH_METHOD, 
  MODE_REACT_ONLY,
  NAV_PATHS,
} from '../Constants/Constants';
import { pickRandomItems, renderItemList } from '../Utils/portalFunctionHelpers';
import { fetchWrapper } from '../Utils/formFunctionHelpers';
import { useHistory } from 'react-router';

const isReactOnlyMode = getCurrentMode() === MODE_REACT_ONLY;

/** 
  * A component that displays a card with a list of 5 random committers.
  */
export const ContributorCard = () => {
  const { isFetching, contributors, viewAll } = useContributorCard();

  return (
    <CustomCard
      isFetching={isFetching}
      subtitle="Your Contributors"
      color={brightBlue}
      icon={<PeopleAltIcon />}
      listItems={renderItemList(contributors, isFetching, 'No contributors yet', '')}
      urlText="View All"
      callBackFunc={viewAll}
    />
  );
};

export default ContributorCard;

const useContributorCard = () => {
  const { orgId, contributors, setContributors, setContactFilterRole } = useContext(PortalContext);
  const [isFetching, setIsFetching] = useState(true);
  const history = useHistory();
  
  /**
    * Navigate to the contact management page with the contributor filter
    * applied. This will show all contributors.
    */
  const viewAll = () => {
    setContactFilterRole('contributor');
    history.push(NAV_PATHS.contactManagement);
  }

  useEffect(() => {
    // If we already have the data, don't fetch it again.
    if (!isFetching && contributors !== null) return;

    // Retrieve the proper URL depending on the environment.
    const urlForContributors = isReactOnlyMode
      ? '/membership_data/test_committers_and_contributors.json'
      : api_prefix() + `/${END_POINT.organizations}/${orgId}/${END_POINT.contributors}`;

    // Set the contributor data state in the PortalContext.
    const saveContributorsData = (data: Array<any>) => {
      const contributorsData = data.map((contributors) => ({
        name: `${contributors.fname} ${contributors.lname}`,
        url: `https://accounts.eclipse.org/users/${contributors.person_id}`,
      }));

      // Pick 5 random contributors to display.
      const fiveRandomContributors =
        contributorsData?.length > 0
          ? pickRandomItems(contributorsData, 5)
          : [
              {
                name: 'No contributors yet',
                url: '',
              },
            ];

      // Set the contributors to be displayed on the card.
      setContributors(fiveRandomContributors);
      setIsFetching(false);
    };

    fetchWrapper(urlForContributors, FETCH_METHOD.GET, saveContributorsData, '', () =>
      setIsFetching(false)
    );
  }, [orgId, contributors, setContributors, isFetching, setIsFetching]);

  return { isFetching, contributors, viewAll };
};

