/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { 
  ListItemIcon, 
  ListItemText, 
  Menu, 
  MenuItem as MuiMenuItem,
  MenuItemProps as MuiMenuItemProps, 
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import PersonIcon from '@material-ui/icons/Person';
import EditIcon from '@material-ui/icons/Edit';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SettingsIcon from '@material-ui/icons/Settings';
import IconButton from '@material-ui/core/IconButton';
import { logout } from '../Utils/formFunctionHelpers';

interface MenuItemProps extends MuiMenuItemProps {
  className?: string;
  external?: boolean;
  href?: string;
  icon?: React.ReactElement;
  onClick?: React.MouseEventHandler;
}

/**
  * An item which belongs to a menu.
  */
const MenuItem: React.FC<MenuItemProps> = ({ 
  className, 
  external = false, 
  href, 
  icon, 
  onClick: handleClick, 
  children 
}) => {
  const classes = useStyles();

  const menuItemRender = (
    <MuiMenuItem className={className} onClick={handleClick}>
      { icon 
        ? <ListItemIcon>{icon}</ListItemIcon> 
        : null 
      }
      <ListItemText>{children}</ListItemText>
    </MuiMenuItem>
  );
  
  // If external is true, wrap the menuItem in an anchor tag. Otherwise, wrap
  // it in a Link component from React Router.
  let linkRender = null;
  if (href) {
    linkRender = external 
      ? <a className={classes.anchor} href={href}>{menuItemRender}</a> 
      : <Link className={classes.anchor} to={href}>{menuItemRender}</Link>;
  }
  
  // If href is provided, use the menu item wrapped in a link render, otherwise
  // render the menu item directly.
  return href ? linkRender : menuItemRender; 
}

interface AccountMenuProps {
  icon?: React.ReactElement;
}

const AccountMenu: React.FC<AccountMenuProps> = ({ icon = <ExpandMoreIcon />}) => {
  const [anchorElement, setAnchorElement] = useState<Element | null>(null);
  const open = Boolean(anchorElement);
  
  // Open the menu when the icon button is clicked.
  const handleIconButtonClick: React.MouseEventHandler = (event) => {
    setAnchorElement(event.currentTarget);
  }

  // Close the menu when a menu item is clicked.
  const handleMenuClose: React.MouseEventHandler = () => {
    setAnchorElement(null);
  };
  
  // Log the user out when the logout menu item is clicked.
  const handleLogoutClick: React.MouseEventHandler = () => logout();

  return (
    <>
      <IconButton onClick={handleIconButtonClick}>
        {icon}
      </IconButton>

      <Menu 
        open={open} 
        anchorEl={anchorElement} 
        onClose={handleMenuClose} 
        keepMounted
      >
        <MenuItem href="https://www.eclipse.org/user" icon={<PersonIcon/>} external>
          View Profile
        </MenuItem>
        <MenuItem href="https://accounts.eclipse.org/user/edit" icon={<EditIcon/>} external>
          Edit Profile
        </MenuItem>
        <MenuItem className="toolbar-manage-cookies" icon={<SettingsIcon/>}>
          Manage Cookies 
        </MenuItem>
        <MenuItem icon={<ExitToAppIcon/>} onClick={handleLogoutClick}>
          Log Out
        </MenuItem>
      </Menu>
    </>
  );
};

export default AccountMenu;

const useStyles = makeStyles((_theme: Theme) =>
  createStyles({
    anchor: {
      color: 'inherit',
      textDecoration: 'none',
      '&:hover': {
        color: 'inherit',
        textDecoration: 'none',
      }
    }
  })
);
