/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { FETCH_HEADER, FETCH_HEADER_WITHOUT_CSRF, getCurrentMode } from '../Constants/Constants';
import { exhaustiveMatchingGuard, transformSnakeToCamel, HttpError, APIResponse, APIResponseError } from './shared';
import { Organization } from '../types';

const apiBasePath = '/api';
const stubbedEndpoint = '/membership_data/test_orgs_info_data.json';

/**
 * Depending on the environment "mode", this function will perform an HTTP
 * network request and returns a Promise that resolves to the response.
 * 
 * @param input - The request info or request object for an HTTP request.
 * @param options - The options for the HTTP request.
 * @returns A promise that resolves to the response.
 */
const contextualFetch = async (input: RequestInfo | Request, options?: RequestInit) => {
  const mode = getCurrentMode();
  switch (mode) {
    case 'MODE_REACT_API':
      return fetch(input, options);
    case 'MODE_REACT_ONLY':
      // Force method to be GET as we can't POST with our mock data. The GET
      // should contain the typical response of the POST.
      if (options?.method) {
        options.method = 'GET';
      }

      return fetch(stubbedEndpoint, options);
    default:
      return exhaustiveMatchingGuard(mode as never);
  }
};

/*
 * Retrieves default headers and handles CSRF
 * @param input - The resource URL or a Request object
 * representing the URL and other request options.
 */
const getDefaultHeaders = (input: string | Request) => {
  const url = typeof input === 'string' ? input : input.url;

  const shouldIncludeCSRF = url[0] === '/';

  return shouldIncludeCSRF ? FETCH_HEADER : FETCH_HEADER_WITHOUT_CSRF;
};

/**
 * Retrieves the full list of organizations.
 * @returns A tuple containing the organizations and any errors.
 */
export const getOrganizations = async (): Promise<APIResponse<Organization[]>> => {
  try {
    const response = await contextualFetch(`${apiBasePath}/organizations`);
    if (!response?.ok) {
      throw new Error(`Could not fetch organizations`);
    }

    const data = await response.json();
    const organizations: Organization[] = transformSnakeToCamel(data);

    return [organizations, null];
  } catch (e) {
    console.error(e);
    const error = {
      code: 400,
      message: 'An unexpected error has occurred when fetching organizations',
    };

    return [null, error];
  }
};

type SlimOrganization = Pick<Organization, 'organizationId' | 'name' | 'logos'>;

/** 
 * Retrieves a list of slim organizations. This is used as a more performant way
 * to retrieve a list of organizations if you don't need the full organization information.
 *
 * @returns A tuple containing the slim organizations and any errors.
 */
export const getSlimOrganizations = async (): Promise<APIResponse<SlimOrganization[]>> => {
  try {
    const response = await contextualFetch(`${apiBasePath}/organizations/slim`);
    if (!response?.ok) {
      throw new HttpError(response.status, response.statusText);
    }

    const data = await response.json();
    const organizations: SlimOrganization[] = transformSnakeToCamel(data);

    return [organizations, null];
  } catch (e) {
    console.error(e);
    const error = {
      code: 400,
      message: 'An unexpected error has occurred when fetching slim organizations',
    };

    return [null, error];
  }
};

interface GetOrganizationOptions {
  /** The cache mode to use for the request. */
  cache?: RequestCache;
}

/** 
 * Retrieves an organization by its ID.
 *
 * @param organizationId - The ID of the organization to retrieve.
 * @param options - The options for the getter.
 * @returns A tuple containing the specified organization and any errors.
 */
export const getOrganization = async (
  organizationId: number,
  options?: GetOrganizationOptions
): Promise<APIResponse<Organization>> => {
  try {
    // An object of options to pass to the internal fetch call.
    // In this case, we want to pass the cache option to the fetch call.
    let requestOptions: RequestInit = {
      cache: options?.cache,
    };

    const response = await contextualFetch(`${apiBasePath}/organizations/${organizationId}`, requestOptions);
    if (!response?.ok) {
      throw new HttpError(response.status, response.statusText);
    }

    const data = await response.json();
    const organization: Organization = transformSnakeToCamel(data);

    return [organization, null];
  } catch (e) {
    console.error(e);
    const error = {
      code: 400,
      message: `An unexpected error has occurred when fetching organization ${organizationId}`,
    };

    return [null, error];
  }
};

interface UpdateOrganizationInfoRequestBody {
  description?: string;
  company_url?: string;
}

interface OrganizationInfo {
  description?: string;
  url?: string;
}

/** 
  * Maps an organization info object to the request body for updating an organization.
  *
  * @param organizationInfo - The organization info to map.
  * @returns The request body for updating an organization.
  */
const updateOrganizationInfoRequestBodyMapper = (
  organizationInfo: OrganizationInfo
): UpdateOrganizationInfoRequestBody => {
  return {
    description: organizationInfo.description,
    company_url: organizationInfo.url,
  };
};

/** 
 * Updates the basic info of an organization. This includes the description and website url.
 *
 * @param organizationId - The ID of the organization to update.
 * @param info - The new organization info.
 */
export const updateOrganizationInfo = async (
  organizationId: number,
  info: OrganizationInfo
): Promise<APIResponse<Organization>> => {
  if (getCurrentMode() === 'MODE_REACT_ONLY') {
    return [{} as Organization, null]; // Stub for react-only mode
  } else {
    try {
      const url = `${apiBasePath}/organizations/${organizationId}`;
      const body = JSON.stringify(updateOrganizationInfoRequestBodyMapper(info));
      const response = await contextualFetch(url, {
        method: 'POST',
        headers: getDefaultHeaders(url),
        body,
      });

      if (!response.ok) throw new HttpError(response.status, 'A problem occurred when saving.');

      const data = (await response.json());
      const updatedOrganization: Organization = transformSnakeToCamel(data);

      return [updatedOrganization, null];
    } catch (e) {
      console.error(e);
      let error: APIResponseError = {
        message: 'A problem occurred when saving.',
      };

      if (e instanceof HttpError) {
        error.code = e.code;
      }

      return [null, error];
    }
  }
};

interface UpdateOrganizationLogoOptions {
  format: 'SMALL' | 'LARGE' | 'PRINT' | 'WEB';
}

/** 
 * Updates the logo of an organization.
 *
 * @param organizationId - The ID of the organization to update.
 * @param logo - The new logo to upload.
 * @param options - The options for the logo upload.
 * @returns A tuple containing the updated organization and any errors.
 */
export const updateOrganizationLogo = async (
  organizationId: number,
  logo: File,
  options: UpdateOrganizationLogoOptions
): Promise<APIResponse<Organization>> => {
  if (getCurrentMode() === 'MODE_REACT_ONLY') {
    console.log({
      image: logo,
      image_mime: logo.type,
      image_format: options.format,
    });
    return [{} as Organization, null]; //  Stub for react-only mode
  } else {
    try {
      const url = `${apiBasePath}/organizations/${organizationId}/logos`;
      
      // Assemble the form data to send to the server for the logo upload.
      const formData = new FormData();
      formData.append('image', logo);
      formData.append('image_mime', logo.type);
      formData.append('image_format', options.format);

      const response = await contextualFetch(url, {
        method: 'POST',
        // We can't use our default headers here. So we create the headers
        // manually.
        headers: { 'x-csrf-token': FETCH_HEADER['x-csrf-token'] },
        body: formData,
      });

      if (!response.ok) throw new HttpError(response.status, 'A problem occurred when saving.');

      const data = (await response.json());
      const updatedOrganization: Organization = transformSnakeToCamel(data) as Organization;

      return [updatedOrganization, null];
    } catch (e) {
      console.error(e);
      let error: APIResponseError = {
        message: 'A problem occurred when saving.',
      };

      if (e instanceof HttpError) {
        error.code = e.code;
      }

      return [null, error];
    }
  }
};
