/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

// This file contains shared functions, classes and types that are used across functions
// which interact with APIs.

export interface APIResponseError {
  /** The HTTP status code. */
  code?: number;
  /** The error message. */
  message: string;
}

/** A tuple containing the response data and any errors. */
export type APIResponse<T> = [T | null, APIResponseError | null];

/** Class representing an HTTP error. It can be thrown to indicate that an HTTP
  * error occurred. 
  */
export class HttpError extends Error {
  public code: number;
  public message: string;

  /** 
    * @param code - The HTTP status code.
    * @param message - The error message.  
    */
  constructor(code: number, message: string) {
    super(`HTTP Error: ${code} ${message}`);
    this.code = code;
    this.message = message;
    this.name = 'HTTPError';
  }
}

/**
 * This function is used to ensure that a switch statement is exhaustive. If
 * this function is called, it means that the switch statement is not exhaustive
 * and a bug exists.
 * 
 * @param _ Value of the switch expression.
 * @throws Will throw an error if called
 */
export const exhaustiveMatchingGuard = (_: never): never => {
  throw new Error('Should not have reached this point');
};

/** 
  * Converts a snake case string into a camel case string.
  *
  * @param str - The snake case string to convert.
  * @returns The camel case string.
  */
export const convertSnakeToCamel = (str: string): string => {
  // Capitalize the first letter of each word except the first.
  return str.split('_')
    .reduce((acc, curr, i) => {
      if (i === 0) {
        return curr;
      }
      return acc + curr[0].toUpperCase() + curr.slice(1);
    }, '');
}

/**
  * Transforms a snake case object into a camel case object.
  *
  * @param input - The object with snake case keys to transform.
  * @returns The transformed object with camel case keys.
  * @example
  * const input = { foo_bar: 'baz' };
  * // Returns { fooBar: 'baz' }
  * transformSnakeToCamel(input);
  */
export const transformSnakeToCamel = <T extends object,>(input: unknown): T => {
  // Ensure the input exists and is not null.
  if (input === null) {
    return input as any;
  }

  // If the input is an array, transform each item in the array.
  if (Array.isArray(input)) {
    return input.map((item) => transformSnakeToCamel(item)) as any;
  }

  // If the input is not an object, return it as-is.
  if (typeof input !== 'object') { 
    return input as T;
  }
  
  let transformedObject: Record<string, any> = {};

  Object.keys(input!).forEach((snakeCaseKey) => {
    // If the key is a property of the object, transform it.
    if (input!.hasOwnProperty(snakeCaseKey)) {
      const camelCaseKey = convertSnakeToCamel(snakeCaseKey);
      const value = input![snakeCaseKey as keyof typeof input];

      if (Array.isArray(value)) {
        // If the value is an array, transform each item in the array.
        transformedObject[camelCaseKey] = (value as any[]).map((input) => transformSnakeToCamel(input));
      } else if (typeof value === 'object' && value !== null) {
        // If the value is an object, transform it directly.
        transformedObject[camelCaseKey] = transformSnakeToCamel(value);
      } else {
        // Otherwise, add the value to the transformed object.
        transformedObject[camelCaseKey] = value;
      }
    }
  });

  return transformedObject as T;
};

