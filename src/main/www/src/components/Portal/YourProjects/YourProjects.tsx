/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import { CircularProgress, createStyles, makeStyles, Theme, Typography } from '@material-ui/core';
import { BusinessCenter as BusinessCenterIcon } from '@material-ui/icons';
import { DataGrid, GridColDef, GridRenderCellParams } from '@mui/x-data-grid';
import {
  api_prefix,
  getCurrentMode,
  MODE_REACT_ONLY,
  borderRadiusSize,
  brightOrange,
  FETCH_METHOD,
  iconGray,
  END_POINT,
} from '../../../Constants/Constants';
import { useContext, useEffect, useState } from 'react';
import PortalContext from '../../../Context/PortalContext';
import { fetchWrapper, fetchWrapperPagination } from '../../../Utils/formFunctionHelpers';
import { renderTableHelperText } from '../../../Utils/portalFunctionHelpers';
import { ProjectsDataBackend } from '../../../Interfaces/portal_interface';
import CustomToolbar from '../../UIComponents/Button/CustomToolbar';
import FullLayout from '../../../layouts/FullLayout';

const isReactOnlyMode = getCurrentMode() === MODE_REACT_ONLY;

const useStyle = makeStyles((theme: Theme) =>
  createStyles({
    headerIcon: {
      fontSize: 80,
      color: iconGray,
      borderBottom: `6px ${brightOrange} solid`,
    },
    pageTitle: {
      margin: theme.spacing(0.5, 0, 4),
    },
    filterText: {
      backgroundColor: brightOrange,
      color: 'white',
      padding: theme.spacing(1),
      borderTopLeftRadius: borderRadiusSize,
      borderTopRightRadius: borderRadiusSize,
    },
    tableContainer: {
      minHeight: 330,
      height: 'calc(100vh - 455px)',
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    table: {
      borderTopRightRadius: 0,
      borderTopLeftRadius: 0,
    },
    noRowsOverlay: {
      position: 'absolute',
      top: 56,
      bottom: 0,
      left: 0,
      right: 0,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  })
);

const columns: GridColDef[] = [
  {
    field: 'name',
    headerName: 'Project Name',
    minWidth: 220,
    flex: 2,
    renderCell: (params: GridRenderCellParams) => {
      const data = params.row;
      return data.status === 'Active' ? (
        <a href={data.url} target="_blank" rel="noreferrer">
          {data.name}
        </a>
      ) : (
        <p>{data.name}</p>
      );
    },
  },
  {
    field: 'id',
    headerName: 'Project ID',
    minWidth: 200,
    flex: 1,
    renderCell: (params: GridRenderCellParams) => {
      const data = params.row;
      return data.status === 'Active' ? (
        <a href={data.url} target="_blank" rel="noreferrer">
          {data.id}
        </a>
      ) : (
        <p>{data.id}</p>
      );
    },
  },
  {
    field: 'status',
    headerName: 'Status',
    flex: 1,
    minWidth: 100,
  },
];

export default function YourProjects() {
  const classes = useStyle();
  const { orgId, allYourProjectsData, setAllYourProjectsData } = useContext(PortalContext);
  const [isFetchingYourProjects, setIsFetchingYourProjects] = useState(true);

  useEffect(() => {
    if (allYourProjectsData !== null) {
      setIsFetchingYourProjects(false);
    }
    if (!orgId || allYourProjectsData !== null) {
      return;
    }

    let allYourProjects: Array<any> = [];

    // For your projects data
    const urlForYourProjects = isReactOnlyMode
      ? '/membership_data/test_your_projects.json'
      : api_prefix() + `/${END_POINT.organizations}/${orgId}/${END_POINT.projects}?page=`;

    const saveYourProjectsData = (data: Array<ProjectsDataBackend>) => {
      allYourProjects = data.map((project) => ({
        name: project.name,
        url: `https://projects.eclipse.org/projects/${project.project_id}`,
        id: project.project_id,
        status: project.active ? 'Active' : 'Inactive',
      }));
      setAllYourProjectsData(allYourProjects);
      setIsFetchingYourProjects(false);
    };

    if (isReactOnlyMode) {
      fetchWrapper(urlForYourProjects, FETCH_METHOD.GET, saveYourProjectsData);
    } else {
      fetchWrapperPagination(urlForYourProjects, 1, saveYourProjectsData, () => setIsFetchingYourProjects(false));
    }
  }, [orgId, allYourProjectsData, setAllYourProjectsData]);

  return (
    <FullLayout>
      <BusinessCenterIcon className={classes.headerIcon} />
      <Typography className={classes.pageTitle} variant="h4" component="h1">
        Your Projects
      </Typography>
      <Typography variant="body1" className={classes.filterText}>
        Projects
      </Typography>
      <div className={classes.tableContainer}>
        {isFetchingYourProjects ? (
          <CircularProgress />
        ) : (
          <DataGrid
            className={classes.table}
            rows={allYourProjectsData || []}
            columns={columns}
            rowHeight={55}
            rowsPerPageOptions={[5, 10, 100]}
            disableSelectionOnClick
            components={{
              NoRowsOverlay: () => (
                <div className={classes.noRowsOverlay}>
                  {renderTableHelperText(allYourProjectsData, isFetchingYourProjects)}
                </div>
              ),
              Toolbar: () => <CustomToolbar fileName="your_projects_data" />,
            }}
          />
        )}
      </div>
    </FullLayout>
  );
}
