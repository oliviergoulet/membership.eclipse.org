/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import { useEffect, useMemo, useState } from 'react';
import { NavLink, useLocation } from 'react-router-dom';
import {
  ListItem,
  ListItemText,
  ListItemIcon,
  Container,
  Drawer,
  List,
  Theme,
  Hidden,
  Button,
} from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import efWhiteLogo from '../../../assets/logos/ef-registered-wht.svg';
import CloseIcon from '@material-ui/icons/Close';
import {
  NAV_OPTIONS_DATA,
  drawerWidth,
  themeBlack,
  darkOrange,
} from '../../../Constants/Constants';
import { scrollToTop } from '../../../Utils/formFunctionHelpers';
import Protected from '../../Protected';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    drawer: {
      [theme.breakpoints.up('md')]: {
        minWidth: drawerWidth,
        flexShrink: 0,
      },
    },
    drawerPaper: {
      backgroundColor: themeBlack,
      [theme.breakpoints.up('md')]: {
        minWidth: drawerWidth,
      },
    },
    navOptions: {
      '&:hover': {
        textDecoration: 'none',
      },
    },
    navItems: {
      height: 55,
      borderLeft: `rgba(0,0,0,0) 5px solid`,
    },
    navItemsActive: {
      height: 55,
      borderLeft: `${darkOrange} 5px solid`,
    },
    navIcons: {
      color: '#A5A4BF',
    },
    navIconsActive: {
      color: darkOrange,
    },
    navText: {
      color: '#FFF',
    },
    divider: {
      borderColor: '#505050',
      margin: theme.spacing(2),
    },
    efLogoCtn: {
      height: 70,
      display: 'flex',
      justifyContent: 'center',
    },
    efLogo: {
      width: 175,
    },
    closeBtn: {
      position: 'fixed',
      top: theme.spacing(1.5),
      right: theme.spacing(2),
    },
    closeIcon: {
      color: 'white',
      fontSize: 30,
    },
  })
);

interface NavOptionProps {
  path: string;
  icon: JSX.Element;
  type: string | undefined;
  name: string;
  hide: boolean;
  handleDrawerToggle: (() => void) | null;
  active?: boolean;
}

interface LeftNavBarProps {
  mobileOpen: boolean;
  handleDrawerToggle: () => void;
}

const NavOption: React.FC<NavOptionProps> = ({ path, icon, type, name, hide, handleDrawerToggle, active: isActive }) => {
  const classes = useStyles();

  if (hide) {
    return null;
  }

  if (type === 'divider') {
    return <hr className={classes.divider} />;
  } else {
    return (
      <NavLink
        className={classes.navOptions}
        to={path}
        key={path}
        onClick={() => {
          handleDrawerToggle && handleDrawerToggle();
          scrollToTop();
        }}
      >
        <ListItem className={isActive ? classes.navItemsActive : classes.navItems} button>
          <ListItemIcon className={isActive ? classes.navIconsActive : classes.navIcons}>{icon}</ListItemIcon>
          <ListItemText className={classes.navText} primary={name} />
        </ListItem>
      </NavLink>
    );
  }
};

const LeftNavBar: React.FC<LeftNavBarProps> = ({ mobileOpen, handleDrawerToggle }) => {
  const classes = useStyles();
  const [activeItem, setActiveItem] = useState<string>('');
  const location = useLocation();
  
  const paths = useMemo(() => NAV_OPTIONS_DATA.map((item) => item.path), []);

  useEffect(() => {
    const locationHash = location.hash.slice(1);

    paths.forEach((path) => {
      let [itemPathname, itemHash] = path.split('#');

      if (!itemHash) itemHash = '';
      if (location.pathname === itemPathname && locationHash === itemHash) {
        setActiveItem(path);
      }
    });

  }, [location, setActiveItem, paths]);

  const renderNavOptions = (isHambergerMenu: boolean) => 
    NAV_OPTIONS_DATA.map((item) => (
      <Protected allowedRoles={item.allowedRoles} strict={item.strict} key={item.name}>
        <NavOption
          path={item.path}
          name={item.name}
          type={item.type}
          icon={item.icon}
          handleDrawerToggle={isHambergerMenu ? handleDrawerToggle : null}
          key={item.path}
          hide={false}
          active={item.path === activeItem}
        />
      </Protected>
    ));

  return (
    <>
      <Hidden mdUp implementation="css">
        <Drawer
          className={classes.drawer}
          variant="temporary"
          open={mobileOpen}
          classes={{
            paper: classes.drawerPaper,
          }}
          anchor="top"
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          <Container className={classes.efLogoCtn}>
            <img src={efWhiteLogo} alt="Eclipse Foundation logo" className={classes.efLogo} />
            <Button onClick={handleDrawerToggle} className={classes.closeBtn}>
              <CloseIcon className={classes.closeIcon} />
            </Button>
          </Container>
          <List>{renderNavOptions(true)}</List>
        </Drawer>
      </Hidden>
      <Hidden smDown implementation="css">
        <Drawer
          className={classes.drawer}
          variant="permanent"
          classes={{
            paper: classes.drawerPaper,
          }}
          open
          anchor="left"
        >
          <Container className={classes.efLogoCtn}>
            <img src={efWhiteLogo} alt="Eclipse Foundation logo" className={classes.efLogo} />
          </Container>
          <List>{renderNavOptions(false)}</List>
        </Drawer>
      </Hidden>
    </>
  );
};

export default LeftNavBar;
