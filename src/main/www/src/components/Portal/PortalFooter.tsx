/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import { Theme } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { iconGray } from '../../Constants/Constants';

const useStyles = makeStyles((theme: Theme) => {
  const color = (props: Props) => props.textColor || iconGray;
  const textShadow = (props: Props) => props.textShadow;

  return createStyles({
    root: {
      position: 'relative',
    },
    footer: {
      color,
      textShadow,
      position: 'absolute',
      fontSize: 12,
      bottom: '100%',
      left: 15,
      right: 15,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      [theme.breakpoints.up('sm')]: {
        flexDirection: 'row',
        alignItems: 'stretch',
        justifyContent: 'space-between',
        left: 25,
        right: 25,
      },
      [theme.breakpoints.up('md')]: {
        left: 65,
        right: 65,
      },
    },
    footerLinkContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      marginLeft: theme.spacing(0),
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(2),
      },
      width: 280,
      '& a': {
        color,
      }
    },
  })}
);

interface Props {
  textColor?: string;
  textShadow?: string;
}

const PortalFooter: React.FC<Props> = (props) => {
  const classes = useStyles(props);

  return (
    <div className={classes.root}>
      <div className={classes.footer}>
        <p>Copyright &#169; Eclipse Foundation. All Rights Reserved.</p>
        <div className={classes.footerLinkContainer}>
          <a target="_blank" rel="noreferrer" href="https://www.eclipse.org/legal/privacy.php">
            Privacy Policy
          </a>
          <a target="_blank" rel="noreferrer" href="https://www.eclipse.org/legal/termsofuse.php">
            Terms of Use
          </a>
          <a target="_blank" rel="noreferrer" href="https://www.eclipse.org/legal/copyright.php">
            Copyright Agent
          </a>
        </div>
      </div>
    </div>
  );
}

export default PortalFooter;
