/*!
 * Copyright (c) 2021, 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Zhou Fang <zhou.fang@eclipse-foundation.org>
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import {
  Button,
  Checkbox,
  CircularProgress,
  createStyles,
  FormControlLabel,
  makeStyles,
  Theme,
  Typography,
  Popover,
} from '@material-ui/core';
import { DataGrid, GridColDef, GridRenderCellParams, getGridStringOperators, GridFilterItem } from '@mui/x-data-grid';
import RecentActorsIcon from '@material-ui/icons/RecentActors';
import {
  api_prefix,
  getCurrentMode,
  MODE_REACT_ONLY,
  borderRadiusSize,
  brightOrange,
  FETCH_METHOD,
  iconGray,
  PERMISSIONS_BASED_ON_ROLES,
  UPDATE_DENYLISTED_RELATIONS,
  ROLE_DESCRIPTION,
} from '../../../Constants/Constants';
import { useContext, useEffect, useState } from 'react';
import PortalContext from '../../../Context/PortalContext';
import { usePreviewMode } from '../../../Context/PreviewModeContext';
import EditIcon from '@material-ui/icons/Edit';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import Input from '../../UIComponents/Inputs/Input';
import ModalWindow from '../../UIComponents/Notifications/ModalWindow';
import {
  AllRelationsFrontend,
  ContactBackend,
  OrganizationRoles,
  OrganizationRolesKeys,
} from '../../../Interfaces/portal_interface';
import { fetchWrapper, fetchWrapperPagination, isProd } from '../../../Utils/formFunctionHelpers';
import GlobalContext from '../../../Context/GlobalContext';
import { checkPermission, renderTableHelperText, showErrMsg } from '../../../Utils/portalFunctionHelpers';
import HelpIcon from '@material-ui/icons/Help';
import CustomToolbar from '../../UIComponents/Button/CustomToolbar';
import FullLayout from '../../../layouts/FullLayout';

const isReactOnlyMode = getCurrentMode() === MODE_REACT_ONLY;

const useStyle = makeStyles((theme: Theme) =>
  createStyles({
    headerIcon: {
      fontSize: 80,
      color: iconGray,
      borderBottom: `6px ${brightOrange} solid`,
    },
    pageTitle: {
      margin: theme.spacing(0.5, 0, 4),
    },
    contactFilterText: {
      backgroundColor: brightOrange,
      color: 'white',
      padding: theme.spacing(1),
      borderTopLeftRadius: borderRadiusSize,
      borderTopRightRadius: borderRadiusSize,
    },
    tableContainer: {
      minHeight: 320,
      height: 'calc(100vh - 510px)',
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    table: {
      borderTopRightRadius: 0,
      borderTopLeftRadius: 0,
    },
    roleCell: {
      display: 'flex',
    },
    roleText: {
      whiteSpace: 'normal',
    },
    checkBoxContainer: {
      display: 'flex',
      alignItems: 'center',
    },
    helpIconContainer: {
      cursor: 'pointer',
    },
    helpIcon: {
      fontSize: 22,
      color: iconGray,
    },
    popoverText: {
      padding: theme.spacing(1.5),
      maxWidth: 400,
    },
    customPopover: {
      '& .MuiPopover-paper': {
        boxShadow: '1px 1px 15px rgba(0,0,0,0.15)',
      },
    },
    editIconCtn: {
      display: 'flex',
      alignItems: 'center',
      cursor: 'pointer',
    },
    editIcon: {
      color: iconGray,
      marginLeft: 10,
    },
    removeBtn: {
      backgroundColor: '#EBEBEB',
    },
    commentDescription: { marginBottom: theme.spacing(2) },
    addContactIcon: {
      margin: theme.spacing(4, 0, 1),
      fontSize: 50,
      color: iconGray,
      borderBottom: `3px ${brightOrange} solid`,
    },
    noRowsOverlay: {
      position: 'absolute',
      top: 56,
      bottom: 0,
      left: 0,
      right: 0,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  })
);

const INITIAL_CHECKED_ROLES: Array<{
  role: string;
  relation: string;
  checked: boolean;
}> = OrganizationRolesKeys.map((item) => ({
  role: OrganizationRoles[item],
  relation: item,
  checked: false,
}));

const ALL_ROLES = INITIAL_CHECKED_ROLES.filter((item) => item.relation !== 'EMPLY' && item.relation !== 'CM').map(
  (item) => ({ relation: item.relation, role: item.role })
);
interface ContactFrontend {
  id: string;
  name: string;
  relations: Array<string>;
  role: Array<string>;
  email: string;
}

interface AllRelationsBackend {
  relation: string;
  description: string;
  active: boolean;
  type: string;
  sort_order: string;
}

interface RoleDescription {
  [key: string]: string;
}

const roleDescriptionObj: RoleDescription = ROLE_DESCRIPTION;

const defaultOperators = getGridStringOperators();

// Add exclude operator for listing contributors (which need to filter out committers)
// Refer here: https://v4.mui.com/components/data-grid/filtering/#create-a-custom-operator
const customOperators = [
  ...defaultOperators,
  {
    value: 'excludes',
    label: 'excludes',
    // The input component can be the same as other operators and they are all the same.
    InputComponent: defaultOperators[0].InputComponent,
    getApplyFilterFn: (filterItem: GridFilterItem) => {
      if (!filterItem.columnField || !filterItem.value || !filterItem.operatorValue) {
        return null;
      }
      return (params: any): boolean => {
        return !params.value.find((item: string) => item.toLowerCase().includes(filterItem.value.toLowerCase()));
      };
    },
  },
];

export default function ContactManagement() {
  const classes = useStyle();
  const { succeededToExecute, failedToExecute } = useContext(GlobalContext);
  const {
    contactFilterRole,
    setContactFilterRole,
    orgId,
    contactData,
    setContactData,
    allRelations,
    setAllRelations,
    currentUserPortal,
  } = useContext(PortalContext);
  const { inPreviewMode } = usePreviewMode();

  const columns: GridColDef[] = [
    {
      field: 'name',
      headerName: 'Name',
      minWidth: 150,
      flex: 1,
    },
    {
      field: 'email',
      headerName: 'Email',
      minWidth: 200,
      flex: 1,
    },
    {
      field: 'role',
      headerName: 'Role(s)',
      flex: 2,
      filterOperators: customOperators,
      minWidth: 320,
      valueFormatter: (value) => value.row.role.join(", "),
      renderCell: (params: GridRenderCellParams) => (
        <div className={classes.roleCell}>
          <Typography variant="body2" className={classes.roleText}>
            {Array.isArray(params.value) &&
              params.value.map((role: string, index: number) => {
                return index >= 1 ? ', ' + role : role;
              })}
          </Typography>
          {checkPermission(PERMISSIONS_BASED_ON_ROLES.editContactRoles, currentUserPortal!.relation) && (
            <div
              className={classes.editIconCtn}
              onClick={() => {
                const currentContact = params.row;
                setSelectedContact({
                  id: currentContact.id,
                  email: currentContact.email,
                  role: currentContact.role,
                  relations: currentContact.relations,
                  name: currentContact.name,
                });
                const shouldCheckedRoles = checkedRoles.map((item) => ({
                  role: item.role,
                  relation: item.relation,
                  checked: currentContact.relations.includes(item.relation),
                }));

                setCheckedRoles(shouldCheckedRoles);
                setOpenEditRoles(true);
              }}
            >
              <EditIcon className={classes.editIcon} />
            </div>
          )}
        </div>
      ),
    },
    {
      field: 'requestRemove',
      headerName: ' ',
      width: 220,
      hide: !checkPermission(PERMISSIONS_BASED_ON_ROLES.requestRemovalOfContacts, currentUserPortal!.relation),
      renderCell: (params: GridRenderCellParams) => (
        <Button
          className={classes.removeBtn}
          onClick={() => {
            const currentContact = params.row;
            setSelectedContact({
              id: currentContact.id,
              email: currentContact.email,
              role: currentContact.role,
              relations: currentContact.role,
              name: currentContact.name,
            });
            setOpenRequestRemoval(true);
          }}
        >
          Request Removal From Org
        </Button>
      ),
    },
  ];

  const [contactList, setContactList] = useState<Array<ContactFrontend> | null>(null);
  const [isFetchingContacts, setIsFetchingContacts] = useState(true);
  const [openRequestRemoval, setOpenRequestRemoval] = useState(false);
  const [openEditRoles, setOpenEditRoles] = useState(false);
  const [removeComment, setRemoveComment] = useState('');
  const [selectedContact, setSelectedContact] = useState<ContactFrontend>({
    id: '',
    email: '',
    role: [''],
    relations: ['EMPLY'],
    name: '',
  });
  const [checkedRoles, setCheckedRoles] = useState(INITIAL_CHECKED_ROLES);
  const [roleDescription, setRoleDescription] = useState('');
  const [anchorEle, setAnchorEle] = useState<HTMLElement | null>(null);
  const openPopover = Boolean(anchorEle);

  const handleRequestRemoval = () => {
    // @todo: Remove preview mode check once this form uses FormContainer.
    // Prevent form from being submitted when in preview mode.
    if (inPreviewMode) {
      failedToExecute('Cannot submit form while in preview mode.', 5000);
      setOpenRequestRemoval(false);
      return;
    }

    setOpenRequestRemoval(false);
    !isProd && console.log('Deleted ', selectedContact.name, '| With comment: ', removeComment);

    if (!isReactOnlyMode) {
      const removalBody = JSON.stringify({
        organization_id: orgId,
        username: selectedContact.id,
        reason: `${removeComment}`,
      });
      fetchWrapper(
        api_prefix() + `/organizations/${orgId}/contacts/${selectedContact.id}`,
        FETCH_METHOD.DELETE,
        () => succeededToExecute(`Requested to remove ${selectedContact.name}`),
        removalBody,
        (errCode: number | string) => showErrMsg(errCode, failedToExecute)
      );
    }
    setRemoveComment('');
  };

  const handleEditRoles = () => {
    // @todo: Remove preview mode check once this form uses FormContainer.
    // Prevent form from being submitted when in preview mode.
    if (inPreviewMode) {
      failedToExecute('Cannot submit form while in preview mode.', 5000);
      setOpenEditRoles(false);
      return;
    }

    const existingCheckedRoles = selectedContact.relations;
    let newSelectedRelations = checkedRoles
      .filter((role) => role.checked) // find out all checked roles
      .map((item) => item.relation); // create a new array of string containing all checked relation

    setOpenEditRoles(false);

    // The ones only in new selected roles array should be added
    const rolesNeedToBeAdded = newSelectedRelations.filter((role) => !existingCheckedRoles.includes(role));
    // The ones only in old checked roles should be deleted, but should not include EMPLY or CM
    const rolesNeedToBeDeleted = existingCheckedRoles.filter(
      (role) => !newSelectedRelations.includes(role) && !UPDATE_DENYLISTED_RELATIONS.includes(role.toLowerCase())
    );

    const saveContactData = () => {
      if (contactData) {
        let newContactData = contactData.map((item) => item);
        const currentContact = newContactData.find((item) => item.id === selectedContact.id);
        if (currentContact) {
          // Add the roles in currentContact in newContactData
          currentContact.relations = [...currentContact.relations, ...rolesNeedToBeAdded];

          // Delete the roles in currentContact in newContactData
          currentContact.relations = currentContact.relations.filter(
            (item) => !rolesNeedToBeDeleted.find((contactToBeDeleted) => contactToBeDeleted === item)
          );

          setContactData(newContactData);
          succeededToExecute(`Updated roles for ${currentContact.first_name} ${currentContact.last_name}`);
        }
      }
    };

    // each new relation will need a POST/DELETE
    const updatedRolesArray = [...rolesNeedToBeAdded, ...rolesNeedToBeDeleted];
    updatedRolesArray.forEach((relation, index) => {
      if (!isReactOnlyMode) {
        // Only update the contact list once.
        const callbackFunc = index === updatedRolesArray.length - 1 ? saveContactData : '';
        fetchWrapper(
          api_prefix() + `/organizations/${orgId}/contacts/${selectedContact.id}/${relation}`,
          index < rolesNeedToBeAdded.length ? FETCH_METHOD.POST : FETCH_METHOD.DELETE,
          callbackFunc,
          '',
          (errCode: number | string) => showErrMsg(errCode, failedToExecute)
        );
      } else {
        saveContactData();
      }
    });
  };

  const renderRemoveReasonOptions = () => (
    <>
      <Typography variant="body1" className={classes.commentDescription}>
        Please provide any details, if necessary, relevant to Eclipse regarding removing this individual from your
        organization in the Eclipse database. (Optional)
      </Typography>
      <Input
        name="description"
        labelName="Details (Optional)"
        value={removeComment}
        onChange={(ev: React.ChangeEvent<HTMLInputElement>) => setRemoveComment(ev.target.value)}
        multiline={true}
        rows={8}
        backgroundColor="#f9f9f9"
        maxLength={700}
        explanationHelperText={'700 characters limit'}
      />
    </>
  );

  const shouldDisableEdit = (role: string) => {
    switch (role) {
      case 'Delegate':
        return !checkPermission(PERMISSIONS_BASED_ON_ROLES.editDelegates, currentUserPortal!.relation);
      case 'Alternate Member Representative':
        return !checkPermission(PERMISSIONS_BASED_ON_ROLES.editCRA, currentUserPortal!.relation);
      case 'Member Representative':
        return true;
      default:
        return false;
    }
  };

  const showDescriptionForRoles = (ev: any, relation: string) => {
    setAnchorEle(ev.currentTarget);
    setRoleDescription(roleDescriptionObj[relation]);
  };

  const renderAvailableRoles = () => {
    return ALL_ROLES.map((role, index) => (
      <div key={index} className={classes.checkBoxContainer}>
        <FormControlLabel
          control={
            <Checkbox
              name={role.role}
              color="primary"
              checked={checkedRoles.find((item) => item.role === role.role)?.checked}
              disabled={shouldDisableEdit(role.role)}
              onChange={(ev) => {
                const shouldCheckedRoles = checkedRoles.map((item) =>
                  // Only update the one that user clicks on. For others, just use what they are
                  item.role === ev.target.name
                    ? {
                        role: item.role,
                        relation: item.relation,
                        checked: ev.target.checked,
                      }
                    : item
                );
                setCheckedRoles(shouldCheckedRoles);
              }}
            />
          }
          label={role.role}
        />
        <div className={classes.helpIconContainer} onClick={(ev) => showDescriptionForRoles(ev, role.relation)}>
          <HelpIcon className={classes.helpIcon} />
        </div>
      </div>
    ));
  };

  const applyFilterFromDashboard = () => {
    switch (contactFilterRole) {
      case 'committer':
        return { items: [{ columnField: 'role', operatorValue: 'contains', value: contactFilterRole }] };
      case 'contributor':
        return { items: [{ columnField: 'role', operatorValue: 'excludes', value: 'committer' }] };
      default:
        return undefined;
    }
  };

  useEffect(() => {
    // Separate the 2 fetch into different useEffect to prevent duplicate fetch calls when re-render
    if (orgId === 0 || contactData !== null) {
      return;
    }

    const isReactOnlyMode = getCurrentMode() === MODE_REACT_ONLY;

    isReactOnlyMode
      ? fetchWrapper('/membership_data/test_contacts.json', FETCH_METHOD.GET, setContactData)
      : fetchWrapperPagination(api_prefix() + `/organizations/${orgId}/contacts?page=`, 1, setContactData, () =>
          setIsFetchingContacts(false)
        );
  }, [orgId, contactData, setContactData]);

  useEffect(() => {
    const saveAllRelations = (data: Array<AllRelationsBackend>) => {
      // Need to include PR type as the CM(committer) is not in CO but PR
      // For now, add CM as a default value to make sure there won't be 'no match'
      const allRelations: AllRelationsFrontend = { CM: 'Committer' };

      data
        .filter((item) => item.type === 'CO' || item.type === 'PR')
        .forEach((item) => {
          allRelations[item.relation] = item.description;
        });

      setAllRelations(allRelations);
    };

    if (allRelations === null) {
      isReactOnlyMode
        ? fetchWrapper('/membership_data/test_contact_relations.json', FETCH_METHOD.GET, saveAllRelations)
        : fetchWrapperPagination(api_prefix() + '/sys/relations?type=CO&page=', 1, saveAllRelations, () =>
            setIsFetchingContacts(false)
          );
    }
  }, [allRelations, setAllRelations]);

  useEffect(() => {
    if (contactData === null || allRelations === null) {
      return;
    }
    const saveContacts = (data: Array<ContactBackend>) => {
      const newData = data.map((contact) => ({
        id: contact.id,
        email: contact.email || 'no email found',
        role: contact.relations.map((item) => allRelations[item]),
        relations: contact.relations,
        name: `${contact.first_name} ${contact.last_name}`,
      }));

      setContactList(newData);
      setIsFetchingContacts(false);
    };

    saveContacts(contactData);
  }, [contactData, allRelations]);

  return (
    <FullLayout>
      <RecentActorsIcon className={classes.headerIcon} />
      <Typography className={classes.pageTitle} variant="h4" component="h1">
        Contact Management
      </Typography>
      <Typography variant="body1" className={classes.contactFilterText}>
        Contacts
      </Typography>
      <div className={classes.tableContainer}>
        {isFetchingContacts ? (
          <CircularProgress />
        ) : (
          <DataGrid
            className={classes.table}
            rows={contactList || []}
            columns={columns}
            rowHeight={55}
            rowsPerPageOptions={[5, 10, 100]}
            disableSelectionOnClick
            filterModel={applyFilterFromDashboard()}
            onFilterModelChange={() => contactFilterRole !== '' && setContactFilterRole('')}
            components={{
              NoRowsOverlay: () => (
                <div className={classes.noRowsOverlay}>{renderTableHelperText(contactList, isFetchingContacts)}</div>
              ),
              Toolbar: () => <CustomToolbar fileName="contacts_data" />,
            }}
          />
        )}
      </div>

      <PersonAddIcon className={classes.addContactIcon} />
      <Typography variant="body1">
        * If you believe a contact is missing from the list, please contact the individual directly and have them update
        their Eclipse.org account to indicate they are a contact of your company. <br /> Once done, their contact record
        will appear in the list.
      </Typography>

      <ModalWindow
        title={'Request to Remove ' + selectedContact.name}
        content={''}
        customContent={renderRemoveReasonOptions}
        handleProceed={handleRequestRemoval}
        shouldOpen={openRequestRemoval}
        setShouldOpen={setOpenRequestRemoval}
        cancelText={'Cancel'}
        yesText={'Submit'}
      />

      <ModalWindow
        title={'Edit ' + selectedContact.name + `'s Roles`}
        content={''}
        customContent={renderAvailableRoles}
        handleProceed={handleEditRoles}
        shouldOpen={openEditRoles}
        setShouldOpen={setOpenEditRoles}
        cancelText={'Cancel'}
        yesText={'Submit'}
      />

      <Popover
        id={openPopover ? 'simple-popover' : undefined}
        className={classes.customPopover}
        elevation={4}
        open={openPopover}
        anchorEl={anchorEle}
        onClose={() => setAnchorEle(null)}
        anchorOrigin={{
          vertical: -20,
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: -10,
        }}
      >
        <Typography variant="body1" className={classes.popoverText}>
          {roleDescription}
        </Typography>
      </Popover>
    </FullLayout>
  );
}
