/*!
 * Copyright (c) 2021, 2022, 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Zhou Fang <zhou.fang@eclipse-foundation.org>
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { Switch, Route, Redirect } from 'react-router-dom';
import { CircularProgress, Theme } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import {
  api_prefix,
  END_POINT,
  FETCH_HEADER,
  getCurrentMode,
  LOGIN_FROM_KEY,
  MODE_REACT_ONLY,
  ORIGINAL_PATH_KEY,
} from '../../Constants/Constants';
import Dashboard from './Dashboard/Dashboard';
import OrgProfile from './OrgProfile/OrgProfiles';
import ContactManagement from './ContactManagement/ContactManagement';
import { useEffect, useContext } from 'react';
import PortalLogin from './Login/PortalLogin';
import YourProjects from './YourProjects/YourProjects';
import PortalContext from '../../Context/PortalContext';
import { isProd } from '../../Utils/formFunctionHelpers';
import GlobalContext from '../../Context/GlobalContext';
import { PreviewModeProvider } from '../../Context/PreviewModeContext';
import ProtectedRoute from '../ProtectedRoute';
import { UnauthorizedPage } from '../../pages/portal/UnauthorizedPage';
import { AdminPage } from '../../pages/portal/AdminPage';
import StandbyPage from '../../pages/portal/StandbyPage';
import permissions from '../../Constants/permissions';
import { checkPermission } from '../../Utils/portalFunctionHelpers';
import { roles } from '../../Constants/roles';
import { getOrganization } from '../../api/membership';

const useStyles = makeStyles((_theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    loadingContainer: {
      backgroundColor: '#fff',
      position: 'fixed',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
  })
);

const originalPath = window.location.pathname !== '/portal/login' ? window.location.pathname : '/portal/dashboard';

export default function Portal({
  isFetchingUser,
  setIsFetchingUser,
}: {
  isFetchingUser: boolean;
  setIsFetchingUser: (param: boolean) => void;
}) {
  const classes = useStyles();
  const { gotCSRF } = useContext(GlobalContext);
  const { setOrgId, currentUserPortal, setCurrentUserPortal } = useContext(PortalContext);
  const loggedIn = currentUserPortal !== null && (checkPermission(currentUserPortal.roles, permissions.accessPortal) || checkPermission(currentUserPortal.relation, permissions.accessPortal));

  useEffect(() => {
    if (getCurrentMode() === MODE_REACT_ONLY) {
      setOrgId(0);
      setCurrentUserPortal({
        name: 'Anonymous',
        relation: [],
        roles: ['admin']
      });
      return;
    }

    const getUserInfo = async () => {
      try {
        const userInfoResponse = await fetch(api_prefix() + `/${END_POINT.userinfo}`, { headers: FETCH_HEADER });
        const userData = await userInfoResponse.json();

        if (!isProd) {
          console.log('user info: ', userData);
        }

        let orgRoles = userData.organizational_roles;
        const additionalRoles = userData.additional_roles;

        // For now, 1 user will only belong to 1 org, so just get the 1st/only id in it
        let orgId = parseInt(Object.keys(orgRoles)?.[0]) || 0;

        // Verify that the user, if in an organization, is part of a member
        // organization.
        if (orgId !== 0) {
          const [,error] = await getOrganization(orgId);

          // If user is not part of a member organization, set orgId to 0. Treat
          // them as organizationless. This will make them redirect to the
          // "/login" page with the error that they are not part of a member
          // organization.
          if (error) {
            orgId = 0;
            orgRoles = {};
          }
        }
        setCurrentUserPortal({
          ...userData,
          name: userData.name,
          givenName: userData.given_name,
          familyName: userData.family_name,
          email: userData.email,
          relation: orgId ? orgRoles[orgId] : [],
          roles: additionalRoles,
        });
        setOrgId(orgId);
      } finally {
        setIsFetchingUser(false);
      }
    };

    getUserInfo();

  }, [setCurrentUserPortal, setIsFetchingUser, setOrgId]);

  useEffect(() => {
    !loggedIn
      ? sessionStorage.setItem(ORIGINAL_PATH_KEY, originalPath)
      : sessionStorage.setItem(ORIGINAL_PATH_KEY, '/portal');
  }, [loggedIn]);

  useEffect(() => {
    getCurrentMode() === MODE_REACT_ONLY && setIsFetchingUser(false);
  }, [setIsFetchingUser]);

  useEffect(() => {
    localStorage.setItem(LOGIN_FROM_KEY, 'Form');
  }, []);

  return (
    <PreviewModeProvider>
      {gotCSRF && !isFetchingUser ? (
        <>
          <Switch>
            <Route path="/portal/login">
              {loggedIn && <Redirect to={sessionStorage.getItem(ORIGINAL_PATH_KEY) as string} />}
              <PortalLogin isFetchingUser={isFetchingUser} setIsFetchingUser={setIsFetchingUser} />
            </Route>
            <ProtectedRoute
              path="/portal/standby"
              component={StandbyPage}
              allowedRoles={permissions.accessStandby}
              strict
            />
            <ProtectedRoute
              path="/portal/dashboard"
              component={ Dashboard }
              allowedRoles={permissions.accessDashboard}
            />
            <ProtectedRoute
              exact
              path="/portal/org-profile" 
              component={OrgProfile}
              allowedRoles={permissions.accessOrgProfile}
            />
            <ProtectedRoute
              exact
              path="/portal/contact-management"
              component={ContactManagement}
              allowedRoles={ permissions.accessContacts }
            />
            <ProtectedRoute
              exact
              path="/portal/your-projects"
              component={YourProjects}
              allowedRoles={permissions.accessDashboard}
            />
            <ProtectedRoute
              exact
              path="/portal/admin"
              component={AdminPage}
              allowedRoles={permissions.accessAdmin}
              strict
            />
            <Route 
              exact
              path="/portal/unauthorized"
              component={UnauthorizedPage}
            />
            <Route path="/portal">
              {/* Redirect elevated and admin users to standby page, otherwise redirect to dashboard */}
              { currentUserPortal?.roles.includes(roles.elevated) || currentUserPortal?.roles.includes(roles.admin) 
                ? <Redirect to="/portal/standby" />
                : <Redirect to="/portal/dashboard" />
              }   
            </Route>
          </Switch>
        </>
      ) : (
        <div className={classes.loadingContainer}>
          <CircularProgress />
        </div>
      )}
    </PreviewModeProvider>
  );
}
