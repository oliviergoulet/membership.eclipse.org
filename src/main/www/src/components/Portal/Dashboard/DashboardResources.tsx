/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import { brightOrange, END_POINT } from '../../../Constants/Constants';
import DescriptionIcon from '@material-ui/icons/Description';
import CustomCard from '../../UIComponents/CustomCard/CustomCard';
import { useEffect, useState, useMemo, useContext } from 'react';
import SectionCtn from '../../UIComponents/CustomContainer/SectionCtn';
import { fetchWrapperPagination } from '../../../Utils/formFunctionHelpers';
import ModalWindow from '../../UIComponents/Notifications/ModalWindow';
import { ListItem } from '@material-ui/core';
import { ResourcesData, ResourcesListItem } from '../../../Interfaces/portal_interface';
import PortalContext from '../../../Context/PortalContext';
import { renderItemList } from '../../../Utils/portalFunctionHelpers';

interface ResourcesDataBackend {
  id: string;
  title: string;
  date: string;
  direct_link: string;
  resource_type: string;
}

export default function DashboardResources() {
  // Using useMemo here to make sure this will only be created once so that the useEffect will not run into an infinite loop,
  // because the initialValue has to be a dependency in it.
  const initialValue = useMemo(
    () => [
      {
        subtitle: 'Survey Report',
        listItems: null,
      },
      {
        subtitle: 'White Paper',
        listItems: null,
      },
      {
        subtitle: 'Market Report',
        listItems: null,
      },
      {
        subtitle: 'Case Study',
        listItems: null,
      },
    ],
    []
  );
  const { resourcesData, setResourcesData, orgId } = useContext(PortalContext);
  const [isFetching, setIsFetching] = useState(true);
  const [open, setOpen] = useState(false);
  const [selectedResourceIndex, setSelectedResourceIndex] = useState(0);

  const renderAllItems = (itemArray: Array<ResourcesListItem>) =>
    itemArray?.map((item) => (
      <ListItem
        key={item.name}
        button
        component="a"
        href={item.url}
        target="_blank"
        rel="noopener"
        style={{ padding: '10px 15px' }}
      >
        {item.name}
      </ListItem>
    ));

  useEffect(() => {
    if (!orgId) {
      // orgId is not needed for resources API call, but add it here to avoid the API call before user log in.
      return;
    }

    if (resourcesData !== null) {
      setIsFetching(false);
      return;
    }

    const formatAndSaveResources = (data: Array<ResourcesDataBackend>) => {
      let newData: Array<ResourcesData> = [...initialValue],
        index = 0;
      newData.forEach((item) => (item.listItems = []));
      data.forEach((item) => {
        switch (item.resource_type) {
          case 'survey_report':
            index = 0;
            break;
          case 'white_paper':
            index = 1;
            break;
          case 'market_report':
            index = 2;
            break;
          case 'case_study':
            index = 3;
            break;
          default:
            break;
        }
        newData[index]?.listItems?.push({
          name: item.title,
          url: item.direct_link,
        });
      });

      setResourcesData(newData);
      setIsFetching(false);
    };

    const errHandler = () => {
      setIsFetching(false);
    };

    fetchWrapperPagination(END_POINT.resources, 1, formatAndSaveResources, errHandler);
  }, [initialValue, resourcesData, orgId, setResourcesData]);

  const renderResourcesItems = () => {
    const theData = resourcesData ? resourcesData : initialValue;

    return theData.map((item, index) => {
      const theList = item.listItems ? item.listItems.slice(0, 3) : null;
      return (
        <CustomCard
          isFetching={isFetching}
          key={item.subtitle + index}
          subtitle={item.subtitle}
          color={brightOrange}
          icon={<DescriptionIcon />}
          listItems={renderItemList(theList, isFetching, `No ${item.subtitle.toLocaleLowerCase()} yet`, '')}
          urlText={theList && theList.length > 3 ? 'View all' : ''}
          callBackFunc={() => {
            setSelectedResourceIndex(index);
            setOpen(true);
          }}
        />
      );
    });
  };

  return (
    <SectionCtn title="Resources" id="resources">
      {renderResourcesItems()}

      <ModalWindow
        title={resourcesData?.[selectedResourceIndex]?.subtitle}
        content=""
        customContent={() => renderAllItems(resourcesData?.[selectedResourceIndex]?.listItems || [])}
        handleProceed={''}
        shouldOpen={open}
        setShouldOpen={setOpen}
        cancelText={'Close'}
        yesText={false}
      />
    </SectionCtn>
  );
}
