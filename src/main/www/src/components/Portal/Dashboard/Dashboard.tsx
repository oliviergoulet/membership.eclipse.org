/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import DashboardCommittersAndContributors from './DashboardCommittersAndContributors/DashboardCommittersAndContributors';
import DashboardFAQs from './DashboardFAQs';
import DashboardIntro from './DashboardIntro';
import DashboardOverview from './DashboardOverview';
import DashboardProjectsAndWG from './DashboardProjectsAndWG/DashboardProjectsAndWG';
import DashboardResources from './DashboardResources';
import { useContext, useEffect, useState } from 'react';
import { fetchWrapper } from '../../../Utils/formFunctionHelpers';
import {
  api_prefix,
  errMsgForGetRequest,
  FETCH_METHOD,
  getCurrentMode,
  MEMBER_LEVELS,
  MODE_REACT_ONLY,
} from '../../../Constants/Constants';
import { OrgInfoBackend } from '../../../Interfaces/portal_interface';
import PortalContext from '../../../Context/PortalContext';
import { CircularProgress, createStyles, makeStyles, Theme, Typography } from '@material-ui/core';
import FullLayout from '../../../layouts/FullLayout';
import useHashScrolling from '../../../hooks/useHashScrolling';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    pageTitle: {
      margin: theme.spacing(0.5, 0, 4),
    },
  })
);

export default function Dashboard() {
  const classes = useStyles();
  const { orgId, orgInfo, setOrgInfo } = useContext(PortalContext);
  const [isFetchingOrg, setIsFetchingOrg] = useState(true);
  useHashScrolling();

  const renderMemberLevel = () => {
    if (orgInfo?.levels.find((item) => item.level.toUpperCase() === MEMBER_LEVELS.strategicMember.code)) {
      return MEMBER_LEVELS.strategicMember.description;
    }

    if (orgInfo?.levels.find((item) => MEMBER_LEVELS.contributingMember.code.includes(item.level.toUpperCase()))) {
      return MEMBER_LEVELS.contributingMember.description;
    }

    if (orgInfo?.levels.find((item) => item.level.toUpperCase() === MEMBER_LEVELS.associateMember.code)) {
      return MEMBER_LEVELS.associateMember.description;
    }
  };

  const renderOrgNameAndLevel = () => {
    if (!orgInfo && !isFetchingOrg) {
      return <Typography variant="body1">{errMsgForGetRequest}</Typography>;
    }

    if (!orgInfo && isFetchingOrg) {
      return <CircularProgress />;
    }
    return `${orgInfo?.name} - ${renderMemberLevel()}`;
  };

  useEffect(() => {
    if (orgId === 0 || orgInfo !== null) {
      return;
    }

    const urlForOrgInfo =
      getCurrentMode() === MODE_REACT_ONLY
        ? '/membership_data/test_org_info_data.json'
        : `${api_prefix()}/organizations/${orgId}`;

    const saveOrgInfo = (data: OrgInfoBackend) => {
      setOrgInfo(data);
      setIsFetchingOrg(false);
    };
    const errHandler = () => {
      setIsFetchingOrg(false);
    };
    fetchWrapper(urlForOrgInfo, FETCH_METHOD.GET, saveOrgInfo, '', errHandler);
  }, [orgId, orgInfo, setOrgInfo]);

  return (
    <FullLayout>
      <Typography className={classes.pageTitle} variant="h4" component="h1">
        {renderOrgNameAndLevel()}
      </Typography>

      <DashboardIntro />
      <DashboardOverview />
      <DashboardProjectsAndWG />
      <DashboardCommittersAndContributors />
      <DashboardResources />
      <DashboardFAQs />
    </FullLayout>
  );
}
