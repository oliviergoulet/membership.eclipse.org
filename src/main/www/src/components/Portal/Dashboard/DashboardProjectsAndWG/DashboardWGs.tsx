/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import {
  
  api_prefix,
  darkOrange,
  END_POINT,
  FETCH_METHOD,
  getCurrentMode,
  MODE_REACT_ONLY,
} from '../../../../Constants/Constants';
import CustomCard from '../../../UIComponents/CustomCard/CustomCard';
import { useEffect, useContext, useState } from 'react';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import { fetchWrapper } from '../../../../Utils/formFunctionHelpers';
import { pickRandomItems, renderItemList } from '../../../../Utils/portalFunctionHelpers';
import PortalContext from '../../../../Context/PortalContext';
import { DashboardProjectsAndWGProps } from '../../../../Interfaces/portal_interface';

const isReactOnlyMode = getCurrentMode() === MODE_REACT_ONLY;

export default function DashboardWGs({ setSelectedItemArray, setOpen }: DashboardProjectsAndWGProps) {
  const [isFetchingYourWGs, setIsFetchingYourWGs] = useState(true);
  const [isFetchingInterestedWGs, setIsFetchingInterestedWGs] = useState(true);
  const { orgId, yourWGData, setYourWGData, allYourWGData, setAllYourWGData, interestedWGData, setInterestedWGData } =
    useContext(PortalContext);

  // Reset the fetching state the WG data state changes. This state change
  // occurs when preview mode is toggled. It is necessary to reset this state
  // so we get loading spinners instead of error messages.
  useEffect(() => {
    setIsFetchingYourWGs(true);
    setIsFetchingInterestedWGs(true);
  }, [orgId, setIsFetchingYourWGs, setIsFetchingInterestedWGs]);

  useEffect(() => {
    // For your wgs
    if (yourWGData !== null) {
      setIsFetchingYourWGs(false);
      return;
    }

    const urlForAllYourWGs = isReactOnlyMode
      ? '/membership_data/test_your_wgs.json'
      : api_prefix() + `/${END_POINT.organizations}/${orgId}/${END_POINT.working_groups}`;

    const saveYourWGs = (data: Array<any>) => {
      const allYourWGs = data.map((item) => ({
        name: item.title || '',
        url: item.resources.website || '',
      }));
      setAllYourWGData(allYourWGs);
      setYourWGData(pickRandomItems(allYourWGs, 4));
      setIsFetchingYourWGs(false);
    };

    const errHandler = () => {
      setIsFetchingYourWGs(false);
      setIsFetchingInterestedWGs(false);
    };

    fetchWrapper(urlForAllYourWGs, FETCH_METHOD.GET, saveYourWGs, '', errHandler);
  }, [orgId, yourWGData, setAllYourWGData, setYourWGData]);

  useEffect(() => {
    // For interested wgs
    if (allYourWGData === null) {
      return;
    }
    if (interestedWGData !== null) {
      setIsFetchingInterestedWGs(false);
      return;
    }

    const urlForAllWGs = isReactOnlyMode
      ? '/membership_data/test_all_wgs.json'
      : api_prefix() + `/${END_POINT.working_groups}`;

    const saveInterestedWGsData = (data: Array<any>) => {
      const allTheWGs = data.map((wg: any) => ({
        name: wg.title || '',
        url: wg.resources?.website || '',
      }));

      const interestedWGs = allTheWGs.filter((wg) => !allYourWGData.find((item) => item.name === wg.name));
      const fourRandomItems = pickRandomItems(interestedWGs, 4);
      setInterestedWGData(fourRandomItems);
      setIsFetchingInterestedWGs(false);
    };

    fetchWrapper(urlForAllWGs, FETCH_METHOD.GET, saveInterestedWGsData, '', () => setIsFetchingInterestedWGs(false));
  }, [interestedWGData, allYourWGData, setInterestedWGData]);
  return (
    <>
      <CustomCard
        isFetching={isFetchingYourWGs}
        subtitle={'Your Working Groups'}
        color={darkOrange}
        icon={<BusinessCenterIcon />}
        listItems={renderItemList(yourWGData, isFetchingYourWGs, 'No Working Groups yet', {
          name: 'Learn more about Working Groups',
          url: 'https://www.eclipse.org/org/workinggroups/',
        })}
        urlText={allYourWGData && allYourWGData.length > 4 ? 'View all' : ''}
        callBackFunc={() => {
          setSelectedItemArray({ title: 'View All Your Working Groups', data: allYourWGData || [] });
          setOpen(true);
        }}
      />
      <CustomCard
        isFetching={isFetchingInterestedWGs}
        subtitle={'Working Groups You Might be Interested in'}
        color={darkOrange}
        icon={<BusinessCenterIcon />}
        listItems={renderItemList(
          interestedWGData,
          isFetchingInterestedWGs,
          'Not found Working Groups you might be interested in',
          ''
        )}
        urlText=""
        callBackFunc={() => {}}
      />
    </>
  );
}
