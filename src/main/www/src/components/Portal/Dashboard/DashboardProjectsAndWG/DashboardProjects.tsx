/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import {
  api_prefix,
  darkOrange,
  END_POINT,
  FETCH_METHOD,
  getCurrentMode,
  MODE_REACT_ONLY,
  NAV_PATHS,
} from '../../../../Constants/Constants';
import CustomCard from '../../../UIComponents/CustomCard/CustomCard';
import { useEffect, useContext, useState } from 'react';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import { fetchWrapper, fetchWrapperPagination } from '../../../../Utils/formFunctionHelpers';
import { pickRandomItems, renderItemList } from '../../../../Utils/portalFunctionHelpers';
import PortalContext from '../../../../Context/PortalContext';
import { AllProjectsBackend } from '../../../../Interfaces/portal_interface';
import { useHistory } from 'react-router';

const isReactOnlyMode = getCurrentMode() === MODE_REACT_ONLY;

export default function DashboardProjects() {
  const history = useHistory();
  const [isFetchingYourProjects, setIsFetchingYourProjects] = useState(true);
  const [isFetchingInterestedProjects, setIsFetchingInterestedProjects] = useState(true);
  const [isFetchingAllYourProjects, setIsFetchingAllYourProjects] = useState(true);
  const {
    orgId,
    yourProjectsData,
    setYourProjectsData,
    allYourProjectsData,
    setAllYourProjectsData,
    interestedProjectsData,
    setInterestedProjectsData,
  } = useContext(PortalContext);
  
  // Reset the fetching state the project data state changes. This state change
  // occurs when preview mode is toggled. It is necessary to reset this state
  // so we get loading spinners instead of error messages.
  useEffect(() => {
    setIsFetchingYourProjects(true);
    setIsFetchingAllYourProjects(true);
    setIsFetchingInterestedProjects(true);
  }, [orgId, setIsFetchingYourProjects, setIsFetchingAllYourProjects, setIsFetchingInterestedProjects]);

  useEffect(() => {
    // For all your projects data

    if (allYourProjectsData !== null || !orgId) {
      return;
    }

    const urlForAllYourProjects = isReactOnlyMode
      ? '/membership_data/test_your_projects.json'
      : api_prefix() + `/${END_POINT.organizations}/${orgId}/${END_POINT.projects}?page=`;

    const saveAllYourProjectsData = (data: Array<any>) => {
      const allYourProjects = data.map((project) => ({
        name: project.name,
        url: project.project_id ? `https://projects.eclipse.org/projects/${project.project_id}` : '',
        id: project.project_id,
        status: project.active ? 'Active' : 'Inactive',
      }));
      setAllYourProjectsData(allYourProjects);
    };

    if (isReactOnlyMode) {
      fetchWrapper(urlForAllYourProjects, FETCH_METHOD.GET, saveAllYourProjectsData);
    } else {
      fetchWrapperPagination(urlForAllYourProjects, 1, saveAllYourProjectsData, () =>
        setIsFetchingAllYourProjects(false)
      );
    }
  }, [orgId, allYourProjectsData, setAllYourProjectsData]);

  useEffect(() => {
    // For your active projects data
    if (yourProjectsData !== null) {
      setIsFetchingYourProjects(false);
      return;
    }

    if (allYourProjectsData === null) {
      if (isFetchingAllYourProjects) {
        // means the fetch call for allYourProjectsData is not finished
        return;
      } else {
        // means the fetch call for allYourProjectsData is failed
        setIsFetchingInterestedProjects(false);
        setIsFetchingYourProjects(false);
        return;
      }
    }

    const yourActiveProjects = allYourProjectsData.filter((project) => project.status === 'Active');
    setYourProjectsData(pickRandomItems(yourActiveProjects, 4));
    setIsFetchingYourProjects(false);
  }, [allYourProjectsData, yourProjectsData, setYourProjectsData, isFetchingAllYourProjects]);

  useEffect(() => {
    // For interested projects data

    if (allYourProjectsData === null) {
      return;
    }
    if (interestedProjectsData !== null) {
      setIsFetchingInterestedProjects(false);
      return;
    }

    // For interested projects data
    const saveInterestedProjectsData = (data: Array<AllProjectsBackend>) => {
      const allProjects = data.map((project) => ({
        name: project.name,
        url: project.url,
        state: project.state
      }));

      // Filter out projects that the user has already marked as interested and
      // also those that are not incubating nor active.
      const interestedProjects = allProjects
        .filter(project =>  project.state === 'Incubating' || project.state === 'Regular')
        .filter(project => !allYourProjectsData.find((item) => item.name === project.name))

      const fourRandomItems = pickRandomItems(interestedProjects, 4);
      setInterestedProjectsData(fourRandomItems);
      setIsFetchingInterestedProjects(false);
    };

    const urlForAllProjects = isReactOnlyMode
      ? '/membership_data/test_all_projects.json'
      : `https://projects.eclipse.org/api/projects?order_by=random&pagesize=${4 + allYourProjectsData.length}`;

    fetchWrapper(urlForAllProjects, FETCH_METHOD.GET, saveInterestedProjectsData, '', () =>
      setIsFetchingInterestedProjects(false)
    );
  }, [allYourProjectsData, interestedProjectsData, setInterestedProjectsData]);

  return (
    <>
      <CustomCard
        isFetching={isFetchingYourProjects}
        subtitle={'Your Projects'}
        color={darkOrange}
        icon={<BusinessCenterIcon />}
        listItems={renderItemList(yourProjectsData, isFetchingYourProjects, 'No projects yet', {
          name: 'Learn more about Projects',
          url: 'https://www.eclipse.org/projects/',
        })}
        urlText={allYourProjectsData && allYourProjectsData.length > 4 ? 'View all' : ''}
        callBackFunc={() => {
          history.push(NAV_PATHS.yourProjects);
        }}
      />
      <CustomCard
        isFetching={isFetchingInterestedProjects}
        subtitle={'Projects You May be Interested in'}
        color={darkOrange}
        icon={<BusinessCenterIcon />}
        listItems={renderItemList(
          interestedProjectsData,
          isFetchingInterestedProjects,
          'Not found projects you might be interested in',
          ''
        )}
        urlText=""
        callBackFunc={() => {}}
      />
    </>
  );
}
