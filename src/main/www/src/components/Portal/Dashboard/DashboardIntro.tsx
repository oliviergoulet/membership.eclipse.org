/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import {
  Avatar,
  Button,
  Card,
  CardMedia,
  CircularProgress,
  Container,
  createStyles,
  Divider,
  Link,
  List,
  ListItem,
  ListItemText,
  makeStyles,
  Theme,
  Typography,
} from '@material-ui/core';
import classNames from 'classnames';
import { useContext, useEffect, useState } from 'react';
import NoteIcon from '@material-ui/icons/Note';
import GroupIcon from '@material-ui/icons/Group';
import EmailIcon from '@material-ui/icons/Email';
import AssignmentIcon from '@material-ui/icons/Assignment';
import { OrganizationRoles, OrgInfoBackend, OrgRep } from '../../../Interfaces/portal_interface';
import PortalContext from '../../../Context/PortalContext';
import ModalWindow from '../../UIComponents/Notifications/ModalWindow';
import {
  api_prefix,
  errMsgForGetRequest,
  FETCH_METHOD,
  getCurrentMode,
  MODE_REACT_ONLY,
} from '../../../Constants/Constants';
import { fetchWrapper } from '../../../Utils/formFunctionHelpers';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    introCtn: {
      padding: 0,
      maxWidth: '100%',
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-between',
    },
    card: {
      minHeight: 250,
      boxShadow: '1px 1px 15px rgba(0,0,0,0.1)',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      minWidth: 255,
      width: '100%',
      margin: theme.spacing(2, 0),
      [theme.breakpoints.up(650)]: {
        width: '45%',
        margin: theme.spacing(2.5, 0.5, 0, 0.5),
      },
      [theme.breakpoints.up(1280)]: {
        width: '30%',
        margin: theme.spacing(2.5, 2, 0, 0),
      },
    },
    companyLogoCard: {
      backgroundColor: '#fff',
    },
    companyLogo: {
      width: '90%',
      maxWidth: 250,
      margin: theme.spacing(0, 'auto'),
    },
    urlNotClickable: {
      pointerEvents: 'none',
    },
    companyRepCard: {
      position: 'relative',
      backgroundColor: '#D0D0D0',
      padding: theme.spacing(2, 0, 3.5),
    },
    repPrimary: {
      fontSize: 18,
      textAlign: 'center',
    },
    repSecondary: {
      fontSize: 16,
      textAlign: 'center',
    },
    companyContentCard: {
      flexDirection: 'column',
      justifyContent: 'start',
      alignItems: 'start',
      padding: theme.spacing(2, 3),
      backgroundColor: '#fff',
    },
    contentTitle: {
      marginBottom: theme.spacing(0.5),
    },
    divider: {
      width: '100%',
      margin: 0,
    },
    contentList: {
      width: '100%',
    },
    contentItemCtn: {
      padding: theme.spacing(1.5, 0),
    },
    contentAvatar: {
      width: 35,
      height: 35,
      margin: theme.spacing(0, 2),
    },
    contentItemText: {
      fontSize: 18,
      display: 'flex',
      padding: 0,
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    contentItemTextSub: {
      color: 'hsl(0, 0%, 75%)',
    },
    viewAllBtnCtn: {
      position: 'absolute',
      bottom: 15,
      left: 15,
      right: 15,
      width: '100%',
      display: 'flex',
      justifyContent: 'end',
      alignItems: 'end',
    },
    viewAllBtn: {
      padding: theme.spacing(0.5, 1.5),
    },
  })
);

export default function DashboardIntro() {
  const classes = useStyles();
  const { orgId, orgInfo, setOrgInfo, orgRepData, setOrgRepData, isFetchingOrgInfo, setIsFetchingOrgInfo } =
    useContext(PortalContext);
  const [orgIntro, setOrgIntro] = useState({ imageURL: '', name: '', website: '' });
  const [isFetchingOrgRep, setIsFetchingOrgRep] = useState(true);
  const [open, setOpen] = useState(false);

  const renderOrgRep = orgRepData?.map((rep, index) => (
    <ListItem key={index}>
      <ListItemText
        classes={{
          primary: classes.repPrimary,
          secondary: classes.repSecondary,
        }}
        primary={rep.name}
        secondary={rep.type}
      />
    </ListItem>
  ));

  const renderOrgLogo = () => {
    if (isFetchingOrgInfo) {
      return <CircularProgress />;
    }

    if (!isFetchingOrgInfo && !orgInfo) {
      // Finished fetching but still no orgInfo means error
      return <Typography variant="body1">{errMsgForGetRequest}</Typography>;
    }

    let isWebsiteValid = false;
    try {
      // This will fail if the URL is not valid
      new URL(orgIntro.website);
      isWebsiteValid = true;
    } catch (error) {
      console.log(error);
    }

    return (
      // Org logo will not be clickable if the website URL does NOT exist or is NOT valid
      <Link
        href={orgIntro.website}
        target="_blank"
        rel="noreferrer"
        className={isWebsiteValid ? '' : classes.urlNotClickable}
      >
        {orgIntro.imageURL ? (
          <CardMedia component="img" className={classes.companyLogo} image={orgIntro.imageURL} alt={orgIntro.name} />
        ) : (
          <Typography variant="body1" component="p" className={classes.contentItemText}>
            Logo is not set
          </Typography>
        )}
      </Link>
    );
  };

  useEffect(() => {
    if (orgId === 0 || orgInfo !== null) {
      orgInfo && setOrgIntro({ imageURL: orgInfo.logos.web || '', name: orgInfo.name, website: orgInfo.website || '' });
      setIsFetchingOrgInfo(false);
      return;
    }

    const urlForOrgInfo =
      getCurrentMode() === MODE_REACT_ONLY
        ? '/membership_data/test_org_info_data.json'
        : `${api_prefix()}/organizations/${orgId}`;

    const saveOrgInfo = (data: OrgInfoBackend) => {
      setOrgInfo(data);
      setOrgIntro({ imageURL: data.logos.web || '', name: data.name, website: data.website || '' });
      setIsFetchingOrgInfo(false);
    };

    const errHandler = () => {
      setIsFetchingOrgInfo(false);
    };
    fetchWrapper(urlForOrgInfo, FETCH_METHOD.GET, saveOrgInfo, '', errHandler);
  }, [orgId, orgInfo, setOrgInfo, setIsFetchingOrgInfo]);

  useEffect(() => {
    if (orgId === 0) {
      setIsFetchingOrgRep(false);
      return;
    }

    const urlForOrgInfo =
      getCurrentMode() === MODE_REACT_ONLY
        ? '/membership_data/test_org_rep.json'
        : `${api_prefix()}/organizations/${orgId}/representatives`;

    const saveOrgRep = (data: any) => {
      const repCR: Array<OrgRep> = [];
      const repCRA: Array<OrgRep> = [];
      const repMA: Array<OrgRep> = [];

      const updateOrgRepData = (contact: any, relation: 'CR' | 'CRA' | 'MA', repArray: Array<OrgRep>) => {
        contact.relations.includes(relation) &&
          repArray.push({ name: `${contact.first_name} ${contact.last_name}`, type: OrganizationRoles[relation] });
      };

      data.forEach((item: any) => {
        updateOrgRepData(item, 'CR', repCR);
        updateOrgRepData(item, 'CRA', repCRA);
        updateOrgRepData(item, 'MA', repMA);
      });

      setOrgRepData([...repCR, ...repCRA, ...repMA]);
      setIsFetchingOrgRep(false);
    };

    const errHandler = () => {
      setOrgRepData([{ name: errMsgForGetRequest, type: '' }]);
      setIsFetchingOrgRep(false);
    };

    fetch(urlForOrgInfo, { cache: 'no-cache' })
      .then((data) => data.json())
      .then(saveOrgRep)
      .catch(errHandler);

  }, [orgId, setOrgRepData]);

  return (
    <Container className={classes.introCtn}>
      <Card className={classNames(classes.card, classes.companyLogoCard)}>{renderOrgLogo()}</Card>

      <Card className={classNames(classes.card, classes.companyRepCard)}>
        <List>{isFetchingOrgRep ? <CircularProgress /> : renderOrgRep?.slice(0, 3)}</List>
        {orgRepData && orgRepData?.length > 3 && (
          <Container className={classes.viewAllBtnCtn}>
            <Button className={classes.viewAllBtn} onClick={() => setOpen(true)}>
              View All
            </Button>
          </Container>
        )}
      </Card>

      <Card className={classNames(classes.card, classes.companyContentCard)}>
        <List className={classes.contentList}>
          <ListItem
            button
            component="a"
            href="https://www.eclipse.org/community/newsletter/"
            target="_blank"
            rel="noopener"
            className={classes.contentItemCtn}
          >
            <Avatar className={classes.contentAvatar}>
              <NoteIcon />
            </Avatar>
            <Typography variant="body1" component="p" className={classes.contentItemText}>
              Member Newsletter
            </Typography>
          </ListItem>

          <Divider className={classes.divider} />

          <ListItem
            button
            component="a"
            href="https://www.eclipse.org/membership/exploreMembership.php"
            target="_blank"
            rel="noopener"
            className={classes.contentItemCtn}
          >
            <Avatar className={classes.contentAvatar}>
              <GroupIcon />
            </Avatar>
            <Container className={classes.contentItemText}>Explore our Members</Container>
          </ListItem>

          <Divider className={classes.divider} />

          <ListItem
            button
            component="a"
            href="https://www.eclipse.org/org/foundation/contact.php"
            target="_blank"
            rel="noopener"
            className={classes.contentItemCtn}
          >
            <Avatar className={classes.contentAvatar}>
              <EmailIcon />
            </Avatar>
            <Container className={classes.contentItemText}>Contact Us</Container>
          </ListItem>

          <Divider className={classes.divider} />

          <ListItem
            button
            component="a"
            href="https://www.eclipse.org/org/documents/"
            target="_blank"
            rel="noopener"
            className={classes.contentItemCtn}
          >
            <Avatar className={classes.contentAvatar}>
              <AssignmentIcon />
            </Avatar>
            <Container className={classes.contentItemText}>Governance Documents</Container>
          </ListItem>
        </List>
      </Card>

      <ModalWindow
        title="Key Representatives"
        content=""
        customContent={() => renderOrgRep}
        handleProceed={''}
        shouldOpen={open}
        setShouldOpen={setOpen}
        cancelText={'Close'}
        yesText={false}
      />
    </Container>
  );
}
