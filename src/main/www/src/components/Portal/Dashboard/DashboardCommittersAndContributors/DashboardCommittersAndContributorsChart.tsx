/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import { CircularProgress, createStyles, makeStyles, Typography } from '@material-ui/core';
import {
  api_prefix,
  brightBlue,
  CONFIG_FOR_BAR_LINE_CHART,
  END_POINT,
  errMsgForGetRequest,
  FETCH_METHOD,
  getCurrentMode,
  MODE_REACT_ONLY,
} from '../../../../Constants/Constants';
import { Bar } from 'react-chartjs-2';
import { memo, useEffect, useContext, useState } from 'react';
import PortalContext from '../../../../Context/PortalContext';
import { fetchWrapper } from '../../../../Utils/formFunctionHelpers';
import { generateMonthArray } from '../../../../Utils/portalFunctionHelpers';

const isReactOnlyMode = getCurrentMode() === MODE_REACT_ONLY;

const useStyles = makeStyles(() =>
  createStyles({
    fullWidthChartCtn: {
      width: '100%',
      height: '100%',
    },
    loadingCtn: {
      width: '100%',
      height: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  })
);

function DashboardCommittersAndContributorsChart() {
  const classes = useStyles();
  const { orgId, chartCommits, setChartCommits, chartCommitters, setChartCommitters, chartMonths, setChartMonths } =
    useContext(PortalContext);
  const [isFetchingCommitters, setIsFetchingCommitters] = useState(true);
  const [isFetchingcommitts, setIsFetchingcommitts] = useState(true);

  const DATA_FOR_BAR_LINE_CHART = {
    labels: chartMonths,
    datasets: [
      {
        type: 'line',
        label: 'Committers',
        borderColor: brightBlue,
        borderWidth: 2,
        fill: false,
        data: chartCommitters || [],
      },
      {
        type: 'bar',
        label: 'Commits',
        backgroundColor: '#F0F2F8',
        data: chartCommits || [],
      },
    ],
  };

  useEffect(() => {
    if (!orgId || chartCommits !== null) {
      setIsFetchingcommitts(false);
      return;
    }

    // For chart - commits data
    const urlForChartCommits = isReactOnlyMode
      ? '/membership_data/test_chart_commits.json'
      : api_prefix() + `/${END_POINT.organizations}/${orgId}/${END_POINT.activity_yearly}`;

    const saveChartCommitsData = (data: { activity: Array<any> }) => {
      // Make sure the latest date will appear at the end/right of the chart
      const sortedData = data.activity.sort((currentItem, nextItem) =>
        currentItem.period >= nextItem.period ? 1 : -1
      );
      const dataCommitsArray = sortedData.map((item) => item.count);

      setChartCommits(dataCommitsArray);
      setChartMonths(generateMonthArray(sortedData));
      setIsFetchingcommitts(false);
    };
    fetchWrapper(urlForChartCommits, FETCH_METHOD.GET, saveChartCommitsData, '', () => setIsFetchingcommitts(false));
  }, [orgId, chartCommits, setChartCommits, setChartMonths]);

  useEffect(() => {
    if (!orgId || chartCommitters !== null) {
      setIsFetchingCommitters(false);
      return;
    }

    // For chart - committers data
    const urlForChartCommitters = isReactOnlyMode
      ? '/membership_data/test_chart_committers.json'
      : api_prefix() + `/${END_POINT.organizations}/${orgId}/${END_POINT.committers_yearly}`;

    const saveChartCommittersData = (data: { activity: Array<any> }) => {
      // Make sure the latest date will appear at the end/right of the chart
      const sortedData = data.activity.sort((currentItem, nextItem) =>
        currentItem.period >= nextItem.period ? 1 : -1
      );
      const dataCommittersArray = sortedData.map((item) => item.count);

      setChartMonths(generateMonthArray(sortedData));
      setChartCommitters(dataCommittersArray);
      setIsFetchingCommitters(false);
    };

    fetchWrapper(urlForChartCommitters, FETCH_METHOD.GET, saveChartCommittersData, '', () =>
      setIsFetchingCommitters(false)
    );
  }, [orgId, chartCommitters, setChartMonths, setChartCommitters]);

  return (
    <div className={classes.fullWidthChartCtn}>
      {isFetchingCommitters || isFetchingcommitts ? (
        <div className={classes.loadingCtn}>
          <CircularProgress />
        </div>
      ) : (
        <>
          <Bar data={DATA_FOR_BAR_LINE_CHART} options={CONFIG_FOR_BAR_LINE_CHART} />
          {(!chartCommitters || !chartCommits) && <br />}
          {!chartCommitters && <Typography variant="body1">Committers: {errMsgForGetRequest}</Typography>}
          {!chartCommits && <Typography variant="body1">Committs: {errMsgForGetRequest}</Typography>}
        </>
      )}
    </div>
  );
}

// Using memo here to prevent the chart from re-rendering when a non-related state changes
export default memo(DashboardCommittersAndContributorsChart);
