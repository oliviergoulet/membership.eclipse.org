/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import {
  CircularProgress,
  Container,
  createStyles,
  Grid,
  makeStyles,
  Theme,
  Typography,
  Popover,
  Link,
} from '@material-ui/core';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import {
  brightBlue,
  brightOrange,
  errMsgForGetRequest,
  FETCH_METHOD,
  getCurrentMode,
  iconGray,
  MODE_REACT_ONLY,
} from '../../../../Constants/Constants';
import SectionCtn from '../../../UIComponents/CustomContainer/SectionCtn';
import { useContext, useEffect, useState } from 'react';
import { fetchWrapper } from '../../../../Utils/formFunctionHelpers';
import { isIE } from '../../../../Utils/portalFunctionHelpers';
import PortalContext from '../../../../Context/PortalContext';
import HelpIcon from '@material-ui/icons/Help';
import DashboardCommittersAndContributorsChart from './DashboardCommittersAndContributorsChart';
import permissions from '../../../../Constants/permissions';
import Protected from '../../../Protected';
import CommitterCard from '../../../../modules/CommitterCard';
import ContributorCard from '../../../../modules/ContributorCard';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    gridCard: {
      '& > div': {
        paddingTop: theme.spacing(0.1),
        margin: 0,
        width: '100%',
        height: '100%',
      },
    },
    cbiCard: {
      position: 'relative',
      marginTop: theme.spacing(4),
      padding: theme.spacing(0, 1.5),
      display: 'flex',
      alignItems: 'center',
      minWidth: 310,
      minHeight: 100,
      backgroundColor: brightOrange,
      boxShadow: '1px 1px 15px rgba(0,0,0,0.1)',
      borderRadius: 4,
    },
    cbiIconCtn: {
      position: 'absolute',
      borderBottom: `4px solid ${brightBlue}`,
      top: theme.spacing(-2.5),
      left: theme.spacing(1.5),
      width: 54,
      height: 54,
      padding: 0,
      '& svg': {
        color: iconGray,
        width: '100%',
        height: '100%',
        paddingBottom: theme.spacing(0.5),
      },
    },
    cbiCardContent: {
      margin: theme.spacing(3, 0, 1, 0),
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: 0,
      width: '100%',
    },
    cbiCardSubtitle: {
      fontSize: 16,
    },
    cardCBIContent: {
      display: 'flex',
      flexDirection: 'column',
      padding: theme.spacing(0.5, 0, 0.5, 0.5),
    },
    cbiItem: {
      display: 'flex',
      alignItems: 'center',
    },
    bigWhiteNumber: {
      color: 'white',
      width: 38,
      marginRight: theme.spacing(1),
      textAlign: 'center',
    },
    boldBodyText: {
      fontWeight: 600,
    },
    helpButton: {
      position: 'absolute',
      right: 10,
      top: 5,
      color: 'white',
      fontSize: 25,
      cursor: 'pointer',
    },
    helpIcon: {
      fontSize: 25,
    },
    popoverText: {
      padding: theme.spacing(1.5),
    },
    customPopover: {
      '& .MuiPopover-paper': {
        boxShadow: '1px 1px 15px rgba(0,0,0,0.15)',
        width: '80vw',
        maxWidth: 600,
      },
    },
    fullWidthChartCtn: {
      width: '100%',
      height: '100%',
    },
    loadingCtn: {
      width: '100%',
      height: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  })
);

const isReactOnlyMode = getCurrentMode() === MODE_REACT_ONLY;

export default function DashboardCommittersAndContributors() {
  const classes = useStyles();

  const [isFetchingCBI, setIsFetchingCBI] = useState(true);
  const [anchorEle, setAnchorEle] = useState<HTMLElement | null>(null);
  const {
    orgId,
    cbiData,
    setCBIData,
  } = useContext(PortalContext);
  const open = Boolean(anchorEle);


  useEffect(() => {
    if (!orgId || cbiData !== null) {
      cbiData !== null && setIsFetchingCBI(false);
      return;
    }

    // For CBI data
    const urlForCBIData = isReactOnlyMode
      ? '/membership_data/cbi_data.json'
      : 'https://api.eclipse.org/cbi/sponsorships';

    const saveCBIData = (data: { memberOrganizationsBenefits: Array<any>; sponsoredProjects: Array<any> }) => {
      const allocated = data.memberOrganizationsBenefits.find((item) => item.id === orgId)?.resourcePacks || 0;
      let inUse = 0;
      data.sponsoredProjects.forEach((item) => {
        item.sponsoringOrganizations.forEach((org: any) => {
          org.id === orgId && inUse++;
        });
      });
      setCBIData({ inUse, allocated });
      setIsFetchingCBI(false);
    };

    fetchWrapper(urlForCBIData, FETCH_METHOD.GET, saveCBIData, '', () => setIsFetchingCBI(false));
  }, [orgId, cbiData, setCBIData]);

  return (
    <SectionCtn title="Committers and Contributors" id="committers-contributors">
      <Grid container spacing={4}>
        <Grid item container xs>
                    
          {/* Committer and contributor cards */}
          <Protected allowedRoles={[...permissions.viewCommitters, ...permissions.viewContributors]}>
            <Grid item container spacing={4}>
              <Grid item xs className={classes.gridCard}>
                <CommitterCard />
              </Grid>
              <Grid item xs className={classes.gridCard}>
                <ContributorCard />
              </Grid>
            </Grid>
          </Protected>

          {/* CBI resource packs */}
          <Grid item xs>
            <Container className={classes.cbiCard}>
              <Container className={classes.cbiIconCtn}>
                <PeopleAltIcon />
              </Container>
              <Container className={classes.cbiCardContent}>
                <Typography variant="h6" component="h3" className={classes.cbiCardSubtitle}>
                  Common Build Infrastructure
                </Typography>
                <Container className={classes.cardCBIContent}>
                  {isFetchingCBI ? (
                    <div className={classes.loadingCtn}>
                      <CircularProgress color="secondary" />
                    </div>
                  ) : cbiData ? (
                    <>
                      <div className={classes.cbiItem}>
                        <Typography variant="h4" className={classes.bigWhiteNumber}>
                          {cbiData?.inUse}
                        </Typography>
                        <Typography variant="body1" className={classes.boldBodyText}>
                          resource packs in use
                        </Typography>
                      </div>
                      <div className={classes.cbiItem}>
                        <Typography variant="h4" className={classes.bigWhiteNumber}>
                          {cbiData?.allocated}
                        </Typography>
                        <Typography variant="body1" className={classes.boldBodyText}>
                          resource packs allocated
                        </Typography>
                      </div>
                    </>
                  ) : (
                    // If the isFetching is false and CBI data is still null, that means GET request fails, show err msg.
                    <Typography variant="body1" className={classes.boldBodyText}>
                      {errMsgForGetRequest}
                    </Typography>
                  )}
                  <div className={classes.helpButton} onClick={(ev) => setAnchorEle(ev.currentTarget)}>
                    <HelpIcon className={classes.helpIcon} />
                  </div>
                  <Popover
                    id={open ? 'simple-popover' : undefined}
                    className={classes.customPopover}
                    elevation={4}
                    open={open}
                    anchorEl={anchorEle}
                    onClose={() => setAnchorEle(null)}
                    anchorOrigin={{
                      vertical: -65,
                      horizontal: 'center',
                    }}
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'center',
                    }}
                  >
                    <Typography variant="body1" className={classes.popoverText}>
                      Eclipse Foundation offers a Common Build environment for all its Open Source projects. Member
                      Organizations have access to additional{' '}
                      <Link
                        href="https://wiki.eclipse.org/CBI#Resource_Packs_Included_in_Membership"
                        target="_blank"
                        rel="noopener"
                      >
                        Resource Packs based on their membership level
                      </Link>
                      . For more information about build resources, please refer to{' '}
                      <Link href="https://wiki.eclipse.org/CBI" target="_blank" rel="noopener">
                        CBI Wiki
                      </Link>
                      .
                    </Typography>
                  </Popover>
                </Container>
              </Container>
            </Container>
          </Grid>
        </Grid>
        {/* Committer and contributor chart */}
        <Grid item xs lg={6}>
          {!isIE() && <DashboardCommittersAndContributorsChart />}
        </Grid>
      </Grid>
    </SectionCtn>
  );
}
