/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import { createStyles, makeStyles, Typography, Button, Grid, Link, Theme, CircularProgress } from '@material-ui/core';
import Input from '../../UIComponents/Inputs/Input';
import { useContext, useEffect, useState, useCallback } from 'react';
import { useFormik } from 'formik';
import { INITIAL_ORG_NEW_LINK } from '../../UIComponents/FormComponents/formFieldModel';
import { VALIDATION_SCHEMA_FOR_NEW_LINK } from '../../UIComponents/FormComponents/ValidationSchema';
import DeleteIcon from '@material-ui/icons/Delete';
import {
  api_prefix,
  errMsgForGetRequest,
  errorRed,
  FETCH_METHOD,
  getCurrentMode,
  iconGray,
  MODE_REACT_ONLY,
} from '../../../Constants/Constants';
import { fetchWrapper } from '../../../Utils/formFunctionHelpers';
import PortalContext from '../../../Context/PortalContext';
import ModalWindow from '../../UIComponents/Notifications/ModalWindow';
import GlobalContext from '../../../Context/GlobalContext';
import { usePreviewMode } from '../../../Context/PreviewModeContext';
import { CurrentLink } from '../../../Interfaces/portal_interface';
import { showErrMsg } from '../../../Utils/portalFunctionHelpers';

const isReactOnlyMode = getCurrentMode() === MODE_REACT_ONLY;

const useStyle = makeStyles((theme: Theme) =>
  createStyles({
    mainSectionHeader: {
      fontWeight: 600,
      margin: theme.spacing(4, 0, 1.5, 0),
    },
    currentLinksCard: {
      display: 'flex',
      justifyContent: 'space-between',
      marginTop: theme.spacing(2),
      padding: theme.spacing(1.5),
      borderRadius: 5,
      boxShadow: '1px 1px 15px rgba(0,0,0,0.1)',
    },
    currentLinksTitle: {
      fontWeight: 500,
      marginBottom: theme.spacing(0.5),
    },
    linksDescription: {
      marginBottom: theme.spacing(1.5),
    },
    deleteIconBtn: {
      color: iconGray,
      width: 60,
      height: 60,
      borderRadius: '30px',
      transition: 'all 0.25s ease-out',
      '&:hover': {
        color: errorRed,
        transition: 'all 0.25s ease-out',
      },
      '& svg': {
        fontSize: 30,
      },
    },
    btns: {
      marginRight: theme.spacing(2),
      width: '100%',
      minWidth: 90,
    },
    loadingCtn: {
      width: '100%',
      height: '230px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  })
);

interface CurrentLinkBackEnd {
  organization_id: number;
  product_id: number;
  name: string;
  description: string;
  product_url: string;
}

export default function OrgProfilesLinks() {
  const classes = useStyle();
  const { orgId, currentLinks, setCurrentLinks } = useContext(PortalContext);
  const { succeededToExecute, failedToExecute } = useContext(GlobalContext);
  const { inPreviewMode } = usePreviewMode();
  const [selectedLink, setSelectedLink] = useState<CurrentLink>();
  const [isFetchingLinks, setIsFetchingLinks] = useState(true);
  const [open, setOpen] = useState(false);

  const handleAddNewLink = () => {
    if (currentLinks === null) {
      return;
    }

    const linkValue = formikNewLinks.values;
    if (currentLinks.find((item) => item.url === linkValue.url)) {
      failedToExecute('The URL already exists');
      return;
    }

    const urlForLinks = isReactOnlyMode
      ? '/membership_data/test_org_links.json'
      : `${api_prefix()}/organizations/${orgId}/products`;

    const linkValueBackend = {
      organization_id: orgId,
      name: linkValue.title,
      description: linkValue.description,
      product_url: linkValue.url,
    };

    const saveNewLink = (data: CurrentLinkBackEnd) => {
      // @todo: Remove preview mode check once this form uses FormContainer.
      // Prevent form from being submitted when in preview mode.
      if (inPreviewMode) {
        failedToExecute('Cannot submit form while in preview mode.', 5000);
        return;
      }

      setCurrentLinks([...currentLinks, { ...linkValue, id: data.product_id }]);
      succeededToExecute('');
      formikNewLinks.resetForm();
      fetchWrapper(urlForLinks, FETCH_METHOD.GET, saveLinks);
    };

    if (!isReactOnlyMode) {
      fetchWrapper(
        urlForLinks,
        FETCH_METHOD.POST,
        saveNewLink,
        JSON.stringify(linkValueBackend),
        (errCode: number | string) => showErrMsg(errCode, failedToExecute)
      );
    } else {
      saveNewLink({
        ...linkValueBackend,
        product_id: currentLinks.length + 1,
      });
    }
  };

  const handleDeleteLink = () => {
    // @todo: Remove preview mode check once this form uses FormContainer.
    // Prevent form from being submitted when in preview mode.
    if (inPreviewMode) {
      failedToExecute('Cannot submit form while in preview mode.', 5000);
      return;
    }

    const failedToDelete = (errCode: number | string) => {
      setIsFetchingLinks(false);
      failedToExecute('');
      setOpen(false);
      showErrMsg(errCode, failedToExecute);
    };

    if (!selectedLink) {
      failedToDelete(-1);
      return;
    }
    setIsFetchingLinks(true);

    const deleteLink = () => {
      if (currentLinks === null) {
        return;
      }
      const newLinks = currentLinks.filter((item) => item.id !== selectedLink.id);
      setCurrentLinks(newLinks);
      succeededToExecute('Deleted successfully!');
      setOpen(false);
      setIsFetchingLinks(false);
    };

    if (!isReactOnlyMode) {
      const urlForLinks = `${api_prefix()}/organizations/${orgId}/products/${selectedLink.id}`;
      fetchWrapper(urlForLinks, FETCH_METHOD.DELETE, deleteLink, {}, failedToDelete);
    } else {
      deleteLink();
    }
  };

  const formikNewLinks = useFormik({
    initialValues: INITIAL_ORG_NEW_LINK,
    validationSchema: VALIDATION_SCHEMA_FOR_NEW_LINK,
    onSubmit: handleAddNewLink,
  });

  const renderCurrentLinks = () => {
    if (currentLinks === null) {
      return !isFetchingLinks && <Typography>{errMsgForGetRequest}</Typography>;
    }
    return currentLinks.length > 0 ? (
      currentLinks.map((link, index) => (
        <div className={classes.currentLinksCard} key={index}>
          <div>
            <Typography variant="subtitle1" className={classes.currentLinksTitle}>
              {link.title}:
            </Typography>
            <Link href={link.url} rel="noreferrer" target="_blank">
              {link.url}
            </Link>
          </div>
          <Button
            className={classes.deleteIconBtn}
            onClick={() => {
              setSelectedLink(link);
              setOpen(true);
            }}
          >
            <DeleteIcon />
          </Button>
        </div>
      ))
    ) : (
      <Typography>No links yet</Typography>
    );
  };

  const saveLinks = useCallback(
    (data: Array<CurrentLinkBackEnd>) => {
      const orgLinks = data.map((link) => ({
        id: link.product_id,
        title: link.name,
        description: link.description,
        url: link.product_url,
      }));
      setCurrentLinks(orgLinks);
      setIsFetchingLinks(false);
    },
    [setCurrentLinks]
  );

  useEffect(() => {
    if (!orgId || currentLinks !== null) {
      currentLinks !== null && setIsFetchingLinks(false);
      return;
    }

    const urlForLinks = isReactOnlyMode
      ? '/membership_data/test_org_links.json'
      : `${api_prefix()}/organizations/${orgId}/products`;

    fetchWrapper(urlForLinks, FETCH_METHOD.GET, saveLinks, '', () => setIsFetchingLinks(false));

    // TO DO: use fetchWrapperPagination when issue467 is resolved
    // https://gitlab.eclipse.org/eclipsefdn/it/websites/membership.eclipse.org/-/issues/467

    // if (isReactOnlyMode) {
    //   fetchWrapper(urlForLinks, FETCH_METHOD.GET, saveLinks);
    // } else {
    //   fetchWrapperPagination(urlForLinks, 1, saveLinks, () => setIsFetchingLinks(false));
    // }
  }, [orgId, currentLinks, saveLinks]);

  return (
    <form onSubmit={formikNewLinks.handleSubmit}>
      <Typography className={classes.mainSectionHeader} variant="h5" component="h2">
        Links
      </Typography>
      <Typography className={classes.linksDescription} variant="body1">
        This enables you to add link to your{' '}
        {orgId ? (
          <Link
            href={`https://www.eclipse.org/membership/showMember.php?member_id=${orgId}`}
            target="_blank"
            rel="noreferrer"
          >
            Membership Page
          </Link>
        ) : (
          'Membership Page'
        )}
        , products and services that aimed to be highlighted. Simply click [add] to add new entree and to edit an
        existing entry. To remove an entry, simply use the delete button for each links you'd like to remove.
      </Typography>
      <Typography className={classes.linksDescription} variant="body1">
        Please email <Link href="mailto:membership_admin@eclipse.org">membership_admin@eclipse.org</Link> if you
        required assistance.
      </Typography>

      <Grid container spacing={4}>
        <Grid item xs>
          <Typography className={classes.mainSectionHeader} variant="h5" component="h2">
            Current Links
          </Typography>
          {isFetchingLinks ? (
            <div className={classes.loadingCtn}>
              <CircularProgress />
            </div>
          ) : (
            renderCurrentLinks()
          )}
        </Grid>
        <Grid item xs>
          <Typography className={classes.mainSectionHeader} variant="h5" component="h2">
            Add a New Link
          </Typography>

          {orgId === 0 ? (
            <div className={classes.loadingCtn}>
              <CircularProgress />
            </div>
          ) : (
            <>
              <Input
                name="title"
                labelName="Title"
                theAriaLabel="Title"
                backgroundColor="#f9f9f9"
                requiredMark={true}
                value={formikNewLinks.values.title}
                onChange={formikNewLinks.handleChange}
                error={formikNewLinks.touched.title && Boolean(formikNewLinks.errors.title)}
                helperText={formikNewLinks.errors.title}
              />
              <Input
                name="description"
                labelName="Description"
                theAriaLabel="Description"
                multiline={true}
                rows={8}
                backgroundColor="#f9f9f9"
                maxLength={700}
                explanationHelperText={'700 characters limit'}
                value={formikNewLinks.values.description}
                onChange={formikNewLinks.handleChange}
                error={formikNewLinks.touched.description && Boolean(formikNewLinks.errors.description)}
                helperText={formikNewLinks.errors.description}
              />
              <Input
                name="url"
                placeholder="https://example.com"
                labelName="URL"
                theAriaLabel="URL"
                backgroundColor="#f9f9f9"
                requiredMark={true}
                value={formikNewLinks.values.url}
                onChange={formikNewLinks.handleChange}
                error={formikNewLinks.touched.url && Boolean(formikNewLinks.errors.url)}
                helperText={formikNewLinks.errors.url}
              />
              <Grid container spacing={2}>
                <Grid item xs>
                  <Button className={classes.btns} variant="contained" color="primary" type="submit">
                    Add
                  </Button>
                </Grid>
                <Grid item xs>
                  <Button
                    className={classes.btns}
                    variant="contained"
                    color="secondary"
                    onClick={() => formikNewLinks.resetForm()}
                  >
                    Cancel
                  </Button>
                </Grid>
              </Grid>
            </>
          )}
        </Grid>
      </Grid>

      <ModalWindow
        title={`Remove link: ${selectedLink?.title}`}
        content={`Would you like to remove ${selectedLink?.title}?`}
        customContent=""
        handleProceed={handleDeleteLink}
        shouldOpen={open}
        setShouldOpen={setOpen}
        cancelText="Cancel"
        yesText="Confirm"
      />
    </form>
  );
}
