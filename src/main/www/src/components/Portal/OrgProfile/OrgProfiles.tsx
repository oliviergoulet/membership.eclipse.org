/*!
 * Copyright (c) 2021, 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Zhou Fang <zhou.fang@eclipse-foundation.org>
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { createStyles, makeStyles, Typography, Theme } from '@material-ui/core';
import BusinessIcon from '@material-ui/icons/Business';
import { brightOrange, iconGray } from '../../../Constants/Constants';
import OrgProfilesBasicInfo from './OrgProfilesBasicInfo';
import OrgProfilesLinks from './OrgProfilesLinks';
import FullLayout from '../../../layouts/FullLayout';

const useStyle = makeStyles((theme: Theme) =>
  createStyles({
    headerIcon: {
      fontSize: 80,
      color: iconGray,
      borderBottom: `6px ${brightOrange} solid`,
    },
    pageHeader: {
      margin: theme.spacing(0.5, 0, 4),
    },
  })
);

export default function OrgProfile() {
  const classes = useStyle();

  return (
    <FullLayout>
      <BusinessIcon className={classes.headerIcon} />
      <Typography variant="h4" component="h1" className={classes.pageHeader}>
        Your Organization Profile
      </Typography>
      <OrgProfilesBasicInfo />
      <OrgProfilesLinks />
    </FullLayout>
  );
}
