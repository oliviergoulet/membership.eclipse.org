/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import { createStyles, makeStyles, Typography, Button, Grid, Theme, CircularProgress } from '@material-ui/core';
import Input from '../../UIComponents/Inputs/Input';
import { DropzoneDialog } from 'material-ui-dropzone';
import { useContext, useEffect, useState } from 'react';
import { useFormik } from 'formik';
import { INITIAL_ORG_PROFILE_VALUE } from '../../UIComponents/FormComponents/formFieldModel';
import { VALIDATION_SCHEMA_FOR_ORG_PROFILE } from '../../UIComponents/FormComponents/ValidationSchema';
import {
  api_prefix,
  borderRadiusSize,
  darkGray,
  errMsgForGetRequest,
  FETCH_METHOD,
  getCurrentMode,
  lightGray,
  LOGO_DIMENSIONS_LIMITATION,
  LOGO_SIZE_LIMITATION_PRINT,
  LOGO_SIZE_LIMITATION_WEB,
  MODE_REACT_ONLY,
} from '../../../Constants/Constants';
import { fetchWrapper, isProd } from '../../../Utils/formFunctionHelpers';
import PortalContext from '../../../Context/PortalContext';
import GlobalContext from '../../../Context/GlobalContext';
import { usePreviewMode } from '../../../Context/PreviewModeContext';
import { OrgInfoBackend } from '../../../Interfaces/portal_interface';
import { showErrMsg } from '../../../Utils/portalFunctionHelpers';

const isReactOnlyMode = getCurrentMode() === MODE_REACT_ONLY;
const NO_LOGO = 'No Logo';

const useStyle = makeStyles((theme: Theme) =>
  createStyles({
    mainSectionHeader: {
      fontWeight: 600,
      margin: theme.spacing(4, 0, 1.5, 0),
    },
    uploadCtn: {
      marginBottom: theme.spacing(2.5),
    },
    subHeader: {
      minWidth: 110,
      fontWeight: 400,
      marginBottom: theme.spacing(1.5),
    },
    orgLogo: {
      margin: theme.spacing(1.5, 0, 2),
      maxWidth: '100%',
      height: 110,
      display: 'block',
      border: `2px ${lightGray} solid`,
      borderRadius: borderRadiusSize,
    },
    orgLogoHintContainer: {
      width: 160,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    orgLogoHintText: {
      padding: theme.spacing(1),
      color: darkGray,
      fontWeight: 600,
    },
    helperText: {
      margin: 0,
    },
    uploadBtn: {
      marginBottom: theme.spacing(2),
      backgroundColor: '#DCDFE5',
      width: 160,
    },
    orgProfileBtnCtn: {
      marginBottom: theme.spacing(4),
    },
    btns: {
      marginRight: theme.spacing(2),
      width: '100%',
      minWidth: 90,
    },
    loadingCtn: {
      width: '100%',
      height: '230px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  })
);

interface OrgInfo {
  orgProfile: { description: string; url: string; name: string };
  logos: { logoForWeb: string; logoForPrint: string };
}

export default function OrgProfilesBasicInfo() {
  const classes = useStyle();
  const { orgId, orgInfo, setOrgInfo } = useContext(PortalContext);
  const { succeededToExecute, failedToExecute } = useContext(GlobalContext);
  const { inPreviewMode } = usePreviewMode();
  const [orgBasicInfo, setOrgBasicInfo] = useState<OrgInfo>(INITIAL_ORG_PROFILE_VALUE);
  const [open, setOpen] = useState(false);
  const [uploadTarget, setUploadTarget] = useState<'Web' | 'Print'>('Web');
  const [isFetchingOrg, setIsFetchingOrg] = useState(true);
  const [dateForUpdatingPrintLogo, setDateForUpdatingPrintLogo] = useState('');
  const [isUploadingWeb, setIsUploadingWeb] = useState(false);
  const [isUploadingPrint, setIsUploadingPrint] = useState(false);

  const openUploadDialog = (target: 'Web' | 'Print') => {
    setUploadTarget(target);
    setOpen(true);
  };

  const postLogo = (files: any) => {
    // @todo: Remove preview mode check once this form uses FormContainer.
    // Prevent form from being submitted when in preview mode.
    if (inPreviewMode) {
      failedToExecute('Cannot submit form while in preview mode.', 5000);
      setOpen(false);
      return;
    }

    !isProd && console.log('Logo Saved!', files[0]);
    
    let fileContent: any;
    const isForPrint = uploadTarget === 'Print';
    isForPrint ? setIsUploadingPrint(true) : setIsUploadingWeb(true);

    const successCallback = () => {
      if (isForPrint) {
        const today = new Date().toString();
        const currentTime = `${today.slice(16, 24)}, ${today.slice(4, 10)}`;
        setDateForUpdatingPrintLogo(currentTime);
        setIsUploadingPrint(false);
      } else {
        setIsUploadingWeb(false);
      }
      // Update Portal Context - orgInfo to make sure everything is synced among sections
      if (orgInfo !== null && uploadTarget === 'Web') {
        setOrgInfo({ ...orgInfo, logos: { ...orgInfo?.logos, web: fileContent } });
      }
      succeededToExecute('');
    };

    const getLogoFileData = () => {
      const data = new FormData();
      const logoFile = files[0];

      data.append('image', logoFile);
      data.append('image_mime', logoFile.type);
      data.append('image_format', isForPrint ? 'PRINT' : 'WEB');
      return data;
    };

    const logoFileInstance = new FileReader();
    logoFileInstance.addEventListener('load', (fileData) => {
      fileContent = fileData.target?.result;
      const logoImg = new Image();
      logoImg.src = fileContent;

      const errMsgWeb = `The image need to be less than ${LOGO_SIZE_LIMITATION_WEB / 1024 / 1024}MB`;
      const errMsgPrint = `The image need to be less than ${LOGO_SIZE_LIMITATION_PRINT / 1024 / 1024}MB`;

      const failedToUploadLogo = (errCode: number) => {
        if (isForPrint) {
          showErrMsg(errCode, failedToExecute, errMsgPrint);
        }

        if (!isForPrint) {
          showErrMsg(errCode, failedToExecute, errMsgWeb);
        }
        setIsFetchingOrg(false);
        setIsUploadingWeb(false);
        setIsUploadingPrint(false);
      };

      if (isForPrint) {
        setOpen(false);

        orgId !== 0 &&
          fetchWrapper(
            api_prefix() + `/organizations/${orgId}/logos`,
            FETCH_METHOD.POST,
            successCallback,
            getLogoFileData(),
            failedToUploadLogo
          );
        return;
      }

      logoImg.onload = () => {
        if (logoImg.height > LOGO_DIMENSIONS_LIMITATION) {
          failedToExecute(`Image height must be ${LOGO_DIMENSIONS_LIMITATION}px or smaller`);
          return;
        }
        if (logoImg.width > LOGO_DIMENSIONS_LIMITATION) {
          failedToExecute(`Image width must be ${LOGO_DIMENSIONS_LIMITATION}px or smaller`);
          return;
        }

        setOpen(false);
        if (!isReactOnlyMode) {
          orgId !== 0 &&
            fetchWrapper(
              api_prefix() + `/organizations/${orgId}/logos`,
              FETCH_METHOD.POST,
              successCallback,
              getLogoFileData(),
              failedToUploadLogo
            );
        } else {
          successCallback();
        }
      };
    });
    logoFileInstance.readAsDataURL(files[0]);
  };

  const handleSaveOrgProfile = () => {
    // @todo: Remove preview mode check once this form uses FormContainer.
    // Prevent form from being submitted when in preview mode.
    if (inPreviewMode) {
      failedToExecute('Cannot submit form while in preview mode.', 5000);
      return;
    }

    const formValue = formikOrg.values;
    setIsFetchingOrg(true);
    !isProd && console.log('Org profile is saved! ', formValue);
    const saveOrgProfile = () => {
      setOrgBasicInfo(formValue);

      // Update Portal Context - orgInfo to make sure everything is synced among sections
      orgInfo !== null &&
        setOrgInfo({
          ...orgInfo,
          description: { long: formValue.orgProfile.description },
          website: formValue.orgProfile.url,
        });

      succeededToExecute('');
      setIsFetchingOrg(false);
    };

    if (!isReactOnlyMode) {
      const newOrgInfo = JSON.stringify({
        description: formValue.orgProfile.description,
        company_url: formValue.orgProfile.url,
      });

      const failedToSaveOrgInfo = (errCode: number | string) => {
        showErrMsg(errCode, failedToExecute);
        setIsFetchingOrg(false);
      };

      orgId !== 0 &&
        fetchWrapper(
          api_prefix() + `/organizations/${orgId}`,
          FETCH_METHOD.POST,
          saveOrgProfile,
          newOrgInfo,
          failedToSaveOrgInfo
        );
    } else {
      saveOrgProfile();
    }
  };

  const formikOrg = useFormik({
    initialValues: INITIAL_ORG_PROFILE_VALUE,
    validationSchema: VALIDATION_SCHEMA_FOR_ORG_PROFILE,
    onSubmit: handleSaveOrgProfile,
  });

  const setOrgValues = formikOrg.setValues;
  const renderFailedMsg = (sectionName: string) => {
    if (orgInfo === null && !isFetchingOrg) {
      return (
        <Typography variant="body1">
          {sectionName}: {errMsgForGetRequest}
        </Typography>
      );
    }
  };

  useEffect(() => {
    if (orgId !== 0 && orgInfo === null) {
      const urlForOrgInfo = isReactOnlyMode
        ? '/membership_data/test_org_info_data.json'
        : `${api_prefix()}/organizations/${orgId}`;

      const saveOrgInfo = (data: OrgInfoBackend) => {
        setOrgInfo(data);
      };
      fetchWrapper(urlForOrgInfo, FETCH_METHOD.GET, saveOrgInfo, '', () => setIsFetchingOrg(false));
    }

    if (orgInfo !== null) {
      // Save OrgBasicInfo based on orgInfo context
      const orgInfoData = {
        orgProfile: {
          url: orgInfo.website || '',
          description: orgInfo.description?.long || '',
          name: orgInfo.name || '',
        },
        logos: {
          logoForPrint: orgInfo.logos.print || '',
          logoForWeb: orgInfo.logos.web || NO_LOGO,
        },
      };
      setOrgBasicInfo(orgInfoData);
      setOrgValues(orgInfoData);
      setIsFetchingOrg(false);
    }
  }, [orgId, orgInfo, setOrgValues, setOrgInfo]);

  return (
    <form onSubmit={formikOrg.handleSubmit}>
      <Grid container spacing={3}>
        {isFetchingOrg ? (
          <div className={classes.loadingCtn}>
            <CircularProgress />
          </div>
        ) : (
          <>
            <Grid item xs>
              {renderFailedMsg('Description')}
              <Input
                name="orgProfile.description"
                labelName="Description"
                theAriaLabel="Company Description"
                multiline={true}
                rows={8}
                backgroundColor="#f9f9f9"
                maxLength={700}
                explanationHelperText={'700 characters limit'}
                value={formikOrg.values.orgProfile.description}
                onChange={formikOrg.handleChange}
                error={formikOrg.touched.orgProfile?.description && Boolean(formikOrg.errors.orgProfile?.description)}
                helperText={formikOrg.errors.orgProfile?.description}
              />
            </Grid>
            <Grid item xs>
              {renderFailedMsg('URL')}
              <Input
                name="orgProfile.url"
                placeholder="https://example.com"
                labelName="Company URL"
                theAriaLabel="Company URL"
                backgroundColor="#f9f9f9"
                value={formikOrg.values.orgProfile.url}
                onChange={formikOrg.handleChange}
                error={formikOrg.touched.orgProfile?.url && Boolean(formikOrg.errors.orgProfile?.url)}
                helperText={formikOrg.errors.orgProfile?.url}
              />
            </Grid>
          </>
        )}
      </Grid>

      <Grid container spacing={3} className={classes.orgProfileBtnCtn}>
        <Grid item xs={6} md={2}>
          <Button className={classes.btns} variant="contained" color="primary" type="submit">
            Save
          </Button>
        </Grid>
        <Grid item xs={6} md={2}>
          <Button
            className={classes.btns}
            variant="contained"
            color="secondary"
            onClick={() => setOrgValues(orgBasicInfo)}
          >
            Cancel
          </Button>
        </Grid>
      </Grid>

      <Grid container spacing={3}>
        <Grid item xs className={classes.uploadCtn}>
          <Typography className={classes.subHeader} variant="h5" component="h2">
            Logo for Web
          </Typography>
          {renderFailedMsg('Web logo')}
          {!isFetchingOrg ? (
            formikOrg.values.logos.logoForWeb === NO_LOGO ? (
              <div className={`${classes.orgLogo} ${classes.orgLogoHintContainer}`}>
                <Typography className={classes.orgLogoHintText} variant="body1">
                  Logo is not set
                </Typography>
              </div>
            ) : (
              <img
                src={formikOrg.values.logos.logoForWeb}
                alt={`${formikOrg.values.orgProfile.name} logo`}
                className={classes.orgLogo}
              />
            )
          ) : (
            <div className={classes.loadingCtn}>
              <CircularProgress />
            </div>
          )}

          <Button className={classes.uploadBtn} onClick={() => openUploadDialog('Web')} disabled={isUploadingWeb}>
            {isUploadingWeb ? <CircularProgress size={20} /> : 'Upload New'}
          </Button>

          <Typography className={classes.helperText} variant="body2">
            The supported formats for uploading your logo include: PNG, or JPG file under{' '}
            {LOGO_SIZE_LIMITATION_WEB / 1024 / 1024} MB in size.
            {/* The calculation is to convert bytes into megabytes */}
          </Typography>
          <Typography className={classes.helperText} variant="body2">
            The logo dimension cannot exceed {LOGO_DIMENSIONS_LIMITATION} by {LOGO_DIMENSIONS_LIMITATION} pixels.
          </Typography>
          <Typography className={classes.helperText} variant="body2">
            To better display the logo across Eclipse Foundation websites, please upload a version with minimal margins.
            Padding will be added to the display.
          </Typography>
        </Grid>

        <Grid item xs>
          <Typography className={classes.subHeader} variant="h5" component="h2">
            Logo for Print
          </Typography>
          {renderFailedMsg('Print logo')}
          <div className={`${classes.orgLogo} ${classes.orgLogoHintContainer}`}>
            {dateForUpdatingPrintLogo ? (
              <Typography variant="body1" className={classes.orgLogoHintText}>
                Updated at <br /> {dateForUpdatingPrintLogo}
              </Typography>
            ) : (
              <Typography variant="body1" className={classes.orgLogoHintText}>
                Has not been updated since opened current page
              </Typography>
            )}
          </div>

          <Button className={classes.uploadBtn} onClick={() => openUploadDialog('Print')} disabled={isUploadingPrint}>
            {isUploadingPrint ? <CircularProgress size={20} /> : 'Upload New'}
          </Button>

          <Typography className={classes.helperText} variant="body2">
            If available please include the .eps file of your company logo and it should be under{' '}
            {LOGO_SIZE_LIMITATION_PRINT / 1024 / 1024} MB in size.
            {/* The calculation is to convert bytes into megabytes */}
          </Typography>
        </Grid>
      </Grid>
      <DropzoneDialog
        dialogTitle={`Upload Logo for ${uploadTarget}`}
        open={open}
        onSave={(file) => postLogo(file)}
        acceptedFiles={uploadTarget === 'Web' ? ['image/jpeg', 'image/png'] : ['.eps']}
        maxFileSize={uploadTarget === 'Web' ? LOGO_SIZE_LIMITATION_WEB : LOGO_SIZE_LIMITATION_PRINT}
        filesLimit={1}
        onClose={() => setOpen(false)}
      />
    </form>
  );
}
