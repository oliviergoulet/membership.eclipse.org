/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React, { useState, useEffect, useMemo} from 'react';
import { TextField } from '@material-ui/core';
import Autocomplete, { AutocompleteChangeReason } from '@material-ui/lab/Autocomplete';
import { getSlimOrganizations } from '../api/membership';

interface AutocompleteOption {
  /** The value of the option. */
  value: number;
  /** The label of the option. This is what is displayed to the user as text. */
  label: string;
}

interface Props {
  /** The label of the Autocomplete component. */
  label?: string,
  /** The size of the Autocomplete component. */
  size?: 'small' | 'medium',
  /** The selected option. */
  value?: AutocompleteOption | null,
  /** The event handler for when the user selects an option. */
  onChange: (event: React.ChangeEvent<{}>, value: AutocompleteOption | null, reason: AutocompleteChangeReason) => void,
}

/**
 * Sort strings which start with a particular substring.
 *
 * @param substring - The prefix substring to compare elements with. 
 * @param a The first string to compare.
 * @param b The second string to compare.
 * @returns Negative value if `a` should come before `b`, positive if `b`
 * should come before `a`, or 0 if equal.
 */
const startsWithComparator = (substring: string, a: string, b: string): number => {
  if (a.startsWith(substring)) {
    return -1;
  } else if (b.startsWith(substring)) {
    return 1;
  } else {
    return 0;
  }
}

/** A component which allows the user to select an organization from a list of
 * organizations. */
export const OrganizationAutocomplete: React.FC<Props> = ({ label = 'Member Organizations', size, value, onChange: handleChange }) => {
  const [organizations, setOrganizations] = useState<any[]>([]);
  const [query, setQuery] = useState<string>('');

  // Convert the organizations into autocomplete options. This is an expensive
  // operation so we cache the result with useMemo.
  const organizationAutocompleteOptions: AutocompleteOption[] = useMemo(() => {
    return organizations.map((organization) => ({
      label: organization.name,
      value: organization.organizationId,
    }));
  }, [organizations]);

  useEffect(() => {
    const loadOrganizations = async () => {
      const [organizations, error] = await getSlimOrganizations();
      if (error) return;
      // Set the organizations state variable to the organizations returned by
      // the API.
      setOrganizations(organizations!);
    };

    loadOrganizations();
  }, []);

  // Update the query state variable when the user types in the input.
  const handleInputChange = (_: any, value: string) => {
    setQuery(value)
  };

  // Filter the options based on the query.
  const filterOptions = (options: AutocompleteOption[]) => {
    // The default options are random and not too meaningful. Only show results
    // when user begins typing.
    if (query.length < 1) return [];

    // Filter the options based on the query if the query includes the substring. 
    // Then we sort the options based on whether or not they start with the query.
    return options
      .filter((option) => option.label
        .toLowerCase()
        .includes(query.toLowerCase()))
      .sort((optionA, optionB) => startsWithComparator(query, optionA.label, optionB.label))
      .slice(0, 6);
  };
  
  return (
    <Autocomplete  
      size={size}
      options={organizationAutocompleteOptions}
      getOptionLabel={(option) => option.label}
      getOptionSelected={(option, value) => option.value === value.value}
      value={value}
      noOptionsText={query.length < 1 ? 'Start typing to find results...' : 'No results found.'}
      filterOptions={filterOptions}
      onChange={handleChange}
      onInputChange={handleInputChange}
      renderInput={(params: any) => 
        <TextField {...params} label={label} variant="outlined" />
      } 
    />
  );
};

export default OrganizationAutocomplete;
