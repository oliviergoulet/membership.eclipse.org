/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React from 'react';
import { Box, useTheme } from '@material-ui/core';

interface Props {
  /** The number of spacing units to add along the x-axis. In other words, the
    * width of the Spacer. 
    */
  x?: number;
  /** The number of spacing units to add along the y-axis. In other words, the
    * height of the Spacer.
    */
  y?: number;
}

/**
  * Spacer is a component that can be used to add space between elements. 
  */
const Spacer: React.FC<Props> = ({ x, y }) => {
  const theme = useTheme();

  const width = x ? theme.spacing(x) : undefined;
  const height = y ? theme.spacing(y) : undefined;

  return (
    <Box width={width} height={height} />
  );
};

export default Spacer;
