/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
/**
 * This is a pure static html table copied from membership site:
 * https://www.eclipse.org/membership/#tab-fees
 */
const MembershipLevelFeeTable = () => {
  return (
    <>
      <p className="margin-top-50">
        Eclipse Foundation charges annual membership fees based on your annual revenues, your type of organization, and
        your class of membership. The following table is shown for illustrative purposes. Please review the{' '}
        <a
          href="https://www.eclipse.org/org/documents/eclipse-foundation-membership-agreement.pdf"
          target="_blank"
          rel="noreferrer"
        >
          Eclipse Foundation Membership Agreement
        </a>{' '}
        for full details.
      </p>
      <div className="vertical-align">
        <table className="table table-stripped" cellSpacing="0">
          <thead>
            <tr>
              <th width="40%" rowSpan="2">
                Annual Corporate Revenue
              </th>
              <th className="text-center" width="60%" colSpan="3">
                Annual Eclipse Foundation Membership Fees
              </th>
            </tr>
            <tr>
              <th className="text-center" width="20%">
                Strategic
              </th>
              <th className="text-center" width="20%">
                Contributing
              </th>
              <th className="text-center" width="20%">
                Associate
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="text-left">&gt; €1&nbsp;billion</td>
              <td className="text-center">€300 000</td>
              <td className="text-center">€25 000</td>
              <td className="text-center">€25 000</td>
            </tr>
            <tr>
              <td className="text-left">
                €100&nbsp;million - €1&nbsp;billion
              </td>
              <td className="text-center">€180 000</td>
              <td className="text-center">€17 500</td>
              <td className="text-center">€17 500</td>
            </tr>
            <tr>
              <td className="text-left">
                €50&nbsp;million - €100&nbsp;million
              </td>
              <td className="text-center">€125 000</td>
              <td className="text-center">€12 500</td>
              <td className="text-center">€12 500</td>
            </tr>
            <tr>
              <td className="text-left">€10&nbsp;million - €50&nbsp;million</td>
              <td className="text-center">€60 000</td>
              <td className="text-center">€9 000</td>
              <td className="text-center">€9 000</td>
            </tr>
            <tr>
              <td className="text-left">&lt; €10&nbsp;million</td>
              <td className="text-center">€30 000</td>
              <td className="text-center">€6 000</td>
              <td className="text-center">€6 000</td>
            </tr>
            <tr>
              <td className="text-left">
                &lt; €1&nbsp;million &lt; 10 employees
              </td>
              <td className="text-center">€30 000</td>
              <td className="text-center">€1 500</td>
              <td className="text-center">€1 500</td>
            </tr>
            <tr>
              <td className="text-left">
                Govt, Govt agencies, Research Organizations, NGOs, etc.
              </td>
              <td className="text-center">€30 000</td>
              <td className="text-center">€6 000</td>
              <td className="text-center">€0</td>
            </tr>
            <tr>
              <td className="text-left">
                Academic, Publishing Organizations, User Groups, etc.
              </td>
              <td className="text-center">€30 000</td>
              <td className="text-center">€1 000</td>
              <td className="text-center">€0</td>
            </tr>
          </tbody>
        </table>
      </div>
    </>
  );
};

export default MembershipLevelFeeTable;
