/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import CustomStepButton from '../../UIComponents/Button/CustomStepButton';
import Input from '../../UIComponents/Inputs/Input';
import { formField } from '../../UIComponents/FormComponents/formFieldModel';
import { useContext, useEffect } from 'react';
import { isObjectEmpty, scrollToTop } from '../../../Utils/formFunctionHelpers';
import { Checkbox, FormControlLabel } from '@material-ui/core';
import MembershipContext from '../../../Context/MembershipContext';
import { CURRENT_STEP, ROUTE_REVIEW, ROUTE_WGS, SIGNING_AUTHORITY_STEP } from '../../../Constants/Constants';

/**
 * Have not added any API calls here,
 * simply use the form fields to render
 * input components for signing Authority Representative
 */

const sectionName = 'signing-authority';
const SigningAuthority = ({ formik, updatedFormValues, setUpdatedFormValues, submitForm }) => {
  const { setCurrentStepIndex } = useContext(MembershipContext);
  const { setFieldValue, values } = formik;
  const { signingAuthorityRepresentative } = formField;
  const name = 'signingAuthorityRepresentative';
  const generateSingleContact = (el) => (
    <div key={el.name} className="col-md-12">
      <Input
        name={`${name}.${el.name}`}
        labelName={el.label}
        placeholder={el.placeholder}
        requiredMark={true}
        disableInput={values.signingAuthorityRepresentative.sameAsCompany}
        ariaLabel={`${name}.${el.name}`}
        onChange={formik.handleChange}
        value={values.signingAuthorityRepresentative[`${el.name}`]}
        error={
          formik.touched.signingAuthorityRepresentative?.[`${el.name}`] &&
          Boolean(formik.errors.signingAuthorityRepresentative?.[`${el.name}`])
        }
        helperText={
          formik.touched.signingAuthorityRepresentative?.[`${el.name}`] &&
          formik.errors.signingAuthorityRepresentative?.[`${el.name}`]
        }
      />
    </div>
  );

  const handleCheckboxChange = (isChecked) => {
    const repInfo = isChecked ? values.representative.member : values.signingAuthorityRepresentative;

    const newValues = {
      ...repInfo,
      sameAsCompany: isChecked,
      id: values.signingAuthorityRepresentative.id,
    };
    formik.setFieldValue('signingAuthorityRepresentative', newValues);
  };

  useEffect(() => {
    scrollToTop();
  }, []);

  useEffect(() => {
    setCurrentStepIndex(4);
  }, [setCurrentStepIndex]);

  useEffect(() => {
    setFieldValue(CURRENT_STEP, SIGNING_AUTHORITY_STEP);
  }, [setFieldValue]);

  useEffect(() => {
    if (updatedFormValues.currentStep === SIGNING_AUTHORITY_STEP || values.currentStep !== SIGNING_AUTHORITY_STEP) {
      return;
    }
    setUpdatedFormValues(values);
  }, [setUpdatedFormValues, values, updatedFormValues]);

  return (
    <>
      <div className="align-center">
        <h1 className="fw-600 h2" id={sectionName}>
          Signing Authority
        </h1>
        <p>Please indicate the individual who has the signing authority for the agreement.</p>

        <FormControlLabel
          control={
            <Checkbox
              name="signingAuthorityRepresentative.sameAsCompany"
              color="primary"
              checked={values.signingAuthorityRepresentative.sameAsCompany}
              onChange={(ev) => handleCheckboxChange(ev.target.checked)}
            />
          }
          label="Same as Member Representative"
        />

        <div className="row">
          {signingAuthorityRepresentative.map((el, index) => index < 2 && generateSingleContact(el))}
        </div>
        <div className="row">
          {signingAuthorityRepresentative.map((el, index) => index > 1 && generateSingleContact(el))}
        </div>
      </div>
      <CustomStepButton
        previousPage={ROUTE_WGS}
        nextPage={ROUTE_REVIEW}
        checkIsEmpty={() => isObjectEmpty(values.signingAuthorityRepresentative)}
        formik={formik}
        updatedFormValues={updatedFormValues}
        submitForm={submitForm}
      />
    </>
  );
};

export default SigningAuthority;
