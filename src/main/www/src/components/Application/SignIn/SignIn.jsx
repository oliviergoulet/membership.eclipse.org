/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import { useContext, useEffect } from 'react';
import MembershipContext from '../../../Context/MembershipContext';
import FormChooser from '../../UIComponents/FormPreprocess/FormChooser';
import {
  FETCH_HEADER,
  END_POINT,
  getCurrentMode,
  MODE_REACT_ONLY,
  MODE_REACT_API,
  ROUTE_COMPANY,
  application_api_prefix,
} from '../../../Constants/Constants';
import Loading from '../../UIComponents/Loading/Loading';
import { isProd } from '../../../Utils/formFunctionHelpers';
import { Button, createMuiTheme, ThemeProvider } from '@material-ui/core';

/**
 * - When it is only running React App without server, uses fake user in public/fake_user.json
 * - When run with server, call the userInfo END_POINT
 * - When logged in, `if(currentUserForm)`, render form chooser
 *
 * //// eslint-disable-next-line ---> not a best practice to use
 *
 * /// But if passing object or function into the dependency array, it is always considered as changed, and keeps re-run the useEffect which causes neverending re-render
 *
 * For functions, the best way to use is useCallback()
 *
 * For Object, need to use the sepecific object value or deconstruct it
 *
 * Please refer to some good explanation about useEffect and dependecies:
 * https://betterprogramming.pub/why-eslint-hates-your-useeffect-dependencies-react-js-560fcac0b1f
 *
 * https://stackoverflow.com/questions/55840294/how-to-fix-missing-dependency-warning-when-using-useeffect-react-hook
 *
 * https://stackoverflow.com/questions/64106594/is-the-useeffect-has-a-missing-dependency-warning-sometimes-wrong
 *
 * https://stackoverflow.com/questions/63201445/react-hook-useeffect-missing-dependencies-warning
 *
 */

const IS_SIGN_IN_CLICKED_KEY = 'isSignInClicked';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#f7941e',
      contrastText: '#fff',
    },
    secondary: {
      main: '#404040',
      contrastText: '#fff',
    },
  },
});

export default function SignIn(props) {
  const { setCurrentFormId, setFurthestPage, needLoadingSignIn, setNeedLoadingSignIn, currentUserForm, setCurrentUserForm } = useContext(MembershipContext);
  const getFakeUser = () => {
    props.history.push(ROUTE_COMPANY);
    setFurthestPage({ index: 1, pathName: ROUTE_COMPANY });
    setCurrentFormId('reactOnly');
    fetch('membership_data/fake_user.json', { headers: FETCH_HEADER })
      .then((resp) => resp.json())
      .then((data) => {
        setCurrentUserForm(data);
      });
  };

  const handleSignIn = () => {
    window.location.assign('/application_api/login');
    localStorage.setItem(IS_SIGN_IN_CLICKED_KEY, 'true');
    setNeedLoadingSignIn(true);
  };

  const renderButtons = () =>
    needLoadingSignIn ? (
      <Loading />
    ) : (
      <div className="text-center margin-bottom-30">
        <p className="h4 text-center margin-bottom-10">
          Get started by logging in with your Eclipse Foundation account:
        </p>
        {getCurrentMode() === MODE_REACT_ONLY && (
          <Button variant="contained" color="secondary" size="large" onClick={() => getFakeUser()}>
            React Only Login
          </Button>
        )}

        {getCurrentMode() === MODE_REACT_API && (
          <Button variant="contained" color="secondary" size="large" onClick={handleSignIn}>
            Log in
          </Button>
        )}
        <Button
          variant="contained"
          color="secondary"
          size="large"
          onClick={() => window.location.assign('https://accounts.eclipse.org/')}
        >
          Create an account
        </Button>
      </div>
    );

  useEffect(() => {
    const isSignInClicked = localStorage.getItem(IS_SIGN_IN_CLICKED_KEY);

    const getUserInfo = () => {
      fetch(application_api_prefix() + `/${END_POINT.userinfo}`, { headers: FETCH_HEADER })
        .then((res) => res.json())
        .then((data) => {
          !isProd && console.log('user info: ', data); // {family_name: "User1", given_name: "User1", name: "user1"}
          setCurrentUserForm(data);
          setNeedLoadingSignIn(false);
        })
        .catch((err) => {
          console.log(err);
          setNeedLoadingSignIn(false);
        });
    };

    // Check if the sign in button is clicked, if so, reload the page to get the q_session cookie ready.
    if (isSignInClicked) {
      localStorage.setItem(IS_SIGN_IN_CLICKED_KEY, '');
      window.location.reload();
    }

    if (getCurrentMode() === MODE_REACT_API) {
      getUserInfo();
    } else {
      setNeedLoadingSignIn(false);
    }
  }, [setCurrentUserForm, setNeedLoadingSignIn]);

  return (
    <ThemeProvider theme={theme}>
      <div className="sign-in-btn-ctn">
        {currentUserForm ? (
          <FormChooser
            history={props.history}
            setIsStartNewForm={props.setIsStartNewForm}
            resetForm={props.resetForm}
            setUpdatedFormValues={props.setUpdatedFormValues}
          />
        ) : (
          renderButtons()
        )}
      </div>
    </ThemeProvider>
  );
}
