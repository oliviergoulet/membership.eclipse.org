/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React from 'react';
import { Role } from '../types';
import { checkPermission } from '../Utils/portalFunctionHelpers';
import { useAuth } from '../hooks';
import { usePreviewMode } from '../Context/PreviewModeContext';

interface Props extends React.PropsWithChildren<{}> {
  /** The role(s) required to render the children. */  
  allowedRoles?: Role[];
  strict?: boolean;
}

/**
  * A component that renders its children if the user is authenticated and 
  * has the required role(s).
  */
const Protected: React.FC<Props> = ({ allowedRoles, strict = false, children }: Props) => {
  let { user } = useAuth();
  const { inPreviewMode } = usePreviewMode();
  // If the user is not authenticated, don't render children.
  if (!user) {
    return null;
  }

  const isAuthorized = allowedRoles === undefined 
      || (inPreviewMode && !strict)
      || checkPermission(allowedRoles, user.roles) 
      || checkPermission(allowedRoles, user.relation)
  
  // If the user is authenticated but does not have the required role(s), don't
  // render children.
  if (!isAuthorized) {
    return null;
  }
  
  // If the user is authenticated and has the required role(s), render
  // children.
  return <>{children}</>;
};

export default Protected;

