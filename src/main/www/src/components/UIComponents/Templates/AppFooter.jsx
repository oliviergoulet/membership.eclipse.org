/*********************************************************************
 * Copyright (c) 2022 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
import { scrollToTop } from '../../../Utils/formFunctionHelpers';

/* eslint-disable jsx-a11y/anchor-is-valid */

export default function AppFooter() {
  return (
    <footer id="footer">
      <div className="container">
        <div className="footer-sections row equal-height-md font-bold">
          <div id="footer-eclipse-foundation" className="footer-section col-md-5 col-sm-8">
            <div className="menu-heading">Eclipse Foundation</div>
            <ul className="nav">
              <ul className="nav">
                <li>
                  <a href="https://www.eclipse.org/org/">About</a>
                </li>
                <li>
                  <a href="https://projects.eclipse.org/">Projects</a>
                </li>
                <li>
                  <a href="https://www.eclipse.org/collaborations/">Collaborations</a>
                </li>
                <li>
                  <a href="https://www.eclipse.org/membership/">Membership</a>
                </li>
                <li>
                  <a href="https://www.eclipse.org/sponsor/">Sponsor</a>
                </li>
              </ul>
            </ul>
          </div>
          <div id="footer-legal" className="footer-section col-md-5 col-sm-8">
            <div className="menu-heading">Legal</div>
            <ul className="nav">
              <ul className="nav">
                <li>
                  <a href="https://www.eclipse.org/legal/privacy.php">Privacy Policy</a>
                </li>
                <li>
                  <a href="https://www.eclipse.org/legal/termsofuse.php">Terms of Use</a>
                </li>
                <li>
                  <a href="https://www.eclipse.org/legal/copyright.php">Copyright Agent</a>
                </li>
                <li>
                  <a href="https://www.eclipse.org/org/documents/Community_Code_of_Conduct.php">Code of Conduct</a>
                </li>
                <li>
                  <a href="https://www.eclipse.org/legal/">Legal Resources</a>
                </li>
              </ul>
              <li>
                <a href="#" className="toolbar-manage-cookies">
                  Manage Cookies
                </a>
              </li>
            </ul>
          </div>
          <div id="footer-more" className="footer-section col-md-5 col-sm-8">
            <div className="menu-heading">More</div>
            <ul className="nav">
              <ul className="nav">
                <li>
                  <a href="https://www.eclipse.org/security/">Report a Vulnerability</a>
                </li>
                <li>
                  <a href="https://www.eclipsestatus.io/">Service Status</a>
                </li>
                <li>
                  <a href="https://www.eclipse.org/org/foundation/contact.php">Contact</a>
                </li>
                <li>
                  <a href="https://www.eclipse.org//projects/support/">Support</a>
                </li>
              </ul>
            </ul>
          </div>
          <div id="footer-end" className="footer-section col-md-8 col-md-offset-1 col-sm-24">
            <div className="footer-end-social-container">
              <div className="footer-end-social">
                <p className="footer-end-social-text">Follow Us:</p>
                <ul className="footer-end-social-links list-inline">
                  <a
                    className="link-unstyled"
                    href="https://www.youtube.com/channel/UCej18QqbZDxuYxyERPgs2Fw"
                    title="YouTube Channel"
                  >
                    <span className="fa fa-stack">
                      <i className="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
                      <i className="fa fa-youtube-play fa-stack-1x fa-inverse" aria-hidden="true"></i>
                    </span>
                  </a>
                  <a className="link-unstyled" href="https://www.linkedin.com/company/eclipse-foundation/" title="LinkedIn">
                    <span className="fa fa-stack">
                      <i className="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
                      <i className="fa fa-linkedin fa-stack-1x fa-inverse" aria-hidden="true"></i>
                    </span>
                  </a>
                  <a className="link-unstyled" href="https://www.facebook.com/eclipse.org/" title="Facebook">
                    <span className="fa fa-stack">
                      <i className="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
                      <i className="fa fa-facebook fa-stack-1x fa-inverse" aria-hidden="true"></i>
                    </span>
                  </a>
                  <a className="link-unstyled" href="https://twitter.com/EclipseFdn" title="Twitter">
                    <span className="fa fa-stack">
                      <i className="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
                      <i className="fa fa-twitter fa-stack-1x fa-inverse"></i>
                    </span>
                  </a>
                </ul>
              </div>
            </div>
            <div className="footer-end-newsletter">
              <div className="footer-end-newsletter">
                <form
                  id="mc-embedded-subscribe-form"
                  action="https://eclipsecon.us6.list-manage.com/subscribe/post"
                  method="post"
                  noValidate=""
                  target="_blank"
                >
                  <label className="footer-end-newsletter-label" htmlFor="email">
                    Subscribe to our Newsletter
                  </label>
                  <div className="footer-end-newsletter-input-wrapper">
                    <input
                      className="footer-end-newsletter-input"
                      type="email"
                      id="email"
                      name="EMAIL"
                      autoComplete="email"
                      placeholder="Enter your email address"
                    />
                    <div>
                      <i className="fa fa-solid fa-envelope fa-lg" aria-hidden="true"></i>
                    </div>
                  </div>
                  <input type="hidden" name="u" value="eaf9e1f06f194eadc66788a85" />
                  <input type="hidden" name="id" value="46e57eacf1" />
                  <input id="mc-embedded-subscribe" type="submit" name="subscribe" hidden />
                </form>
              </div>
            </div>
          </div>
        </div>
        <div className="col-sm-24">
          <div className="row">
            <div id="copyright" className="col-md-16">
              <p id="copyright-text">Copyright &copy; Eclipse Foundation, Inc. All Rights Reserved.</p>
            </div>
          </div>
        </div>
        <span className="scrollup" style={{ bottom: '100px', display: 'inline' }} onClick={() => scrollToTop()}>
          Back to the top
        </span>
      </div>
    </footer>
  );
}
