/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import AppFooter from './AppFooter';
import AppHeader from './AppHeader';

const AppTemplate = (props) => {
  return (
    <>
      <AppHeader />
      <div className="container eclipsefdn-membership-webform">
        {props.children}
      </div>
      <AppFooter />
    </>
  );
};
export default AppTemplate;
