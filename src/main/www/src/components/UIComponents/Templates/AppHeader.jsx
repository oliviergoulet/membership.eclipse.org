/*********************************************************************
 * Copyright (c) 2022 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
import { useContext } from 'react';
import MembershipContext from '../../../Context/MembershipContext';
import { logout } from '../../../Utils/formFunctionHelpers';

/* eslint-disable jsx-a11y/anchor-is-valid */

export default function AppHeader() {
  const { setNeedLoadingSignIn, currentUserForm } = useContext(MembershipContext);

  return (
    <header className="header-wrapper header-wrapper" id="header">
      <div className="header-toolbar">
        <div className="container">
          <div className="header-toolbar-row">
            <div className="toolbar-btn toolbar-search-btn dropdown">
              <button
                className="dropdown-toggle"
                id="toolbar-search"
                type="button"
                data-toggle="dropdown"
                tabIndex="0"
                aria-label="Search"
              >
                <i className="fa fa-search fa-lg" aria-hidden="true"></i>
              </button>
              <div
                className="toolbar-search-bar-wrapper dropdown-menu dropdown-menu-right"
                aria-labelledby="toolbar-search"
              >
                <form action="https://www.eclipse.org/home/search" method="get">
                  <div className="search-bar">
                    <input className="search-bar-input" name="q" placeholder="Search" />
                    <button>
                      <i className="fa fa-search" type="submit"></i>
                    </button>
                  </div>
                </form>
              </div>
            </div>
            <div className="toolbar-btn toolbar-user-menu-btn dropdown">
              <button
                className="dropdown-toggle"
                id="toolbar-user-menu"
                type="button"
                data-toggle="dropdown"
                tabIndex="0"
                aria-label="User Menu"
              >
                <i className="fa fa-user fa-lg"></i>
              </button>
              <ul
                className="toolbar-user-menu dropdown-menu dropdown-menu-right text-center"
                aria-labelledby="toolbar-user-menu"
              >
                { currentUserForm ? (
                  <>
                    <li>
                      <a href="https://accounts.eclipse.org/user">
                        <i className="fa fa-user" />{' '}
                        View My Account
                      </a>
                    </li>
                    <li>
                      <a href="https://accounts.eclipse.org/user/edit">
                        <i className="fa fa-edit" />{' '}
                        Edit My Account
                      </a>
                    </li>
                    <li>
                      <a className="toolbar-manage-cookies">
                        <i className="fa fa-wrench" />{' '}
                        Manage Cookies
                      </a>
                    </li>
                    <li className="divider" />
                    <li>
                      <a>
                        <i className="fa fa-sign-out" onClick={logout} />{' '}
                        Log out
                      </a>
                    </li>
                  </>
                ) : (
                  <>
                    <li>
                      <a href="/application_api/login" onClick={() => setNeedLoadingSignIn(true)}>
                        <i className="fa fa-sign-in" />{' '}
                        Log in
                      </a>
                    </li>
                    <li>
                      <a className="toolbar-manage-cookies">
                        <i className="fa fa-wrench"></i>
                        Manage Cookies
                      </a>
                    </li>
                  </>
                )}
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div className="header-navbar-wrapper">
        <div className="container">
          <div className="header-navbar">
            <div className="header-navbar-brand">
              <a className="logo-wrapper" href="https://www.eclipse.org/" title="Eclipse Foundation">
                <img
                  src="https://www.eclipse.org/eclipse.org-common/themes/solstice/public/images/logo/eclipse-foundation-grey-orange.svg"
                  alt=""
                  width="160"
                />
              </a>
            </div>

            <nav className="header-navbar-nav">
              <ul className="header-navbar-nav-links">
                <li className="navbar-nav-links-item ">
                  <button
                    className="nav-link-js btn-link link-unstyled"
                    type="button"
                    aria-expanded="true"
                    data-menu-target="projects-menu"
                  >
                    Projects
                  </button>
                </li>

                <li className="navbar-nav-links-item ">
                  <button
                    className="nav-link-js btn-link link-unstyled"
                    type="button"
                    aria-expanded="true"
                    data-menu-target="supporters-menu"
                  >
                    Supporters
                  </button>
                </li>

                <li className="navbar-nav-links-item ">
                  <button
                    className="nav-link-js btn-link link-unstyled"
                    type="button"
                    aria-expanded="true"
                    data-menu-target="collaborations-menu"
                  >
                    Collaborations
                  </button>
                </li>

                <li className="navbar-nav-links-item ">
                  <button
                    className="nav-link-js btn-link link-unstyled"
                    type="button"
                    aria-expanded="true"
                    data-menu-target="resources-menu"
                  >
                    Resources
                  </button>
                </li>

                <li className="navbar-nav-links-item ">
                  <button
                    className="nav-link-js btn-link link-unstyled"
                    type="button"
                    aria-expanded="true"
                    data-menu-target="the-foundation-menu"
                  >
                    The Foundation
                  </button>
                </li>
              </ul>
            </nav>

            <div className="header-navbar-end">
              <a className="header-navbar-end-download-btn btn btn-primary" href="https://www.eclipse.org/downloads/">
                <i className="fa " aria-hidden="true"></i>
                Download
              </a>
              <button className="mobile-menu-btn" aria-label="Toggle mobile navigation menu" aria-expanded="false">
                <i className="fa fa-bars fa-xl"></i>
              </button>
            </div>
          </div>
        </div>
      </div>

      <nav className="mobile-menu hidden">
        <ul>
          <li className="mobile-menu-dropdown">
            <a className="mobile-menu-item mobile-menu-dropdown-toggle" data-target="projects-menu">
              <span>Projects</span>
              <i className="fa fa-chevron-down" aria-hidden="true"></i>
            </a>
            <div className="mobile-menu-sub-menu-wrapper">
              <ul className="mobile-menu-sub-menu hidden" id="projects-menu">
                <li className="mobile-menu-dropdown">
                  <a
                    data-target="projects-technologies-sub-menu"
                    className="mobile-menu-item mobile-menu-dropdown-toggle"
                    aria-expanded="false"
                  >
                    <span>Technologies</span>
                    <i className="fa fa-chevron-down" aria-hidden="true"></i>
                  </a>
                  <div className="mobile-menu-sub-menu-wrapper">
                    <ul
                      className="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                      id="projects-technologies-sub-menu"
                    >
                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/topics/ide/">
                          Developer Tools &amp; IDEs
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/topics/cloud-native/">
                          Cloud Native
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/topics/edge-and-iot">
                          Edge &amp; IoT
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/topics/automotive-and-mobility/">
                          Automotive &amp; Mobility
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>

                <li className="mobile-menu-dropdown">
                  <a
                    data-target="projects-projects-sub-menu"
                    className="mobile-menu-item mobile-menu-dropdown-toggle"
                    aria-expanded="false"
                  >
                    <span>Projects</span>
                    <i className="fa fa-chevron-down" aria-hidden="true"></i>
                  </a>
                  <div className="mobile-menu-sub-menu-wrapper">
                    <ul className="mobile-menu-sub-menu mobile-menu-links-menu hidden" id="projects-projects-sub-menu">
                      <li>
                        <a className="mobile-menu-item" href="https://projects.eclipse.org/">
                          Project Finder
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/projects/project_activity.php">
                          Project Activity
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/projects/resources">
                          Project Resources
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/specifications/">
                          Specifications
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/contribute/">
                          Contribute
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </li>

          <li className="mobile-menu-dropdown">
            <a className="mobile-menu-item mobile-menu-dropdown-toggle" data-target="supporters-menu">
              <span>Supporters</span>
              <i className="fa fa-chevron-down" aria-hidden="true"></i>
            </a>
            <div className="mobile-menu-sub-menu-wrapper">
              <ul className="mobile-menu-sub-menu hidden" id="supporters-menu">
                <li className="mobile-menu-dropdown">
                  <a
                    data-target="supporters-membership-sub-menu"
                    className="mobile-menu-item mobile-menu-dropdown-toggle"
                    aria-expanded="false"
                  >
                    <span>Membership</span>
                    <i className="fa fa-chevron-down" aria-hidden="true"></i>
                  </a>
                  <div className="mobile-menu-sub-menu-wrapper">
                    <ul
                      className="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                      id="supporters-membership-sub-menu"
                    >
                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/membership/exploreMembership.php">
                          Our Members
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/membership/">
                          Member Benefits
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/membership/#tab-levels">
                          Membership Levels &amp; Fees
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://membership.eclipse.org/application">
                          Membership Application
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://membership.eclipse.org/portal">
                          Member Portal
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>

                <li className="mobile-menu-dropdown">
                  <a
                    data-target="supporters-sponsorship-sub-menu"
                    className="mobile-menu-item mobile-menu-dropdown-toggle"
                    aria-expanded="false"
                  >
                    <span>Sponsorship</span>
                    <i className="fa fa-chevron-down" aria-hidden="true"></i>
                  </a>
                  <div className="mobile-menu-sub-menu-wrapper">
                    <ul
                      className="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                      id="supporters-sponsorship-sub-menu"
                    >
                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/sponsor/">
                          Sponsor
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/org/corporate_sponsors">
                          Corporate Sponsorship
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/sponsor/collaboration/">
                          Sponsor a Collaboration
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </li>

          <li className="mobile-menu-dropdown">
            <a className="mobile-menu-item mobile-menu-dropdown-toggle" data-target="collaborations-menu">
              <span>Collaborations</span>
              <i className="fa fa-chevron-down" aria-hidden="true"></i>
            </a>
            <div className="mobile-menu-sub-menu-wrapper">
              <ul className="mobile-menu-sub-menu hidden" id="collaborations-menu">
                <li className="mobile-menu-dropdown">
                  <a
                    data-target="collaborations-industry-collaborations-sub-menu"
                    className="mobile-menu-item mobile-menu-dropdown-toggle"
                    aria-expanded="false"
                  >
                    <span>Industry Collaborations</span>
                    <i className="fa fa-chevron-down" aria-hidden="true"></i>
                  </a>
                  <div className="mobile-menu-sub-menu-wrapper">
                    <ul
                      className="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                      id="collaborations-industry-collaborations-sub-menu"
                    >
                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/collaborations/">
                          About Industry Collaborations
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/org/workinggroups/explore.php">
                          Current Collaborations
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/org/workinggroups/about.php">
                          About Working Groups
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/collaborations/interest-groups/">
                          About Interest Groups
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>

                <li className="mobile-menu-dropdown">
                  <a
                    data-target="collaborations-research-collaborations-sub-menu"
                    className="mobile-menu-item mobile-menu-dropdown-toggle"
                    aria-expanded="false"
                  >
                    <span>Research Collaborations</span>
                    <i className="fa fa-chevron-down" aria-hidden="true"></i>
                  </a>
                  <div className="mobile-menu-sub-menu-wrapper">
                    <ul
                      className="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                      id="collaborations-research-collaborations-sub-menu"
                    >
                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/research/">
                          Research @ Eclipse
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </li>

          <li className="mobile-menu-dropdown">
            <a className="mobile-menu-item mobile-menu-dropdown-toggle" data-target="resources-menu">
              <span>Resources</span>
              <i className="fa fa-chevron-down" aria-hidden="true"></i>
            </a>
            <div className="mobile-menu-sub-menu-wrapper">
              <ul className="mobile-menu-sub-menu hidden" id="resources-menu">
                <li className="mobile-menu-dropdown">
                  <a
                    data-target="resources-open-source-sub-menu"
                    className="mobile-menu-item mobile-menu-dropdown-toggle"
                    aria-expanded="false"
                  >
                    <span>Open Source for Business</span>
                    <i className="fa fa-chevron-down" aria-hidden="true"></i>
                  </a>
                  <div className="mobile-menu-sub-menu-wrapper">
                    <ul
                      className="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                      id="resources-open-source-sub-menu"
                    >
                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/org/value/">
                          Business Value of Open Source
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/os4biz/ospo/">
                          Open Source Program Offices
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>

                <li className="mobile-menu-dropdown">
                  <a
                    data-target="resources-happening-sub-menu"
                    className="mobile-menu-item mobile-menu-dropdown-toggle"
                    aria-expanded="false"
                  >
                    <span>What's Happening</span>
                    <i className="fa fa-chevron-down" aria-hidden="true"></i>
                  </a>
                  <div className="mobile-menu-sub-menu-wrapper">
                    <ul
                      className="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                      id="resources-happening-sub-menu"
                    >
                      <li>
                        <a className="mobile-menu-item" href="https://newsroom.eclipse.org/">
                          News
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://events.eclipse.org/">
                          Events
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/community/eclipse_newsletter/">
                          Newsletter
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://newsroom.eclipse.org/news/press-releases">
                          Press Releases
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/org/foundation/eclipseawards/">
                          Awards &amp; Recognition
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>

                <li className="mobile-menu-dropdown">
                  <a
                    data-target="resources-developer-sub-menu"
                    className="mobile-menu-item mobile-menu-dropdown-toggle"
                    aria-expanded="false"
                  >
                    <span>Developer Resources</span>
                    <i className="fa fa-chevron-down" aria-hidden="true"></i>
                  </a>
                  <div className="mobile-menu-sub-menu-wrapper">
                    <ul
                      className="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                      id="resources-developer-sub-menu"
                    >
                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/forums/">
                          Forum
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://wiki.eclipse.org/">
                          Wiki
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://accounts.eclipse.org/mailing-list">
                          Mailing Lists
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/blogs-and-videos/">
                          Blogs &amp; Videos
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/resources/marketplaces/">
                          Marketplaces
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </li>

          <li className="mobile-menu-dropdown">
            <a className="mobile-menu-item mobile-menu-dropdown-toggle" data-target="the-foundation-menu">
              <span>The Foundation</span>
              <i className="fa fa-chevron-down" aria-hidden="true"></i>
            </a>
            <div className="mobile-menu-sub-menu-wrapper">
              <ul className="mobile-menu-sub-menu hidden" id="the-foundation-menu">
                <li className="mobile-menu-dropdown">
                  <a
                    data-target="the-foundation-about-sub-menu"
                    className="mobile-menu-item mobile-menu-dropdown-toggle"
                    aria-expanded="false"
                  >
                    <span>About</span>
                    <i className="fa fa-chevron-down" aria-hidden="true"></i>
                  </a>
                  <div className="mobile-menu-sub-menu-wrapper">
                    <ul
                      className="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                      id="the-foundation-about-sub-menu"
                    >
                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/org/">
                          About the Eclipse Foundation
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/org/governance">
                          Board &amp; Governance
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/org/foundation/staff.php">
                          Staff
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/org/services">
                          Services
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>

                <li className="mobile-menu-dropdown">
                  <a
                    data-target="the-foundation-legal-sub-menu"
                    className="mobile-menu-item mobile-menu-dropdown-toggle"
                    aria-expanded="false"
                  >
                    <span>Legal</span>
                    <i className="fa fa-chevron-down" aria-hidden="true"></i>
                  </a>
                  <div className="mobile-menu-sub-menu-wrapper">
                    <ul
                      className="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                      id="the-foundation-legal-sub-menu"
                    >
                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/legal/">
                          Legal Policies
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/legal/privacy.php">
                          Privacy Policy
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/legal/termsofuse.php">
                          Terms of Use
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/legal/copyright.php">
                          Copyright Agent
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/legal/epl-2.0/">
                          Eclipse Public License
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>

                <li className="mobile-menu-dropdown">
                  <a
                    data-target="the-foundation-more-sub-menu"
                    className="mobile-menu-item mobile-menu-dropdown-toggle"
                    aria-expanded="false"
                  >
                    <span>More</span>
                    <i className="fa fa-chevron-down" aria-hidden="true"></i>
                  </a>
                  <div className="mobile-menu-sub-menu-wrapper">
                    <ul
                      className="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                      id="the-foundation-more-sub-menu"
                    >
                      <li>
                        <a className="mobile-menu-item" href="https://newsroom.eclipse.org/news/press-releases">
                          Press Releases
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/careers/">
                          Careers
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/org/artwork/">
                          Logos &amp; Artwork
                        </a>
                      </li>

                      <li>
                        <a className="mobile-menu-item" href="https://www.eclipse.org/org/foundation/contact.php">
                          Contact Us
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </li>
        </ul>
      </nav>

      <div className="eclipsefdn-mega-menu">
        <div className="mega-menu-submenu container hidden" data-menu-id="projects-menu">
          <div className="mega-menu-submenu-featured-story">
            <p className="mega-menu-submenu-featured-story-heading">Projects</p>
            <p className="mega-menu-submenu-featured-story-text">
              {' '}
              The Eclipse Foundation is home to the Eclipse IDE, Jakarta EE, and hundreds of open source projects,
              including runtimes, tools, specifications, and frameworks for cloud and edge applications, IoT, AI,
              automotive, systems engineering, open processor designs, and many others.
            </p>
          </div>
          <div className="mega-menu-submenu-links-section">
            <div className="mega-menu-submenu-links">
              <p className="menu-heading">Technologies</p>
              <ul>
                <li>
                  <a href="https://www.eclipse.org/topics/ide/">Developer Tools &amp; IDEs</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/topics/cloud-native/">Cloud Native</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/topics/edge-and-iot">Edge &amp; IoT</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/topics/automotive-and-mobility/">Automotive &amp; Mobility</a>
                </li>
              </ul>
            </div>

            <div className="mega-menu-submenu-links">
              <p className="menu-heading">Projects</p>
              <ul>
                <li>
                  <a href="https://projects.eclipse.org/">Project Finder</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/projects/project_activity.php">Project Activity</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/projects/resources">Project Resources</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/specifications/">Specifications</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/contribute/">Contribute</a>
                </li>
              </ul>
            </div>
          </div>
          <div className="mega-menu-submenu-ad-wrapper">
            <div
              className="eclipsefdn-mega-menu-promo-content mega-menu-promo-content"
              data-ad-format="ads_square"
              data-ad-publish-to="eclipse_org_home"
            ></div>
          </div>
        </div>

        <div className="mega-menu-submenu container hidden" data-menu-id="supporters-menu">
          <div className="mega-menu-submenu-featured-story">
            <p className="mega-menu-submenu-featured-story-heading">Supporters</p>
            <p className="mega-menu-submenu-featured-story-text">
              {' '}
              The Eclipse Foundation is an international non-profit association supported by our members, including
              industry leaders who value open source as a key enabler for their business strategies.
            </p>
          </div>
          <div className="mega-menu-submenu-links-section">
            <div className="mega-menu-submenu-links">
              <p className="menu-heading">Membership</p>
              <ul>
                <li>
                  <a href="https://www.eclipse.org/membership/exploreMembership.php">Our Members</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/membership/">Member Benefits</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/membership/#tab-levels">Membership Levels &amp; Fees</a>
                </li>

                <li>
                  <a href="https://membership.eclipse.org/application">Membership Application</a>
                </li>

                <li>
                  <a href="https://membership.eclipse.org/portal">Member Portal</a>
                </li>
              </ul>
            </div>

            <div className="mega-menu-submenu-links">
              <p className="menu-heading">Sponsorship</p>
              <ul>
                <li>
                  <a href="https://www.eclipse.org/sponsor/">Sponsor</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/org/corporate_sponsors">Corporate Sponsorship</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/sponsor/collaboration/">Sponsor a Collaboration</a>
                </li>
              </ul>
            </div>
          </div>
          <div className="mega-menu-submenu-ad-wrapper">
            <div
              className="eclipsefdn-mega-menu-promo-content mega-menu-promo-content"
              data-ad-format="ads_square"
              data-ad-publish-to="eclipse_org_home"
            ></div>
          </div>
        </div>

        <div className="mega-menu-submenu container hidden" data-menu-id="collaborations-menu">
          <div className="mega-menu-submenu-featured-story">
            <p className="mega-menu-submenu-featured-story-heading">Collaborations</p>
            <p className="mega-menu-submenu-featured-story-text">
              {' '}
              Whether you intend on contributing to Eclipse technologies that are important to your product strategy, or
              simply want to explore a specific innovation area with like-minded organizations, the Eclipse Foundation
              is the open source home for industry collaboration.
            </p>
          </div>
          <div className="mega-menu-submenu-links-section">
            <div className="mega-menu-submenu-links">
              <p className="menu-heading">Industry Collaborations</p>
              <ul>
                <li>
                  <a href="https://www.eclipse.org/collaborations/">About Industry Collaborations</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/org/workinggroups/explore.php">Current Collaborations</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/org/workinggroups/about.php">About Working Groups</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/collaborations/interest-groups/">About Interest Groups</a>
                </li>
              </ul>
            </div>

            <div className="mega-menu-submenu-links">
              <p className="menu-heading">Research Collaborations</p>
              <ul>
                <li>
                  <a href="https://www.eclipse.org/research/">Research @ Eclipse</a>
                </li>
              </ul>
            </div>
          </div>
          <div className="mega-menu-submenu-ad-wrapper">
            <div
              className="eclipsefdn-mega-menu-promo-content mega-menu-promo-content"
              data-ad-format="ads_square"
              data-ad-publish-to="eclipse_org_home"
            ></div>
          </div>
        </div>

        <div className="mega-menu-submenu container hidden" data-menu-id="resources-menu">
          <div className="mega-menu-submenu-featured-story">
            <p className="mega-menu-submenu-featured-story-heading">Resources</p>
            <p className="mega-menu-submenu-featured-story-text">
              {' '}
              The Eclipse community consists of individual developers and organizations spanning many industries. Stay
              up to date on our open source community and find resources to support your journey.
            </p>
          </div>
          <div className="mega-menu-submenu-links-section">
            <div className="mega-menu-submenu-links">
              <p className="menu-heading">Open Source for Business</p>
              <ul>
                <li>
                  <a href="https://www.eclipse.org/org/value/">Business Value of Open Source</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/os4biz/ospo/">Open Source Program Offices</a>
                </li>
              </ul>
            </div>

            <div className="mega-menu-submenu-links">
              <p className="menu-heading">What's Happening</p>
              <ul>
                <li>
                  <a href="https://newsroom.eclipse.org/">News</a>
                </li>

                <li>
                  <a href="https://events.eclipse.org/">Events</a>
                </li>

                <li>
                  <a href="/community/eclipse_newsletter/">Newsletter</a>
                </li>

                <li>
                  <a href="https://newsroom.eclipse.org/news/press-releases">Press Releases</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/org/foundation/eclipseawards/">Awards &amp; Recognition</a>
                </li>
              </ul>
            </div>

            <div className="mega-menu-submenu-links">
              <p className="menu-heading">Developer Resources</p>
              <ul>
                <li>
                  <a href="https://eclipse.org/forums/">Forum</a>
                </li>

                <li>
                  <a href="https://wiki.eclipse.org/">Wiki</a>
                </li>

                <li>
                  <a href="https://accounts.eclipse.org/mailing-list">Mailing Lists</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/blogs-and-videos/">Blogs &amp; Videos</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/resources/marketplaces/">Marketplaces</a>
                </li>
              </ul>
            </div>
          </div>
          <div className="mega-menu-submenu-ad-wrapper">
            <div
              className="eclipsefdn-mega-menu-promo-content mega-menu-promo-content"
              data-ad-format="ads_square"
              data-ad-publish-to="eclipse_org_home"
            ></div>
          </div>
        </div>

        <div className="mega-menu-submenu container hidden" data-menu-id="the-foundation-menu">
          <div className="mega-menu-submenu-featured-story">
            <p className="mega-menu-submenu-featured-story-heading">The Foundation</p>
            <p className="mega-menu-submenu-featured-story-text">
              {' '}
              The Eclipse Foundation provides our global community of individuals and organizations with a mature,
              scalable, and vendor-neutral environment for open source software collaboration and innovation.
            </p>
          </div>
          <div className="mega-menu-submenu-links-section">
            <div className="mega-menu-submenu-links">
              <p className="menu-heading">About</p>
              <ul>
                <li>
                  <a href="https://www.eclipse.org/org/">About the Eclipse Foundation</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/org/governance">Board &amp; Governance</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/org/foundation/staff.php">Staff</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/org/services">Services</a>
                </li>
              </ul>
            </div>

            <div className="mega-menu-submenu-links">
              <p className="menu-heading">Legal</p>
              <ul>
                <li>
                  <a href="https://www.eclipse.org/legal/">Legal Policies</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/legal/privacy.php">Privacy Policy</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/legal/termsofuse.php">Terms of Use</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/legal/copyright.php">Copyright Agent</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/legal/epl-2.0/">Eclipse Public License</a>
                </li>
              </ul>
            </div>

            <div className="mega-menu-submenu-links">
              <p className="menu-heading">More</p>
              <ul>
                <li>
                  <a href="https://newsroom.eclipse.org/news/press-releases">Press Releases </a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/careers/">Careers</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/org/artwork/">Logos &amp; Artwork</a>
                </li>

                <li>
                  <a href="https://www.eclipse.org/org/foundation/contact.php">Contact Us</a>
                </li>
              </ul>
            </div>
          </div>
          <div className="mega-menu-submenu-ad-wrapper">
            <div
              className="eclipsefdn-mega-menu-promo-content mega-menu-promo-content"
              data-ad-format="ads_square"
              data-ad-publish-to="eclipse_org_home"
            ></div>
          </div>
        </div>
      </div>
    </header>
  );
}
