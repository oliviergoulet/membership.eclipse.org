/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
const Loading = () => {
  return (
    <div className="display-center loadingIcon">
      <i className="fa fa-spinner fa-spin fa-3x fa-fw"></i>
    </div>
  );
};

export default Loading;
