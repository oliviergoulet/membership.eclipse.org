/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import { TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  root: {
    marginBottom: 14,
    marginTop: 6,
  },
  input: {
    backgroundColor: 'white',
  },
}));

export default function ReadOnlyInput({ value, label, required }) {
  const classes = useStyles();

  return (
    <TextField
      value={value || 'Not Provided'}
      label={label}
      disabled={!value}
      required={required}
      size="small"
      variant="outlined"
      className={classes.root}
      fullWidth
      InputProps={{
        className: classes.input,
        inputProps: {
          readOnly: true,
          maxLength: 255,
        },
      }}
    />
  );
}
