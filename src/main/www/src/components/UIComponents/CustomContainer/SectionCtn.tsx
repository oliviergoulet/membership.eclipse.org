/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import { Typography, createStyles, makeStyles, Container, Grid, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    main: { padding: theme.spacing(9, 0, 0), margin: 0, maxWidth: '100%' },
    projectAndWGCtn: {
      maxWidth: '100%',
      display: 'flex',
      flexWrap: 'wrap',
      margin: theme.spacing(4, 0, 0),
      padding: 0,
      justifyContent: 'center',
      [theme.breakpoints.up('sm')]: {
        justifyContent: 'space-between',
      },
    },
    sectionTitle: {
      textAlign: 'center',
      [theme.breakpoints.up('sm')]: {
        textAlign: 'left',
      },
    },
  })
);

interface SectionCtnProps {
  id: string;
  title: string;
  children: any;
}

export default function SectionCtn(props: SectionCtnProps) {
  const classes = useStyles();

  return (
    <Container className={classes.main} id={props.id}>
      <Typography className={classes.sectionTitle} variant="h4" component="h1">
        {props.title}
      </Typography>

      <Grid container spacing={4} className={classes.projectAndWGCtn}>
        {props.children}
      </Grid>
    </Container>
  );
}
