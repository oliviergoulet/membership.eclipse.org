/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import { GridToolbarContainer, GridToolbarExport } from '@mui/x-data-grid';
import { CustomToolbarProps } from '../../../Interfaces/portal_interface';

export default function CustomToolbar({ fileName }: CustomToolbarProps) {
  return (
    <GridToolbarContainer>
      <GridToolbarExport csvOptions={{ fileName: fileName }} />
    </GridToolbarContainer>
  );
}
