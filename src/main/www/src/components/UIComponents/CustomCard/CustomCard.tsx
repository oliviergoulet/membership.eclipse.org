/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import {
  createStyles,
  makeStyles,
  Container,
  List,
  Typography,
  ListItemProps,
  ListItem,
  ListItemText,
  Theme,
  CircularProgress,
  Button,
} from '@material-ui/core';
import { borderRadiusSize, darkGray, iconGray } from '../../../Constants/Constants';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    customCard: {
      padding: 0,
      position: 'relative',
      minWidth: 230,
      minHeight: 240,
      backgroundColor: '#fff',
      boxShadow: '1px 1px 15px rgba(0,0,0,0.1)',
      borderRadius: borderRadiusSize,
      margin: theme.spacing(2, 0),
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        margin: theme.spacing(2.5, 2, 2, 0),
        width: '46%',
      },
      [theme.breakpoints.up(1200)]: {
        margin: theme.spacing(2.5, 2, 2, 0),
        width: '22%',
      },
    },
    loadingCtn: {
      position: 'absolute',
      left: 0,
      top: 30,
      bottom: 0,
      right: 0,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    customIconCtn: {
      position: 'absolute',
      top: theme.spacing(-2.5),
      left: theme.spacing(1.5),
      width: 55,
      height: 55,
      padding: 0,
      '& svg': {
        color: iconGray,
        width: '100%',
        height: '100%',
        paddingBottom: theme.spacing(0.5),
      },
    },
    customContent: {
      marginTop: theme.spacing(4),
      padding: theme.spacing(0, 1.5, 0),
      width: '100%',
    },
    customSubtitle: {
      fontWeight: 600,
      color: darkGray,
    },
    customList: {
      paddingBottom: theme.spacing(1.5),
    },
    customItem: {
      padding: theme.spacing(0.5, 1.5),
      minHeight: 30,
    },
    viewAllBtnCtn: {
      position: 'absolute',
      bottom: 15,
      left: 15,
      right: 15,
      width: '100%',
      display: 'flex',
      justifyContent: 'end',
      alignItems: 'end',
    },
    viewAllBtn: {
      padding: theme.spacing(0.5, 1.5),
    },
    viewAllBtnCtnPlaceholder: { height: 48 },
  })
);

const ListItemLink = (props: ListItemProps<'a', { button?: true }>) => {
  return <ListItem button component="a" {...props} />;
};

interface CustomCardProps {
  subtitle: string;
  color: string;
  icon: any;
  isFetching: boolean;
  urlText: string;
  listItems: Array<{
    name: string;
    url: string;
  }>;
  callBackFunc: () => void;
}

export default function CustomCard(props: CustomCardProps) {
  const classes = useStyles();
  return (
    <Container className={classes.customCard}>
      <Container className={classes.customIconCtn} style={{ borderBottom: `4px solid ${props.color}` }}>
        {props.icon}
      </Container>
      <Container className={classes.customContent}>
        <Typography variant="subtitle1" className={classes.customSubtitle}>
          {props.subtitle}
        </Typography>

        {props.isFetching ? (
          <Container className={classes.loadingCtn}>
            <CircularProgress />
          </Container>
        ) : (
          <>
            <List aria-label="custom list" className={classes.customList}>
              {props.listItems.map((listItem, index) =>
                listItem.url ? (
                  <ListItemLink
                    key={listItem.name + index}
                    className={classes.customItem}
                    href={listItem.url}
                    target="_blank"
                    rel="noopener"
                  >
                    <ListItemText primary={listItem.name} />
                  </ListItemLink>
                ) : (
                  <ListItemText key={listItem.name + index} className={classes.customItem} primary={listItem.name} />
                )
              )}
            </List>
            {props.urlText && (
              <>
                <Container className={classes.viewAllBtnCtn}>
                  <Button className={classes.viewAllBtn} onClick={() => props.callBackFunc()}>
                    {props.urlText}
                  </Button>
                </Container>
                <div className={classes.viewAllBtnCtnPlaceholder}></div>
              </>
            )}
          </>
        )}
      </Container>
    </Container>
  );
}
