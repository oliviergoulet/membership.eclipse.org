/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React from 'react';
import { Route, RouteProps, Redirect } from 'react-router-dom'
import { useAuth } from '../hooks';
import { Role } from '../types';
import { roles } from '../Constants';
import { checkPermission } from '../Utils/portalFunctionHelpers';
import { usePreviewMode } from '../Context/PreviewModeContext';

interface Props extends RouteProps {
    component: React.ComponentType<any>;
    allowedRoles?: Role[];
    strict?: boolean;
}

const ProtectedRoute: React.FC<Props> = ({ component: Component, allowedRoles, strict = false, ...otherProps }: Props) => {
    const { user } = useAuth();
    const { inPreviewMode } = usePreviewMode();

    // Redirect to login page if user is not logged in, or if the user has no EMPLY role
    // The minimum requirement for a user to have access to the membership portal is to have the 
    // EMPLY role    
    if (!user || (!user.roles.includes(roles.admin) && !user.roles.includes(roles.elevated) && !user.relation.includes(roles.employee))) {
      return <Redirect to="/portal/login" />;
    }

    const isAuthorized = allowedRoles === undefined 
        || (inPreviewMode && !strict)
        || checkPermission(allowedRoles, user.roles) 
        || checkPermission(allowedRoles, user.relation)

    if (!isAuthorized) {
        return <Redirect to="/portal/unauthorized" />;
    }

    return <Route 
      { ...otherProps } 
      render={(props) => <Component {...props} />} 
    />;
}

export default ProtectedRoute;
