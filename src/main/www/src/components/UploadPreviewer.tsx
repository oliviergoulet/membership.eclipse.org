/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React, { useState } from 'react';
import { Box, Typography, Button, createStyles, makeStyles, Theme } from '@material-ui/core';
import { DropzoneDialog } from 'material-ui-dropzone';
import { useImageFile } from '../hooks';

const defaultAcceptedFiles = ['image/jpeg', 'image/png'];

interface Props {
  /** The title of the upload dialog. */
  title?: string;
  /** The initial image URL. */
  initialImage?: string;
  /** The placeholder text to display if there is no image. */
  placeholder?: string;
  /** The accepted file mime types. */
  acceptedFiles?: string[];
  /** The maximum file size in MB. */
  maxFileSize?: number;
  /** Whether or not the upload dialog is open. */
  dialogOpened?: boolean;
  /** The event handler for when the user saves the image. */
  onSave?: (files: File[], event: React.SyntheticEvent) => void
}

/** `UploadPreviewer` is a component that allows the user to upload an image
 * via a dialog and preview the results. It can be used for forms.*/
const UploadPreviewer = ({ 
  title, 
  initialImage,
  placeholder = 'No image',
  maxFileSize = 100,
  acceptedFiles = defaultAcceptedFiles,
  onSave: incompleteHandleSave
}: Props) => {
  const classes = useStyle();

  const [uploadDialogOpen, setUploadDialogOpen] = useState(false);
  const { image: previewImage, uploader } = useImageFile();

  const handleUploadButtonClick = () => {
    setUploadDialogOpen(true);
  }

  const handleUploadDialogClose = () => {
    setUploadDialogOpen(false);
  }
  
  // Run the event handler passed down as props and close the dialog
  const handleSave = incompleteHandleSave && ((files: File[], event: React.SyntheticEvent) => {
    const imageFile = files[0];

    uploader(imageFile);
    incompleteHandleSave(files, event);
    setUploadDialogOpen(false);
  });

  return (
    <>
      <Box my="1rem">
        <Box my="1rem" className={classes.imageContainer}>
          { 
            initialImage || previewImage 
              ? <img 
                  className={classes.image} 
                  src={previewImage as string || initialImage} 
                  alt=""
                />
              : <Typography>{placeholder}</Typography>
          }
        </Box>
        <Box className={classes.buttonContainer}>
          <Button 
            variant="contained" 
            color="secondary"
            onClick={handleUploadButtonClick}
          >
            Upload 
          </Button>
          <Button 
            type="submit"
            variant="contained" 
            color="primary"
            disabled={!previewImage}
          >
            Save Changes 
          </Button>
        </Box>
      </Box>
      <DropzoneDialog
        dialogTitle={title || "Upload Image"}
        filesLimit={1}
        acceptedFiles={acceptedFiles}
        maxFileSize={maxFileSize}
        open={uploadDialogOpen}
        onClose={handleUploadDialogClose}
        onSave={handleSave}
      />
    </>
  );
}

const useStyle = makeStyles((_theme: Theme) =>
  createStyles({
    imageContainer: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      height: '12rem',
      border: '1px solid #ccc',
      borderRadius: '0.25rem',
    },
    image: {
      maxWidth: '100%',
      maxHeight: '100%',
      border: '1px dashed #f00',
    },
    buttonContainer: {
      display: 'flex',
      gap: '1rem',
    }
  })
);

export default UploadPreviewer;
