/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { TextField as MUITextField, TextFieldProps, Theme, makeStyles, createStyles } from '@material-ui/core';
import { useField } from 'formik';

type Props = {} & TextFieldProps;

const useStyles = makeStyles((_theme: Theme) => 
  createStyles({
    input: {
      lineHeight: '1.8em !important',
    }
}));

/** A text field component which can be used within a form. It contains the web
 * app's default styles. */
const TextField = ({ name, helperText, ...props }: Props) => {
  const classes = useStyles();
  const { error, touched } = useFormField(name);
  const hasError = touched && !!error;

  return (
    <MUITextField
      name={name}
      variant="outlined"
      helperText={hasError ? error : helperText}
      error={hasError}
      InputProps={{
        className: classes.input
      }}
      {...props}
    />
  ); 
};

/** A custom hook which returns the error and touched state of a form field. */
const useFormField = (fieldName: string = ''): { error?: string, touched: boolean } => {
  const [,meta] = useField(fieldName);
  return meta;
};

export default TextField;
