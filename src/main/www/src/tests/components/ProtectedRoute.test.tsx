// @ts-nocheck
/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '@testing-library/jest-dom/extend-expect';
import ProtectedRoute from '../../components/ProtectedRoute';
import * as hooksModule from '../../hooks';
import { MemoryRouter } from 'react-router-dom';
import { render, cleanup } from '@testing-library/react';

const MockComponent = () => <div>Protected Component</div>;

describe('ProtectedRoute', () => {
    let useAuthSpy: jest.SpyInstance;

    beforeEach(() => {
        useAuthSpy = jest.spyOn(hooksModule, 'useAuth');
    });

    afterEach(() => {
        cleanup();
    });

    it('Renders component if user is authenticated', () => {
        useAuthSpy.mockReturnValue({
            user: { 
                name: 'Tester', 
                roles: [],
                relation: ['EMPLY'] 
            }
        });

        const { getByText } = render(
            <MemoryRouter initialEntries={['/']}>
                <ProtectedRoute
                    path="/"
                    component={ MockComponent }
                />
            </MemoryRouter>
        );

        const element = getByText('Protected Component');
        expect(element).toBeInTheDocument();
    });

    it('Does not render the component if user is not authenticated', () => {
        useAuthSpy.mockReturnValue({
            user: null
        });

        const { queryByText } = render(
            <MemoryRouter initialEntries={['/']}>
                <ProtectedRoute
                    path="/"
                    component={ MockComponent }
                />
            </MemoryRouter>
        );

        const element = queryByText('Protected Component');
        expect(element).not.toBeInTheDocument();
    });

    it('Renders component if user is authorized', () => {
        useAuthSpy.mockReturnValue({
            user: {
                name: 'Tester',
                roles: ['admin'],
                relation: ['EMPLY']
            }
        });

        const { queryByText } = render(
            <MemoryRouter initialEntries={['/']}>
                <ProtectedRoute
                    path="/"
                    component={ MockComponent }
                    allowedRoles={['admin']}
                />
            </MemoryRouter>
        );

        const element = queryByText('Protected Component');
        expect(element).toBeInTheDocument();
    });

    it('Does not render the component if user is not authorized', () => {
        useAuthSpy.mockReturnValue({
            user: {
                name: 'Tester',
                roles: ['moderator'],
                relation: ['EMPLY']
            }
        });

        const { queryByText } = render(
            <MemoryRouter initialEntries={['/']}>
                <ProtectedRoute
                    path="/"
                    component={ MockComponent }
                    allowedRoles={['admin']}
                />
            </MemoryRouter>
        );

        const element = queryByText('Protected Component');
        expect(element).not.toBeInTheDocument();
    });

    it('Renders the component if user is authorized by organizational role', () => {
        useAuthSpy.mockReturnValue({
            user: {
                name: 'Tester',
                roles: [],
                relation: ['EMPLY', 'CR']
            }
        });

        const { queryByText } = render(
            <MemoryRouter initialEntries={['/']}>
                <ProtectedRoute
                    path="/"
                    component={ MockComponent }
                    allowedRoles={['CR']}
                />
            </MemoryRouter>
        );

        const element = queryByText('Protected Component');
        expect(element).toBeInTheDocument();
    });

    it('Does not render the component if user is not authorized by organizational role', () => {
        useAuthSpy.mockReturnValue({
            user: {
                name: 'Tester',
                roles: [],
                relation: ['EMPLY']
            }
        });

        const { queryByText }= render(
            <MemoryRouter initialEntries={['/']}>
                <ProtectedRoute
                    path="/"
                    component={ MockComponent }
                    allowedRoles={['CR']}
                />
            </MemoryRouter>
        );

        const element = queryByText('Protected Component');
        expect(element).not.toBeInTheDocument();
    });

    it('Does not render the component if the user does not have the minimum role of EMPLY', () => {
        useAuthSpy.mockReturnValue({
            user: {
                name: 'Tester',
                roles: [],
                relation: []
            }
        });

        const { queryByText } = render(
            <MemoryRouter initialEntries={['/']}>
                <ProtectedRoute
                    path="/"
                    component={ MockComponent }
                />
            </MemoryRouter>
        );

        const element = queryByText('Protected Component');
        expect(element).not.toBeInTheDocument();
    });
});
