/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
import { mapPurchasingAndVAT } from '../Utils/formFunctionHelpers';

test.skip('Map Purchasing and VAT data to match front-end form', () => {
  const dataFromBackEnd = {
    id: 'id',
    legal_name: 'legal_name',
    purchase_order_required: 'yes',
    vat_number: 'vat_number',
    registration_country: 'registration_country',
  };

  expect(mapPurchasingAndVAT(dataFromBackEnd)).toEqual({
    id: 'id',
    legalName: 'legal_name',
    purchasingProcess: 'yes',
    vatNumber: 'vat_number',
    countryOfRegistration: 'registration_country',
  });
});
