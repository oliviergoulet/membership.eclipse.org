/*!
 * Copyright (c) 2023, 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { createContext, useCallback, useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import PortalContext from './PortalContext';
import { api_prefix } from '../Constants/Constants';
import { OrgInfoBackend } from '../Interfaces/portal_interface';

interface PreviewModeValue {
  /** Whether or not the user is in preview mode. */
  inPreviewMode: boolean;
  /** The organization that is being previewed. */
  previewedOrganization: OrgInfoBackend | null; 
  /** Enables preview mode.
    *  
    * @param organizationId - The id of the organization that is to be
    * previewed.
    */
  enablePreviewMode: (organizationId: number) => void;
  /**
    * Disables preview mode. 
    *
    * @param options - Options for disabling preview mode.
    */
  disablePreviewMode: (options?: DisablePreviewModeOptions) => void;
}

interface DisablePreviewModeOptions {
  /** Toggle whether or not user should be redirected to standby page when
    * preview mode is disabled. 
    */
  redirect?: boolean;
}

export const PreviewModeContext = createContext<PreviewModeValue | null>(null);

interface PreviewModeProviderProps extends React.PropsWithChildren<{}> {}

/** 
 * The context provider for preview mode related state. 
 * This context is dependant upon `PortalContext`.
 */
export const PreviewModeProvider: React.FC<PreviewModeProviderProps> = ({ children }) => {
  const [inPreviewMode, setInPreviewMode] = useState(false);
  const [isLoadingPreview, setLoadingPreview] = useState(false);
  // The originalOrganization state represents the user's actual organization.
  // This is required because orgId is now used to represent the organization
  // that is being previewed.
  const history = useHistory();
  
  const { 
    orgId, 
    setOrgId, 
    orgInfo, 
    setOrgInfo, 
    setYourWGData,
    setAllYourProjectsData,
    setInterestedWGData,
    setInterestedProjectsData,
    setYourProjectsData,
    setAllYourWGData,
    setContactData,
    setAllRelations,
    setCurrentLinks,
    setOrgRepData,
    setResourcesData,
    setCommitters,
    setContributors,
    setCBIData,
    setChartCommits,
    setChartCommitters,
    setChartMonths,
    setContactFilterRole,
  } = useContext(PortalContext);

  /** 
    * Resets state to null for organization-related data. This will allow
    * components to refetch data when an organization changes. 
    */
  const resetOrganizationState = useCallback(() => {
    // @todo: Once PortalContext is refactored, this won't be needed and will
    // benefit performance.
    setOrgInfo(null!);
    setContactData(null!);
    setAllRelations(null!);
    setCurrentLinks(null!);
    setOrgRepData(null!);
    setResourcesData(null!);
    setCommitters(null!);
    setContributors(null!);
    setCBIData(null!);
    setChartCommits(null!);
    setChartCommitters(null!);
    setChartMonths(null!);
    setContactFilterRole(null!);

    setYourWGData(null!);
    setAllYourWGData(null!);
    setInterestedWGData(null!);
    setYourProjectsData(null!);
    setAllYourProjectsData(null!);
    setInterestedProjectsData(null!);
  }, [
    setOrgInfo, 
    setContactData, 
    setAllRelations, 
    setCurrentLinks, 
    setOrgRepData, 
    setResourcesData,
    setCommitters,
    setContributors,
    setCBIData,
    setChartCommits,
    setChartCommitters,
    setChartMonths,
    setContactFilterRole,
    setYourWGData,
    setAllYourWGData,
    setInterestedWGData,
    setYourProjectsData,
    setAllYourProjectsData,
    setInterestedProjectsData,
  ]);

  /**
    * Enables the preview mode. The user will be navigated to the portal's
    * front page and will be able to view their selected organization. 
    *
    * @param organizationId - The id of the organization that is to be
    * previewed.
    */
  const enablePreviewMode = useCallback((organizationId: number): void => {
    setInPreviewMode(true); 
    setLoadingPreview(true);
    setOrgId(organizationId);
    resetOrganizationState();
    history.push('/portal/dashboard');
  }, [setInPreviewMode, setLoadingPreview, setOrgId, resetOrganizationState, history]);

  /**
    * Disables the preview mode. The user will be navigated to the portal's
    * front page and will be able to view their original organization.
    *
    * @param options - Options for disabling preview mode.
    */
  const disablePreviewMode = useCallback((options: DisablePreviewModeOptions | undefined = { redirect: true }): void => {
    setInPreviewMode(false);
    setOrgId(0!);
    resetOrganizationState();

    if (options.redirect) { 
      history.push('/portal/standby');
    }
  }, [setInPreviewMode, setOrgId, resetOrganizationState, history]);

  useEffect(() => {
    // Recover preview mode from a refresh by retrieving the organization id
    // that was persisted.
    const persistedPreviewModeOrganizationId = getPersistedPreviewModeOrganizationId();
    if (persistedPreviewModeOrganizationId) {
      enablePreviewMode(persistedPreviewModeOrganizationId);
    }
  }, [enablePreviewMode]);

  useEffect(() => {
    // If we are no longer in preview mode, unpersist it from session storage.
    if (!inPreviewMode) {
      unpersistPreviewMode();
    }
  }, [inPreviewMode]);

  useEffect(() => {
    // If the user is not in preview mode (orgId is 0), we do not need to fetch
    // the organization info.
    if (orgId === 0) {
      return;
    };

    // If user is in preview mode (org is not 0), we persist the preview mode
    // to session storage. We need to persist to recover preview mode from a page
    // refresh.
    persistPreviewMode(orgId);

    // If the user is in preview mode, retrieve the organization info.
    fetch(`${api_prefix()}/organizations/${orgId}`)
      .then((data) => data.json())
      .then((organization) => {
        setLoadingPreview(false);
        setOrgInfo(organization);
      })
      .catch(err => console.error(err));

  }, [inPreviewMode, isLoadingPreview, orgId, setOrgInfo]);

  // OrgInfo only corresponds to the previewed organization when inPreviewMode
  // is set to true. Otherwise, we cannot interpret orgInfo as the preview
  // target.
  const previewedOrganization = inPreviewMode ? orgInfo : null;

  return (
    <PreviewModeContext.Provider value={{ enablePreviewMode, disablePreviewMode, inPreviewMode, previewedOrganization }}>
      {children}
    </PreviewModeContext.Provider>
  );
}

/** 
  * A hook for using the preview mode context.
  *
  * @returns The preview mode context values.
  */
export const usePreviewMode = () => {
  const context = useContext(PreviewModeContext);

  if (!context) {
    throw new Error('usePreviewMode must be used within a PreviewModeProvider component');
  }

  return context;
};

/**
  * Persists the organization id being previewed to session storage.
  */
const persistPreviewMode = (organizationId: number): void => {
  sessionStorage.setItem('orgId', organizationId.toString());
}

/**
  * Clear the organization id being previewd from session storage.
  */
const unpersistPreviewMode = (): void => {
  sessionStorage.removeItem('orgId');
}

/**
  * Get the organization id of the organization currently being previewed from
  * session storage. 
  *
  * @returns organization id of previewed organization
  */
const getPersistedPreviewModeOrganizationId = (): number | null => {
  const storedOrganizationId = sessionStorage.getItem('orgId');
  
  if (!storedOrganizationId) return null;

  return Number.parseInt(storedOrganizationId);
}
