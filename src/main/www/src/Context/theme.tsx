/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { createMuiTheme, Color, ThemeProvider as MuiThemeProvider } from '@material-ui/core';
import { darkGray } from '../Constants/Constants';

type PartialColor = Partial<Color>;

// Define palette options below.
interface Neutral extends PartialColor {
  50: string;
  900: string;
}

// Define the extended type below.
interface PaletteExtensions {
  neutral: Neutral
}

// We need to modify the types of Material UI to include our PaletteExtensions.
declare module '@material-ui/core/styles/createPalette' {
  interface Palette extends PaletteExtensions {}
  interface PaletteOptions extends PaletteExtensions {}
}

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#f7941e',
      contrastText: '#fff', // for button text color
    },
    secondary: {
      main: darkGray,
      contrastText: '#fff',
    },
    neutral: {
      50: '#fff',
      900: '#000',
    }
  },
  spacing: 10,
  typography: {
    htmlFontSize: 10,
    fontSize: 12,
  }
});

export type Theme = typeof theme;

type Props = React.PropsWithChildren<{}>

/** 
  * ThemeProvider is the context provider for the Portal theme.
  */
export const ThemeProvider: React.FC<Props> = ({ children }) => {
  return (
    <MuiThemeProvider theme={theme}>
      {children}
    </MuiThemeProvider>
  );
}
