/*!
 * Copyright (c) 2021, 2022, 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Zhou Fang <zhou.fang@eclipse-foundation.org>
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import React from 'react';
import {
  AllRelationsFrontend,
  CBIData,
  CommitterOrContributor,
  ContactBackend,
  CurrentLink,
  OrgInfoBackend,
  OrgRep,
  ProjectsAndWGItem,
  ResourcesData,
  ProjectsDataFrontend,
} from '../Interfaces/portal_interface';
import { User } from '../types';

interface PortalContextType {
  orgId: number;
  setOrgId: (orgId: number) => void;
  contactFilterRole: string;
  setContactFilterRole: (role: string) => void;
  contactData: Array<ContactBackend> | null;
  setContactData: (data: Array<ContactBackend>) => void;
  orgInfo: OrgInfoBackend | null;
  setOrgInfo: (orgInfo: OrgInfoBackend) => void;
  isFetchingOrgInfo: boolean;
  setIsFetchingOrgInfo: (isFetching: boolean) => void;
  allRelations: AllRelationsFrontend | null;
  setAllRelations: (relations: AllRelationsFrontend) => void;
  currentLinks: Array<CurrentLink> | null;
  setCurrentLinks: (links: Array<CurrentLink>) => void;
  orgRepData: Array<OrgRep> | null;
  setOrgRepData: (repData: Array<OrgRep>) => void;
  resourcesData: Array<ResourcesData> | null;
  setResourcesData: (resourcesData: Array<ResourcesData>) => void;
  committers: Array<CommitterOrContributor> | null;
  setCommitters: (committersData: Array<CommitterOrContributor>) => void;
  contributors: Array<CommitterOrContributor> | null;
  setContributors: (contributorsData: Array<CommitterOrContributor>) => void;
  cbiData: CBIData | null;
  setCBIData: (cbiData: CBIData) => void;
  chartCommits: Array<number> | null;
  setChartCommits: (commitsData: Array<number>) => void;
  chartCommitters: Array<number> | null;
  setChartCommitters: (committersData: Array<number>) => void;
  interestedProjectsData: Array<ProjectsAndWGItem> | null;
  setInterestedProjectsData: (interestedProjectsData: Array<ProjectsAndWGItem>) => void;
  yourProjectsData: Array<ProjectsAndWGItem> | null;
  setYourProjectsData: (yourProjectsData: Array<ProjectsAndWGItem>) => void;
  allYourProjectsData: Array<ProjectsDataFrontend> | null;
  setAllYourProjectsData: (allYourProjectsData: Array<ProjectsDataFrontend>) => void;
  yourWGData: Array<ProjectsAndWGItem> | null;
  setYourWGData: (yourWGData: Array<ProjectsAndWGItem>) => void;
  allYourWGData: Array<ProjectsAndWGItem> | null;
  setAllYourWGData: (allYourWGData: Array<ProjectsAndWGItem>) => void;
  interestedWGData: Array<ProjectsAndWGItem> | null;
  setInterestedWGData: (interestedWGData: Array<ProjectsAndWGItem>) => void;
  chartMonths: Array<string> | null;
  setChartMonths: (monthData: Array<string>) => void;
  currentUserPortal: User | null;
  setCurrentUserPortal: (userData: any) => void;
}

const PortalContext = React.createContext<PortalContextType>({
  orgId: 0,
  setOrgId: (orgId: number | string) => {},
  contactFilterRole: '',
  setContactFilterRole: (role: string) => {},
  contactData: null,
  setContactData: (data: Array<ContactBackend>) => {},
  orgInfo: null,
  setOrgInfo: (orgInfo: OrgInfoBackend) => {},
  isFetchingOrgInfo: true,
  setIsFetchingOrgInfo: (isFetching: boolean) => {},
  allRelations: null,
  setAllRelations: (relations: AllRelationsFrontend) => {},
  currentLinks: null,
  setCurrentLinks: (links: Array<CurrentLink>) => {},
  orgRepData: null,
  setOrgRepData: (repData: Array<OrgRep>) => {},
  resourcesData: null,
  setResourcesData: (resourcesData: Array<ResourcesData>) => {},
  committers: null,
  setCommitters: (committersData: Array<CommitterOrContributor>) => {},
  contributors: null,
  setContributors: (contributorsData: Array<CommitterOrContributor>) => {},
  cbiData: null,
  setCBIData: (cbiData: CBIData) => {},
  chartCommits: null,
  setChartCommits: (commitsData: Array<number>) => {},
  chartCommitters: null,
  setChartCommitters: (committersData: Array<number>) => {},
  interestedProjectsData: null,
  setInterestedProjectsData: (interestedProjectsData: Array<ProjectsAndWGItem>) => {},
  yourProjectsData: null,
  setYourProjectsData: (yourProjectsData: Array<ProjectsAndWGItem>) => {},
  allYourProjectsData: null,
  setAllYourProjectsData: (allYourProjectsData: Array<ProjectsDataFrontend>) => {},
  yourWGData: null,
  setYourWGData: (yourWGData: Array<ProjectsAndWGItem>) => {},
  allYourWGData: null,
  setAllYourWGData: (allYourWGData: Array<ProjectsAndWGItem>) => {},
  interestedWGData: null,
  setInterestedWGData: (interestedWGData: Array<ProjectsAndWGItem>) => {},
  chartMonths: null,
  setChartMonths: (monthData: Array<string>) => {},
  currentUserPortal: null,
  setCurrentUserPortal: (userData: any) => {},
});

export default PortalContext;
