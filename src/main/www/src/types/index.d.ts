/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

 // User Models
export type UserRole = 'admin' | 'eclipsefdn_membership_portal_elevated_access';
export type OrganizationalRole = 
    | 'CA'
    | 'AD'
    | 'AC'
    | 'BR'
    | 'BA'
    | 'BABE'
    | 'CC'
    | 'EMPLY'
    | 'SDCP'
    | 'DE'
    | 'BRBE'
    | 'BRUS'
    | 'CI'
    | 'CL'
    | 'MA'
    | 'CR'
    | 'CRA'
    | 'PC'
    | 'TWGR'
    | 'WGR'
    | 'WGSC';
export type Role = UserRole | OrganizationalRole;
export interface User {
    name: string;
    givenName: string;
    familyName: string;
    relation: string[];
    roles: Role[];
};

// Organization Models
export type OrganizationLevel = 'SD' | 'OHAP' | 'AP' | 'AS';
export interface OrganizationLevelDetailed {
  level: OrganizationLevel;
  description: string;
  sortOrder: number;
}
export interface ParticipationAgreement {
  documentId: string;
  description: string | null;
  level: string | null;
  workingGroup: string;
}
export interface Organization {
  organizationId: number;
  name: string;
  description: {
    long: string;
  } | null;
  logos: {
    print: string | null;
    web: string | null;
  };
  website: string | null;
  levels: OrganizationLevelDetailed[];
  participationAgreements: ParticipationAgreement[];
}
