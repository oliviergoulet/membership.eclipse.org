SHELL = /bin/bash
## Quick start requirements
pre-setup:;
	[ -f ./config/mariadb/main/init.d/eclipsefoundation.sql ] && echo "EF DB dump already exists, skipping fetch" || (scp api-vm1:~/webdev/sql/eclipsefoundation.sql.gz ./config/mariadb/main/init.d/ && gzip -d ./config/mariadb/main/init.d/eclipsefoundation.sql.gz)
	touch ./config/application/secret.properties ./config/foundationdb/secret.properties ./config/portal/secret.properties ./config/wg/secret.properties
	mkdir -p volumes/imagestore
setup: clean pre-setup start-services;
	@echo "Requesting sudo to update imagestore permissions"
	@sudo chmod 755 ./volumes/imagestore && sudo chown 185:185 ./volumes/imagestore
dev-start: setup;
	(CONFIG_SECRET_PATH=$$PWD/config/application/secret.properties mvn compile quarkus:dev -Dconfig.secret.properties=$$PWD/config/application/secret.properties -Djava.net.preferIPv4Stack=true)

## Compilation tasks
clean:;
	mvn clean
	yarn --cwd application run clean
	yarn --cwd portal run clean
compile-java:compile-test-resources;
	mvn compile package
compile-java-quick: ;
	mvn compile package -Dmaven.test.skip=true
compile: clean compile-java;
compile-test-resources: install-njs;
	yarn --cwd application run generate-json-schema
	yarn --cwd portal run generate-json-schema
compile-quick: clean compile-java-quick;
install-njs:;
	yarn --cwd src/main/www install --frozen-lockfile --audit
	yarn --cwd application install --frozen-lockfile
	yarn --cwd portal install --frozen-lockfile
generate-cert:;
	rm -rf certs && mkdir -p certs
	openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout certs/www.rem.docker.key -out certs/www.rem.docker.crt

## Docker/service binds - services soft split from application to enable dev-start easier
compile-start: compile-quick start;
start:;
	docker compose build
	docker compose up -d
start-services: stop-services;
	docker compose up mariadb -d
start-opt: stop-opt;
	docker compose --profile optional-services up -d
stop: stop-application;
stop-application:;
	docker compose down
stop-services:;
	docker compose stop mariadb
stop-opt:;
	docker compose --profile optional-services down
stop-all: stop-application stop-services stop-opt;

