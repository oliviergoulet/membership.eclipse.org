/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*           Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.resources;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;

import org.eclipsefoundation.membership.portal.model.OrganizationProductData;
import org.eclipsefoundation.membership.portal.request.model.OrganizationInfoUpdateRequest;
import org.eclipsefoundation.membership.portal.test.namespaces.AuthParameters;
import org.eclipsefoundation.membership.portal.test.namespaces.SchemaNamespaces;
import org.eclipsefoundation.testing.helpers.AuthHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;

@QuarkusTest
class OrganizationResourceTest {

    public static final String ORGANIZATION_CONTACT_USER_NAME = "opearson";
    public static final String ORGANIZATION_SAMPLE_ID = "15";
    public static final String ORGANIZATION_PRODUCT_ID = "1";
    public static final String ORGANIZATION_ACTIVITY_PERIOD = "202002";

    public static final String ORGANIZATIONS_BASE_URL = "/organizations";
    public static final String ORGANIZATIONS_SLIM_URL = ORGANIZATIONS_BASE_URL + "/slim";
    public static final String ORGANIZATION_BY_ID_URL = ORGANIZATIONS_BASE_URL + "/{organizationID}";
    public static final String ORGANIZATION_ACTIVITY_URL = ORGANIZATION_BY_ID_URL + "/activity?period={period}";
    public static final String ORGANIZATION_ACTIVITY_OVERVIEW_URL = ORGANIZATION_BY_ID_URL + "/activity/overview";
    public static final String ORGANIZATION_ACTIVITY_YEARLY_URL = ORGANIZATION_BY_ID_URL + "/activity/yearly";
    public static final String ORGANIZATION_CBI_URL = ORGANIZATION_BY_ID_URL + "/cbi";

    public static final String ORGANIZATION_COMMITTERS_URL = ORGANIZATION_BY_ID_URL + "/committers";
    public static final String ORGANIZATION_COMMITTERS_ACTIVITY_URL = ORGANIZATION_COMMITTERS_URL + "/activity";
    public static final String ORGANIZATION_COMMITTERS_YEARLY_URL = ORGANIZATION_COMMITTERS_ACTIVITY_URL + "/yearly";

    public static final String ORGANIZATION_CONTRIBUTORS_URL = ORGANIZATION_BY_ID_URL + "/contributors";

    public static final String ORGANIZATION_REPRESENTATIVES_URL = ORGANIZATION_BY_ID_URL + "/representatives";
    public static final String ORGANIZATION_WGS_URL = ORGANIZATION_BY_ID_URL + "/working_groups";
    public static final String ORGANIZATION_PROJECTS_URL = ORGANIZATION_BY_ID_URL + "/projects";

    public static final String ORGANIZATION_COUNTRY_URL = ORGANIZATION_BY_ID_URL + "/country";
    public static final String ORGANIZATION_PRODUCTS_URL = ORGANIZATION_BY_ID_URL + "/products";
    public static final String ORGANIZATION_PRODUCT_BY_ID_URL = ORGANIZATION_PRODUCTS_URL + "/{productID}";

    public static final EndpointTestCase GET_ORGS_CASE = TestCaseHelper
            .buildSuccessCase(ORGANIZATIONS_BASE_URL, new String[] {}, SchemaNamespaces.ORGANIZATIONS_SCHEMA_PATH);

    public static final EndpointTestCase GET_SLIM_ORG_CASE = TestCaseHelper
            .buildSuccessCase(ORGANIZATIONS_SLIM_URL, new String[] {}, SchemaNamespaces.ORGANIZATIONS_SCHEMA_PATH);

    public static final EndpointTestCase GET_BY_ID_CASE = TestCaseHelper
            .buildSuccessCase(ORGANIZATION_BY_ID_URL, new String[] { ORGANIZATION_SAMPLE_ID }, SchemaNamespaces.ORGANIZATION_SCHEMA_PATH);

    public static final EndpointTestCase GET_ACTIVITY_CASE = TestCaseHelper
            .buildSuccessCase(ORGANIZATION_ACTIVITY_URL, new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_ACTIVITY_PERIOD },
                    SchemaNamespaces.COMMITTER_ACTIVITY_SCHEMA_PATH);

    public static final EndpointTestCase GET_ACTIVITY_OVERVIEW_CASE = TestCaseHelper
            .buildSuccessCase(ORGANIZATION_ACTIVITY_OVERVIEW_URL, new String[] { ORGANIZATION_SAMPLE_ID },
                    SchemaNamespaces.ORGANIZATION_ACTIVITY_SCHEMA_PATH);

    public static final EndpointTestCase GET_ACTIVITY_YEARLY_CASE = TestCaseHelper
            .buildSuccessCase(ORGANIZATION_ACTIVITY_YEARLY_URL, new String[] { ORGANIZATION_SAMPLE_ID },
                    SchemaNamespaces.ORGANIZATION_YEARLY_ACTIVITY_SCHEMA_PATH);

    public static final EndpointTestCase GET_CBI_CASE = TestCaseHelper
            .buildSuccessCase(ORGANIZATION_CBI_URL, new String[] { ORGANIZATION_SAMPLE_ID }, SchemaNamespaces.ORGANIZATION_CBI_SCHEMA_PATH);

    public static final EndpointTestCase GET_ORG_COMMITTERS_CASE = TestCaseHelper
            .buildSuccessCase(ORGANIZATION_COMMITTERS_URL, new String[] { ORGANIZATION_SAMPLE_ID }, SchemaNamespaces.PEOPLE_SCHEMA_PATH);

    public static final EndpointTestCase GET_ORG_COMMITTERS_ACTIVITY_CASE = TestCaseHelper
            .buildSuccessCase(ORGANIZATION_COMMITTERS_ACTIVITY_URL, new String[] { ORGANIZATION_SAMPLE_ID },
                    SchemaNamespaces.ORGANIZATION_ACTIVITY_SCHEMA_PATH);

    public static final EndpointTestCase GET_ORG_COMMITTERS_YEARLY_CASE = TestCaseHelper
            .buildSuccessCase(ORGANIZATION_COMMITTERS_YEARLY_URL, new String[] { ORGANIZATION_SAMPLE_ID },
                    SchemaNamespaces.ORGANIZATION_YEARLY_ACTIVITY_SCHEMA_PATH);

    public static final EndpointTestCase GET_ORG_CONTRIBUTORS_CASE = TestCaseHelper
            .buildSuccessCase(ORGANIZATION_CONTRIBUTORS_URL, new String[] { ORGANIZATION_SAMPLE_ID }, SchemaNamespaces.PEOPLE_SCHEMA_PATH);

    public static final EndpointTestCase GET_ORG_PROJECTS_CASE = TestCaseHelper
            .buildSuccessCase(ORGANIZATION_PROJECTS_URL, new String[] { ORGANIZATION_SAMPLE_ID }, SchemaNamespaces.PEOPLE_SCHEMA_PATH);

    public static final EndpointTestCase GET_ORG_PRODUCTS_CASE = TestCaseHelper
            .buildSuccessCase(ORGANIZATION_PRODUCTS_URL, new String[] { ORGANIZATION_SAMPLE_ID },
                    SchemaNamespaces.ORGANIZATION_PRODUCTS_SCHEMA_PATH);

    public static final EndpointTestCase GET_ORG_PRODUCT_CASE = TestCaseHelper
            .buildSuccessCase(ORGANIZATION_PRODUCT_BY_ID_URL, new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID },
                    SchemaNamespaces.ORGANIZATION_PRODUCT_SCHEMA_PATH);

    public static final EndpointTestCase POST_ORG_PRODUCT_CASE = TestCaseHelper
            .prepareTestCase(ORGANIZATION_PRODUCTS_URL, new String[] { ORGANIZATION_SAMPLE_ID }, null)
            .setStatusCode(Response.Status.NO_CONTENT.getStatusCode())
            .build();

    public static final EndpointTestCase PUT_ORG_PRODUCT_CASE = TestCaseHelper
            .buildSuccessCase(ORGANIZATION_PRODUCT_BY_ID_URL, new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID },
                    SchemaNamespaces.ORGANIZATION_PRODUCT_SCHEMA_PATH);

    public static final EndpointTestCase GET_ORG_REPRESENTATIVES_CASE = TestCaseHelper
            .buildSuccessCase(ORGANIZATION_REPRESENTATIVES_URL, new String[] { ORGANIZATION_SAMPLE_ID },
                    SchemaNamespaces.BASIC_PERSONS_SCHEMA_PATH);

    public static final EndpointTestCase GET_ORG_WGS_CASE = TestCaseHelper
            .buildSuccessCase(ORGANIZATION_WGS_URL, new String[] { ORGANIZATION_SAMPLE_ID }, SchemaNamespaces.WORKING_GROUPS_SCHEMA_PATH);

    @Inject
    ObjectMapper mapper;

    //
    // GET /organizations
    //
    @Test
    void getOrganizations_success() {
        EndpointTestBuilder.from(GET_ORGS_CASE).run();
    }

    @Test
    void getOrganizations_success_validateSchema() {
        EndpointTestBuilder.from(GET_ORGS_CASE).andCheckSchema().run();
    }

    @Test
    void getSlimOrganizations_success() {
        EndpointTestBuilder.from(GET_SLIM_ORG_CASE).run();
    }

    @Test
    void getSlimOrganizations_success_validateSchema() {
        EndpointTestBuilder.from(GET_SLIM_ORG_CASE).andCheckSchema().run();
    }

    @Test
    void getSlimOrganizations_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_SLIM_ORG_CASE).andCheckFormat().run();
    }

    //
    // GET /organizations/{organizationID}
    //
    @Test
    void getOrganization_success() {
        EndpointTestBuilder.from(GET_BY_ID_CASE).run();
    }

    @Test
    void getOrganization_success_validateSchema() {
        EndpointTestBuilder.from(GET_BY_ID_CASE).andCheckSchema().run();
    }

    @Test
    void getOrganization_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_BY_ID_CASE).andCheckFormat().run();
    }

    //
    // POST /organizations/{organizationID}
    //
    @Test
    void postOrganization_failure_noAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_BY_ID_URL, new String[] { ORGANIZATION_SAMPLE_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doPost(generateOrgUpdateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postOrganization_failure_csrfGuard() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_BY_ID_URL, new String[] { ORGANIZATION_SAMPLE_ID }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .doPost(generateOrgUpdateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postOrganization_success() {
        EndpointTestBuilder.from(GET_BY_ID_CASE).doPost(generateOrgUpdateSample()).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postOrganization_success_validateSchema() {
        EndpointTestBuilder.from(GET_BY_ID_CASE).doPost(generateOrgUpdateSample()).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postOrganization_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_BY_ID_CASE).doPost(generateOrgUpdateSample()).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ELEVATED_ROLE)
    void postOrganization_failure_elevatedAccess() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildForbiddenCase(ORGANIZATION_BY_ID_URL, new String[] { ORGANIZATION_SAMPLE_ID },
                                SchemaNamespaces.ERROR_SCHEMA_PATH))
                .doPost(generateOrgUpdateSample())
                .run();
    }

    //
    // GET /organizations/{organizationID}/activity - TESTS DISABLED DUE TO FAILURES, manually tested as working
    //

    // @Test
    void getOrganizationCommitterActivity_failure_noAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_ACTIVITY_URL, new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_ACTIVITY_PERIOD },
                                null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .run();
    }

    // @Test
    // @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationCommitterActivity_success() {
        EndpointTestBuilder.from(GET_ACTIVITY_CASE).run();
    }

    // @Test
    // @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationCommitterActivity_success_validateSchema() {
        EndpointTestBuilder.from(GET_ACTIVITY_CASE).andCheckSchema().run();
    }

    // @Test
    // @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationCommitterActivity_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_ACTIVITY_CASE).andCheckFormat().run();
    }

    // @Test
    // @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationCommitterActivity_failure_notfound() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildNotFoundCase(ORGANIZATION_ACTIVITY_URL, new String[] { ORGANIZATION_SAMPLE_ID, "1" }, null))
                .run();
    }

    // @Test
    // @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationCommitterActivity_failure_invalidOrg() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildNotFoundCase(ORGANIZATION_ACTIVITY_URL, new String[] { "invalid", ORGANIZATION_ACTIVITY_PERIOD }, null))
                .run();
    }

    // @Test
    // @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ELEVATED_ROLE)
    void getOrganizationCommitterActivity_success_elevatedAccess() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildSuccessCase(ORGANIZATION_ACTIVITY_URL, new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_ACTIVITY_PERIOD },
                                SchemaNamespaces.COMMITTER_ACTIVITY_SCHEMA_PATH))
                .run();
    }

    //
    // GET /organizations/{organizationID}/activity/overview
    //
    @Test
    void getOrganizationActivityOverview_failure_noAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_ACTIVITY_OVERVIEW_URL, new String[] { ORGANIZATION_SAMPLE_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationActivityOverview_success() {
        EndpointTestBuilder.from(GET_ACTIVITY_OVERVIEW_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationActivityOverview_success_validateSchema() {
        EndpointTestBuilder.from(GET_ACTIVITY_OVERVIEW_CASE).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationActivityOverview_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_ACTIVITY_OVERVIEW_CASE).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationActivityOverview_failure_invalidOrg() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildNotFoundCase(ORGANIZATION_ACTIVITY_OVERVIEW_URL, new String[] { "invalid" }, null))
                .run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ELEVATED_ROLE)
    void getOrganizationActivityOverview_success_elevatedAccess() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildSuccessCase(ORGANIZATION_ACTIVITY_OVERVIEW_URL, new String[] { ORGANIZATION_SAMPLE_ID },
                                SchemaNamespaces.ORGANIZATION_ACTIVITY_SCHEMA_PATH))
                .run();
    }

    //
    // GET /organizations/{organizationID}/activity/overview
    //
    @Test
    void getOrganizationActivityYearly_failure_noAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_ACTIVITY_YEARLY_URL, new String[] { ORGANIZATION_SAMPLE_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationActivityYearly_success() {
        EndpointTestBuilder.from(GET_ACTIVITY_YEARLY_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationActivityYearly_success_validateSchema() {
        EndpointTestBuilder.from(GET_ACTIVITY_YEARLY_CASE).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationActivityYearly_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_ACTIVITY_YEARLY_CASE).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationActivityYearly_failure_invalidOrg() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildNotFoundCase(ORGANIZATION_ACTIVITY_YEARLY_URL, new String[] { "invalid" }, null))
                .run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ELEVATED_ROLE)
    void getOrganizationActivityYearly_failure_elevatedAccess() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildSuccessCase(ORGANIZATION_ACTIVITY_YEARLY_URL, new String[] { ORGANIZATION_SAMPLE_ID },
                                SchemaNamespaces.ORGANIZATION_YEARLY_ACTIVITY_SCHEMA_PATH))
                .run();
    }

    /**
     * 
     */
    @Test
    void getCbiSponsorships_success() {
        EndpointTestBuilder.from(GET_CBI_CASE).run();
    }

    @Test
    void getCbiSponsorships_success_validateSchema() {
        EndpointTestBuilder.from(GET_CBI_CASE).andCheckSchema().run();
    }

    @Test
    void getCbiSponsorships_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_CBI_CASE).andCheckFormat().run();
    }

    @Test
    void getCbiSponsorships_failure_invalidOrg() {
        EndpointTestBuilder.from(TestCaseHelper.buildNotFoundCase(ORGANIZATION_CBI_URL, new String[] { "123456" }, null)).run();
    }

    /*
     * GET /organizations/{organizationID}/committers
     */
    @Test
    void getOrganizationCommiters_failure_noAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_COMMITTERS_URL, new String[] { ORGANIZATION_SAMPLE_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationCommiters_success() {
        EndpointTestBuilder.from(GET_ORG_COMMITTERS_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationCommiters_success_validateSchema() {
        EndpointTestBuilder.from(GET_ORG_COMMITTERS_CASE).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationCommiters_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_ORG_COMMITTERS_CASE).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationCommiters_failure_invalidOrg() {
        EndpointTestBuilder.from(TestCaseHelper.buildNotFoundCase(ORGANIZATION_COMMITTERS_URL, new String[] { "invalid" }, null)).run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ELEVATED_ROLE)
    void getOrganizationCommiters_success_elevatedAccess() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildSuccessCase(ORGANIZATION_COMMITTERS_URL, new String[] { ORGANIZATION_SAMPLE_ID },
                                SchemaNamespaces.PEOPLE_SCHEMA_PATH))
                .run();
    }

    /*
     * GET /organizations/{organizationID}/committers/activity
     */
    @Test
    void getOrganizationCommitersActivity_failure_noAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_COMMITTERS_ACTIVITY_URL, new String[] { ORGANIZATION_SAMPLE_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationCommitersActivity_success() {
        EndpointTestBuilder.from(GET_ORG_COMMITTERS_ACTIVITY_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationCommitersActivity_success_validateSchema() {
        EndpointTestBuilder.from(GET_ORG_COMMITTERS_ACTIVITY_CASE).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationCommitersActivity_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_ORG_COMMITTERS_ACTIVITY_CASE).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationCommitersActivity_failure_invalidOrg() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildNotFoundCase(ORGANIZATION_COMMITTERS_ACTIVITY_URL, new String[] { "invalid" }, null))
                .run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ELEVATED_ROLE)
    void getOrganizationCommitersActivity_success_elevatedAccess() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildSuccessCase(ORGANIZATION_COMMITTERS_ACTIVITY_URL, new String[] { ORGANIZATION_SAMPLE_ID },
                                SchemaNamespaces.ORGANIZATION_ACTIVITY_SCHEMA_PATH))
                .run();
    }

    /*
     * GET /organizations/{organizationID}/committers/activity/yearly
     */
    @Test
    void getOrganizationCommitersYearly_failure_noAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_COMMITTERS_YEARLY_URL, new String[] { ORGANIZATION_SAMPLE_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationCommitersYearly_success() {
        EndpointTestBuilder.from(GET_ORG_COMMITTERS_YEARLY_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationCommitersYearly_success_validateSchema() {
        EndpointTestBuilder.from(GET_ORG_COMMITTERS_YEARLY_CASE).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationCommitersYearly_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_ORG_COMMITTERS_YEARLY_CASE).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationCommitersYearly_failure_invalidOrg() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildNotFoundCase(ORGANIZATION_COMMITTERS_YEARLY_URL, new String[] { "invalid" }, null))
                .run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ELEVATED_ROLE)
    void getOrganizationCommitersYearly_success_elevatedAccess() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildSuccessCase(ORGANIZATION_COMMITTERS_YEARLY_URL, new String[] { ORGANIZATION_SAMPLE_ID },
                                SchemaNamespaces.ORGANIZATION_YEARLY_ACTIVITY_SCHEMA_PATH))
                .run();
    }

    /*
     * GET /organizations/{organizationID}/projects
     */

    @Test
    void getOrganizationProjects_success() {
        EndpointTestBuilder.from(GET_ORG_PROJECTS_CASE).run();
    }

    @Test
    void getOrganizationProjects_success_validateSchema() {
        EndpointTestBuilder.from(GET_ORG_PROJECTS_CASE).andCheckSchema().run();
    }

    @Test
    void getOrganizationProjects_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_ORG_PROJECTS_CASE).andCheckFormat().run();
    }

    @Test
    void getOrganizationProjects_failure_invalidOrg() {
        EndpointTestBuilder.from(TestCaseHelper.buildNotFoundCase(ORGANIZATION_PROJECTS_URL, new String[] { "invalid" }, null)).run();
    }

    /*
     * GET /organizations/{organizationID}/contributors
     */
    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationContributors_success() {
        EndpointTestBuilder.from(GET_ORG_CONTRIBUTORS_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationContributors_success_validateSchema() {
        EndpointTestBuilder.from(GET_ORG_CONTRIBUTORS_CASE).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationContributors_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_ORG_CONTRIBUTORS_CASE).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationContributors_failure_invalidOrg() {
        EndpointTestBuilder.from(TestCaseHelper.buildNotFoundCase(ORGANIZATION_CONTRIBUTORS_URL, new String[] { "invalid" }, null)).run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ELEVATED_ROLE)
    void getOrganizationContributors_success_elevatedAccess() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildSuccessCase(ORGANIZATION_CONTRIBUTORS_URL, new String[] { ORGANIZATION_SAMPLE_ID },
                                SchemaNamespaces.PEOPLE_SCHEMA_PATH))
                .run();
    }

    //
    // GET /organizations/{organizationID}/products
    //
    @Test
    void getOrganizationProducts_success() {
        EndpointTestBuilder.from(GET_ORG_PRODUCTS_CASE).run();
    }

    @Test
    void getOrganizationProducts_success_validateSchema() {
        EndpointTestBuilder.from(GET_ORG_PRODUCTS_CASE).andCheckSchema().run();
    }

    @Test
    void getOrganizationProducts_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_ORG_PRODUCTS_CASE).andCheckFormat().run();
    }

    @Test
    void getOrganizationProducts_failure_invalidOrg() {
        EndpointTestBuilder.from(TestCaseHelper.buildNotFoundCase(ORGANIZATION_PRODUCTS_URL, new String[] { "invalid" }, null)).run();
    }

    //
    // POST /organizations/{organizationID}/products
    //
    @Test
    void postOrganizationProducts_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_PRODUCTS_URL, new String[] { ORGANIZATION_SAMPLE_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doPost(generateProductSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postOrganizationProducts_failure_csrfGuard() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_PRODUCTS_URL, new String[] { ORGANIZATION_SAMPLE_ID }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .doPost(generateProductSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postOrganizationProducts_success() {
        EndpointTestBuilder.from(POST_ORG_PRODUCT_CASE).doPost(generateProductSample()).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postOrganizationProducts_success_validateRequestSchema() {
        String request = generateProductSample();
        Assertions.assertTrue(matchesJsonSchemaInClasspath(SchemaNamespaces.ORGANIZATION_PRODUCT_SCHEMA_PATH).matches(request));

        EndpointTestBuilder.from(POST_ORG_PRODUCT_CASE).doPost(request).run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ELEVATED_ROLE)
    void postOrganizationProducts_failure_elevatedAccess() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildForbiddenCase(ORGANIZATION_PRODUCTS_URL, new String[] { ORGANIZATION_SAMPLE_ID },
                                SchemaNamespaces.ERROR_SCHEMA_PATH))
                .doPost(generateProductSample())
                .run();
    }

    //
    // GET /organizations/{organizationID}/products/{productID}
    //
    @Test
    void getOrganizationProduct_success() {
        EndpointTestBuilder.from(GET_ORG_PRODUCT_CASE).run();
    }

    @Test
    void getOrganizationProduct_success_validateSchema() {
        EndpointTestBuilder.from(GET_ORG_PRODUCT_CASE).andCheckSchema().run();
    }

    @Test
    void getOrganizationProduct_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_ORG_PRODUCT_CASE).andCheckFormat().run();
    }

    @Test
    void getOrganizationProduct_failure_invalidOrg() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildNotFoundCase(ORGANIZATION_PRODUCT_BY_ID_URL, new String[] { "invalid", ORGANIZATION_PRODUCT_ID }, null))
                .run();
    }

    //
    // PUT /organizations/{organizationId}/products/{productID}
    //
    @Test
    void putOrganizationProductByID_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_PRODUCT_BY_ID_URL, new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID },
                                null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doPut(generateProductSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putOrganizationProductByID_failure_csrfGuard() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_PRODUCT_BY_ID_URL, new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID },
                                null)
                        .setDisableCsrf(true)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .build())
                .doPut(generateProductSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putOrganizationProductByID_failure_emptyBody() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_PRODUCT_BY_ID_URL, new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID },
                                null)
                        .setStatusCode(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
                        .build())
                .doPut(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putOrganizationProductByID_success() {
        EndpointTestBuilder.from(PUT_ORG_PRODUCT_CASE).doPut(generateProductSample()).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putOrganizationProductByID_success_validateSchema() {
        EndpointTestBuilder.from(PUT_ORG_PRODUCT_CASE).doPut(generateProductSample()).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putOrganizationProductByID_success_validateResponseFormat() {
        EndpointTestBuilder.from(PUT_ORG_PRODUCT_CASE).doPut(generateProductSample()).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putOrganizationProductByID_success_validateRequestSchema() {
        String request = generateProductSample();
        Assertions.assertTrue(matchesJsonSchemaInClasspath(SchemaNamespaces.ORGANIZATION_PRODUCT_SCHEMA_PATH).matches(request));

        EndpointTestBuilder.from(PUT_ORG_PRODUCT_CASE).doPut(request).run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ELEVATED_ROLE)
    void putOrganizationProductByID_failure_elevatedAccess() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildForbiddenCase(ORGANIZATION_PRODUCT_BY_ID_URL,
                                new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID }, SchemaNamespaces.ERROR_SCHEMA_PATH))
                .doPut(generateProductSample())
                .run();
    }

    //
    // DELETE /organizations/{organizationId}/products/{productID}
    //
    @Test
    void deleteOrganizationProductByID_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_PRODUCT_BY_ID_URL, new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID },
                                null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doDelete(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void deleteOrganizationProductByID_failure_csrfGuard() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_PRODUCT_BY_ID_URL, new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID },
                                null)
                        .setDisableCsrf(true)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .build())
                .doDelete(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void deleteOrganizationProductByID_success() {
        EndpointTestBuilder.from(PUT_ORG_PRODUCT_CASE).doDelete(null).run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ELEVATED_ROLE)
    void deleteOrganizationProductByID_failure_elevatedAccess() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildForbiddenCase(ORGANIZATION_PRODUCT_BY_ID_URL,
                                new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID }, SchemaNamespaces.ERROR_SCHEMA_PATH))
                .doDelete(null)
                .run();
    }

    // TODO: Add for POST /organizations/{organizationID}/logos

    //
    // GET /organizations/{organizationID}/representatives
    //
    @Test
    void getOrganizationRepresentatives_failure_noAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_REPRESENTATIVES_URL, new String[] { ORGANIZATION_SAMPLE_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doGet()
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationRepresentatives_failure_wrongOrg() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_REPRESENTATIVES_URL, new String[] { "20" }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .build())
                .doGet()
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationRepresentatives_success() {
        EndpointTestBuilder.from(GET_ORG_REPRESENTATIVES_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationRepresentatives_success_validateSchema() {
        EndpointTestBuilder.from(GET_ORG_REPRESENTATIVES_CASE).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationRepresentatives_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_ORG_REPRESENTATIVES_CASE).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationRepresentatives_failure_invalidOrg() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildNotFoundCase(ORGANIZATION_REPRESENTATIVES_URL, new String[] { "invalid" }, null))
                .run();
    }

    //
    // GET /organizations/{organizationID}/working_groups
    //
    @Test
    void getOrganizationWGs_success() {
        EndpointTestBuilder.from(GET_ORG_WGS_CASE).run();
    }

    @Test
    void getOrganizationWGs_success_validateSchema() {
        EndpointTestBuilder.from(GET_ORG_WGS_CASE).andCheckSchema().run();
    }

    @Test
    void getOrganizationWGs_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_ORG_WGS_CASE).andCheckFormat().run();
    }

    @Test
    void getOrganizationWGs_failure_invalidOrg() {
        EndpointTestBuilder.from(TestCaseHelper.buildNotFoundCase(ORGANIZATION_WGS_URL, new String[] { "invalid" }, null)).run();
    }

    private String generateProductSample() {
        try {
            return mapper
                    .writeValueAsString(OrganizationProductData
                            .builder()
                            .setDescription("description")
                            .setName("name")
                            .setOrganizationId(15)
                            .setProductId(1)
                            .setProductUrl("url")
                            .build());
        } catch (JsonProcessingException e) {
        }
        return null;
    }

    private String generateOrgUpdateSample() {
        try {
            return mapper
                    .writeValueAsString(OrganizationInfoUpdateRequest
                            .builder()
                            .setCompanyUrl("company.com")
                            .setDescription("Some description")
                            .build());
        } catch (JsonProcessingException e) {
        }
        return null;
    }
}
