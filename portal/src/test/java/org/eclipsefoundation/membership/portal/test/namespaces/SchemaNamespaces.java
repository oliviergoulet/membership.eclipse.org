/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.test.namespaces;

/**
 * Indicates the file paths for the various schema files used to validate test objects and formats.
 * 
 * @author Martin Lowe
 *
 */
public class SchemaNamespaces {
    public static final String BASE_SCHEMAS_PATH = "schemas/";
    public static final String BASE_SCHEMAS_PATH_SUFFIX = "-schema.json";

    public static final String MEMBERSHIP_FORM_SCHEMA_PATH = BASE_SCHEMAS_PATH + "membership-form" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String MEMBERSHIP_FORMS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "membership-forms" + BASE_SCHEMAS_PATH_SUFFIX;

    public static final String CONTACTS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "contacts" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String CONTACT_SCHEMA_PATH = BASE_SCHEMAS_PATH + "contact" + BASE_SCHEMAS_PATH_SUFFIX;

    public static final String FORM_ORGANIZATIONS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "form-organizations" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String FORM_ORGANIZATION_SCHEMA_PATH = BASE_SCHEMAS_PATH + "form-organization" + BASE_SCHEMAS_PATH_SUFFIX;

    public static final String PEOPLE_SCHEMA_PATH = BASE_SCHEMAS_PATH + "people" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String WORKING_GROUPS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "working-groups" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String WORKING_GROUP_SCHEMA_PATH = BASE_SCHEMAS_PATH + "working-group" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String ORGANIZATIONS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "organizations" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String ORGANIZATION_SCHEMA_PATH = BASE_SCHEMAS_PATH + "organization" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String SLIM_ORGANIZATIONS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "slim-organizations" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String SLIM_ORGANIZATION_SCHEMA_PATH = BASE_SCHEMAS_PATH + "slim-organization" + BASE_SCHEMAS_PATH_SUFFIX;

    public static final String COMMITTER_ACTIVITY_SCHEMA_PATH = BASE_SCHEMAS_PATH + "committer-activity-list" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String ORGANIZATION_CBI_SCHEMA_PATH = BASE_SCHEMAS_PATH + "organization-cbi" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String ORGANIZATION_CONTACTS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "organization-contacts" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String ORGANIZATION_CONTACT_SCHEMA_PATH = BASE_SCHEMAS_PATH + "organization-contact" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String ORGANIZATION_ACTIVITY_SCHEMA_PATH = BASE_SCHEMAS_PATH + "organization-activity" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String ORGANIZATION_YEARLY_ACTIVITY_SCHEMA_PATH = BASE_SCHEMAS_PATH + "organization-yearly-activity"
            + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String ORGANIZATION_PROJECTS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "projects" + BASE_SCHEMAS_PATH_SUFFIX;

    public static final String ORGANIZATION_PRODUCTS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "organization-products" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String ORGANIZATION_PRODUCT_SCHEMA_PATH = BASE_SCHEMAS_PATH + "organization-product" + BASE_SCHEMAS_PATH_SUFFIX;

    public static final String ENHANCED_PERSONS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "enhanced-persons" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String BASIC_PERSONS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "basic-persons" + BASE_SCHEMAS_PATH_SUFFIX;

    public static final String ERROR_SCHEMA_PATH = BASE_SCHEMAS_PATH + "error" + BASE_SCHEMAS_PATH_SUFFIX;

    public static final String SYS_RELATIONS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "sys-relations" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String SYS_RELATION_SCHEMA_PATH = BASE_SCHEMAS_PATH + "sys-relation" + BASE_SCHEMAS_PATH_SUFFIX;

    public static final String INFO_WRAPPER_SCHEMA_PATH = BASE_SCHEMAS_PATH + "info-wrapper" + BASE_SCHEMAS_PATH_SUFFIX;

    public static final String SLACK_RESPONSE_SCHEMA_PATH = BASE_SCHEMAS_PATH + "slack-response" + BASE_SCHEMAS_PATH_SUFFIX;

    private SchemaNamespaces() {
    }
}
