/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.dtos.eclipse;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class OrganizationInformationTest {

    @Inject
    FilterService filters;
    @Inject
    DefaultHibernateDao dao;

    private static final RequestWrapper WRAP = new FlatRequestWrapper(URI.create("https://membership.eclipse.org"));

    @Test
    void getByOrgId() {
        List<OrganizationInformation> addResults = persistOrgInfo(656);
        Assertions.assertFalse(addResults.isEmpty(),
                "OrganizationInformation persistence failed unexpectedly, cannot perform test");

        // Fetch the same record for comparison
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), "656");

        List<OrganizationInformation> getResults = dao
                .get(new RDBMSQuery<>(WRAP, filters.get(OrganizationInformation.class), params));
        Assertions.assertEquals(1, getResults.size(), "There should be one OrganizationInformation record found");
        Assertions.assertEquals(addResults.get(0), getResults.get(0), "The add and fetch result should be the same");

        // Test again with an invalid orgId
        params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), "42");

        getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(OrganizationInformation.class), params));
        Assertions.assertTrue(getResults.isEmpty(), "There should be 0 OrganizationInformation records found");
    }

    private List<OrganizationInformation> persistOrgInfo(int orgId) {
        OrganizationInformation orgInfo = new OrganizationInformation();
        orgInfo.setOrganizationID(orgId);
        orgInfo.setShortDescription("Great company");
        orgInfo.setLongDescription("this is a great company");
        orgInfo.setCompanyUrl("google.com");
        orgInfo.setSmallMime("image/jpg");
        orgInfo.setSmallWidth(300);
        orgInfo.setSmallHeight(300);
        orgInfo.setLargeMime("image/jpg");
        orgInfo.setLargeWidth(500);
        orgInfo.setLargeHeight(500);
        return dao.add(new RDBMSQuery<>(WRAP, filters.get(OrganizationInformation.class)), Arrays.asList(orgInfo));
    }
}
