/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Alternative;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.core.Response;

import org.apache.maven.shared.utils.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.model.OrganizationContactData;
import org.eclipsefoundation.foundationdb.client.model.OrganizationData;
import org.eclipsefoundation.foundationdb.client.model.PeopleData;
import org.eclipsefoundation.membership.portal.api.OrganizationAPI;

@Alternative()
@Priority(1)
@ApplicationScoped
@RestClient
public class MockOrganizationsAPI implements OrganizationAPI {

    public static final String ORGANIZATION_CONTACT_USER_NAME = "opearson";
    public static final String ORGANIZATION_SAMPLE_ID = "15";
    public static final String SECOND_ORGANIZATION_SAMPLE_ID = "20";

    private List<PeopleData> people;
    private List<OrganizationData> orgs;
    private List<OrganizationContactData> contacts;

    public MockOrganizationsAPI() {
        this.people = new ArrayList<>();
        this.people
                .addAll(Arrays
                        .asList(PeopleData
                                .builder()
                                .setBlog("blog url")
                                .setComments("comments")
                                .setEmail("some_email@dummy.co")
                                .setFax("fax")
                                .setFname("First name")
                                .setIssuesPending(false)
                                .setLatitude("lat")
                                .setLname("Last name")
                                .setLongitude("lon")
                                .setMember(true)
                                .setMemberPassword("password")
                                .setMemberSince(new Date())
                                .setMobile("mobile phone")
                                .setProvisioning("provisioning")
                                .setScrmGuid("scrm_guid")
                                .setType("type")
                                .setUnixAcctCreated(true)
                                .setPhone("phone number")
                                .setPersonID(ORGANIZATION_CONTACT_USER_NAME)
                                .build()));

        this.orgs = new ArrayList<>();
        this.orgs
                .addAll(Arrays
                        .asList(OrganizationData
                                .builder()
                                .setComments("")
                                .setFax("")
                                .setLatLong("")
                                .setMemberSince(new Date())
                                .setName1("")
                                .setName2("")
                                .setOrganizationID(Integer.valueOf(ORGANIZATION_SAMPLE_ID))
                                .setPhone("")
                                .setScrmGUID("")
                                .setTimestamp(new Date())
                                .build(),
                                OrganizationData
                                        .builder()
                                        .setComments("")
                                        .setFax("")
                                        .setLatLong("")
                                        .setMemberSince(new Date())
                                        .setName1("")
                                        .setName2("")
                                        .setOrganizationID(Integer.valueOf(SECOND_ORGANIZATION_SAMPLE_ID))
                                        .setPhone("")
                                        .setScrmGUID("")
                                        .setTimestamp(new Date())
                                        .build()));

        this.contacts = new ArrayList<>();
        this.contacts
                .addAll(Arrays
                        .asList(OrganizationContactData
                                .builder()
                                .setComments("")
                                .setOrganizationID(Integer.valueOf(ORGANIZATION_SAMPLE_ID))
                                .setPersonID(ORGANIZATION_CONTACT_USER_NAME)
                                .setRelation("CR")
                                .setTitle("sample")
                                .build()));
    }

    @Override
    public Response getOrganizationContacts(@BeanParam BaseAPIParameters baseParams, OrganizationRequestParams params) {

        return Response.ok(contacts.stream().filter(c -> c.getPersonID().equalsIgnoreCase(params.getPersonID()))).build();
    }

    @Override
    public Response getOrganizationContactsWithSearch(String id, @BeanParam BaseAPIParameters baseParams,
            OrganizationRequestParams params) {
        List<OrganizationContactData> results = contacts
                .stream()
                .filter(c -> c.getOrganizationID() == Integer.valueOf(id) && filterByOrgSearchParam(c, params))
                .collect(Collectors.toList());
        return Response.ok(results).build();
    }

    @Override
    public OrganizationContactData updateOrganizationContacts(String id, OrganizationContactData contact) {
        contacts.removeIf(c -> c.getOrganizationID() == Integer.valueOf(id) && c.getPersonID().equalsIgnoreCase(contact.getPersonID()));
        contacts.add(contact);
        return contact;
    }

    @Override
    public Response removeOrganizationContacts(String id, String personID, String relation) {
        List<OrganizationContactData> results = contacts
                .stream()
                .filter(c -> c.getOrganizationID() == Integer.valueOf(id) && c.getPersonID().equalsIgnoreCase(personID)
                        && c.getRelation().equalsIgnoreCase(relation))
                .collect(Collectors.toList());
        contacts.removeAll(results);
        return Response.ok(results).build();
    }

    @Override
    public Response getOrganizationContact(String id, String personID, @BeanParam BaseAPIParameters baseParams,
            OrganizationRequestParams params) {
        List<OrganizationContactData> results = contacts
                .stream()
                .filter(c -> c.getPersonID().equalsIgnoreCase(personID) && c.getOrganizationID() == Integer.valueOf(id))
                .collect(Collectors.toList());
        return Response.ok(results).build();
    }

    private boolean filterByOrgSearchParam(OrganizationContactData contact, OrganizationRequestParams params) {
        return StringUtils.isNotBlank(params.getRelation()) && contact.getRelation().equalsIgnoreCase(params.getRelation())
                && StringUtils.isNotBlank(params.getOrganizationID())
                && contact.getOrganizationID() == Integer.valueOf(params.getOrganizationID()) && !params.getDocumentIds().isEmpty()
                && params.getDocumentIds().stream().anyMatch(d -> d.equalsIgnoreCase(contact.getPersonID()));
    }
}
