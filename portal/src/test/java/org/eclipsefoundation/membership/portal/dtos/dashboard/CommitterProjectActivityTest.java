/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.dtos.dashboard;

import java.net.URI;
import java.util.List;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.membership.portal.daos.DashboardPersistenceDAO;
import org.eclipsefoundation.membership.portal.namespace.MembershipPortalParameterNames;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

// Tests disabled due to persistent failures that do not appear in live instances
//@QuarkusTest
class CommitterProjectActivityTest {

    @Inject
    DashboardPersistenceDAO dao;

    @Inject
    FilterService filters;

    private static final RequestWrapper WRAP = new FlatRequestWrapper(URI.create("https://www.membership.eclipse.org"));

    //@Test
    void testGetByCommitterId() {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.LOGIN.getName(), "opearson");
        List<CommitterProjectActivity> results = dao
                .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://www.membership.eclipse.org")),
                        filters.get(CommitterProjectActivity.class), params));
        Assertions.assertEquals(1, results.size(), "There should be a single CommitterProjectActivity result");

        // Test with invalid committer id
        params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.LOGIN.getName(), "invalid");
        results = dao.get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://www.membership.eclipse.org")), filters.get(CommitterProjectActivity.class), params));
        Assertions.assertEquals(0, results.size(), "There should be no CommitterProjectActivity results");
    }

    //@Test
    void testGetByProjectsId() {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.PROJECT.getName(), "test-project");
        List<CommitterProjectActivity> results = dao
                .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://www.membership.eclipse.org")),
                        filters.get(CommitterProjectActivity.class), params));
        Assertions.assertEquals(1, results.size(), "There should be a single CommitterProjectActivity result");

        // Test with invalid project id
        params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.PROJECT.getName(), "invalid");
        results = dao.get(new RDBMSQuery<>(WRAP, filters.get(CommitterProjectActivity.class), params));
        Assertions.assertEquals(0, results.size(), "There should be no CommitterProjectActivity results");
    }

    //@Test
    void testGetByOrgId() {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.ORGANIZATION_ID.getName(), "15");
        List<CommitterProjectActivity> results = dao.get(new RDBMSQuery<>(WRAP, filters.get(CommitterProjectActivity.class), params));
        Assertions.assertEquals(1, results.size(), "There should be a single CommitterProjectActivity result");

        // Test with invalid org id
        params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.ORGANIZATION_ID.getName(), "0");
        results = dao.get(new RDBMSQuery<>(WRAP, filters.get(CommitterProjectActivity.class), params));
        Assertions.assertEquals(0, results.size(), "There should be no CommitterProjectActivity results");
    }

    //@Test
    void testGetByPeriod() {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.PERIOD.getName(), "202002");
        List<CommitterProjectActivity> results = dao.get(new RDBMSQuery<>(WRAP, filters.get(CommitterProjectActivity.class), params));
        Assertions.assertEquals(1, results.size(), "There should be a single CommitterProjectActivity result");

        // Test with invalid from date
        params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.PERIOD.getName(), "202306");
        results = dao.get(new RDBMSQuery<>(WRAP, filters.get(CommitterProjectActivity.class), params));
        Assertions.assertEquals(0, results.size(), "There should be no CommitterProjectActivity results");
    }

    //@Test
    void testGetByFromPeriod() {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.FROM_PERIOD.getName(), "202001");
        List<CommitterProjectActivity> results = dao.get(new RDBMSQuery<>(WRAP, filters.get(CommitterProjectActivity.class), params));
        Assertions.assertEquals(1, results.size(), "There should be a single CommitterProjectActivity result");

        // Test with invalid start date
        params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.FROM_PERIOD.getName(), "202306");
        results = dao.get(new RDBMSQuery<>(WRAP, filters.get(CommitterProjectActivity.class), params));
        Assertions.assertEquals(0, results.size(), "There should be no CommitterProjectActivity results");
    }

    //@Test
    void testGetByHasOrganization() {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.HAS_ORGANIZATION.getName(), "true");
        List<CommitterProjectActivity> results = dao.get(new RDBMSQuery<>(WRAP, filters.get(CommitterProjectActivity.class), params));
        Assertions.assertEquals(1, results.size(), "There should be a single CommitterProjectActivity result");
    }
}
