/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*           Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.resources;

import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.membership.portal.daos.REMPersistenceDAO;
import org.eclipsefoundation.membership.portal.dtos.rem.Sessions;
import org.eclipsefoundation.membership.portal.namespace.MembershipPortalParameterNames;
import org.eclipsefoundation.membership.portal.test.namespaces.AuthParameters;
import org.eclipsefoundation.membership.portal.test.namespaces.SchemaNamespaces;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.testing.helpers.AuthHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.ConfigMetadata;
import io.quarkus.test.security.oidc.OidcSecurity;

/**
 * Tests OIDC Resource endpoints. Mainly looks to test the CSRF endpoint and useragent, as the login and logout
 * endpoints are stubs to facilitate better usage of the oauth extension.
 * 
 * @author Martin Lowe <martin.lowe@eclipsefoundation.org>
 * @author Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
 */
@QuarkusTest
class OIDCResourceTest {

    public static final String LOGIN_URL = "/login";
    public static final String CSRF_URL = "/csrf";
    public static final String USER_INFO_URL = "/userinfo";
    public static final String USER_INFO_ELEVATED_URL = USER_INFO_URL + "?organization_id={param1}&role={param2}";
    public static final String USER_INFO_ELEVATED_NO_ROLE_URL = USER_INFO_URL + "?organization_id={param1}";

    private static final Map<String, Object> USER_INFO_SUCCESS_BODY_PARAMS = Map
            .of(AuthHelper.GIVEN_NAME_CLAIM_KEY, AuthHelper.GIVEN_NAME_CLAIM_VALUE, AuthHelper.FAMILY_NAME_CLAIM_KEY,
                    AuthHelper.FAMILY_NAME_CLAIM_VALUE, "name", AuthHelper.TEST_USER_NAME);

    private static final Map<String, Object> USER_INFO_ELEVATED_SUCCESS_BODY_PARAMS = Map
            .of(AuthHelper.GIVEN_NAME_CLAIM_KEY, AuthParameters.ELEVATED_GIVEN_NAME_CLAIM_VALUE, AuthHelper.FAMILY_NAME_CLAIM_KEY,
                    AuthParameters.ELEVATED_FAMILY_NAME_CLAIM_VALUE, AuthHelper.NAME_KEY, AuthParameters.TEST_ELEVATED_USER_NAME,
                    AuthParameters.ADDITIONAL_ROLES, Arrays.asList(AuthParameters.PORTAL_ELEVATED_ROLE),
                    AuthParameters.ORGANIZATIONAL_ROLES + ".\"1\"", Arrays.asList("MA"));

    private static final Map<String, Object> USER_INFO_ELEVATED_NO_ROLE_SUCCESS_BODY_PARAMS = Map
            .of(AuthHelper.GIVEN_NAME_CLAIM_KEY, AuthParameters.ELEVATED_GIVEN_NAME_CLAIM_VALUE, AuthHelper.FAMILY_NAME_CLAIM_KEY,
                    AuthParameters.ELEVATED_FAMILY_NAME_CLAIM_VALUE, AuthHelper.NAME_KEY, AuthParameters.TEST_ELEVATED_USER_NAME,
                    AuthParameters.ADDITIONAL_ROLES, Arrays.asList(AuthParameters.PORTAL_ELEVATED_ROLE),
                    AuthParameters.ORGANIZATIONAL_ROLES + ".\"1\"", Arrays.asList("CR"));

    private static final Map<String, Object> USER_INFO_ELEVATED_NO_ORG_SUCCESS_BODY_PARAMS = Map
            .of(AuthHelper.GIVEN_NAME_CLAIM_KEY, AuthParameters.ELEVATED_GIVEN_NAME_CLAIM_VALUE, AuthHelper.FAMILY_NAME_CLAIM_KEY,
                    AuthParameters.ELEVATED_FAMILY_NAME_CLAIM_VALUE, AuthHelper.NAME_KEY, AuthParameters.TEST_ELEVATED_USER_NAME,
                    AuthParameters.ADDITIONAL_ROLES, Arrays.asList(AuthParameters.PORTAL_ELEVATED_ROLE),
                    AuthParameters.ORGANIZATIONAL_ROLES, Collections.emptyList());

    public static final EndpointTestCase LOGIN_CASE_SUCCESS = TestCaseHelper.prepareTestCase(LOGIN_URL, new String[] {}, null).build();

    public static final EndpointTestCase CSRF_CASE_SUCCESS = TestCaseHelper.prepareTestCase(CSRF_URL, new String[] {}, null).build();

    public static final EndpointTestCase USER_INFO_SUCCESS = TestCaseHelper
            .buildSuccessCase(USER_INFO_URL, new String[] {}, SchemaNamespaces.INFO_WRAPPER_SCHEMA_PATH);

    public static final EndpointTestCase USER_INFO_SUCCESS_BODY = TestCaseHelper
            .prepareTestCase(USER_INFO_URL, new String[] {}, null)
            .setBodyValidationParams(USER_INFO_SUCCESS_BODY_PARAMS)
            .build();

    public static final EndpointTestCase USER_INFO_ELEVATED_SUCCESS = TestCaseHelper
            .buildSuccessCase(USER_INFO_ELEVATED_URL, new String[] { "1", "MA" }, SchemaNamespaces.INFO_WRAPPER_SCHEMA_PATH);

    public static final EndpointTestCase USER_INFO_ELEVATED_AUTH_SUCCESS_BODY = TestCaseHelper
            .prepareTestCase(USER_INFO_ELEVATED_URL, new String[] { "1", "MA" }, SchemaNamespaces.INFO_WRAPPER_SCHEMA_PATH)
            .setBodyValidationParams(USER_INFO_ELEVATED_SUCCESS_BODY_PARAMS)
            .build();

    public static final EndpointTestCase USER_INFO_ELEVATED_NO_ROLE_AUTH_SUCCESS_BODY = TestCaseHelper
            .prepareTestCase(USER_INFO_ELEVATED_NO_ROLE_URL, new String[] { "1" }, SchemaNamespaces.INFO_WRAPPER_SCHEMA_PATH)
            .setBodyValidationParams(USER_INFO_ELEVATED_NO_ROLE_SUCCESS_BODY_PARAMS)
            .build();

    public static final EndpointTestCase USER_INFO_ELEVATED_NO_ORG_AUTH_SUCCESS_BODY = TestCaseHelper
            .prepareTestCase(USER_INFO_URL, new String[] {}, SchemaNamespaces.INFO_WRAPPER_SCHEMA_PATH)
            .setBodyValidationParams(USER_INFO_ELEVATED_NO_ORG_SUCCESS_BODY_PARAMS)
            .build();

    @Inject
    FilterService filters;
    @Inject
    REMPersistenceDAO dao;

    private static final RequestWrapper WRAP = new FlatRequestWrapper(URI.create("https://membership.eclipse.org"));

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void testLogin_success() {
        EndpointTestBuilder.from(LOGIN_CASE_SUCCESS).run();

        // Validate the login was successfully tracked
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.USERNAME.getName(), AuthHelper.TEST_USER_NAME);
        List<Sessions> results = dao.get(new RDBMSQuery<>(WRAP, filters.get(Sessions.class), params));

        // There is already a session tracked in the flyway SQL
        Assertions.assertEquals(2, results.size(), "There should be 2 Sessions entity found");
        Assertions.assertEquals(AuthHelper.TEST_USER_NAME, results.get(0).getUsername(), "The entity username should match the user logging in");
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void ensureCSRFEndpoint() {
        EndpointTestBuilder.from(CSRF_CASE_SUCCESS).run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void ensureCSRFEndpoint_elevatedAccess() {
        EndpointTestBuilder.from(CSRF_CASE_SUCCESS).run();
    }

    @Test
    void ensureCSRFEndpoint_noAuth() {
        EndpointTestBuilder.from(CSRF_CASE_SUCCESS).run();
    }

    @Test
    void userInfo_csrfGuard() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(USER_INFO_URL, new String[] {}, null)
                        .setStatusCode(Response.Status.NO_CONTENT.getStatusCode())
                        .build())
                .run();
    }

    @Test
    void userInfo_failure_noauth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(USER_INFO_URL, new String[] {}, null)
                        .setDisableCsrf(true)
                        .setStatusCode(Response.Status.NO_CONTENT.getStatusCode())
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void userInfo_success() {
        EndpointTestBuilder.from(USER_INFO_SUCCESS).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void userInfo_success_validateSchema() {
        EndpointTestBuilder.from(USER_INFO_SUCCESS).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void userInfo_success_validateResponseFormat() {
        EndpointTestBuilder.from(USER_INFO_SUCCESS).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void userInfo_success_bodyValidation() {
        EndpointTestBuilder.from(USER_INFO_SUCCESS_BODY).run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ELEVATED_ROLE)
    void userInfo_success_elevatedAccess() {
        EndpointTestBuilder.from(USER_INFO_ELEVATED_SUCCESS).run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ELEVATED_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthParameters.ELEVATED_EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthParameters.ELEVATED_GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthParameters.ELEVATED_FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void userInfo_success_elevatedAccess_validateSchema() {
        EndpointTestBuilder.from(USER_INFO_ELEVATED_SUCCESS).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ELEVATED_ROLE)
    void userInfo_success_elevatedAccess_validateResponseFormat() {
        EndpointTestBuilder.from(USER_INFO_ELEVATED_SUCCESS).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ELEVATED_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthParameters.ELEVATED_EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthParameters.ELEVATED_GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthParameters.ELEVATED_FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void userInfo_success_elevatedAccess_bodyValidation() {
        EndpointTestBuilder.from(USER_INFO_ELEVATED_AUTH_SUCCESS_BODY).run();
        EndpointTestBuilder.from(USER_INFO_ELEVATED_NO_ORG_AUTH_SUCCESS_BODY).run();
        EndpointTestBuilder.from(USER_INFO_ELEVATED_NO_ROLE_AUTH_SUCCESS_BODY).run();
    }
}
