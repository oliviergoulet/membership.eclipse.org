/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.helper;

import java.util.Optional;

import org.eclipsefoundation.membership.portal.exception.UnsupportedImageFormatException;
import org.eclipsefoundation.membership.portal.namespace.ImageStoreFormat;
import org.eclipsefoundation.membership.portal.namespace.ImageStoreFormat.ImageStoreFormats;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class ImageFileHelperTest {
    private static final String SEPARATOR = "-";

    @Test
    void sanitizeFileName_lowerCase() {
        String testString = "WOWWHATATEST";
        Assertions.assertEquals(testString.toLowerCase(),
                ImageFileHelper.sanitizeFileName(testString, Optional.empty()));

        ImageStoreFormats format = ImageStoreFormats.LARGE;
        Assertions.assertEquals(testString.toLowerCase() + SEPARATOR + format.getName().toLowerCase(),
                ImageFileHelper.sanitizeFileName(testString, Optional.of(format)));
    }

    @Test
    void sanitizeFileName_separator() {
        String testString = "wowwhatatest";
        ImageStoreFormats format = ImageStoreFormats.LARGE;
        Assertions.assertEquals(testString + SEPARATOR + format.getName().toLowerCase(),
                ImageFileHelper.sanitizeFileName(testString, Optional.of(format)));
    }

    @Test
    void sanitizeFileName_specialCharactersReplaced() {
        String testString = "wow@what;a:test";
        // build rather than type it so we can use variable separator for easier maint
        StringBuilder sb = new StringBuilder();
        sb.append("wow").append(SEPARATOR);
        sb.append("what").append(SEPARATOR);
        sb.append("a").append(SEPARATOR);
        sb.append("test");
        Assertions.assertEquals(sb.toString(), ImageFileHelper.sanitizeFileName(testString, Optional.empty()));
    }

    @Test
    void sanitizeFileName_specialCharactersReplaced_format() {
        String testString = "wow@what;a:test";
        ImageStoreFormat format = ImageStoreTestFormats.TEST;
        // build rather than type it so we can use variable separator for easier maint
        StringBuilder sb = new StringBuilder();
        sb.append("wow").append(SEPARATOR);
        sb.append("what").append(SEPARATOR);
        sb.append("a").append(SEPARATOR);
        sb.append("test").append(SEPARATOR);
        sb.append("some").append(SEPARATOR);
        sb.append("format");
        Assertions.assertEquals(sb.toString(), ImageFileHelper.sanitizeFileName(testString, Optional.of(format)));
    }

    @Test
    void sanitizeFileName_specialCharactersReplaced_trimLeadingTrailing() {
        String testString = "@wow@test!";
        ImageStoreFormat format = ImageStoreTestFormats.TEST;
        // build rather than type it so we can use variable separator for easier maint
        StringBuilder sb = new StringBuilder();
        sb.append("wow").append(SEPARATOR);
        sb.append("test").append(SEPARATOR);
        sb.append("some").append(SEPARATOR);
        sb.append("format");
        Assertions.assertEquals(sb.toString(), ImageFileHelper.sanitizeFileName(testString, Optional.of(format)));
    }

    @Test
    void convertMimeType_gif() {
        Assertions.assertEquals(".gif", ImageFileHelper.convertMimeType("image/gif"));
    }

    @Test
    void convertMimeType_png() {
        Assertions.assertEquals(".png", ImageFileHelper.convertMimeType("image/png"));
        Assertions.assertEquals(".png", ImageFileHelper.convertMimeType("image/x-png"));
    }

    @Test
    void convertMimeType_bmp() {
        Assertions.assertEquals(".bmp", ImageFileHelper.convertMimeType("image/bmp"));
    }

    @Test
    void convertMimeType_jpg() {
        Assertions.assertEquals(".jpg", ImageFileHelper.convertMimeType("image/jpeg"));
        Assertions.assertEquals(".jpg", ImageFileHelper.convertMimeType("image/pjpeg"));
    }

    @Test
    void convertMimeType_default() {
        Assertions.assertThrows(UnsupportedImageFormatException.class,
                () -> ImageFileHelper.convertMimeType("image/unknown-type"));
    }

    @Test
    void convertMimeTypeToExtension_gif() {
        Assertions.assertEquals("gif", ImageFileHelper.convertMimeTypeToExtension("image/gif"));
    }

    @Test
    void convertMimeTypeToExtension_png() {
        Assertions.assertEquals("png", ImageFileHelper.convertMimeTypeToExtension("image/png"));
        Assertions.assertEquals("png", ImageFileHelper.convertMimeTypeToExtension("image/x-png"));
    }

    @Test
    void convertMimeTypeToExtension_bmp() {
        Assertions.assertEquals("bmp", ImageFileHelper.convertMimeTypeToExtension("image/bmp"));
    }

    @Test
    void convertMimeTypeToExtension_jpg() {
        Assertions.assertEquals("jpg", ImageFileHelper.convertMimeTypeToExtension("image/jpeg"));
        Assertions.assertEquals("jpg", ImageFileHelper.convertMimeTypeToExtension("image/pjpeg"));
    }

    @Test
    void convertMimeTypeToExtension_default() {
        Assertions.assertThrows(UnsupportedImageFormatException.class,
                () -> ImageFileHelper.convertMimeTypeToExtension("image/unknown-type"));
    }

    public enum ImageStoreTestFormats implements ImageStoreFormat {
        TEST("@SOME_format");

        private String custName;

        private ImageStoreTestFormats(String custName) {
            this.custName = custName;
        }

        @Override
        public String getName() {
            return this.custName;
        }
    }
}
