/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.resources;

import java.time.Instant;
import java.util.Map;
import java.util.Optional;

import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.params.KeyParameter;
import org.eclipsefoundation.membership.portal.config.SlackConfig;
import org.eclipsefoundation.membership.portal.test.namespaces.SchemaNamespaces;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import jakarta.inject.Inject;

@QuarkusTest
class SlackWebhookResourceTest {

    private static final String BASE_URL = "/webhook/slack/m";
    private static final long NOW_STAMP = Instant.now().toEpochMilli();
    private static final String SAMPLE_INCOMPLETE_BODY = "&token=test-token&team_id=T123&team_domain=eclipsefoundation&channel_id=CHAN123&channel_name=webdev&user_id=USER123&user_name=tester&command=%2Fm";

    @Inject
    SlackConfig config;

    @Test
    void searchOrg_success() {
        // Should match against a member org by name
        String body = "text=sample" + SAMPLE_INCOMPLETE_BODY;
        Map<String, Object> VALID_HEADERS = Map.of("x-slack-request-timestamp", NOW_STAMP,
                "x-slack-signature", createSlackRequestHash(NOW_STAMP, body));

        EndpointTestCase testCase = TestCaseHelper
                .prepareTestCase(BASE_URL, new String[] {}, SchemaNamespaces.SLACK_RESPONSE_SCHEMA_PATH)
                .setStatusCode(200)
                .setHeaderParams(Optional.of(VALID_HEADERS))
                .setRequestContentType(ContentType.URLENC).build();

        EndpointTestBuilder.from(testCase).doPost(body).andCheckSchema().run();

        // Should match against a member org by orgId
        body = "text=15" + SAMPLE_INCOMPLETE_BODY;
        VALID_HEADERS = Map.of("x-slack-request-timestamp", NOW_STAMP,
                "x-slack-signature", createSlackRequestHash(NOW_STAMP, body));

        testCase = TestCaseHelper
                .prepareTestCase(BASE_URL, new String[] {}, SchemaNamespaces.SLACK_RESPONSE_SCHEMA_PATH)
                .setStatusCode(200)
                .setHeaderParams(Optional.of(VALID_HEADERS))
                .setRequestContentType(ContentType.URLENC).build();

        EndpointTestBuilder.from(testCase).doPost(body).andCheckSchema().run();
    }

    @Test
    void searchOrg_failure_notFound() {
        // Org does not exist
        String body = "text=nope" + SAMPLE_INCOMPLETE_BODY;
        Map<String, Object> VALID_HEADERS = Map.of("x-slack-request-timestamp", NOW_STAMP,
                "x-slack-signature", createSlackRequestHash(NOW_STAMP, body));

        // Return a 200 when member can't be found
        EndpointTestCase testCase = TestCaseHelper.prepareTestCase(BASE_URL, new String[] {}, null)
                .setStatusCode(200)
                .setHeaderParams(Optional.of(VALID_HEADERS))
                .setRequestContentType(ContentType.URLENC).build();
        ValidatableResponse response = EndpointTestBuilder.from(testCase).doPost(body).run();

        int attachmentSize = response.extract().body().jsonPath().getList("attachments").size();
        Assertions.assertEquals(0, attachmentSize, "There should be 0 attachments when a member is not found");
    }

    @Test
    void searchOrg_failure_invalidSearchParam() {
        // Search param is too short
        String body = "text=sa" + SAMPLE_INCOMPLETE_BODY;
        Map<String, Object> VALID_HEADERS = Map.of("x-slack-request-timestamp", NOW_STAMP,
                "x-slack-signature", createSlackRequestHash(NOW_STAMP, body));

        EndpointTestCase testCase = TestCaseHelper.prepareTestCase(BASE_URL, new String[] {}, null)
                .setStatusCode(200)
                .setHeaderParams(Optional.of(VALID_HEADERS))
                .setRequestContentType(ContentType.URLENC).build();
        ValidatableResponse response = EndpointTestBuilder.from(testCase).doPost(body).run();

        int attachmentSize = response.extract().body().jsonPath().getList("attachments").size();
        Assertions.assertEquals(0, attachmentSize, "There should be 0 attachments when a member is not found");

        // No search param set
        body = "text=" + SAMPLE_INCOMPLETE_BODY;
        VALID_HEADERS = Map.of("x-slack-request-timestamp", NOW_STAMP,
                "x-slack-signature", createSlackRequestHash(NOW_STAMP, body));

        testCase = TestCaseHelper.prepareTestCase(BASE_URL, new String[] {}, null)
                .setStatusCode(200)
                .setHeaderParams(Optional.of(VALID_HEADERS))
                .setRequestContentType(ContentType.URLENC).build();
        response = EndpointTestBuilder.from(testCase).doPost(body).run();

        attachmentSize = response.extract().body().jsonPath().getList("attachments").size();
        Assertions.assertEquals(0, attachmentSize, "There should be 0 attachments when a member is not found");
    }

    @Test
    void searchOrg_failure_invalidHeaderParams() {
        // Valid body, but hash won't match
        String body = "text=sample" + SAMPLE_INCOMPLETE_BODY;
        Map<String, Object> INVALID_HEADERS = Map.of("x-slack-request-timestamp", NOW_STAMP,
                "x-slack-signature", "baab4104853a8bd32bf8a3c4dd5181fd");

        EndpointTestCase testCase = TestCaseHelper.prepareTestCase(BASE_URL, new String[] {}, null)
                .setStatusCode(403)
                .setHeaderParams(Optional.of(INVALID_HEADERS))
                .setRequestContentType(ContentType.URLENC).build();
        // Will fail due to a hash mismatch
        EndpointTestBuilder.from(testCase).doPost(body).run();
    }

    // Mocks the hashing done by slack
    private String createSlackRequestHash(long timestamp, String body) {
        String signatureBase = "v0:" + timestamp + ":" + body;
        HMac sha256HMac = new HMac(new SHA256Digest());
        sha256HMac.init(new KeyParameter(config.signingSecret().getBytes()));
        sha256HMac.update(signatureBase.getBytes(), 0, signatureBase.getBytes().length);

        // Compute the final stage into a byte array
        byte[] hmacOut = new byte[sha256HMac.getMacSize()];
        sha256HMac.doFinal(hmacOut, 0);

        // Convert the byte array to it's hex equivalent as a string
        StringBuilder sb = new StringBuilder(hmacOut.length * 2);
        for (byte by : hmacOut) {
            sb.append(String.format("%02x", by));
        }
        return "v0=" + sb.toString();
    }
}
