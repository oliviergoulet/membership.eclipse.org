/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.resources;

import org.eclipsefoundation.membership.portal.test.namespaces.SchemaNamespaces;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class SysResourceTest {
    public static final String SYS_URL_BASE = "/sys/relations";
    public static final String SYS_URL_TYPE = SYS_URL_BASE + "?type={type}";

    public static final EndpointTestCase GET_SUCCESS_CASE = TestCaseHelper
            .buildSuccessCase(SYS_URL_TYPE, new String[] { "CO" }, SchemaNamespaces.SYS_RELATIONS_SCHEMA_PATH);

    @Test
    void getSysRelations_success() {
        EndpointTestBuilder.from(GET_SUCCESS_CASE).run();
    }

    @Test
    void getSysRelations_success_validateSchema() {
        EndpointTestBuilder.from(GET_SUCCESS_CASE).andCheckSchema().run();
    }

    @Test
    void getSysRelations_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_SUCCESS_CASE).andCheckFormat().run();
    }

    @Test
    void getSysRelations_success_noType() {
        EndpointTestBuilder.from(TestCaseHelper.buildSuccessCase(SYS_URL_BASE, new String[] {}, null)).run();
    }

    @Test
    void getSysRelations_success_invalidType() {
        EndpointTestBuilder.from(TestCaseHelper.buildSuccessCase(SYS_URL_TYPE, new String[] { "admin" }, null)).run();
    }
}
