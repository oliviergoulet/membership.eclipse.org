/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Alternative;
import jakarta.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.efservices.api.WorkingGroupsAPI;
import org.eclipsefoundation.efservices.api.models.WorkingGroup;
import org.eclipsefoundation.efservices.api.models.WorkingGroup.WorkingGroupParticipationAgreement;
import org.eclipsefoundation.efservices.api.models.WorkingGroup.WorkingGroupParticipationAgreements;
import org.eclipsefoundation.efservices.api.models.WorkingGroup.WorkingGroupParticipationLevel;
import org.eclipsefoundation.efservices.api.models.WorkingGroup.WorkingGroupResources;

@Alternative()
@Priority(1)
@ApplicationScoped
@RestClient
public class MockWorkingGroupsAPI implements WorkingGroupsAPI {
    private List<WorkingGroup> internal;

    public MockWorkingGroupsAPI() {
        this.internal = Arrays
                .asList(WorkingGroup
                        .builder()
                        .setAlias("adoptium")
                        .setDescription("description")
                        .setLevels(Arrays
                                .asList(WorkingGroupParticipationLevel.builder().setDescription("sample")
                                        .setRelation("WGSD").build()))
                        .setLogo("http://sample.com")
                        .setParentOrganization("eclipse")
                        .setResources(WorkingGroupResources
                                .builder()
                                .setCharter("")
                                .setContactForm("")
                                .setMembers("members")
                                .setWebsite("website")
                                .setSponsorship("sponsorship")
                                .setParticipationAgreements(WorkingGroupParticipationAgreements
                                        .builder()
                                        .setIndividual(WorkingGroupParticipationAgreement
                                                .builder()
                                                .setDocumentId("32c5a745df264aa2a809")
                                                .setPdf("pdf.link")
                                                .build())
                                        .build())
                                .build())
                        .setStatus("active")
                        .setTitle("Sample wg")
                        .build());
    }

    @Override
    public Response get(BaseAPIParameters arg0) {
        return Response.ok(new ArrayList<>(internal)).build();
    }

    @Override
    public Response getAllByStatuses(BaseAPIParameters baseParams, List<String> statuses) {
        return Response
                .ok(internal.stream().filter(wg -> statuses.contains(wg.getStatus())).collect(Collectors.toList()))
                .build();
    }
}
