/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.test.api;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.efservices.api.ProjectsAPI;
import org.eclipsefoundation.efservices.api.models.GitlabProject;
import org.eclipsefoundation.efservices.api.models.InterestGroup;
import org.eclipsefoundation.efservices.api.models.InterestGroup.Descriptor;
import org.eclipsefoundation.efservices.api.models.InterestGroup.InterestGroupParticipant;
import org.eclipsefoundation.efservices.api.models.InterestGroup.Organization;
import org.eclipsefoundation.efservices.api.models.InterestGroup.Resource;

import io.quarkus.test.Mock;

@Mock
@ApplicationScoped
@RestClient
public class MockCommonsProjectsAPI implements ProjectsAPI {

    private List<InterestGroup> igs;

    public MockCommonsProjectsAPI() {
        this.igs = Arrays
                .asList(InterestGroup
                        .builder()
                        .setDescription(Descriptor.builder().setFull("full").setSummary("summary").build())
                        .setProjectId("foundationdbProjectId")
                        .setGitlab(GitlabProject
                                .builder()
                                .setIgnoredSubGroups(Collections.emptyList())
                                .setProjectGroup("projectGroup")
                                .build())
                        .setId("123456")
                        .setLeads(Arrays.asList(InterestGroupParticipant.builder().setUrl("url")
                                .setUsername("opearson")
                                .setFullName("Oli Pearson")
                                .setOrganization(Organization.builder()
                                        .setDocuments(Collections.emptyMap())
                                        .setId("656")
                                        .setName("org").build())
                                .build()))
                        .setLogo("logo")
                        .setParticipants(Arrays.asList())
                        .setScope(Descriptor.builder().setFull("full").setSummary("summary").build())
                        .setState("active")
                        .setTitle("Some title")
                        .setShortProjectId("title")
                        .setResources(Resource.builder().setMembers("members").setWebsite("google.com").build())
                        .setMailingList("mailinglist.com")
                        .build());
    }

    @Override
    public Response getInterestGroups(BaseAPIParameters params) {
        return Response.ok(igs).build();
    }

    @Override
    public Response getProjects(BaseAPIParameters arg0, int arg1) {
        throw new UnsupportedOperationException("Unimplemented method 'getProjects'");
    }
}
