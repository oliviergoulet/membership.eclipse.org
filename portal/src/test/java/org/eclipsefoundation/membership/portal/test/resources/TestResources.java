/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.test.resources;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * Used for enhanced CORS filter testing.
 * 
 * @author Martin Lowe
 *
 */
@Path("test")
@Produces(MediaType.APPLICATION_JSON)
public class TestResources {

    /**
     * Covered by test1 CORS config
     * 
     * @return ok response
     */
    @GET
    @Path("1")
    public Response get() {
        return Response.ok().build();
    }

    /**
     * Covered by test1 CORS config
     * 
     * @return ok response
     */
    @GET
    @Path("1/sub")
    public Response getFirstSubresource() {
        return Response.ok().build();
    }

    /**
     * Covered by test1 CORS config
     * 
     * @return ok response
     */
    @PUT
    @Path("1/sub")
    public Response putFirstSubresource() {
        return Response.ok().build();
    }

    /**
     * Covered by test1 CORS config
     * 
     * @return ok response
     */
    @GET
    @Path("1/other")
    public Response getFirstOtherResource() {
        return Response.ok().build();
    }

    /**
     * Covered by no CORS config (wrong method, get not covered)
     * 
     * @return ok response
     */
    @GET
    @Path("2")
    public Response otherGet() {
        return Response.ok().build();
    }

    /**
     * Covered by test2 CORS config
     * 
     * @return ok response
     */
    @POST
    @Path("2")
    public Response post() {
        return Response.ok().build();
    }

    /**
     * Covered by test2 CORS config
     * 
     * @return ok response
     */
    @POST
    @Path("2/sub")
    public Response postSecondSubresource() {
        return Response.ok().build();
    }

    /**
     * Covered by no CORS config (not wildcard/regex matched endpoints)
     * 
     * @return ok response
     */
    @POST
    @Path("2/other")
    public Response postSecondOtherResource() {
        return Response.ok().build();
    }
}
