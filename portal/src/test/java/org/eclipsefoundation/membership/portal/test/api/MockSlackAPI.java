/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.test.api;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.membership.portal.api.SlackAPI;
import org.eclipsefoundation.membership.portal.model.SlackResponse;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.Response;

@RestClient
@ApplicationScoped
public class MockSlackAPI implements SlackAPI {

    @Override
    public Response postSlackPayload(String path, SlackResponse payload) {
        return Response.ok().build();
    }
}
