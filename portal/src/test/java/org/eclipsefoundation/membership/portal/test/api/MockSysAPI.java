/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.test.api;

import java.util.Arrays;
import java.util.List;

import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Alternative;
import jakarta.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.model.SysModLogData;
import org.eclipsefoundation.foundationdb.client.model.SysRelationData;
import org.eclipsefoundation.membership.portal.api.SysAPI;

@Alternative()
@Priority(1)
@ApplicationScoped
@RestClient
public class MockSysAPI implements SysAPI {

    private final List<SysRelationData> relations;

    public MockSysAPI() {
        this.relations = Arrays.asList(
                SysRelationData.builder().setRelation("ABRT").setDescription("Aborted/Lost").setActive(true)
                        .setType("TS").build(),
                SysRelationData.builder().setRelation("AC").setDescription("Architecture Council Representative")
                        .setActive(true).setType("CO").build(),
                SysRelationData.builder().setRelation("AD").setDescription("Admin Contact").setActive(true)
                        .setType("CO").build(),
                SysRelationData.builder().setRelation("AP").setDescription("Solutions Member").setActive(true)
                        .setType("ME").build(),
                SysRelationData.builder().setRelation("AR").setDescription("Add-In Provider Representative")
                        .setActive(true).setType("GE").build(),
                SysRelationData.builder().setRelation("AS").setDescription("Associate Member").setActive(true)
                        .setType("ME").build(),
                SysRelationData.builder().setRelation("AZ").setDescription("Architecture Council Chair").setActive(true)
                        .setType("GE").build(),
                SysRelationData.builder().setRelation("BITBK").setDescription("External ID: BitBucket").setActive(true)
                        .setType("ID").build(),
                SysRelationData.builder().setRelation("BR").setDescription("Board Rep").setActive(true).setType("CO")
                        .build(),
                SysRelationData.builder().setRelation("CA").setDescription("Accounting Contact").setActive(true)
                        .setType("CO").build(),
                SysRelationData.builder().setRelation("CAD").setDescription("Canadian Dollars").setActive(true)
                        .setType("CR").build(),
                SysRelationData.builder().setRelation("CB").setDescription("Committer Board Representative")
                        .setActive(true).setType("GE").build(),
                SysRelationData.builder().setRelation("CC").setDescription("Committer Member").setActive(true)
                        .setType("CO").build(),
                SysRelationData.builder().setRelation("CE").setDescription("Committer Emeritus").setActive(true)
                        .setType("PR").build(),
                SysRelationData.builder().setRelation("CEPND").setDescription("Committer Emeritus - Pending")
                        .setActive(true).setType("PR").build(),
                SysRelationData.builder().setRelation("CI").setDescription("IT Contact").setActive(true).setType("CO")
                        .build(),
                SysRelationData.builder().setRelation("CL").setDescription("Legal Contact").setActive(true)
                        .setType("CO").build(),
                SysRelationData.builder().setRelation("CM").setDescription("Committer").setActive(true).setType("PR")
                        .build(),
                SysRelationData.builder().setRelation("CN").setDescription("Contributor").setActive(true).setType("PR")
                        .build(),
                SysRelationData.builder().setRelation("CO").setDescription("Contact").setActive(true).setType("GE")
                        .build(),
                SysRelationData.builder().setRelation("CONFQ").setDescription("Eclipse Conference Organizer")
                        .setActive(true).setType("GE").build(),
                SysRelationData.builder().setRelation("CR").setDescription("Company Representative").setActive(true)
                        .setType("CO").build(),
                SysRelationData.builder().setRelation("CS").setDescription("Sales Contact").setActive(true)
                        .setType("CO").build(),
                SysRelationData.builder().setRelation("DE").setDescription("Delegate").setActive(true).setType("CO")
                        .build(),
                SysRelationData.builder().setRelation("EA").setDescription("Appointed-Architecture Council")
                        .setActive(true).setType("GE").build());
    }

    @Override
    public Response getSysRelations(BaseAPIParameters baseParams, String type) {
        return Response.ok(relations).build();
    }

    @Override
    public List<SysModLogData> postModLog(SysModLogData modLog) {
        return Arrays.asList(modLog);
    }

}
