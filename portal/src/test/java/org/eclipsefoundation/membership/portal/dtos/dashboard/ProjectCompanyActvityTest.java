/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.dtos.dashboard;

import java.net.URI;
import java.util.List;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.membership.portal.daos.DashboardPersistenceDAO;
import org.eclipsefoundation.membership.portal.namespace.MembershipPortalParameterNames;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class ProjectCompanyActvityTest {

    @Inject
    DashboardPersistenceDAO dao;

    @Inject
    FilterService filters;

    private static final RequestWrapper WRAP = new FlatRequestWrapper(URI.create("https://membership.eclipse.org"));

    @Test
    void testGetByProjectId() {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.PROJECT.getName(), "test-project");
        List<ProjectCompanyActivity> results = dao
                .get(new RDBMSQuery<>(WRAP, filters.get(ProjectCompanyActivity.class), params));

        Assertions.assertEquals(1, results.size(), "There should be a single record in the DB");
        Assertions.assertEquals("test-project", results.get(0).getCompositeId().getProject().getId(),
                "The projet ids should be the same");

        // Perform the test again with an invalid project id
        params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.PROJECT.getName(), "invalid-project");
        results = dao.get(new RDBMSQuery<>(WRAP, filters.get(ProjectCompanyActivity.class), params));

        Assertions.assertTrue(results.isEmpty(), "There should be no matching records in the DB");
    }

    @Test
    void testGetByOrgId() {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.ORGANIZATION_ID.getName(), "15");
        List<ProjectCompanyActivity> results = dao
                .get(new RDBMSQuery<>(WRAP, filters.get(ProjectCompanyActivity.class), params));

        Assertions.assertEquals(1, results.size(), "There should be a single record in the DB");
        Assertions.assertEquals(15, results.get(0).getCompositeId().getOrgId(),
                "The org ids should be the same");

        // Perform the test again with an invalid org id
        params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.ORGANIZATION_ID.getName(), "0");
        results = dao.get(new RDBMSQuery<>(WRAP, filters.get(ProjectCompanyActivity.class), params));

        Assertions.assertTrue(results.isEmpty(), "There should be no matching records in the DB");
    }

    @Test
    void testGetByHasOrganization() {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.HAS_ORGANIZATION.getName(), "true");
        List<ProjectCompanyActivity> results = dao
                .get(new RDBMSQuery<>(WRAP, filters.get(ProjectCompanyActivity.class), params));

        Assertions.assertEquals(1, results.size(), "There should be a single record in the DB");
    }
}
