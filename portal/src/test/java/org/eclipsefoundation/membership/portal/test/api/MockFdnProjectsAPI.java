/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Alternative;
import jakarta.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.model.ProjectData;
import org.eclipsefoundation.membership.portal.api.ProjectAPI;

@Alternative()
@Priority(1)
@ApplicationScoped
@RestClient
public class MockFdnProjectsAPI implements ProjectAPI {

    private List<ProjectData> internal;

    public MockFdnProjectsAPI() {
        internal = new ArrayList<>();
        internal.addAll(Arrays.asList(
                ProjectData.builder()
                        .setProjectID("test-project")
                        .setActive(true)
                        .setBugsName("bug")
                        .setComponent(false)
                        .setDateActive(null)
                        .setDescription("description")
                        .setDiskQuotaGB(4)
                        .setLevel(2)
                        .setName("Test Project")
                        .setParentProjectID("eclipse")
                        .setProjectPhase("Regular")
                        .setSortOrder(0)
                        .setStandard(false)
                        .setUrlDownload("another url")
                        .setUrlIndex("some url")
                        .build()

        ));
    }

    @Override
    public Response getProjects(BaseAPIParameters baseParams, String organizationID) {
        return Response.ok(internal).build();
    }

    @Override
    public ProjectData getProject(BaseAPIParameters baseParams, String projectID) {
        return internal.stream().filter(p -> p.getProjectID().equalsIgnoreCase(projectID)).findFirst().orElse(null);
    }
}
