/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.model.PeopleData;
import org.eclipsefoundation.membership.portal.api.PeopleAPI;

@RestClient
@ApplicationScoped
public class MockPeopleAPI implements PeopleAPI {

    List<PeopleData> internal;

    public MockPeopleAPI() {
        internal = new ArrayList<>();
        internal.addAll(Arrays.asList(PeopleData.builder()
                .setEmail("oli.pearson@eclipse.org")
                .setPersonID("opearson")
                .setFname("first name")
                .setLname("last name")
                .setType("member")
                .setMember(false)
                .setUnixAcctCreated(false)
                .setIssuesPending(false)
                .build()));
    }

    @Override
    public Response getPeople(BaseAPIParameters baseParams, PeopleRequestParams params) {
        List<PeopleData> results = internal.stream().filter(p -> p.getPersonID().equalsIgnoreCase(params.getUsername()))
                .collect(Collectors.toList());
        return Response.ok(results).build();
    }

}
