/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.resources;

import java.util.List;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;

import org.eclipsefoundation.membership.portal.request.model.ContactRemovalRequest;
import org.eclipsefoundation.membership.portal.service.MailerService;
import org.eclipsefoundation.membership.portal.test.namespaces.AuthParameters;
import org.eclipsefoundation.membership.portal.test.namespaces.SchemaNamespaces;
import org.eclipsefoundation.testing.helpers.AuthHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.mailer.Mail;
import io.quarkus.mailer.MockMailbox;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectSpy;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.ConfigMetadata;
import io.quarkus.test.security.oidc.OidcSecurity;

@QuarkusTest
class OrganizationContactsResourceTest {

    public static final String ORGANIZATION_CONTACT_USER_NAME = "opearson";
    public static final String ORGANIZATION_SAMPLE_ID = "15";
    public static final String ORGANIZATION_SAMPLE_RELATION = "DE";
    public static final String ORGANIZATION_SAMPLE_RELATION_FAIL = "CR";

    public static final String ORGANIZATION_CONTACTS_URL = "/organizations/{organizationID}/contacts";
    public static final String ORGANIZATION_CONTACT_BY_ID_URL = ORGANIZATION_CONTACTS_URL + "/{contactID}";
    public static final String ORGANIZATION_CONTACT_RELATION_URL = ORGANIZATION_CONTACT_BY_ID_URL + "/{relation}";

    public static final EndpointTestCase GET_ORG_CONTACTS_CASE = TestCaseHelper
            .buildSuccessCase(ORGANIZATION_CONTACTS_URL, new String[] { ORGANIZATION_SAMPLE_ID },
                    SchemaNamespaces.ENHANCED_PERSONS_SCHEMA_PATH);

    public static final EndpointTestCase GET_ORG_CONTACT_CASE = TestCaseHelper
            .buildSuccessCase(ORGANIZATION_CONTACT_BY_ID_URL, new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME },
                    SchemaNamespaces.ORGANIZATION_CONTACTS_SCHEMA_PATH);

    public static final EndpointTestCase GET_ORG_CONTACT_RELATION_CASE = TestCaseHelper
            .buildSuccessCase(ORGANIZATION_CONTACT_RELATION_URL,
                    new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME, ORGANIZATION_SAMPLE_RELATION },
                    SchemaNamespaces.ORGANIZATION_CONTACTS_SCHEMA_PATH);

    public static final EndpointTestCase DELETE_RELATION_400_CASE = TestCaseHelper
            .buildBadRequestCase(ORGANIZATION_CONTACT_RELATION_URL,
                    new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME, ORGANIZATION_SAMPLE_RELATION_FAIL },
                    SchemaNamespaces.ERROR_SCHEMA_PATH);

    public static final EndpointTestCase DELETE_RELATION_FORBIDDEN_CASE = TestCaseHelper
            .buildForbiddenCase(ORGANIZATION_CONTACT_RELATION_URL,
                    new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME, ORGANIZATION_SAMPLE_RELATION },
                    SchemaNamespaces.ERROR_SCHEMA_PATH);

    public static final EndpointTestCase DELETE_CONTACT_FORBIDDEN_CASE = TestCaseHelper
            .buildForbiddenCase(ORGANIZATION_CONTACT_BY_ID_URL, new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME },
                    SchemaNamespaces.ERROR_SCHEMA_PATH);

    public static final EndpointTestCase POST_CONTACT_FORBIDDEN_CASE = TestCaseHelper
            .buildForbiddenCase(ORGANIZATION_CONTACT_RELATION_URL,
                    new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME, ORGANIZATION_SAMPLE_RELATION },
                    SchemaNamespaces.ERROR_SCHEMA_PATH);

    @Inject
    ObjectMapper mapper;

    @InjectSpy
    MailerService mailer;

    @Inject
    MockMailbox mailbox;

    //
    // GET /organizations/{organizationID}/contacts
    //
    @Test
    void getOrganizationContacts_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_CONTACTS_URL, new String[] { ORGANIZATION_SAMPLE_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationContacts_failure_csrfGuard() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_CONTACTS_URL, new String[] { ORGANIZATION_SAMPLE_ID }, null)
                        .setDisableCsrf(true)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .build())
                .run();
    }

    // TODO: Disabled as it is registering as a 403 in testing, but is 404 when manually hit
    //@Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationContacts_failure_badOrg() {
        EndpointTestBuilder.from(TestCaseHelper.buildNotFoundCase(ORGANIZATION_CONTACTS_URL, new String[] { "not-org" }, null)).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationContacts_success() {
        EndpointTestBuilder.from(GET_ORG_CONTACTS_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationContacts_success_validateSchema() {
        EndpointTestBuilder.from(GET_ORG_CONTACTS_CASE).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationContacts_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_ORG_CONTACTS_CASE).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ELEVATED_ROLE)
    void getOrganizationContacts_success_elevatedAccess() {
        EndpointTestBuilder.from(GET_ORG_CONTACTS_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ADMIN_USER_NAME, roles = AuthParameters.PORTAL_ADMIN_ROLE)
    void getOrganizationContacts_success_admin() {
        EndpointTestBuilder.from(GET_ORG_CONTACTS_CASE).run();
    }

    //
    // GET /organizations/{organizationID}/contacts/{contactId}
    //
    @Test
    void getOrganizationContact_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_CONTACT_BY_ID_URL,
                                new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationContact_failure_csrfGuard() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_CONTACT_BY_ID_URL,
                                new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .run();
    }

    // TODO: Disabled as it is registering as a 403 in testing, but is 404 when manually hit
    //@Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationContact_failure_badOrg() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildNotFoundCase(ORGANIZATION_CONTACT_BY_ID_URL, new String[] { "not-org", ORGANIZATION_CONTACT_USER_NAME },
                                null))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationContact_success() {
        EndpointTestBuilder.from(GET_ORG_CONTACT_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationContact_success_validateSchema() {
        EndpointTestBuilder.from(GET_ORG_CONTACT_CASE).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationContact_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_ORG_CONTACT_CASE).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ELEVATED_ROLE)
    void getOrganizationContact_success_elevatedAccess() {
        EndpointTestBuilder.from(GET_ORG_CONTACT_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ADMIN_USER_NAME, roles = AuthParameters.PORTAL_ADMIN_ROLE)
    void getOrganizationContact_success_admin() {
        EndpointTestBuilder.from(GET_ORG_CONTACT_CASE).run();
    }

    //
    // DELETE /organizations/{organizationID}/contacts/{contactId}
    //
    @Test
    void deleteOrganizationContact_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_CONTACT_BY_ID_URL,
                                new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doDelete(generateRemovalRequestSample(Integer.valueOf(ORGANIZATION_SAMPLE_ID)))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void deleteOrganizationContact_failure_csrfGuard() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_CONTACT_BY_ID_URL,
                                new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .doDelete(generateRemovalRequestSample(Integer.valueOf(ORGANIZATION_SAMPLE_ID)))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = "email", value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void deleteOrganizationContact_success() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildSuccessCase(ORGANIZATION_CONTACT_BY_ID_URL,
                                new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME }, null))
                .doDelete(generateRemovalRequestSample(Integer.valueOf(ORGANIZATION_SAMPLE_ID)))
                .run();

        // Verify the mailer is sending the same data passed by client
        Mockito
                .verify(mailer)
                .sendContactRemovalRequest(ContactRemovalRequest
                        .builder()
                        .setOrganizationId(Integer.valueOf(ORGANIZATION_SAMPLE_ID))
                        .setUsername(ORGANIZATION_CONTACT_USER_NAME)
                        .setReason("No reason")
                        .build());

        // Validate membership mail is properly sent
        List<Mail> inbox = mailbox.getMessagesSentTo("test-to@eclipse-foundation.org");
        Assertions.assertEquals(1, inbox.size());
        Assertions.assertEquals(1, inbox.get(0).getBcc().size());
        Assertions.assertTrue(inbox.get(0).getBcc().stream().anyMatch(s -> "membership-bcc@eclipse-foundation.org".equalsIgnoreCase(s)));
        Assertions.assertEquals("membership-reply-to@eclipse-foundation.org", inbox.get(0).getReplyTo());

        // Validate author mail is properly sent
        inbox = mailbox.getMessagesSentTo(AuthHelper.EMAIL_CLAIM_VALUE);
        Assertions.assertEquals(1, inbox.size());
        Assertions.assertEquals(1, inbox.get(0).getBcc().size());
        Assertions.assertTrue(inbox.get(0).getBcc().stream().anyMatch(s -> "author-bcc@eclipse-foundation.org".equalsIgnoreCase(s)));
        Assertions.assertEquals("author-reply-to@eclipse-foundation.org", inbox.get(0).getReplyTo());
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void deleteOrganizationContact_failure_badOrganization() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildBadRequestCase(ORGANIZATION_CONTACT_BY_ID_URL,
                                new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME }, null))
                .doDelete(generateRemovalRequestSample(10))
                .run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ELEVATED_ROLE)
    void deleteOrganizationContact_failure_elevatedAccess() {
        EndpointTestBuilder
                .from(DELETE_CONTACT_FORBIDDEN_CASE)
                .doDelete(generateRemovalRequestSample(Integer.valueOf(ORGANIZATION_SAMPLE_ID)))
                .run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ADMIN_USER_NAME, roles = AuthParameters.PORTAL_ADMIN_ROLE)
    void deleteOrganizationContact_failure_admin() {
        EndpointTestBuilder
                .from(DELETE_CONTACT_FORBIDDEN_CASE)
                .doDelete(generateRemovalRequestSample(Integer.valueOf(ORGANIZATION_SAMPLE_ID)))
                .run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_BOTH_ROLES_USER_NAME, roles = { AuthParameters.PORTAL_ADMIN_ROLE,
            AuthParameters.PORTAL_ELEVATED_ROLE })
    void deleteOrganizationContact_failure_adminAndElevated() {
        EndpointTestBuilder
                .from(DELETE_CONTACT_FORBIDDEN_CASE)
                .doDelete(generateRemovalRequestSample(Integer.valueOf(ORGANIZATION_SAMPLE_ID)))
                .run();
    }

    //
    // GET /organizations/{organizationID}/contacts/{contactId}/{relation}
    //
    @Test
    void getOrganizationContactRelation_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_CONTACT_RELATION_URL,
                                new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME, ORGANIZATION_SAMPLE_RELATION }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationContactRelation_failure_csrfGuard() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_CONTACT_RELATION_URL,
                                new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME, ORGANIZATION_SAMPLE_RELATION }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationContactRelation_success() {
        EndpointTestBuilder.from(GET_ORG_CONTACT_RELATION_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationContactRelation_success_validateSchema() {
        EndpointTestBuilder.from(GET_ORG_CONTACT_RELATION_CASE).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getOrganizationContactRelation_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_ORG_CONTACT_RELATION_CASE).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ELEVATED_ROLE)
    void getOrganizationContactRelation_success_elevatedAccess() {
        EndpointTestBuilder.from(GET_ORG_CONTACT_RELATION_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ADMIN_USER_NAME, roles = AuthParameters.PORTAL_ADMIN_ROLE)
    void getOrganizationContactRelation_success_admin() {
        EndpointTestBuilder.from(GET_ORG_CONTACT_RELATION_CASE).run();
    }

    //
    // POST /organizations/{organizationID}/contacts/{contactId}/{relation}
    //
    @Test
    void postOrganizationContactRelation_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_CONTACT_RELATION_URL,
                                new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME, ORGANIZATION_SAMPLE_RELATION }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doPost(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postOrganizationContactRelation_failure_csrfGuard() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_CONTACT_RELATION_URL,
                                new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME, ORGANIZATION_SAMPLE_RELATION }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .doPost(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postOrganizationContactRelation_failure_invalidRelation() {
        EndpointTestBuilder.from(DELETE_RELATION_400_CASE).doPost(null).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postOrganizationContactRelation_failure_invalidRelation_validateSchema() {
        EndpointTestBuilder.from(DELETE_RELATION_400_CASE).doPost(null).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postOrganizationContactRelation_success() {
        EndpointTestBuilder.from(GET_ORG_CONTACT_RELATION_CASE).doPost(null).run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postOrganizationContactRelation_failure_elevatedAccess() {
        EndpointTestBuilder.from(POST_CONTACT_FORBIDDEN_CASE).doPost(null).run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ADMIN_USER_NAME, roles = AuthParameters.PORTAL_ADMIN_ROLE)
    void postOrganizationContactRelation_failure_admin() {
        EndpointTestBuilder.from(POST_CONTACT_FORBIDDEN_CASE).doPost(null).run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_BOTH_ROLES_USER_NAME, roles = { AuthParameters.PORTAL_ADMIN_ROLE,
            AuthParameters.PORTAL_ELEVATED_ROLE })
    void postOrganizationContactRelation_failure_adminAndElevated() {
        EndpointTestBuilder.from(POST_CONTACT_FORBIDDEN_CASE).doPost(null).run();
    }

    //
    // DELETE /organizations/{organizationID}/contacts/{contactId}/{relation}
    //
    @Test
    void deleteOrganizationContactRelation_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_CONTACT_RELATION_URL,
                                new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME, ORGANIZATION_SAMPLE_RELATION }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doDelete(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void deleteOrganizationContactRelation_failure_csrfGuard() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(ORGANIZATION_CONTACT_RELATION_URL,
                                new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME, ORGANIZATION_SAMPLE_RELATION }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .doDelete(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void deleteOrganizationContactRelation_failure_badRelation() {
        EndpointTestBuilder.from(DELETE_RELATION_400_CASE).doDelete(null).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void deleteOrganizationContactRelation_failure_badRelation_validateSchema() {
        EndpointTestBuilder.from(DELETE_RELATION_400_CASE).doDelete(null).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void deleteOrganizationContactRelation_success() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildSuccessCase(ORGANIZATION_CONTACT_RELATION_URL,
                                new String[] { ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME, ORGANIZATION_SAMPLE_RELATION },
                                null))
                .doDelete(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ELEVATED_USER_NAME, roles = AuthParameters.PORTAL_ELEVATED_ROLE)
    void deleteOrganizationContactRelation_failure_elevatedAccess() {
        EndpointTestBuilder.from(DELETE_RELATION_FORBIDDEN_CASE).doDelete(null).run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_ADMIN_USER_NAME, roles = AuthParameters.PORTAL_ADMIN_ROLE)
    void deleteOrganizationContactRelation_failure_admin() {
        EndpointTestBuilder.from(DELETE_RELATION_FORBIDDEN_CASE).doDelete(null).run();
    }

    @Test
    @TestSecurity(user = AuthParameters.TEST_BOTH_ROLES_USER_NAME, roles = { AuthParameters.PORTAL_ADMIN_ROLE,
            AuthParameters.PORTAL_ELEVATED_ROLE })
    void deleteOrganizationContactRelation_failure_adminElevated() {
        EndpointTestBuilder.from(DELETE_RELATION_FORBIDDEN_CASE).doDelete(null).run();
    }

    private String generateRemovalRequestSample(int orgId) {
        try {
            return mapper
                    .writeValueAsString(ContactRemovalRequest
                            .builder()
                            .setOrganizationId(orgId)
                            .setUsername(ORGANIZATION_CONTACT_USER_NAME)
                            .setReason("No reason")
                            .build());
        } catch (JsonProcessingException e) {
        }
        return null;
    }
}
