/**
 * Copyright (c) 2022, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.test.api;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.response.PaginatedResultsFilter;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.model.EnhancedOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.model.MemberOrganizationData;
import org.eclipsefoundation.foundationdb.client.model.PeopleData;
import org.eclipsefoundation.foundationdb.client.model.SysRelationData;
import org.eclipsefoundation.membership.portal.api.ComplexAPI;
import org.eclipsefoundation.membership.portal.api.OrganizationAPI.OrganizationRequestParams;

@ApplicationScoped
@RestClient
public class MockComplexAPI implements ComplexAPI {

    private List<MemberOrganizationData> memberOrgs;
    private List<EnhancedOrganizationContactData> orgContacts;

    @PostConstruct
    void init() {
        this.memberOrgs = Arrays
                .asList(MemberOrganizationData
                        .builder()
                        .setDocuments(Collections.emptyList())
                        .setName("Sample member org")
                        .setMemberSince(LocalDate.now())
                        .setLatestMembershipStart(LocalDate.now())
                        .setOrganizationID(15)
                        .setRelations(Collections.emptyList())
                        .build());

        this.orgContacts = Arrays.asList(EnhancedOrganizationContactData.builder()
                .setOrganizationId(15)
                .setIsCommitter(true)
                .setRelations(Arrays.asList(SysRelationData.builder()
                        .setRelation("CR")
                        .setActive(true)
                        .setDescription("some desc")
                        .setType("CO")
                        .build()))
                .setPerson(PeopleData.builder()
                        .setBlog("blog url")
                        .setComments("comments")
                        .setEmail("some_email@dummy.co")
                        .setFax("fax")
                        .setFname("First name")
                        .setIssuesPending(false)
                        .setLatitude("lat")
                        .setLname("Last name")
                        .setLongitude("lon")
                        .setMember(true)
                        .setMemberPassword("password")
                        .setMemberSince(new Date())
                        .setMobile("mobile phone")
                        .setProvisioning("provisioning")
                        .setScrmGuid("scrm_guid")
                        .setType("type")
                        .setUnixAcctCreated(true)
                        .setPhone("phone number")
                        .setPersonID("opearson")
                        .build())
                .build());
    }

    @Override
    public Response getContacts(BaseAPIParameters params, String orgId, String personId) {
        return Response.ok(orgContacts.stream().filter(c -> c.getOrganizationId() == Integer.valueOf(orgId))
                .collect(Collectors.toList())).build();
    }

    @Override
    public List<MemberOrganizationData> getMember(String id) {
        Integer orgId = Integer.valueOf(id);
        return memberOrgs.stream().filter(o -> o.getOrganizationID() == orgId).collect(Collectors.toList());
    }

    @Override
    public Response getMembers(BaseAPIParameters params, OrganizationRequestParams orgParams) {
        return Response
                .ok(new ArrayList<>(memberOrgs))
                .header(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER, 25)
                .header(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER, memberOrgs.size())
                .build();
    }
}
