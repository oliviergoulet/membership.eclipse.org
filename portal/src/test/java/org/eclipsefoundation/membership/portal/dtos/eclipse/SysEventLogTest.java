/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.dtos.eclipse;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.portal.namespace.MembershipPortalParameterNames;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class SysEventLogTest {

    @Inject
    FilterService filters;
    @Inject
    DefaultHibernateDao dao;

    private static final RequestWrapper WRAP = new FlatRequestWrapper(URI.create("https://membership.eclipse.org"));

    @Test
    void getByLogId() {
        // Add a record for testing
        List<SysEventLog> addResults = persistEventLog(656, "personId", "orgId");
        Assertions.assertFalse(addResults.isEmpty(),
                "SysEventLog persistence failed unexpectedly, cannot perform test");

        // Fetch the same record for comparison
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), "656");

        List<SysEventLog> getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(SysEventLog.class), params));
        Assertions.assertEquals(1, getResults.size(), "There should be one SysEventLog record found");
        Assertions.assertEquals(addResults.get(0), getResults.get(0), "The add and fetch result should be the same");

        // Test again with an invalid logId
        params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), "42");

        getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(SysEventLog.class), params));
        Assertions.assertTrue(getResults.isEmpty(), "There should be 0 SysEventLog records found");
    }

    @Test
    void getByUid() {
        // Add a record for testing
        List<SysEventLog> addResults = persistEventLog(123, "some-person", "pk1");
        Assertions.assertFalse(addResults.isEmpty(),
                "SysEventLog persistence failed unexpectedly, cannot perform test");

        // Fetch the same record for comparison
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.USERNAME.getName(), "some-person");

        List<SysEventLog> getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(SysEventLog.class), params));
        Assertions.assertEquals(1, getResults.size(), "There should be one SysEventLog record found");
        Assertions.assertEquals(addResults.get(0), getResults.get(0), "The add and fetch result should be the same");

        // Test again with an invalid uid
        params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.USERNAME.getName(), "nope");

        getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(SysEventLog.class), params));
        Assertions.assertTrue(getResults.isEmpty(), "There should be 0 SysEventLog records found");
    }

    @Test
    void getByPk1() {
        // Add a record for testing
        List<SysEventLog> addResults = persistEventLog(1234, "uid", "656");
        Assertions.assertFalse(addResults.isEmpty(),
                "SysEventLog persistence failed unexpectedly, cannot perform test");

        // Fetch the same record for comparison
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.ORGANIZATION_ID.getName(), "656");

        List<SysEventLog> getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(SysEventLog.class), params));
        Assertions.assertEquals(1, getResults.size(), "There should be one SysEventLog record found");
        Assertions.assertEquals(addResults.get(0), getResults.get(0), "The add and fetch result should be the same");

        // Test again with an invalid uid
        params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.ORGANIZATION_ID.getName(), "nope");

        getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(SysEventLog.class), params));
        Assertions.assertTrue(getResults.isEmpty(), "There should be 0 SysEventLog records found");
    }

    private List<SysEventLog> persistEventLog(int logId, String uid, String pk1) {
        SysEventLog event = new SysEventLog();
        event.setLogId(logId);
        event.setLogTable("sessions");
        event.setPk1(pk1);
        event.setPk2("server name");
        event.setUid(uid);
        event.setLogAction("INSERT");
        event.setEvtDateTime(LocalDateTime.now());

        return dao.add(new RDBMSQuery<>(WRAP, filters.get(SysEventLog.class)), Arrays.asList(event));
    }
}
