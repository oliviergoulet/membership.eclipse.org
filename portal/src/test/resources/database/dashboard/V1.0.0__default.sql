CREATE TABLE Committer (
  id varchar(128) NOT NULL,
  first varchar(128) NOT NULL,
  last varchar(128) NOT NULL,
  email varchar(128) NOT NULL,
  country varchar(2) DEFAULT NULL,
  ts timestamp NOT NULL
);
INSERT INTO Committer(id,first,last,email,country,ts)
  VALUES('opearson', 'first name', 'last name', 'a@a.com', 'CA', CURRENT_TIMESTAMP());

CREATE TABLE CommitterAffiliation (
  id varchar(128) NOT NULL,
  orgId int DEFAULT NULL,
  orgName varchar(128) NOT NULL,
  activeDate date DEFAULT NULL,
  inactiveDate date DEFAULT NULL,
  ts timestamp NOT NULL
);
INSERT INTO CommitterAffiliation(id, orgId, orgName, activeDate, inactiveDate, ts)
  VALUES('opearson', 15, 'Sample org', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

CREATE TABLE CommitterProjectActivity (
  login varchar(128) NOT NULL,
  project varchar(128) NOT NULL,
  period varchar(6) NOT NULL DEFAULT '',
  count bigint NOT NULL DEFAULT 0
);
INSERT INTO CommitterProjectActivity(login, project, period, count)
  VALUES('opearson', 'test-project', '202002', 15);
  
CREATE TABLE Project (
  id varchar(128) NOT NULL,
  parentId varchar(128) DEFAULT NULL,
  short varchar(32) NOT NULL,
  name varchar(256) NOT NULL,
  license varchar(128) DEFAULT NULL,
  specification varchar(32) DEFAULT NULL,
  url varchar(256) NOT NULL,
  start date DEFAULT NULL
);
INSERT INTO Project(id,short,name,license,specification,url,start)
  VALUES('test-project', 'test.project', 'Test project', 'EPL 2.0', 'specification url', 'some.url', CURRENT_TIMESTAMP());

CREATE TABLE ProjectCompanyActivity (
  project varchar(128) NOT NULL,
  company varchar(128) DEFAULT NULL,
  orgId int DEFAULT NULL,
  count bigint NOT NULL DEFAULT 0
);
INSERT INTO ProjectCompanyActivity(project, company, orgId, count)
  VALUES('test-project', 'Sample org', '15', 15);