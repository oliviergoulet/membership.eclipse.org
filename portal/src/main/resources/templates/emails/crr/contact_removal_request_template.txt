Eclipse Foundation AISBL Contact Update  
Date submitted: {now}  
  
New Contact Removal Request by: {name} ({current})  
  
Organization: {data.getOrganizationId}  
Username: {data.getUsername}  
Reason: {data.getReason}  
  
----------

This is an automatic message from https://membership.eclipse.org
Twitter: @EclipseFdn
