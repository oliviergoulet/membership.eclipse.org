/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.membership.portal.model.ContactRemovalRequestData;
import org.eclipsefoundation.membership.portal.request.model.ContactRemovalRequest;
import org.eclipsefoundation.membership.portal.service.MailerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.mailer.Mail;
import io.quarkus.mailer.Mailer;
import io.quarkus.qute.Location;
import io.quarkus.qute.Template;
import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.config.ConfigMapping;
import io.smallrye.jwt.auth.principal.DefaultJWTCallerPrincipal;

/**
 * Default implementation of the mailer service using the Qute templating engine
 * and the mailer extensions built into Quarkus. Application scoped so that it
 * can be spied with Mockito.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class DefaultMailerService implements MailerService {
    public static final Logger LOGGER = LoggerFactory.getLogger(DefaultMailerService.class);

    private static final String EMAIL_DATA_VAR = "data";
    private static final String EMAIL_NOW_VAR = "now";
    private static final String EMAIL_NAME_VAR = "name";
    private static final String EMAIL_CURRENT_USER_VAR = "current";
    private static final String IS_PDF_VAR = "isPDF";

    @Inject
    EclipseMailerConfig mailerConfig;

    @Inject
    SecurityIdentity ident;
    @Inject
    Mailer mailer;

    @Location("emails/crr/contact_removal_request_web_template")
    Template contactRemovalRequestWeb;
    @Location("emails/crr/contact_removal_request_template")
    Template contactRemovalRequest;

    @Location("emails/crr/contact_removal_notification_web_template")
    Template contactRemovalNotificationWeb;
    @Location("emails/crr/contact_removal_notification_template")
    Template contactRemovalNotification;

    @Override
    public void sendContactRemovalRequest(ContactRemovalRequest request) {
        checkRemovalRequest(request);
        // get information about the logged in user
        DefaultJWTCallerPrincipal defaultPrin = (DefaultJWTCallerPrincipal) ident.getPrincipal();
        String name = generateName(defaultPrin);
        // send the mail
        Mail m = Mail
                .withHtml("",
                        String.format("[Contact Removal Request] - Request to remove user '%s' from organization %s",
                                request.getUsername(), request.getOrganizationId()),
                        contactRemovalRequestWeb.data(EMAIL_DATA_VAR, request, EMAIL_NAME_VAR, name, EMAIL_NOW_VAR,
                                LocalDateTime.now(), IS_PDF_VAR, false, EMAIL_CURRENT_USER_VAR, defaultPrin.getName())
                                .render())
                .setText(contactRemovalRequest.data(EMAIL_DATA_VAR, request, EMAIL_NAME_VAR, name, EMAIL_NOW_VAR,
                        LocalDateTime.now(), IS_PDF_VAR, false, EMAIL_CURRENT_USER_VAR, defaultPrin.getName())
                        .render());
        // set the to field to the list of addresses
        m.setTo(mailerConfig.contactRemoval().to());
        // set extra mail options
        mailerConfig.contactRemoval().membershipMessage().bcc().ifPresent(m::setBcc);
        mailerConfig.contactRemoval().membershipMessage().replyTo().ifPresent(m::setReplyTo);
        mailer.send(m);
    }

    private void checkRemovalRequest(ContactRemovalRequest request) {
        // check that we have a valid request
        if (request == null || StringUtils.isBlank(request.getUsername())) {
            throw new IllegalStateException("Incomplete request made, could not determine user to remove");
        } else if (request.getOrganizationId() == null || request.getOrganizationId() < 0) {
            throw new IllegalStateException(
                    "Incomplete request made, could not determine organization to remove user from");
        } else if (ident.isAnonymous()) {
            throw new IllegalStateException("Invalid request made, cannot request removal while anonymous");
        }
    }

    @Override
    public void sendContactRemovalNotification(ContactRemovalRequestData request) {
        // check access and data integrity before sending notification
        checkRemovalRequest(request.getRawRequest());
        // get the logged in user who will receive the notification
        DefaultJWTCallerPrincipal defaultPrin = (DefaultJWTCallerPrincipal) ident.getPrincipal();
        String name = generateName(defaultPrin);
        // send the mail
        Mail m = Mail
                .withHtml(defaultPrin.getClaim("email"),
                        String.format("[Contact Removal Request] - Request to remove user '%s' from organization %s",
                                request.getPerson().getPersonID(), request.getOrganization().getOrganizationID()),
                        contactRemovalNotificationWeb.data(EMAIL_DATA_VAR, request, EMAIL_NAME_VAR, name, EMAIL_NOW_VAR,
                                LocalDateTime.now(), IS_PDF_VAR, false, EMAIL_CURRENT_USER_VAR, defaultPrin.getName())
                                .render())
                .setText(contactRemovalNotification.data(EMAIL_DATA_VAR, request, EMAIL_NAME_VAR, name, EMAIL_NOW_VAR,
                        LocalDateTime.now(), IS_PDF_VAR, false, EMAIL_CURRENT_USER_VAR, defaultPrin.getName())
                        .render());
        // set extra mail options
        mailerConfig.contactRemoval().authorMessage().bcc().ifPresent(m::setBcc);
        mailerConfig.contactRemoval().authorMessage().replyTo().ifPresent(m::setReplyTo);
        mailer.send(m);
    }

    private String generateName(DefaultJWTCallerPrincipal defaultPrin) {
        return new StringBuilder().append((String) defaultPrin.getClaim("given_name")).append(" ")
                .append((String) defaultPrin.getClaim("family_name")).toString();
    }

    /**
     * Represents configuration for the default mailer service.
     * 
     * @author Martin Lowe
     *
     */
    @ConfigMapping(prefix = "eclipse.mailer")
    public interface EclipseMailerConfig {

        public ContactRemoval contactRemoval();

        /**
         * Represents configurations for the organization contact removal message.
         */
        public interface ContactRemoval {
            public List<String> to();

            public MessageConfiguration authorMessage();

            public MessageConfiguration membershipMessage();
        }

        public interface MessageConfiguration {
            public Optional<String> replyTo();

            public Optional<List<String>> bcc();
        }
    }

}
