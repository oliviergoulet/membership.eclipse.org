/**
 * Copyright (c) 2021, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.service.impl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.file.DirectoryStream;
import java.nio.file.DirectoryStream.Filter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.portal.config.ImageServiceConfig;
import org.eclipsefoundation.membership.portal.dtos.eclipse.OrganizationInformation;
import org.eclipsefoundation.membership.portal.helper.ImageFileHelper;
import org.eclipsefoundation.membership.portal.namespace.ImageStoreFormat;
import org.eclipsefoundation.membership.portal.namespace.ImageStoreFormat.ImageStoreFormats;
import org.eclipsefoundation.membership.portal.service.ImageStoreService;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.imgscalr.Scalr;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.runtime.Startup;
import io.undertow.server.handlers.form.MultiPartParserDefinition.FileTooLargeException;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.ServerErrorException;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

/**
 * Default implementation of the image service. Writes images to a file system location then provides a web-mounted path
 * for external consumption.
 * 
 * @author Martin Lowe
 *
 */
@Startup
@ApplicationScoped
public class DefaultImageStoreService implements ImageStoreService {
    public static final Logger LOGGER = LoggerFactory.getLogger(DefaultImageStoreService.class);

    private static final String IMG_STORE_CLEAN_ERROR_MSG_FORMAT = "Could not clean out old images for organization %s";
    private static final String FILE_TOO_LARGE_MSG_FORMAT = "Passed image is larger than allowed size of '%d' bytes";

    private static final int MAX_FILE_SIZE_DIFF = 1000;
    private static final int MAX_TABLE_SIZE = 65535;

    @Inject
    ImageServiceConfig config;
    @Inject
    DefaultHibernateDao dao;
    @Inject
    FilterService filters;

    // local copy of image store location
    private Path imageStoreRoot;

    @PostConstruct
    public void start() throws IOException {
        // get a path object using config prop
        this.imageStoreRoot = Path.of(config.filePath());

        // store the exists boolean to save some ops
        boolean storeExists = Files.exists(imageStoreRoot);
        // check state of path target (is it a file, does it exist, etc)
        if (!Files.exists(imageStoreRoot)) {
            // dir does not exist (just create it)
            Files.createDirectory(imageStoreRoot);
        } else if (storeExists && !Files.isDirectory(imageStoreRoot)) {
            // dir exists but isn't a dir
            throw new IllegalArgumentException(
                    "Image service is misconfigured, image root cannot be a non-directory file");
        } else if (storeExists && !Files.isWritable(imageStoreRoot)) {
            // dir exists but is not writable
            throw new IllegalStateException(
                    "Image service cannot write data to the filesystem, please check permissions of dir at "
                            + imageStoreRoot.toAbsolutePath());
        } else if (LOGGER.isInfoEnabled()) {
            LOGGER.info("The live image service is live and mounted on path '{}'", config.filePath());
        }
    }

    @Override
    public String retrieveImageUrl(String fileName, Optional<ImageStoreFormat> format) {
        // get the the full file name without extension given a format and base filename
        String santizedName = ImageFileHelper.sanitizeFileName(fileName, format);
        // for images whos file name starts with `<sanitized org name>.`, delete them
        try (DirectoryStream<Path> ds = getFilesStartingWith(santizedName + '.')) {
            // convert the stream to a list
            List<Path> matchingPaths = StreamSupport.stream(ds.spliterator(), false).collect(Collectors.toList());
            if (matchingPaths.isEmpty()) {
                LOGGER.debug("Could not find an image with name '{}'. Returning null", santizedName);
                return null;
            } else if (matchingPaths.size() > 1) {
                LOGGER.debug("Multiple images available for file '{}' with format of {}", fileName, format);
                matchingPaths.sort((a, b) -> {
                    try {
                        BasicFileAttributes aView = Files.readAttributes(a, BasicFileAttributes.class);
                        BasicFileAttributes bView = Files.readAttributes(b, BasicFileAttributes.class);
                        // compare b to a for reverse sort
                        return bView.lastModifiedTime().compareTo(aView.lastModifiedTime());
                    } catch (IOException e) {
                        LOGGER.warn("Error while attempting to sort on mod time for files at paths {} and {}", a, b);
                        return 0;
                    }
                });
            }
            // convert the most relevant path to a web url
            return getWebUrl(matchingPaths.get(0));
        } catch (IOException e) {
            throw new ServerErrorException(String.format(IMG_STORE_CLEAN_ERROR_MSG_FORMAT, fileName),
                    Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), e);
        }
    }

    @Override
    public String writeImage(Supplier<byte[]> imageBytes, String fileName, String mimeType,
            Optional<ImageStoreFormat> format) throws FileTooLargeException {
        // get file metadata
        Path p = imageStoreRoot.resolve(ImageFileHelper.getFileNameWithExtension(fileName, format, mimeType));
        BasicFileAttributeView attrView = Files.getFileAttributeView(p, BasicFileAttributeView.class);
        try {
            // for images bigger than the current max that don't already exist, refuse to write them
            byte[] bytes = imageBytes.get();
            if (bytes == null) {
                LOGGER.warn("Could not generate image file with name {} and format {}, no image passed", fileName,
                        format);
                // cannot provide image, return null
                return null;
            }
            if (format.isPresent() && format.get().equals(ImageStoreFormats.PRINT)) {
                if (bytes.length > config.maxSizeInBytes().print()) {
                    throw new FileTooLargeException(String.format(FILE_TOO_LARGE_MSG_FORMAT, config.maxSizeInBytes().print()));
                }
                // Any print files should not attempt compression (as they are meant to be big)
                return getWebUrl(Files.write(p, bytes));
            } else {
                if (bytes.length > config.maxSizeInBytes().web()) {
                    throw new FileTooLargeException(String.format(FILE_TOO_LARGE_MSG_FORMAT, config.maxSizeInBytes().web()));
                }
                // compress image and then compare max size
                byte[] compressedImage = compress(bytes, mimeType);
                if (compressedImage.length > config.maxSizeInBytes().webPostCompression()
                        && (!Files.exists(p) || !approximatelyMatch(compressedImage.length,
                                attrView.readAttributes().size(), MAX_FILE_SIZE_DIFF))) {
                    throw new FileTooLargeException("Passed image is larger than allowed size of '"
                            + config.maxSizeInBytes().webPostCompression() + "' bytes after compression");
                }
                // if enabled, update the EclipseDB on logo update when image name is numeric
                // max size check is related to max blob size of 64kb
                // TODO remove once the Drupal API references this API as this will no longer be needed then
                handleDBPersist(fileName, compressedImage, mimeType, format);

                // write the orginal bytes to preserve source file in case of emergency
                Files.write(imageStoreRoot.resolve(ImageFileHelper.getFileNameWithExtension(fileName,
                        Optional.of(ImageStoreFormats.WEB_SRC), mimeType)), bytes);

                // write will create and overwrite file by default if it exists
                return getWebUrl(Files.write(p, compressedImage));

            }
        } catch (FileTooLargeException e) {
                throw e;
        } catch (IOException e) {
            throw new ServerErrorException("Could not write image for organization " + fileName,
                    Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), e);
        }
    }

    @Override
    public String getDefaultImageLocation() {
        return config.defaultImageUrl();
    }
    
    @Override
    public void removeImages(String fileName, Optional<ImageStoreFormat> format) {
        String santizedName = ImageFileHelper.sanitizeFileName(fileName, format);
        // for images whos file name starts with `<sanitized org name>.`, delete them
        try (DirectoryStream<Path> ds = getFilesStartingWith(santizedName + '.')) {
            ds.forEach(path -> {
                try {
                    Files.delete(path);
                } catch (IOException e) {
                    throw new ServerErrorException(String.format(IMG_STORE_CLEAN_ERROR_MSG_FORMAT, fileName),
                            Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), e);
                }
            });
        } catch (IOException e) {
            throw new ServerErrorException(String.format(IMG_STORE_CLEAN_ERROR_MSG_FORMAT, fileName),
                    Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), e);
        }
    }

    /**
     * Creates a directorystream that filters files on their file name, ensuring that they start with the passed
     * filename. This can be used to find images without having to know their exact extension.
     * 
     * @param fileName the name of image files being searched for
     * @return a directory stream that returns files starting with the given file name.
     * @throws IOException if there is an error in accessing the root image store directory
     */
    private DirectoryStream<Path> getFilesStartingWith(String fileName) throws IOException {
        return Files.newDirectoryStream(imageStoreRoot, new Filter<Path>() {
            @Override
            public boolean accept(Path arg0) throws IOException {
                return arg0.getFileName().toString().startsWith(fileName);
            }
        });
    }

    /**
     * Handles persistence to the DB layer for images. Includes checks for size, format, and format before persisting to
     * the database.
     * 
     * @param fileName name of the file to be persisted
     * @param compressedImage the compressed image bytes
     * @param mimeType the mime type of the image
     * @param format the format of the file
     */
    private void handleDBPersist(String fileName, byte[] compressedImage, String mimeType,
            Optional<ImageStoreFormat> format) {
        if (config.persistToDb() && format.isPresent() && ImageStoreFormat.ImageStoreFormats.WEB.equals(format.get())
                && StringUtils.isNumeric(fileName)) {
            if (compressedImage.length > MAX_TABLE_SIZE) {
                LOGGER.warn(
                        "Cannot persist image with name {}, size of compressed image is over max size of table ({} vs. max 65535)",
                        fileName, compressedImage.length);
            } else {
                // get a ref to the given organization information object
                MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
                params.add(DefaultUrlParameterNames.ID.getName(), fileName);
                List<OrganizationInformation> infoRefs = dao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                                filters.get(OrganizationInformation.class), params));
                // if ref doesn't exist, create one
                OrganizationInformation oi;
                if (infoRefs.isEmpty()) {
                    oi = new OrganizationInformation();
                    oi.setOrganizationID(Integer.valueOf(fileName));
                    oi.setCompanyUrl("");
                } else {
                    oi = infoRefs.get(0);
                }
                // as long as this org exists, update the logo
                oi.setLargeLogo(compressedImage);
                oi.setSmallLogo(compressedImage);
                oi.setLargeMime(mimeType);
                oi.setSmallMime(mimeType);
                dao.add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(OrganizationInformation.class)), Arrays.asList(oi));

            }
        }
    }

    /**
     * Fuzzy match used to check byte size of files. Used just in case there is some minor changes to the data done via
     * metadata or the like.
     * 
     * @param arg0 first number to compare
     * @param arg1 second number to compare
     * @param maxDiff the max fuzziness of the match
     * @return true if roughly match, otherwise false
     */
    private boolean approximatelyMatch(long arg0, long arg1, int maxDiff) {
        return Math.abs(arg0 - arg1) < maxDiff;
    }

    /**
     * Used to get the web URL for the given file.
     * 
     * @param imagePath path of the image in the image store
     * @return the public URL for the given image path
     */
    private String getWebUrl(Path imagePath) {
        return config.webRoot() + imagePath.getFileName();
    }

    /**
     * Compresses images using AWT image library if larger than a configurable size. When compressing, the image will be
     * compressed to 80% quality by default. This value is configurable via the `eclipse.image-store.compression.factor`
     * property.
     * 
     * @param imageBytes the images raw bytes
     * @param mimeType the image mime type, used for encoding/compresssion
     * @return the compressed image bytes for writing, or the passed in byte array if below threshold
     */
    private byte[] compress(byte[] imageBytes, String mimeType) {
        // check if we should skip processing (saves processing when images are small enough for web)
        if (!config.compression().enabled() || imageBytes.length < config.compression().thresholdInBytes()) {
            return imageBytes;
        }
        ImageWriter writer = null;
        try (ByteArrayOutputStream os = new ByteArrayOutputStream();
                ImageOutputStream ios = ImageIO.createImageOutputStream(os)) {

            // get the image writer for the current mime type
            Iterator<ImageWriter> writers = ImageIO
                    .getImageWritersBySuffix(ImageFileHelper.convertMimeTypeToExtension(mimeType));
            if (!writers.hasNext()) {
                LOGGER.warn("Unknown mimetype passed for image compression: {}", mimeType);
                return imageBytes;
            }
            writer = writers.next();
            writer.setOutput(ios);

            // read in the image bytes for image io
            // Note: if this fails due to an zlib exception, the image is most likely corrupt somehow
            BufferedImage image = rescale(ImageIO.read(new ByteArrayInputStream(imageBytes)));

            // set compression level (80% default)
            ImageWriteParam param = writer.getDefaultWriteParam();
            param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            param.setCompressionQuality(config.compression().factor());
            writer.write(null, new IIOImage(image, null, null), param);
            return os.toByteArray();
        } catch (IOException e) {
            LOGGER.warn("Unable to compress image with mime type of '{}', returning original bytes", mimeType, e);
            return imageBytes;
        } finally {
            // close the writer once done (not autoclosable)
            if (writer != null) {
                writer.dispose();
            }
        }
    }

    /**
     * Using Scalr library, uses quality-focused resizing algorithms to shrink images larger than the configured maximum
     * dimension.
     * 
     * @param original the original image to resize
     * @return the resized image if larger than max dimensions, or original image
     */
    private BufferedImage rescale(BufferedImage original) {
        // check if we need to rescale the image to save cycles
        if (original.getHeight() < config.compression().maxDimension()
                && original.getWidth() < config.compression().maxDimension()) {
            return original;
        }
        // use Scalr to maintain ratio and resize image
        return Scalr.resize(original, Scalr.Method.QUALITY, config.compression().maxDimension());
    }
}
