/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.api;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.model.OrganizationContactData;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

import io.quarkus.oidc.client.filter.OidcClientFilter;
import jakarta.annotation.Nullable;
import jakarta.annotation.security.RolesAllowed;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@OidcClientFilter
@Path("organizations")
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "fdndb-api")
public interface OrganizationAPI {

    @GET
    @Path("contacts")
    @RolesAllowed("fdb_read_organization_employment")
    Response getOrganizationContacts(@BeanParam BaseAPIParameters baseParams,
            @BeanParam OrganizationRequestParams params);

    @GET
    @Path("{id}/contacts")
    @RolesAllowed("fdb_read_organization_employment")
    Response getOrganizationContactsWithSearch(@PathParam("id") String orgId, @BeanParam BaseAPIParameters baseParams,
            @BeanParam OrganizationRequestParams params);

    @PUT
    @Path("{id}/contacts")
    @RolesAllowed("fdb_write_organization_employment")
    OrganizationContactData updateOrganizationContacts(@PathParam("id") String orgId, OrganizationContactData contact);

    @GET
    @Path("{id}/contacts/{personID}")
    @RolesAllowed("fdb_read_organization_employment")
    Response getOrganizationContact(@PathParam("id") String orgId, @PathParam("personID") String personID,
            @BeanParam BaseAPIParameters baseParams, @BeanParam OrganizationRequestParams params);

    @DELETE
    @Path("{id}/contacts/{personID}/{relation}")
    @RolesAllowed("fdb_delete_organization_employment")
    Response removeOrganizationContacts(@PathParam("id") String orgId, @PathParam("personID") String personID,
            @PathParam("relation") String relation);

    @AutoValue
    @JsonDeserialize(builder = AutoValue_OrganizationAPI_OrganizationRequestParams.Builder.class)
    public abstract static class OrganizationRequestParams {

        @Nullable
        @QueryParam("id")
        public abstract String getOrganizationID();

        @Nullable
        @QueryParam("personID")
        public abstract String getPersonID();

        @Nullable
        @QueryParam("email")
        public abstract String getEmail();

        @Nullable
        @QueryParam("fName")
        public abstract String getFirstName();

        @Nullable
        @QueryParam("lName")
        public abstract String getLastName();

        @Nullable
        @QueryParam("relation")
        public abstract String getRelation();

        @Nullable
        @QueryParam("working_group")
        public abstract String getWorkingGroup();

        @Nullable
        @QueryParam("is_not_expired")
        public abstract Boolean getIsNotExpired();

        @Nullable
        @QueryParam("ids")
        public abstract List<String> getIds();

        @Nullable
        @QueryParam("documentIDs")
        public abstract List<String> getDocumentIds();

        @Nullable
        @QueryParam("people_ids")
        public abstract List<String> getPeopleIds();

        @Nullable
        @QueryParam("levels")
        public abstract List<String> getLevels();

        public static Builder builder() {
            return new AutoValue_OrganizationAPI_OrganizationRequestParams.Builder().setIsNotExpired(true);
        }

        /**
         * Checks the internal parameters to check if the parameters are all empty.
         * 
         * @return true if no params are set, false otherwise
         */
        public boolean isEmpty() {
            return allListsAreEmpty() && StringUtils.isAllBlank(getEmail(), getFirstName(), getLastName(),
                    getOrganizationID(), getPersonID(), getRelation());
        }

        private boolean allListsAreEmpty() {
            return CollectionUtils.isEmpty(getIds()) && CollectionUtils.isEmpty(getDocumentIds())
                    && CollectionUtils.isEmpty(getPeopleIds()) && CollectionUtils.isEmpty(getLevels());
        }

        /**
         * Generate a parameter map from the current request, passing through any set
         * parameters to the bean mapping.
         * 
         * @param w the wrapper for the current request.
         * @return a bean mapping with values populated from the current request
         */
        public static Builder populateFromWrap(RequestWrapper w) {
            // default levels for membership calls
            List<String> levels = w.getParams(FoundationDBParameterNames.LEVELS);
            return builder().setPersonID(w.getFirstParam(FoundationDBParameterNames.USER_NAME).orElseGet(() -> null))
                    .setOrganizationID(
                            w.getFirstParam(FoundationDBParameterNames.ORGANIZATION_ID).orElseGet(() -> null))
                    .setRelation(w.getFirstParam(FoundationDBParameterNames.RELATION).orElseGet(() -> null))
                    .setEmail(w.getFirstParam(FoundationDBParameterNames.EMAIL).orElseGet(() -> null))
                    .setWorkingGroup(w.getFirstParam(FoundationDBParameterNames.WORKING_GROUP).orElseGet(() -> null))
                    .setDocumentIds(w.getParams(FoundationDBParameterNames.DOCUMENT_IDS))
                    .setLevels(levels != null && !levels.isEmpty() ? levels : Arrays.asList("SD", "AP", "AS", "OHAP"))
                    .setIds(w.getParams(DefaultUrlParameterNames.IDS));
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            @QueryParam("id")
            public abstract Builder setOrganizationID(@Nullable String organizationID);

            @QueryParam("personID")
            public abstract Builder setPersonID(@Nullable String personID);

            @QueryParam("email")
            public abstract Builder setEmail(@Nullable String email);

            @QueryParam("fName")
            public abstract Builder setFirstName(@Nullable String firstName);

            @QueryParam("lName")
            public abstract Builder setLastName(@Nullable String lastName);

            @QueryParam("relation")
            public abstract Builder setRelation(@Nullable String relation);

            @QueryParam("working_group")
            public abstract Builder setWorkingGroup(@Nullable String workingGroup);

            @QueryParam("ids")
            public abstract Builder setIds(@Nullable List<String> ids);

            @QueryParam("documentIDs")
            public abstract Builder setDocumentIds(@Nullable List<String> documentIds);

            @QueryParam("people_ids")
            public abstract Builder setPeopleIds(@Nullable List<String> peopleIds);

            @QueryParam("levels")
            public abstract Builder setLevels(@Nullable List<String> levels);

            @QueryParam("is_not_expired")
            public abstract Builder setIsNotExpired(@Nullable Boolean isNotExpired);

            public abstract OrganizationRequestParams build();
        }
    }
}
