/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.dtos.dashboard;

import java.util.Date;
import java.util.Objects;

import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.model.DtoTable;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

/**
 * Committer record from the Dashboard database.
 * 
 * @author Martin Lowe
 *
 */
@Entity
@Table
public class Committer extends BareNode {
    public static final DtoTable TABLE = new DtoTable(Committer.class, "c");

    @Id
    private String id;
    private String country;
    private String first;
    private String last;
    private String email;
    private Date ts;

    @Override
    public String getId() {
        return this.id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the first
     */
    public String getFirst() {
        return first;
    }

    /**
     * @param first the first to set
     */
    public void setFirst(String first) {
        this.first = first;
    }

    /**
     * @return the last
     */
    public String getLast() {
        return last;
    }

    /**
     * @param last the last to set
     */
    public void setLast(String last) {
        this.last = last;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the ts
     */
    public Date getTs() {
        return ts;
    }

    /**
     * @param ts the ts to set
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(country, email, first, id, last, ts);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Committer other = (Committer) obj;
        return Objects.equals(country, other.country) && Objects.equals(email, other.email) && Objects.equals(first, other.first)
                && Objects.equals(id, other.id) && Objects.equals(last, other.last) && Objects.equals(ts, other.ts);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Committer [id=");
        builder.append(id);
        builder.append(", country=");
        builder.append(country);
        builder.append(", first=");
        builder.append(first);
        builder.append(", last=");
        builder.append(last);
        builder.append(", email=");
        builder.append(email);
        builder.append(", ts=");
        builder.append(ts);
        builder.append("]");
        return builder.toString();
    }

}
