package org.eclipsefoundation.membership.portal.api;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategies.LowerCamelCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("cbi/sponsorships")
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "ef-api")
public interface CbiAPI {
    
    @GET
    CBI getSponsorships();

    @JsonNaming(LowerCamelCaseStrategy.class)
    public record CBI(List<MemberOrganizationsBenefits> memberOrganizationsBenefits, List<SponsoredProject> sponsoredProjects) {

    }

    @JsonNaming(LowerCamelCaseStrategy.class)
    public record MemberOrganizationsBenefits(int id, String displayName, int dedicatedAgents,
            @JsonProperty(value = "dedicatedAgents_used") int dedicatedAgentsUsed, int ghLargeRunners,
            @JsonProperty(value = "ghLargeRunners_used") int ghLargeRunnersUsed, String membership, int resourcePacks,
            @JsonProperty(value = "resourcePacks_used") int resourcePacksUsed) {

    }

    @JsonNaming(LowerCamelCaseStrategy.class)
    public record SponsoredProject(ProjectId project, List<SponsoringOrganization> sponsoringOrganizations) {

    }

    public record ProjectId(String id) {
    }

    @JsonNaming(LowerCamelCaseStrategy.class)
    public record SponsoringOrganization(int id, String displayName, int resourcePacks, int dedicatedAgents, int ghLargeRunners,
            String comment, List<String> requestTickets) {
    }
}
