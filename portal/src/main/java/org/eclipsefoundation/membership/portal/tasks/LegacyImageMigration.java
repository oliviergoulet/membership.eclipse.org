/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.tasks;

import java.net.URI;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.portal.config.ImageServiceConfig;
import org.eclipsefoundation.membership.portal.dtos.eclipse.OrganizationInformation;
import org.eclipsefoundation.membership.portal.namespace.ImageStoreFormat.ImageStoreFormats;
import org.eclipsefoundation.membership.portal.service.ImageStoreService;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.infrastructure.Infrastructure;
import io.undertow.server.handlers.form.MultiPartParserDefinition.FileTooLargeException;

/**
 * Imports images from the Eclipse DB OrganizationInformation table into the local imagestore. This will overwrite
 * existing logos with the legacy content, and should be used to create fresh stores rather than on top of an existing
 * store.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class LegacyImageMigration {
    public static final Logger LOGGER = LoggerFactory.getLogger(LegacyImageMigration.class);

    @Inject
    ImageServiceConfig config;

    @Inject
    DefaultHibernateDao edbDao;
    @Inject
    FilterService filters;
    @Inject
    ImageStoreService images;

    /**
     * On start, check if the legacy images should be migrated to the image store. This will run asynchronously so it
     * should not impact startup time, though may present some load to the server as it executes.
     */
    @PostConstruct
    void run() {
        if (config.legacyMigrationEnabled()) {
            // start the migration task, throwing exceptions to the
            Uni.createFrom().item(UUID::randomUUID).emitOn(Infrastructure.getDefaultWorkerPool()).subscribe()
                    .with(this::worker);
        }
    }

    /**
     * Facilitates the migration of the legacy images into the image store.
     * 
     * @param uuid Job ID created by the run call. Used to track subscription.
     * @return
     */
    Uni<Void> worker(UUID uuid) {
        LOGGER.info("Starting import of legacy images for job '{}'", uuid);
        RequestWrapper wrapper = new FlatRequestWrapper(URI.create("https://membership.eclipse.org"));
        // atomic used for access inside of an iterable action thread to ensure safe access
        AtomicInteger count = new AtomicInteger();
        int page = 1;
        // count the results so we can limit the results
        RDBMSQuery<OrganizationInformation> countQuery = new RDBMSQuery<>(wrapper,
                filters.get(OrganizationInformation.class), null);
        countQuery.setRoot(false);
        long total = edbDao.count(countQuery);
        // while we still have results, page through the results
        while (count.get() < total) {
            // get the current page of information
            MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
            params.add(DefaultUrlParameterNames.PAGE.getName(), Integer.toString(page++));
            try {
                // create a query to get the current page of information and increment the page for next call
                RDBMSQuery<OrganizationInformation> q = new RDBMSQuery<>(wrapper,
                        filters.get(OrganizationInformation.class), params);
                q.setRoot(false);

                // get the results and persist the organization logos if present w/ mime type
                edbDao.get(q).forEach(info -> {
                    LOGGER.info("Importing logos for organization {}", info.getOrganizationID());
                    // check and import the logos
                    if (info.getLargeLogo() != null && StringUtils.isNotBlank(info.getLargeMime())) {
                        writeImageSafely(info::getLargeLogo, info.getOrganizationID(), info.getLargeMime(),
                                ImageStoreFormats.WEB);
                    }
                    // increment the count
                    count.getAndIncrement();
                });
            } catch (RuntimeException e) {
                LOGGER.error("Error while importing legacy images, aborting process", e);
                break;
            }
        }
        LOGGER.info("Finished import of legacy images for job '{}', imported {} images", uuid, count.get());
        return Uni.createFrom().voidItem();
    }

    /**
     * Wrap call to update the image in the image store to wrap calls to catch and log exceptions so we can finish
     * import and log errors instead.
     * 
     * @param logo the logo file supplier
     * @param organizationID the organization that is being migrated
     * @param mime the mime type of the file.
     * @param format the internal format of the image file, used to store multiple copies of the same logo
     */
    void writeImageSafely(Supplier<byte[]> logo, Integer organizationID, String mime, ImageStoreFormats format) {
        try {
            images.writeImage(logo, Integer.toString(organizationID), mime, Optional.of(format));
        } catch (FileTooLargeException | RuntimeException e) {
            LOGGER.error("Error while writing logo for organization {} with format {}", organizationID, format, e);
        }
    }
}
