/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.api;

import jakarta.annotation.security.RolesAllowed;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.model.ProjectData;

import io.quarkus.oidc.client.filter.OidcClientFilter;

@OidcClientFilter
@Path("projects")
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "fdndb-api")
public interface ProjectAPI {

    @GET
    @RolesAllowed("fdb_read_projects")
    Response getProjects(@BeanParam BaseAPIParameters baseParams, @QueryParam("organizationID") String organizationID);

    @GET
    @RolesAllowed("fdb_read_projects")
    @Path("{projectID}")
    ProjectData getProject(@BeanParam BaseAPIParameters baseParams, @PathParam("projectID") String projectID);

}
