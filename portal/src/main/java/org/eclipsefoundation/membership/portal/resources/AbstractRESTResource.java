/**
 * Copyright (c) 2021, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.resources;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import jakarta.inject.Inject;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.core.Context;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.helper.CSRFHelper;
import org.eclipsefoundation.core.helper.TransformationHelper;
import org.eclipsefoundation.core.model.AdditionalUserData;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.foundationdb.client.model.EnhancedOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.model.SysRelationData;
import org.eclipsefoundation.membership.portal.api.ComplexAPI;
import org.eclipsefoundation.membership.portal.api.PeopleAPI;
import org.eclipsefoundation.membership.portal.helper.ModLogHelper;
import org.eclipsefoundation.membership.portal.helper.UserAccessHelper;
import org.eclipsefoundation.membership.portal.model.EnhancedPersonData;
import org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType;
import org.eclipsefoundation.membership.portal.service.OrganizationsService;
import org.eclipsefoundation.persistence.service.FilterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.security.identity.SecurityIdentity;

/**
 * Provides access to commonly required services and containers for REST request
 * serving.
 *
 * @author Martin Lowe
 */
public abstract class AbstractRESTResource {
    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractRESTResource.class);
    public static final String ALL_CACHE_PLACEHOLDER = "all";

    @Context
    HttpServletRequest request;

    @Inject
    FilterService filters;
    @Inject
    CachingService cache;

    @Inject
    RequestWrapper wrap;

    @Inject
    CSRFHelper csrfHelper;
    @Inject
    ModLogHelper modLogHelper;
    @Inject
    AdditionalUserData aud;
    @Inject
    SecurityIdentity ident;
    @Inject
    UserAccessHelper accessHelper;

    @Inject
    OrganizationsService orgService;

    @RestClient
    @Inject
    PeopleAPI peopleAPI;
    @RestClient
    @Inject
    ComplexAPI complexAPI;
    @Inject
    APIMiddleware middle;

    /**
     * Builds a list of people data that includes name and relations for the users associated with the passed organization.
     * 
     * @param organizationID the organization to fetch users for
     * @param relations the relations that should be filtered to, if any
     * @return the list of people, filtered by relation type if provided.
     */
    protected List<EnhancedPersonData> getEnhancedPeopleDetails(String organizationID,
            List<OrganizationalUserType> userTypes) {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching org contacts for org '{}'", TransformationHelper.formatLog(organizationID));
        }

        // get the cached contacts data
        Optional<List<EnhancedOrganizationContactData>> ocs = cache
                .get(organizationID, null, EnhancedOrganizationContactData.class,
                        () -> middle
                                .paginationPassThrough(p -> complexAPI.getContacts(p, organizationID, null), wrap,
                                        EnhancedOrganizationContactData.class))
                .getData();
        if (ocs.isEmpty()) {
            return Collections.emptyList();
        }
        boolean canViewProtectedField = accessHelper.userCanViewProtectedField(organizationID);

        // get a reusable list for easier comparisons, null checking the param as well
        List<String> userTypeRaw = userTypes == null ? Collections.emptyList()
                : userTypes.stream().map(OrganizationalUserType::name).toList();
        // convert the contact data to a more usable format
        return ocs.get().stream().map(p -> {
            LOGGER
                    .debug("Converting '{}' from org '{}' to EnhancedPersonData", p.getPerson().getPersonID(),
                            TransformationHelper.formatLog(organizationID));

            // only expose email if user has elevated permissions
            return EnhancedPersonData
                    .builder()
                    .setEmail(canViewProtectedField ? p.getPerson().getEmail() : null)
                    .setFirstName(p.getPerson().getFname())
                    .setLastName(p.getPerson().getLname())
                    .setId(p.getPerson().getPersonID())
                    .setRelations(getActualRelationsForContact(p.getRelations(), userTypes, p.getIsCommitter()))
                    .build();
        }).filter(contact -> userTypeRaw.isEmpty() || contact.getRelations().stream().anyMatch(userTypeRaw::contains)).toList();
    }

    /**
     * Converts all person's relations to a List of Strings and adds committer
     * relation to list of relations if the user is a committer.
     * 
     * @param userRelations The person's SysRelationData records
     * @param userTypes     The list of OrganizationalUserTypes mapped to this user
     * @param isCommitter   The commmitter flag
     * @return The list of relations
     */
    private List<String> getActualRelationsForContact(List<SysRelationData> userRelations,
            List<OrganizationalUserType> userTypes, boolean isCommitter) {
        List<String> actualRels = userRelations
                .stream()
                .map(SysRelationData::getRelation)
                .filter(r -> userTypes.contains(OrganizationalUserType.valueOfChecked(r)))
                .collect(Collectors.toList());
        if (isCommitter) {
            actualRels.add("CM");
        }
        return actualRels;
    }
}
