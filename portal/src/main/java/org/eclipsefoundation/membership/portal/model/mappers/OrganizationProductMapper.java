/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.model.mappers;

import org.eclipsefoundation.membership.portal.dtos.eclipse.OrganizationProducts;
import org.eclipsefoundation.membership.portal.model.OrganizationProductData;
import org.eclipsefoundation.membership.portal.model.mappers.BaseEntityMapper.QuarkusMappingConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = QuarkusMappingConfig.class)
public interface OrganizationProductMapper extends BaseEntityMapper<OrganizationProducts, OrganizationProductData> {

    @Mapping(source = "compositeId.productId", target = "productId")
    @Mapping(source = "compositeId.organizationId", target = "organizationId")
    OrganizationProductData toModel(OrganizationProducts dtoEntity);
}
