/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.dtos.dashboard;

import java.io.Serializable;
import java.util.Objects;

import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.model.DtoTable;

import jakarta.persistence.Embeddable;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Table
@Entity
public class ProjectCompanyActivity extends BareNode {
    public static final DtoTable TABLE = new DtoTable(ProjectCompanyActivity.class, "pca");

    @EmbeddedId
    private ProjectCompanyActivityCompositeId compositeId;
    private String company;
    private int count;

    /**
     * @return the id
     */
    @Override
    public ProjectCompanyActivityCompositeId getId() {
        return getCompositeId();
    }

    /**
     * @return the compositeId
     */
    public ProjectCompanyActivityCompositeId getCompositeId() {
        return compositeId;
    }

    /**
     * @param compositeId the compositeId to set
     */
    public void setCompositeId(ProjectCompanyActivityCompositeId compositeId) {
        this.compositeId = compositeId;
    }

    /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(String company) {
        this.company = company;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(compositeId, company, count);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ProjectCompanyActivity other = (ProjectCompanyActivity) obj;
        return Objects.equals(compositeId, other.compositeId) && Objects.equals(company, other.company)
                && count == other.count;
    }

    @Embeddable
    public static class ProjectCompanyActivityCompositeId implements Serializable {
        @ManyToOne
        @JoinColumn(name = "project")
        private Project project;
        private Integer orgId;

        /**
         * @return the project
         */
        public Project getProject() {
            return project;
        }

        /**
         * @param project the project to set
         */
        public void setProject(Project project) {
            this.project = project;
        }

        /**
         * @return the orgId
         */
        public Integer getOrgId() {
            return orgId;
        }

        /**
         * @param orgId the orgId to set
         */
        public void setOrgId(Integer orgId) {
            this.orgId = orgId;
        }
    }
}
