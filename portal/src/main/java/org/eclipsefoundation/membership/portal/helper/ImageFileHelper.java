/**
 * Copyright (c) 2021, 2022 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.helper;

import java.util.Optional;

import org.eclipsefoundation.membership.portal.exception.UnsupportedImageFormatException;
import org.eclipsefoundation.membership.portal.namespace.ImageStoreFormat;

/**
 * Helper class when handling Image files and their names.
 * 
 * @author Martin Lowe
 *
 */
public final class ImageFileHelper {
    public static final String SEPARATOR = "-";

    /**
     * Sanitize the organization name to normalize access to image store.
     * 
     * @param name organizations name, pre-normalization
     * @return normalized and sanitized organization name.
     */
    public static String sanitizeFileName(String name, Optional<ImageStoreFormat> format) {
        if (name == null) {
            return null;
        } else if (format.isPresent()) {
            // also sanitize the format to prevent bad inputs
            return sanitizeString(name).toLowerCase() + SEPARATOR + sanitizeString(format.get().getName());
        } else {
            return sanitizeString(name).toLowerCase();
        }
    }

    /**
     * Retrieves a file name for an image given the mime type and organization name.
     * 
     * @param name organizations name, pre-normalization
     * @param mimeType the mime type of the file to be written
     * @return
     */
    public static String getFileNameWithExtension(String name, Optional<ImageStoreFormat> format, String mimeType) {
        return sanitizeFileName(name, format) + convertMimeType(mimeType);
    }

    /**
     * Converts the mimetype to a file extension. This is only for image formats and does not contain other formats.
     * 
     * @param mime the image mimetype
     * @return the extension corresponding to the mimetype, defaulting to .jpg.
     */
    public static String convertMimeType(String mime) {
        return '.' + convertMimeTypeToExtension(mime);
    }

    public static String convertMimeTypeToExtension(String mime) {
        String ext;
        switch (mime) {
            case "image/bmp":
                ext = "bmp";
                break;
            case "image/gif":
                ext = "gif";
                break;
            case "image/jpeg":
            case "image/pjpeg":
                ext = "jpg";
                break;
            case "image/png":
            case "image/x-png":
                ext = "png";
                break;
            case "application/postscript":
            case "application/eps":
            case "application/x-eps":
            case "image/eps":
            case "image/x-eps":
                ext = "eps";
                break;
            default:
                throw new UnsupportedImageFormatException(
                        String.format("Format %s is not supported currently and will not be accepted", mime));
        }
        return ext;
    }

    /**
     * Centralize calls to clean strings for use in file names.
     * 
     * @param val the string to sanitize
     * @return the sanitized string
     */
    private static String sanitizeString(String val) {
        // replace non-word characters w/ separator character, and remove leading/trailing separators
        return val.replaceAll("[\\W_]+", SEPARATOR).replaceAll(SEPARATOR + "+$", "")
                .replaceAll('^' + SEPARATOR + '+', "").toLowerCase();
    }

    private ImageFileHelper() {

    }
}
