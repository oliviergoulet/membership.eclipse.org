/**
 * Copyright (c) 2021, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.resources;

import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.CR;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.CRA;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.DE;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.EMPLY;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.MA;

import java.net.URI;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.helper.TransformationHelper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.efservices.services.WorkingGroupService;
import org.eclipsefoundation.foundationdb.client.model.PeopleData;
import org.eclipsefoundation.foundationdb.client.model.ProjectData;
import org.eclipsefoundation.membership.portal.api.CbiAPI;
import org.eclipsefoundation.membership.portal.api.CbiAPI.CBI;
import org.eclipsefoundation.membership.portal.api.CbiAPI.MemberOrganizationsBenefits;
import org.eclipsefoundation.membership.portal.api.CbiAPI.SponsoredProject;
import org.eclipsefoundation.membership.portal.api.FoundationDBParameterNames;
import org.eclipsefoundation.membership.portal.api.SysAPI;
import org.eclipsefoundation.membership.portal.daos.DashboardPersistenceDAO;
import org.eclipsefoundation.membership.portal.dtos.dashboard.CommitterProjectActivity;
import org.eclipsefoundation.membership.portal.dtos.eclipse.OrganizationInformation;
import org.eclipsefoundation.membership.portal.dtos.eclipse.OrganizationProducts;
import org.eclipsefoundation.membership.portal.helper.ImageFileHelper;
import org.eclipsefoundation.membership.portal.helper.ImageGenerator;
import org.eclipsefoundation.membership.portal.model.BasicPersonData;
import org.eclipsefoundation.membership.portal.model.EnhancedPersonData;
import org.eclipsefoundation.membership.portal.model.MemberOrganization;
import org.eclipsefoundation.membership.portal.model.OrganizationActivity;
import org.eclipsefoundation.membership.portal.model.OrganizationProductData;
import org.eclipsefoundation.membership.portal.model.OrganizationYearlyActivity;
import org.eclipsefoundation.membership.portal.model.SlimMemberOrganization;
import org.eclipsefoundation.membership.portal.model.mappers.CommitterProjectActivityMapper;
import org.eclipsefoundation.membership.portal.model.mappers.OrganizationProductMapper;
import org.eclipsefoundation.membership.portal.namespace.ImageStoreFormat;
import org.eclipsefoundation.membership.portal.namespace.ImageStoreFormat.ImageStoreFormats;
import org.eclipsefoundation.membership.portal.namespace.MembershipPortalParameterNames;
import org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType;
import org.eclipsefoundation.membership.portal.request.RolesAllowed;
import org.eclipsefoundation.membership.portal.request.model.OrganizationInfoUpdateRequest;
import org.eclipsefoundation.membership.portal.request.model.OrganizationLogoUpdateRequest;
import org.eclipsefoundation.membership.portal.service.ImageStoreService;
import org.eclipsefoundation.membership.portal.service.ProjectsService;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.security.Authenticated;
import io.undertow.server.handlers.form.MultiPartParserDefinition.FileTooLargeException;
import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

/**
 * Allows for external organizations data to be retrieved and displayed.
 */
@Path("organizations")
@Produces(MediaType.APPLICATION_JSON)
public class OrganizationResource extends AbstractRESTResource {
    private static final int MONTHS_IN_A_YEAR = 12;
    private static final int ORG_DESC_LIMIT = 700;
    private static final String INVALID_ORG_ID_KEY = "invalid-org";

    public static final Logger LOGGER = LoggerFactory.getLogger(OrganizationResource.class);

    private static final Pattern PERIOD_DATE_FORMAT = Pattern.compile("^[\\d]{4}(0[\\d]|1[0-2])$");

    @Inject
    DashboardPersistenceDAO dashboardDao;
    @Inject
    DefaultHibernateDao eclipseDBDao;
    @Inject
    WorkingGroupService wgService;
    @Inject
    ProjectsService projectsService;
    @Inject
    ImageStoreService images;

    @RestClient
    @Inject
    SysAPI sysAPI;
    @RestClient
    CbiAPI cbi;

    @Inject
    CommitterProjectActivityMapper mapper;
    @Inject
    OrganizationProductMapper productMapper;

    @GET
    public Response getAll(@QueryParam("working_group") String workingGroup, @QueryParam("level") String level) {
        return Response.ok(new ArrayList<>(getMemberOrganizations(workingGroup, level))).build();
    }

    @GET
    @Path("slim")
    public Response getSlimOrganizations(@QueryParam("working_group") String workingGroup, @QueryParam("level") String level) {
        return Response
                .ok(getMemberOrganizations(workingGroup, level)
                        .stream()
                        .map(o -> SlimMemberOrganization
                                .builder()
                                .setLogos(o.getLogos())
                                .setName(o.getName())
                                .setOrganizationID(o.getOrganizationID())
                                .build()))
                .build();
    }

    @GET
    @Path("{orgID:\\d+}")
    public Response get(@PathParam("orgID") String organizationID) {
        Optional<MemberOrganization> org = orgService.getByID(organizationID);
        if (org.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND.getStatusCode()).build();
        }
        return Response.ok(org.get()).build();
    }

    @POST
    @Authenticated
    @RolesAllowed({ CR, DE, CRA, MA })
    @Path("{orgID:\\d+}")
    public Response update(@PathParam("orgID") String organizationID, OrganizationInfoUpdateRequest updateRequest) {
        if (updateRequest.getDescription().length() > ORG_DESC_LIMIT) {
            return new org.eclipsefoundation.core.model.Error(Response.Status.BAD_REQUEST,
                    "Organization description should not be over " + Integer.toString(ORG_DESC_LIMIT) + " characters").asResponse();
        }
        // get ref and update the object
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), organizationID);
        List<OrganizationInformation> infoRefs = eclipseDBDao
                .get(new RDBMSQuery<>(wrap, filters.get(OrganizationInformation.class), params));
        // if ref doesn't exist, create one
        OrganizationInformation infoRef;
        if (infoRefs.isEmpty()) {
            infoRef = new OrganizationInformation();
            infoRef.setOrganizationID(Integer.valueOf(organizationID));
        } else {
            infoRef = infoRefs.get(0);
        }
        infoRef.setCompanyUrl(updateRequest.getCompanyUrl());
        infoRef.setLongDescription(updateRequest.getDescription());

        // update the org info
        List<OrganizationInformation> updatedOrg = eclipseDBDao
                .add(new RDBMSQuery<>(wrap, filters.get(OrganizationInformation.class), params), Arrays.asList(infoRef));
        if (updatedOrg.isEmpty()) {
            return Response.status(Response.Status.SERVICE_UNAVAILABLE.getStatusCode()).build();
        }
        // clear the caches and return a fresh copy
        cache.fuzzyRemove(organizationID, OrganizationInformation.class);
        cache.fuzzyRemove(organizationID, MemberOrganization.class);
        Optional<MemberOrganization> org = orgService.getByID(organizationID);
        if (org.isEmpty()) {
            return Response.status(Response.Status.SERVICE_UNAVAILABLE.getStatusCode()).build();
        }
        modLogHelper.postUpdateModLog(ident, OrganizationInformation.TABLE, organizationID, null);
        return Response.ok(org.get()).build();
    }

    @GET
    @Path("{orgID:\\d+}/web-logo")
    public Response getLogo(@PathParam("orgID") String organizationID) throws FileTooLargeException {
        // Check that the organization exists before fetching a logo
        Optional<MemberOrganization> org = orgService.getByID(organizationID);
        if (org.isEmpty()) {
            String webLogo = images.retrieveImageUrl(INVALID_ORG_ID_KEY, Optional.of(ImageStoreFormats.WEB));
            if (StringUtils.isBlank(webLogo)) {
                byte[] img = ImageGenerator.generateEmptyImage();
                if (img == null || img.length == 0) {
                    // Use backup if image was not properly generated
                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("Empty image not generated. Using fallback logo");
                    }                
                    return Response.status(302).location(URI.create(images.getDefaultImageLocation())).build();
                }

                images.writeImage(() ->  img, INVALID_ORG_ID_KEY, "image/gif", Optional.of(ImageStoreFormats.WEB));
                webLogo = images.retrieveImageUrl(INVALID_ORG_ID_KEY, Optional.of(ImageStoreFormats.WEB));
                return Response.status(302).location(URI.create(webLogo)).build();
            }
                  
            return Response.status(302).location(URI.create(webLogo)).build();
        }

        // Get the path for the known web logo of the organization, generating a new one if not available
        String webLogo = images.retrieveImageUrl(organizationID, Optional.of(ImageStoreFormats.WEB));
        if (StringUtils.isBlank(webLogo)) {
            // Generate image, write to imagestore, and redirect to new image URL
            byte[] img = ImageGenerator.generateImageFromText(org.get().getName());
            if (img == null || img.length == 0) {
                // Use backup if image was not properly generated
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Using fallback logo for org '{}'", TransformationHelper.formatLog(organizationID));
                }                
                return Response.status(302).location(URI.create(images.getDefaultImageLocation())).build();
            }

            images.writeImage(() ->  img, organizationID, "image/png", Optional.of(ImageStoreFormats.WEB));
            webLogo = images.retrieveImageUrl(organizationID, Optional.of(ImageStoreFormats.WEB));
            return Response.status(302).location(URI.create(webLogo)).build();
        }
        return Response.status(302).location(URI.create(webLogo)).build();
    }

    @GET
    @Authenticated
    @RolesAllowed({ EMPLY, CR, DE, CRA, MA })
    @Path("{orgID:\\d+}/activity")
    public Response getPeriodActivity(@PathParam("orgID") String organizationID, @QueryParam("period") String period,
            @QueryParam("from_period") String fromPeriod) {
        List<CommitterProjectActivity> orgCountryActivity = getCommitterActivity(organizationID, period, fromPeriod);
        // if there is no data returned, return a not found
        if (orgCountryActivity.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND.getStatusCode()).build();
        }
        return Response.ok(orgCountryActivity.stream().map(mapper::toModel).collect(Collectors.toList())).build();
    }

    @GET
    @Authenticated
    @RolesAllowed({ EMPLY, CR, DE, CRA, MA })
    @Path("{orgID:\\d+}/activity/overview")
    public Response getPeriodActivityOverview(@PathParam("orgID") String organizationID, @QueryParam("period") String period,
            @QueryParam("from_period") String fromPeriod) {
        List<CommitterProjectActivity> orgCountryActivity = getCommitterActivity(organizationID, period, fromPeriod);

        // reduce count using sum, and build immutable
        return Response
                .ok(OrganizationActivity
                        .builder()
                        .setCount(orgCountryActivity.stream().reduce(0, (sub, el) -> sub + el.getCount(), Integer::sum))
                        .build())
                .build();
    }

    @GET
    @Authenticated
    @RolesAllowed({ EMPLY, CR, DE, CRA, MA })
    @Path("{orgID:\\d+}/activity/yearly")
    public Response getPeriodActivityYearly(@PathParam("orgID") String organizationID) {
        LocalDate d = LocalDate.now();
        // get the previous year of results
        List<CommitterProjectActivity> orgCountryActivity = getCommitterActivity(organizationID, null,
                getPeriodStringForDate(d.minusYears(1)));

        // group results by the activity period, then reduce the count into a single int
        // result, and transform into an output.
        List<OrganizationActivity> activityList = orgCountryActivity
                .stream()
                .collect(Collectors.groupingBy((CommitterProjectActivity cpa) -> cpa.getCompositeID().getPeriod()))
                .entrySet()
                .stream()
                .map(e -> OrganizationActivity
                        .builder()
                        .setPeriod(e.getKey())
                        .setCount(e.getValue().stream().reduce(0, (sub, el) -> sub + el.getCount(), Integer::sum))
                        .build())
                .collect(Collectors.toList());
        // backfill activity log for 1 year
        activityList = backfillHistoricActivityData(activityList, d);
        // sort the period, sorting from greatest (most recent) to smallest (least
        // recent)
        activityList.sort((o1, o2) -> o2.getPeriod().compareTo(o1.getPeriod()));

        return Response.ok(OrganizationYearlyActivity.builder().setActivity(activityList).build()).build();
    }

    /**
     * Retrieves common build infrastructure information relevant to the current organization. This will be built using the static CBI
     * sponsorship API and some filtering on the cached data.
     * 
     * @param organizationID the organization to retrieve CBI information for
     * @return the CBI usage information for the given organization
     */
    @GET
    @Path("{orgID:\\d+}/cbi")
    public Response getCBISponsorships(@PathParam("orgID") Integer organizationID) {
        Optional<CBI> cbiData = cache.get(Integer.toString(organizationID), null, CBI.class, () -> {
            CBI base = cbi.getSponsorships();
            List<MemberOrganizationsBenefits> benefits = base
                    .memberOrganizationsBenefits()
                    .stream()
                    .filter(mob -> mob.id() == organizationID)
                    .toList();
            // to reduce confusion in the data, we will create a copy of the sponsored projects where just the current org is listed
            // new instances are required as the records are unmodifiable, and we wouldn't want to mutate the cache record accidentally
            List<SponsoredProject> sponsoredProjects = base
                    .sponsoredProjects()
                    .stream()
                    .filter(sp -> sp.sponsoringOrganizations().stream().anyMatch(so -> so.id() == organizationID))
                    .map(sp -> new SponsoredProject(sp.project(),
                            sp.sponsoringOrganizations().stream().filter(so -> so.id() == organizationID).toList()))
                    .toList();
            return new CBI(benefits, sponsoredProjects);
        }).getData();
        if (cbiData.isEmpty() || cbiData.get().memberOrganizationsBenefits().isEmpty()) {
            throw new NotFoundException("Unable to extract CBI data for current organization");
        }
        // get the working groups for each WGPA the org has
        return Response.ok(cbiData.get()).build();
    }

    @GET
    @Authenticated
    @RolesAllowed({ CR, DE, CRA, MA })
    @Path("{orgID:\\d+}/committers")
    public Response getCommitters(@PathParam("orgID") String organizationID) {
        Optional<List<PeopleData>> committers = orgService.getCommittersForOrganization(organizationID);
        if (committers.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        // get the working groups for each WGPA the org has
        return Response.ok(committers.get()).build();
    }

    @GET
    @Authenticated
    @RolesAllowed({ EMPLY, CR, DE, CRA, MA })
    @Path("{orgID:\\d+}/committers/activity")
    public Response getCommitterActivityOverview(@PathParam("orgID") String organizationID, @QueryParam("period") String period,
            @QueryParam("from_period") String fromPeriod) {
        List<CommitterProjectActivity> orgCountryActivity = getCommitterActivity(organizationID, period, fromPeriod);

        // reduce count using sum, and build immutable
        return Response
                .ok(OrganizationActivity
                        .builder()
                        .setCount(orgCountryActivity
                                .stream()
                                .collect(Collectors
                                        .groupingBy((CommitterProjectActivity cpa) -> cpa
                                                .getCompositeID()
                                                .getCommitterAffiliation()
                                                .getCommitter()
                                                .getId()))
                                .keySet()
                                .size())
                        .setPeriod(determinePeriodParameterName(period, fromPeriod)
                                .equals(MembershipPortalParameterNames.FROM_PERIOD.getName()) ? fromPeriod : checkPeriodOrDefault(period))
                        .build())
                .build();
    }

    @GET
    @Authenticated
    @RolesAllowed({ EMPLY, CR, DE, CRA, MA })
    @Path("{orgID:\\d+}/committers/activity/yearly")
    public Response getCommitterActivityYearly(@PathParam("orgID") String organizationID) {
        LocalDate d = LocalDate.now();
        // get the previous year of results
        List<CommitterProjectActivity> orgCountryActivity = getCommitterActivity(organizationID, null,
                getPeriodStringForDate(d.minusYears(1)));

        // group results by the activity period, then reduce the count into a single int
        // result, and transform into an
        // output.
        List<OrganizationActivity> activityList = orgCountryActivity
                .stream()
                .collect(Collectors.groupingBy((CommitterProjectActivity cpa) -> cpa.getCompositeID().getPeriod()))
                .entrySet()
                .stream()
                .map(e -> OrganizationActivity
                        .builder()
                        .setPeriod(e.getKey())
                        .setCount(e
                                .getValue()
                                .stream()
                                .collect(Collectors
                                        .groupingBy((CommitterProjectActivity cpa) -> cpa
                                                .getCompositeID()
                                                .getCommitterAffiliation()
                                                .getCommitter()
                                                .getId()))
                                .keySet()
                                .size())
                        .build())
                .collect(Collectors.toList());
        // backfill activity log for 1 year
        activityList = backfillHistoricActivityData(activityList, d);
        // sort the period, sorting from greatest (most recent) to smallest (least
        // recent)
        activityList.sort((o1, o2) -> o2.getPeriod().compareTo(o1.getPeriod()));

        return Response.ok(OrganizationYearlyActivity.builder().setActivity(activityList).build()).build();
    }

    @GET
    @Authenticated
    @RolesAllowed({ CR, DE, CRA, MA })
    @Path("{orgID:\\d+}/contributors")
    public Response getContributors(@PathParam("orgID") String organizationID) {
        // get users, where we only care about if they are committers and employees
        List<EnhancedPersonData> people = getEnhancedPeopleDetails(organizationID,
                Arrays.asList(OrganizationalUserType.CM, OrganizationalUserType.EMPLY));
        if (people.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        // filter out the people who have the CM role
        return Response.ok(people.stream().filter(p -> !p.getRelations().contains(OrganizationalUserType.CM.name())).map(p -> {
            PeopleData.Builder out = PeopleData
                    .builder()
                    .setPersonID(p.getId())
                    .setFname(p.getFirstName())
                    .setLname(p.getLastName())
                    .setIssuesPending(false)
                    .setMember(true)
                    .setUnixAcctCreated(true)
                    .setType("XX");
            if (accessHelper.userCanViewProtectedField(organizationID)) {
                out.setEmail(p.getEmail());
            } else {
                out.setEmail("");
            }
            return out.build();
        }).collect(Collectors.toList())).build();
    }

    @GET
    @Path("{orgID:\\d+}/projects")
    public Response getProjects(@PathParam("orgID") String organizationID) {
        List<ProjectData> memberOrgs = projectsService.getProjectsForOrganization(organizationID, wrap);
        if (memberOrgs.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        // get the working groups for each WGPA the org has
        return Response.ok(memberOrgs).build();
    }

    @GET
    @Path("{orgID:\\d+}/products")
    public Response getProducts(@PathParam("orgID") String organizationID) {
        // limit results to given org
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.ORGANIZATION_ID.getName(), organizationID);

        List<OrganizationProducts> orgProducts = eclipseDBDao.get(new RDBMSQuery<>(wrap, filters.get(OrganizationProducts.class), params));
        if (orgProducts.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        return Response.ok(orgProducts.stream().map(productMapper::toModel).collect(Collectors.toList())).build();
    }

    @POST
    @Authenticated
    @RolesAllowed({ CR, DE, CRA, MA })
    @Path("{orgID:\\d+}/products")
    public Response createProduct(@PathParam("orgID") String organizationID, OrganizationProductData newProduct) {
        List<OrganizationProducts> orgProducts = eclipseDBDao
                .add(new RDBMSQuery<>(wrap, filters.get(OrganizationProducts.class), new MultivaluedMapImpl<>()),
                        Arrays.asList(productMapper.toDTO(newProduct, eclipseDBDao)));
        if (orgProducts.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        // track creation/update of product
        Integer pId = orgProducts.get(0).getCompositeId().getProductId();
        modLogHelper.postInsertModLog(ident, OrganizationProducts.TABLE, organizationID, pId != null ? Integer.toString(pId) : null);
        return Response.noContent().build();
    }

    @GET
    @Path("{orgID:\\d+}/products/{productID:\\d+}")
    public Response getProduct(@PathParam("orgID") Integer organizationID, @PathParam("productID") Integer productID) {
        // limit results to given org
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.ORGANIZATION_ID.getName(), Integer.toString(organizationID));
        params.add(MembershipPortalParameterNames.PRODUCT_ID.getName(), Integer.toString(productID));

        List<OrganizationProducts> orgProducts = eclipseDBDao.get(new RDBMSQuery<>(wrap, filters.get(OrganizationProducts.class), params));
        if (orgProducts.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        return Response.ok(productMapper.toModel(orgProducts.get(0))).build();
    }

    @PUT
    @Authenticated
    @RolesAllowed({ CR, DE, CRA, MA })
    @Path("{orgID:\\d+}/products/{productID:\\d+}")
    public Response updateProducts(@PathParam("orgID") Integer organizationID, OrganizationProductData newProduct,
            @PathParam("productID") Integer productID) {
        List<OrganizationProducts> orgProducts = eclipseDBDao
                .add(new RDBMSQuery<>(wrap, filters.get(OrganizationProducts.class), new MultivaluedMapImpl<>()),
                        Arrays.asList(productMapper.toDTO(newProduct, eclipseDBDao)));
        if (orgProducts.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        // track creation/update of product
        modLogHelper.postUpdateModLog(ident, OrganizationProducts.TABLE, Integer.toString(organizationID), Integer.toString(productID));
        return Response.ok(productMapper.toModel(orgProducts.get(0))).build();
    }

    @DELETE
    @Authenticated
    @RolesAllowed({ CR, DE, CRA, MA })
    @Path("{orgID:\\d+}/products/{productID}")
    public Response deleteProduct(@PathParam("orgID") String organizationID, @PathParam("productID") String productID) {
        // limit results to given org + product
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.ORGANIZATION_ID.getName(), organizationID);
        params.add(MembershipPortalParameterNames.PRODUCT_ID.getName(), productID);
        eclipseDBDao.delete(new RDBMSQuery<>(wrap, filters.get(OrganizationProducts.class), params));
        modLogHelper.postDeleteModLog(ident, OrganizationProducts.TABLE, organizationID, productID);
        return Response.ok().build();
    }

    @GET
    @Authenticated
    @RolesAllowed({ CR, DE, CRA, MA, EMPLY })
    @Path("{orgID:\\d+}/representatives")
    public Response getRepresentatives(@PathParam("orgID") String organizationID) {
        // don't cache return as results are sensitive to user state.
        List<EnhancedPersonData> people = getEnhancedPeopleDetails(organizationID, Arrays.asList(CR, CRA, MA, DE));
        if (people == null) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()).build();
        }
        // convert the people to basic entries that omits email, as it isn't needed here
        return Response
                .ok(people
                        .stream()
                        .map(p -> BasicPersonData
                                .builder()
                                .setFirstName(p.getFirstName())
                                .setId(p.getId())
                                .setLastName(p.getLastName())
                                .setRelations(p.getRelations())
                                .build()))
                .build();
    }

    @GET
    @Path("{orgID:\\d+}/working_groups")
    public Response getWorkingGroups(@PathParam("orgID") String organizationID) {
        Optional<MemberOrganization> memberOrg = orgService.getByID(organizationID);
        if (memberOrg.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        // get the working groups for each WGPA the org has
        return Response
                .ok(memberOrg
                        .get()
                        .getWgpas()
                        .stream()
                        .map(wgpa -> wgService.getByName(wgpa.getWorkingGroup()))
                        .filter(Optional::isPresent)
                        .collect(Collectors.toList()))
                .build();
    }

    @POST
    @Authenticated
    @RolesAllowed({ CR, CRA, DE, MA })
    @Path("{orgID:\\d+}/logos")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response postOrganizationLogoUpdate(@PathParam("orgID") String organizationID,
            @MultipartForm OrganizationLogoUpdateRequest request) throws FileTooLargeException {
        // handle writing and checking image data
        ImageStoreFormat format = ImageStoreFormats.getFormat(request.imageFormat);
        String extension = ImageFileHelper.convertMimeType(request.imageMIME);
        if (extension != null) {
            LOGGER.debug("{} image: {} bytes", format, request.image.length);
            // business logic - only print should use eps
            if (!format.equals(ImageStoreFormats.PRINT) && extension.equalsIgnoreCase("eps")) {
                throw new BadRequestException("EPS format should not be used for non-print logos");
            }
            images.writeImage(() -> request.image, organizationID, request.imageMIME, Optional.of(format));
        }
        // clear the item from the cache
        cache.fuzzyRemove(organizationID, MemberOrganization.class);
        // return the org
        Optional<MemberOrganization> org = orgService.getByID(organizationID);
        if (org.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND.getStatusCode()).build();
        }
        modLogHelper.postUpdateModLog(ident, OrganizationInformation.TABLE, organizationID, format.getName());
        return Response.ok(org.get()).build();
    }

    private List<MemberOrganization> getMemberOrganizations(String workingGroup, String level) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Looking up docids for WG: {}", TransformationHelper.formatLog(workingGroup));
        }

        List<String> docids = wgService.getWGPADocumentIDs().getOrDefault(workingGroup, Collections.emptyList());
        if (!docids.isEmpty()) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER
                        .trace("Looking up organizations for '{}' working group with the following agreement IDs: {}",
                                TransformationHelper.formatLog(workingGroup), docids);
            }
            wrap.setParam(FoundationDBParameterNames.DOCUMENT_IDS.getName(), docids);
        } else if (StringUtils.isNotBlank(workingGroup)) {
            // return empty response if wg was set but no results
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("No working group found with alias '{}', returning empty", TransformationHelper.formatLog(workingGroup));
            }
            return Collections.emptyList();
        }

        // once we've collected docIDs and ensured valid start state, fetch
        // organizations to be filtered
        List<MemberOrganization> orgs = orgService.get(wrap);
        if (orgs.isEmpty()) {
            return Collections.emptyList();
        }
        // if we have level and working group set, filter the output
        if (StringUtils.isNotEmpty(workingGroup) && StringUtils.isNotEmpty(level)) {
            orgs = orgs
                    .stream()
                    .filter(o -> o
                            .getWgpas()
                            .stream()
                            .anyMatch(wgpa -> wgpa.getWorkingGroup().equalsIgnoreCase(workingGroup)
                                    && wgpa.getLevel().equalsIgnoreCase(level)))
                    .collect(Collectors.toList());
        }
        return orgs;
    }

    /**
     * Retrieves all commit activity count entries for an organization, either for the given period, or from the second passed period.
     * 
     * @param organizationID organization to retrieve entries for
     * @param period the singular period to retrieve entries for (optional)
     * @param fromPeriod the starting period to retrieve entries from (optional)
     * @return list of committer activity entries.
     */
    private List<CommitterProjectActivity> getCommitterActivity(String organizationID, String period, String fromPeriod) {
        // set up params for current call
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        // get the period parameter in use (period and from_period are not compatible
        // with each other)
        String paramName = determinePeriodParameterName(period, fromPeriod);
        if (paramName.equals(MembershipPortalParameterNames.FROM_PERIOD.getName())) {
            params.add(paramName, checkPeriodOrDefault(fromPeriod));
        } else {
            params.add(paramName, checkPeriodOrDefault(period));
        }
        params.add(MembershipPortalParameterNames.ORGANIZATION_ID.getName(), organizationID);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Retrieving organization activity with params {}", params);
        }
        RDBMSQuery<CommitterProjectActivity> q = new RDBMSQuery<>(wrap, filters.get(CommitterProjectActivity.class), params);
        q.setUseLimit(false);
        // get cached activity as there is no need to refresh it too often.
        return dashboardDao.get(q);
    }

    /**
     * Determine the parameter name based on validity of period values, using the below priority:
     * 
     * period > from period > default (single period, this month)
     * 
     * @param period the proposed period value
     * @param fromPeriod the proposed from period value
     * @return either the {@linkplain MembershipPortalParameterNames.PERIOD} or {@linkplain MembershipPortalParameterNames.FROM_PERIOD}
     * parameter names.
     */
    private String determinePeriodParameterName(String period, String fromPeriod) {
        if (isPeriodFormatValid(period)) {
            return MembershipPortalParameterNames.PERIOD.getName();
        } else if (isPeriodFormatValid(fromPeriod)) {
            return MembershipPortalParameterNames.FROM_PERIOD.getName();
        } else {
            return MembershipPortalParameterNames.PERIOD.getName();
        }
    }

    /**
     * Provides a checked period or the default, which is the current month
     * 
     * @param period the proposed time that should be used. Accurate to the month, in the format YYYYMM
     * @return the passed parameter if valid, otherwise returns default value.
     */
    private String checkPeriodOrDefault(String period) {
        return isPeriodFormatValid(period) ? period : getPeriodStringForDate(LocalDate.now());
    }

    private String getPeriodStringForDate(LocalDate d) {
        // check that month is 2 characters
        String month = Integer.toString(d.getMonthValue());
        if (month.length() == 1) {
            month = "0" + month;
        }
        return Integer.toString(d.getYear()) + month;
    }

    private boolean isPeriodFormatValid(String proposedPeriod) {
        return StringUtils.isNotBlank(proposedPeriod) && PERIOD_DATE_FORMAT.matcher(proposedPeriod).matches();
    }

    private List<OrganizationActivity> backfillHistoricActivityData(List<OrganizationActivity> base, LocalDate from) {
        List<OrganizationActivity> activityList = new ArrayList<>(base);
        LocalDate d = from;
        for (int i = 0; i < MONTHS_IN_A_YEAR; i++) {
            // get current period string and update date string
            String currentPeriodString = getPeriodStringForDate(d);
            d = d.minus(1, ChronoUnit.MONTHS);

            // check if we need to back fill this period
            if (activityList.stream().noneMatch(oa -> oa.getPeriod().equals(currentPeriodString))) {
                activityList.add(OrganizationActivity.builder().setCount(0).setPeriod(currentPeriodString).build());
            }
        }
        return activityList;
    }

}
