/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.dtos.dashboard;

import java.util.Date;
import java.util.Objects;

import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.model.DtoTable;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

/**
 * CommitterAffiliation table record. As this is never read in directly as an entity, no filters have been created.
 * 
 * @author Martin Lowe
 *
 */
@Table
@Entity
public class CommitterAffiliation extends BareNode {
    public static final DtoTable TABLE = new DtoTable(CommitterAffiliation.class, "ca");

    @Id
    @ManyToOne
    @JoinColumn(name = "id")
    private Committer committer;
    private Integer orgId;
    private String orgName;
    private Date activeDate;
    private Date inactiveDate;
    private Date ts;

    @Override
    public Committer getId() {
        return getCommitter();
    }

    public Committer getCommitter() {
        return this.committer;
    }

    /**
     * @param committer the id to set
     */
    public void setCommitter(Committer committer) {
        this.committer = committer;
    }

    /**
     * @return the orgId
     */
    public Integer getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    /**
     * @return the orgName
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * @param orgName the orgName to set
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * @return the activeDate
     */
    public Date getActiveDate() {
        return activeDate;
    }

    /**
     * @param activeDate the activeDate to set
     */
    public void setActiveDate(Date activeDate) {
        this.activeDate = activeDate;
    }

    /**
     * @return the inactiveDate
     */
    public Date getInactiveDate() {
        return inactiveDate;
    }

    /**
     * @param inactiveDate the inactiveDate to set
     */
    public void setInactiveDate(Date inactiveDate) {
        this.inactiveDate = inactiveDate;
    }

    /**
     * @return the ts
     */
    public Date getTs() {
        return ts;
    }

    /**
     * @param ts the ts to set
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(activeDate, committer, inactiveDate, orgId, orgName, ts);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        CommitterAffiliation other = (CommitterAffiliation) obj;
        return Objects.equals(activeDate, other.activeDate) && Objects.equals(committer, other.committer)
                && Objects.equals(inactiveDate, other.inactiveDate) && Objects.equals(orgId, other.orgId)
                && Objects.equals(orgName, other.orgName) && Objects.equals(ts, other.ts);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CommitterAffiliation [committer=");
        builder.append(committer);
        builder.append(", orgId=");
        builder.append(orgId);
        builder.append(", orgName=");
        builder.append(orgName);
        builder.append(", activeDate=");
        builder.append(activeDate);
        builder.append(", inactiveDate=");
        builder.append(inactiveDate);
        builder.append(", ts=");
        builder.append(ts);
        builder.append("]");
        return builder.toString();
    }

}
