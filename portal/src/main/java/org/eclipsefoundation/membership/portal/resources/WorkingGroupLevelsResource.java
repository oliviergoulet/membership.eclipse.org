/**
 * Copyright (c) 2021, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.resources;

import java.util.List;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.eclipsefoundation.efservices.services.WorkingGroupService;

/**
 * Retrieves working group definitions from the working groups service.
 *
 * @author Martin Lowe
 */
@Path("working_groups")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class WorkingGroupLevelsResource extends AbstractRESTResource {

    @Inject
    WorkingGroupService wgService;

    @GET
    public Response getWorkingGroups(@QueryParam("parent_organization") List<String> parentOrganization,
            @QueryParam("status") List<String> statuses) {
        // return the results as a response
        return Response.ok(wgService.get(parentOrganization, statuses)).build();
    }

    @GET
    @Path("{name}")
    public Response getWorkingGroup(@PathParam("name") String name) {
        // return the results as a response
        return Response.ok(wgService.getByName(name)).build();
    }
}
