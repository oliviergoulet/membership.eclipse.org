/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.config;

import java.util.List;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/**
 * Contains all properties related to role filtering. Includes an enabled flag,
 * a list of admin users, and a lsit of elevated users.
 */
@ConfigMapping(prefix = "eclipse.membership-portal.role-filter")
public interface RoleFilterConfig {

    @WithDefault(value = "true")
    boolean enabled();

    List<String> adminUsers();

    List<String> elevatedUsers();
}
