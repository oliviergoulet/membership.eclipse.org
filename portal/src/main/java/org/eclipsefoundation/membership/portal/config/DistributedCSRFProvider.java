/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.config;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.Dependent;
import jakarta.enterprise.inject.Produces;

import org.eclipsefoundation.core.model.CSRFGenerator;
import org.eclipsefoundation.membership.portal.daos.REMPersistenceDAO;
import org.eclipsefoundation.persistence.dto.DistributedCSRFToken.DistributedCSRFTokenFilter;
import org.eclipsefoundation.persistence.model.DistributedCSRFGenerator;

@Dependent
public class DistributedCSRFProvider {

    @Produces
    @ApplicationScoped
    public CSRFGenerator distributedGenerator(REMPersistenceDAO dao, DistributedCSRFTokenFilter filter) {
        return new DistributedCSRFGenerator(dao, filter);
    }
}
