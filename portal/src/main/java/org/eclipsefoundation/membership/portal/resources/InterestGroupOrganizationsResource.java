/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.resources;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.efservices.api.models.InterestGroup;
import org.eclipsefoundation.efservices.api.models.InterestGroup.InterestGroupParticipant;
import org.eclipsefoundation.efservices.api.models.InterestGroup.Organization;
import org.eclipsefoundation.membership.portal.model.MemberOrganization;
import org.eclipsefoundation.membership.portal.service.InterestGroupService;
import org.eclipsefoundation.membership.portal.service.OrganizationsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Produces(MediaType.APPLICATION_JSON)
@Path("interest_groups/{id}/organizations")
public class InterestGroupOrganizationsResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(InterestGroupOrganizationsResource.class);

    @Inject
    InterestGroupService igService;
    @Inject
    OrganizationsService orgService;
    
    @Inject
    RequestWrapper wrap;

    @GET
    public Response getById(@PathParam("id") String id) {
        // check that IG exists before querying for assoc. orgs
        Optional<InterestGroup> ig = igService.getById(id);
        if (ig.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND.getStatusCode()).build();
        }

        return Response.ok(loadOrganizationByIG(ig.get())).build();
    }

    /**
     * Filters out and returns all {@link MemberOrganization} entities that
     * contribute to a given Interest Group. Matches the member org ids against all
     * IG participant's org ids.
     * 
     * @param ig The given interest group id
     * @return Returns a list of all {@link MemberOrganization} entities belonging
     *         to a particular interest group
     */
    private List<MemberOrganization> loadOrganizationByIG(InterestGroup ig) {
        // combine lists of IG participants + leads, then deduplicate org ids
        List<InterestGroupParticipant> igUsers = Stream.concat(ig.getLeads().stream(), ig.getParticipants().stream())
                .collect(Collectors.toList());

        LOGGER.debug("Filtering out distinct org ids");
        List<String> orgIds = igUsers.stream()
                .map(InterestGroupParticipant::getOrganization)
                .map(Organization::getId)
                .distinct().collect(Collectors.toList());

        // Fetch MemberOrganizations from loading cache
        List<MemberOrganization> allMembers = orgService.get(wrap);

        // Filter for member orgs that have the ids of all IG participant orgs
        LOGGER.debug("Returning MemberOrganizations with ids: {}", orgIds);
        return allMembers.stream()
                .filter(m -> orgIds.stream().anyMatch(id -> Integer.valueOf(id) == m.getOrganizationID()))
                .collect(Collectors.toList());
    }
}
