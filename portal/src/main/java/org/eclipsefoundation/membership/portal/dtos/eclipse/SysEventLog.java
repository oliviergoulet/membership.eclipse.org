/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.dtos.eclipse;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Objects;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.portal.namespace.MembershipPortalParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

@Entity
@Table(name = "SYS_EvtLog")
public class SysEventLog extends BareNode {
    public static final DtoTable TABLE = new DtoTable(SysEventLog.class, "sel");

    @Id
    private int logId;
    private String logTable;
    @Column(name = "PK1")
    private String pk1;
    @Column(name = "PK2")
    private String pk2;
    private String logAction;
    private String uid;
    private LocalDateTime evtDateTime;

    @Override
    public Object getId() {
        return getLogId();
    }

    /**
     * @return the logId
     */
    public int getLogId() {
        return logId;
    }

    /**
     * @param logId the logId to set
     */
    public void setLogId(int logId) {
        this.logId = logId;
    }

    /**
     * @return the logTable
     */
    public String getLogTable() {
        return logTable;
    }

    /**
     * @param logTable the logTable to set
     */
    public void setLogTable(String logTable) {
        this.logTable = logTable;
    }

    /**
     * @return the pk1
     */
    public String getPk1() {
        return pk1;
    }

    /**
     * @param pk1 the pk1 to set
     */
    public void setPk1(String pk1) {
        this.pk1 = pk1;
    }

    /**
     * @return the pk2
     */
    public String getPk2() {
        return pk2;
    }

    /**
     * @param pk2 the pk2 to set
     */
    public void setPk2(String pk2) {
        this.pk2 = pk2;
    }

    /**
     * @return the logAction
     */
    public String getLogAction() {
        return logAction;
    }

    /**
     * @param logAction the logAction to set
     */
    public void setLogAction(String logAction) {
        this.logAction = logAction;
    }

    /**
     * @return the uid
     */
    public String getUid() {
        return uid;
    }

    /**
     * @param uid the uid to set
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * @return the evtDateTime
     */
    public LocalDateTime getEvtDateTime() {
        return evtDateTime;
    }

    /**
     * @param evtDateTime the evtDateTime to set
     */
    public void setEvtDateTime(LocalDateTime evtDateTime) {
        this.evtDateTime = evtDateTime;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SysEventLog [logId=");
        builder.append(logId);
        builder.append(", logTable=");
        builder.append(logTable);
        builder.append(", pk1=");
        builder.append(pk1);
        builder.append(", pk2=");
        builder.append(pk2);
        builder.append(", logAction=");
        builder.append(logAction);
        builder.append(", uid=");
        builder.append(uid);
        builder.append(", evtDateTime=");
        builder.append(evtDateTime);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(logTable, pk1, pk2, logAction, logAction, uid, evtDateTime, logId);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        SysEventLog other = (SysEventLog) obj;
        return Objects.equals(logTable, other.logTable) && Objects.equals(pk1, other.pk1)
                && Objects.equals(pk2, other.pk2) && Objects.equals(logAction, other.logAction)
                && Objects.equals(uid, other.uid) && DateTimeHelper.looseCompare(this.evtDateTime.atZone(ZoneId.of("UTC")), other.evtDateTime.atZone(ZoneId.of("UTC")))
                && logId == other.logId;
    }

    @Singleton
    public static class SysEventLogFilter implements DtoFilter<SysEventLog> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            if (isRoot) {
                // log ID check
                String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
                if (id != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".logId = ?",
                            new Object[] { Integer.valueOf(id) }));
                }
                // username check
                String username = params.getFirst(MembershipPortalParameterNames.USERNAME.getName());
                if (username != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".uid = ?",
                            new Object[] { username }));
                }
                // username check
                String orgId = params.getFirst(MembershipPortalParameterNames.ORGANIZATION_ID.getName());
                if (orgId != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".pk1 = ?",
                            new Object[] { orgId }));
                }
            }

            return stmt;
        }

        @Override
        public Class<SysEventLog> getType() {
            return SysEventLog.class;
        }
    }
}
