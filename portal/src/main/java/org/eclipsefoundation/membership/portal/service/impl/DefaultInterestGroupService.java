/**
 * Copyright (c) 2022, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.service.impl;

import java.util.List;
import java.util.Optional;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import org.eclipsefoundation.core.model.ParameterizedCacheKey;
import org.eclipsefoundation.core.service.LoadingCacheManager;
import org.eclipsefoundation.efservices.api.models.InterestGroup;
import org.eclipsefoundation.efservices.precaches.InterestGroupPrecacheProvider;
import org.eclipsefoundation.membership.portal.service.InterestGroupService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation of the {@link InterestGroupService}. Fetches all
 * {@link InterestGroup} data from the {@link InterestGroupPrecacheProvider} via
 * the {@link LoadingCacheManager}.
 */
@ApplicationScoped
public class DefaultInterestGroupService implements InterestGroupService {
    public static final Logger LOGGER = LoggerFactory.getLogger(DefaultInterestGroupService.class);

    private static final String DEFAULT_CACHE_ID = "all";

    @Inject
    LoadingCacheManager cacheManager;

    @Override
    public List<InterestGroup> getAll() {
        LOGGER.trace("Loading all interest groups");
        return cacheManager.getList(ParameterizedCacheKey.builder()
                .setId(DEFAULT_CACHE_ID)
                .setClazz(InterestGroup.class)
                .setParams(new MultivaluedMapImpl<>()).build());
    }

    @Override
    public Optional<InterestGroup> getById(String igId) {
        return getAll().stream().filter(ig -> ig.getId().equalsIgnoreCase(igId)).findFirst();
    }
}
