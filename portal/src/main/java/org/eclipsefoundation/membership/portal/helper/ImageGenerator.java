/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.helper;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.AttributedCharacterIterator;
import java.text.AttributedString;
import java.util.Map;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A class with util methods that allow image generation.
 */
public final class ImageGenerator {
    private static final Logger LOGGER = LoggerFactory.getLogger(ImageGenerator.class);

    private static final int IMG_WIDTH = 250;
    private static final int IMG_HEIGHT = 150;
    private static final int IMG_PADDING = 2;
    private static final int IMG_SIZE_EMPTY = 1;

    // Text length cutoffs
    private static final int CHAR_LIMIT_SMALL = 10;
    private static final int CHAR_LIMIT_MID = 50;

    private static final Map<TextAttribute, Object> STR_ATTRIBS_DEFAULT = Map.of(TextAttribute.FONT,
            new Font("Arial", Font.PLAIN, 24));
    private static final Map<TextAttribute, Object> STR_ATTRIBS_LARGE = Map.of(TextAttribute.FONT,
            new Font("Arial", Font.PLAIN, 46));
    private static final Map<TextAttribute, Object> STR_ATTRIBS_SMALL = Map.of(TextAttribute.FONT,
            new Font("Arial", Font.PLAIN, 18));

    /**
     * Generates an image from a given piece of text Using the {@link Graphics2D} and {@link BufferedImage} APIs.
     * Used to auto-generate org logos when they don't have a web version.
     * The generated images are pngs with black text on a transparent background.
     * 
     * @param text The text used to generate the image.
     * @return The serialized content of the newly generated image. Or an empty
     *         array if unsuccessful
     */
    public static byte[] generateImageFromText(String text) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Generating image for text: '{}'", text);
        }

        // Generate an image with alpha support to allow transparency
        BufferedImage image = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphic = image.createGraphics();

        // Set all rendering settings, opting for higher quality
        graphic.setColor(Color.BLACK);
        graphic.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION,
                RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        graphic.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphic.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        graphic.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
        graphic.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        graphic.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphic.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        graphic.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);

        AttributedCharacterIterator textIterator = createIteratorForText(text);

        // Create line measurer and set starting position
        LineBreakMeasurer lineMeasurer = new LineBreakMeasurer(textIterator, graphic.getFontRenderContext());
        lineMeasurer.setPosition(textIterator.getBeginIndex());

        // Calculate vertical padding to allow centered text
        float yPos = calcStartYPos(lineMeasurer, textIterator.getEndIndex());
        lineMeasurer.setPosition(textIterator.getBeginIndex());

        // Iterate over text until the entire paragraph has been generated
        while (lineMeasurer.getPosition() < textIterator.getEndIndex()) {

            TextLayout layout = lineMeasurer.nextLayout(IMG_WIDTH - IMG_PADDING);

            // Calculate horizontal padding to allow centered text
            float xPos = (IMG_WIDTH - layout.getAdvance()) / 2;
            if (xPos < 0) {
                xPos = 0;
            }

            // Update y position based on current line ascent, then move y past current line
            // once drawn
            yPos += layout.getAscent();
            layout.draw(graphic, xPos, yPos);
            yPos += layout.getDescent() + layout.getLeading();
        }

        graphic.dispose();

        // Serialize image for downstream persistence
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageIO.write(image, "png", baos);
            return baos.toByteArray();
        } catch (IOException e) {
            LOGGER.error("Problem generating image from text: {}", text, e);
            return new byte[] {};
        }
    }

    /**
     * Generates a 1px x 1px empty gif and returns a byte[] of it's contents.
     * Used in the cases where a logo was requested with an invalid orgid.
     * 
     * @return The serialized content of the newly generated image. Or an empty
     *         array if unsuccessful
     */
    public static byte[] generateEmptyImage() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Generating empty image");
        }

        // Serialize image for downstream persistence
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageIO.write(new BufferedImage(IMG_SIZE_EMPTY, IMG_SIZE_EMPTY, BufferedImage.TYPE_INT_ARGB), "gif", baos);
            return baos.toByteArray();
        } catch (IOException e) {
            LOGGER.error("Problem generating empty image", e);
            return new byte[] {};
        }
    }

    /**
     * Returns an instance of an AttributedCharacterIterator based on the given text.
     * Instantiates an AttributedString using the given text and attributes, then returns it's corresponding AttributedCharacterIterator.
     * The attributes are determined by the text length. Text with length below the CHAR_LIMIT_SMALL(10) threshold will be larger. 
     * Text with length above the CHAR_LIMIT_MID(50) threshold will be smaller. Any other text will use the default.
     * 
     * @param text The text used to generate the iterator.
     * @return The appropriate AttributedCharacterIterator for the given text.
     */
    private static AttributedCharacterIterator createIteratorForText(String text) {
        if (text.length() <= CHAR_LIMIT_SMALL) {
            return new AttributedString(text, STR_ATTRIBS_LARGE).getIterator();
        }
        if (text.length() >= CHAR_LIMIT_MID) {
            return new AttributedString(text, STR_ATTRIBS_SMALL).getIterator();
        }
        return new AttributedString(text, STR_ATTRIBS_DEFAULT).getIterator();
    }

    /**
     * Calculates the starting Y-axis cursor position to allow text to be centered vertically.
     * This calculation is done by iterating over the text, gathering the sum of all
     * line heights, and determining the vertical padding needed based on the image height.
     * 
     * @param lineMeasurer The measurer used to get the TextLayout for each line of text
     * @param endIndex The index of the last character in the text string
     * @return The starting Y-axis cursor position that allows for centered text
     */
    private static float calcStartYPos(LineBreakMeasurer lineMeasurer, int endIndex) {
        float totalTextHeight = 0;

        // Iterate over text once to get total text height (there's no other easy way to calc this)
        while (lineMeasurer.getPosition() < endIndex) {
            TextLayout layout = lineMeasurer.nextLayout(IMG_WIDTH - IMG_PADDING);
            totalTextHeight += layout.getAscent() + layout.getDescent() + layout.getLeading();
        }

        // Calculate vertical padding to allow centered text
        return (IMG_HEIGHT - totalTextHeight) / 2;
    }

    private ImageGenerator() {

    }
}
