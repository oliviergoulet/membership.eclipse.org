/**
 * Copyright (c) 2021, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.resources;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.foundationdb.client.model.SysRelationData;
import org.eclipsefoundation.membership.portal.api.FoundationDBParameterNames;
import org.eclipsefoundation.membership.portal.api.SysAPI;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;

/**
 * Handles system CRUD requests.
 *
 * @author Martin Lowe
 */
@Path("sys")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SysResource extends AbstractRESTResource {

    @RestClient
    @Inject
    SysAPI sysAPI;

    @GET
    @Path("relations")
    public Response getAll(@QueryParam("type") String type) {
        // create parameter map
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(FoundationDBParameterNames.TYPE.getName(), type);
        Optional<List<SysRelationData>> relations = cache
                .get(type != null ? type : "n/a", params, SysRelationData.class,
                        () -> middle.getAll(i -> sysAPI.getSysRelations(i, type), SysRelationData.class))
                .getData();
        // retrieve the possible object
        if (relations.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        // return the results as a response
        return Response.ok(relations.get()).build();
    }
}
