/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.dtos.rem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.portal.namespace.MembershipPortalParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.core.MultivaluedMap;

@Table
@Entity
public class Sessions extends BareNode {
    public static final DtoTable TABLE = new DtoTable(Sessions.class, "sess");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String username;
    private Date time;
    private Integer organization;

    @Override
    public Object getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getOrganization() {
        return organization;
    }

    public void setOrganization(Integer organization) {
        this.organization = organization;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(id, username, time, organization);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Sessions other = (Sessions) obj;
        return Objects.equals(this.id, other.id) && Objects.equals(this.username, other.username)
                && Objects.equals(this.time, other.time) && Objects.equals(this.organization, other.organization);
    }

    @Singleton
    public static class SessionsFilter implements DtoFilter<Sessions> {
        private final SimpleDateFormat simpleISODateFormat = new SimpleDateFormat("yyyy-MM-dd");

        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);

            if (isRoot) {
                // id check
                String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
                if (StringUtils.isNumeric(id)) {
                    stmt.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".id = ?",
                                    new Object[] { Integer.valueOf(id) }));
                }

                // username check
                String username = params.getFirst(MembershipPortalParameterNames.USERNAME.getName());
                if (StringUtils.isNotBlank(username)) {
                    stmt.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".username = ?",
                                    new Object[] { username }));
                }

                // organization check
                String organizationID = params.getFirst(MembershipPortalParameterNames.ORGANIZATION_ID.getName());
                if (StringUtils.isNumeric(organizationID)) {
                    stmt.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".organization = ?",
                                    new Object[] { Integer.valueOf(organizationID) }));
                }

                // since check
                String since = params.getFirst(MembershipPortalParameterNames.SINCE.getName());
                if (StringUtils.isNotBlank(since)) {
                    try {
                        stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".time >= ?",
                                new Object[] { simpleISODateFormat.parse(since) }));
                    } catch (ParseException e) {
                        // Possible, but we already confirm the format upon receipt
                        throw new BadRequestException(e);
                    }
                }
            }

            return stmt;
        }

        @Override
        public Class<Sessions> getType() {
            return Sessions.class;
        }
    }
}
