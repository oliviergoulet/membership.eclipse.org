/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.api;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.model.MemberOrganizationData;
import org.eclipsefoundation.membership.portal.api.OrganizationAPI.OrganizationRequestParams;

import io.quarkus.oidc.client.filter.OidcClientFilter;
import jakarta.annotation.security.RolesAllowed;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("views")
@OidcClientFilter
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "fdndb-api")
public interface ComplexAPI {

    @GET
    @Path("members")
    @RolesAllowed({ "fdb_read_organization", "fdb_read_organization_documents" })
    public Response getMembers(@BeanParam BaseAPIParameters params, @BeanParam OrganizationRequestParams orgParams);

    @GET
    @Path("members")
    @RolesAllowed({ "fdb_read_organization", "fdb_read_organization_documents" })
    public List<MemberOrganizationData> getMember(@QueryParam("id") String id);

    @GET
    @Path("contacts")
    @RolesAllowed({ "fdb_read_organization_employment", "fdb_read_people" })
    public Response getContacts(@BeanParam BaseAPIParameters params, @QueryParam("organization_id") String orgId,
            @QueryParam("person_id") String personId);
}
