/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.namespace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import jakarta.inject.Singleton;

import org.eclipsefoundation.core.namespace.UrlParameterNamespace;

@Singleton
public final class MembershipPortalParameterNames implements UrlParameterNamespace {
    public static final UrlParameter USER_ID = new UrlParameter("userID");
    public static final UrlParameter FORM_ID = new UrlParameter("formID");
    public static final UrlParameter FORM_IDS = new UrlParameter("formIDs");
    public static final UrlParameter FORM_STATE = new UrlParameter("state");
    public static final UrlParameter APPLICATION_GROUP = new UrlParameter("application_group");
    public static final UrlParameter BEFORE_DATE_UPDATED_IN_MILLIS = new UrlParameter("beforeDateUpdatedMillis");
    public static final UrlParameter INTEREST_GROUP_ID = new UrlParameter("ig_id");

    public static final UrlParameter PRODUCT_ID = new UrlParameter("product_id");
    public static final UrlParameter USERNAME = new UrlParameter("username");
    public static final UrlParameter SERVER_NAME = new UrlParameter("server_name");
    public static final UrlParameter SINCE = new UrlParameter("since");
    public static final UrlParameter LOG_TABLE = new UrlParameter("log_table");

    public static final UrlParameter LOGIN = new UrlParameter("login");
    public static final UrlParameter ORGANIZATION_ID = new UrlParameter("organization_id");
    public static final UrlParameter PROJECT = new UrlParameter("project");
    public static final UrlParameter PERIOD = new UrlParameter("period");
    public static final UrlParameter FROM_PERIOD = new UrlParameter("from_period");
    public static final UrlParameter HAS_ORGANIZATION = new UrlParameter("has_organization");
    public static final UrlParameter ROLE = new UrlParameter("role");

    private static final List<UrlParameter> params = Collections.unmodifiableList(
            Arrays.asList(USER_ID, FORM_ID, FORM_IDS, FORM_STATE, BEFORE_DATE_UPDATED_IN_MILLIS, APPLICATION_GROUP,
                    INTEREST_GROUP_ID, LOGIN, PROJECT, PERIOD, ORGANIZATION_ID, FROM_PERIOD, HAS_ORGANIZATION, ROLE,
                    PRODUCT_ID, USERNAME, SERVER_NAME, SINCE, LOG_TABLE));

    @Override
    public List<UrlParameter> getParameters() {
        return new ArrayList<>(params);
    }
}
