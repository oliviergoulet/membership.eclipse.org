/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.dtos.dashboard;

import java.util.Date;
import java.util.Objects;

import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.model.DtoTable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table
public class Project extends BareNode {
    public static final DtoTable TABLE = new DtoTable(Project.class, "c");

    @Id
    private String id;
    private String parentId;
    @Column(name = "short")
    private String shortID;
    private String name;
    private String license;
    private String specification;
    private String url;
    private Date start;

    @Override
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the parendId
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * @param parentId the parendId to set
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the shortID
     */
    public String getShortID() {
        return shortID;
    }

    /**
     * @param shortID the shortID to set
     */
    public void setShortID(String shortID) {
        this.shortID = shortID;
    }

    /**
     * @return the license
     */
    public String getLicense() {
        return license;
    }

    /**
     * @param license the license to set
     */
    public void setLicense(String license) {
        this.license = license;
    }

    /**
     * @return the specification
     */
    public String getSpecification() {
        return specification;
    }

    /**
     * @param specification the specification to set
     */
    public void setSpecification(String specification) {
        this.specification = specification;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the start
     */
    public Date getStart() {
        return start;
    }

    /**
     * @param start the start to set
     */
    public void setStart(Date start) {
        this.start = start;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(id, license, name, parentId, shortID, specification, start, url);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Project other = (Project) obj;
        return Objects.equals(id, other.id) && Objects.equals(license, other.license)
                && Objects.equals(name, other.name) && Objects.equals(parentId, other.parentId)
                && Objects.equals(shortID, other.shortID) && Objects.equals(specification, other.specification)
                && Objects.equals(start, other.start) && Objects.equals(url, other.url);
    }
}
