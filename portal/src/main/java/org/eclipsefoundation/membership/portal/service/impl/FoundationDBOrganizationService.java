/**
 * Copyright (c) 2021, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.service.impl;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.core.response.PaginatedResultsFilter;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.efservices.api.models.WorkingGroup;
import org.eclipsefoundation.efservices.api.models.WorkingGroup.WorkingGroupParticipationLevel;
import org.eclipsefoundation.efservices.services.WorkingGroupService;
import org.eclipsefoundation.foundationdb.client.model.MemberOrganizationData;
import org.eclipsefoundation.foundationdb.client.model.OrganizationContactData;
import org.eclipsefoundation.foundationdb.client.model.PeopleData;
import org.eclipsefoundation.membership.portal.api.ComplexAPI;
import org.eclipsefoundation.membership.portal.api.FoundationDBParameterNames;
import org.eclipsefoundation.membership.portal.api.OrganizationAPI;
import org.eclipsefoundation.membership.portal.api.OrganizationAPI.OrganizationRequestParams;
import org.eclipsefoundation.membership.portal.api.PeopleAPI;
import org.eclipsefoundation.membership.portal.api.PeopleAPI.PeopleRequestParams;
import org.eclipsefoundation.membership.portal.api.ProjectAPI;
import org.eclipsefoundation.membership.portal.dtos.eclipse.OrganizationInformation;
import org.eclipsefoundation.membership.portal.model.MemberOrganization;
import org.eclipsefoundation.membership.portal.model.MemberOrganization.MemberOrganizationDescription;
import org.eclipsefoundation.membership.portal.model.MemberOrganization.MemberOrganizationLevel;
import org.eclipsefoundation.membership.portal.model.MemberOrganization.MemberOrganizationLogos;
import org.eclipsefoundation.membership.portal.model.MemberOrganization.OrganizationWGPA;
import org.eclipsefoundation.membership.portal.namespace.ImageStoreFormat.ImageStoreFormats;
import org.eclipsefoundation.membership.portal.namespace.MembershipPortalParameterNames;
import org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType;
import org.eclipsefoundation.membership.portal.service.ImageStoreService;
import org.eclipsefoundation.membership.portal.service.OrganizationsService;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.arc.Arc;
import io.quarkus.arc.InstanceHandle;
import io.quarkus.runtime.Startup;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.control.ActivateRequestContext;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Builds a list of working group definitions from the FoundationDB, making use of some data from the FoundationDB system API to retrieve
 * common values such as relation descriptions.
 * 
 * @author Martin Lowe
 */
@Startup
@ApplicationScoped
public class FoundationDBOrganizationService implements OrganizationsService {
    public static final Logger LOGGER = LoggerFactory.getLogger(FoundationDBOrganizationService.class);

    private static final String ALL_LIST_CACHE_KEY = "allEntries";
    private static final int SECOND_PAGE = 2;

    // internal service beans
    @Inject
    DefaultHibernateDao dao;
    @Inject
    DefaultHibernateDao eDBdao;
    @Inject
    FilterService filters;
    @Inject
    ImageStoreService imageService;
    @Inject
    WorkingGroupService wgService;
    @Inject
    CachingService cache;

    // api interaction beans
    @Inject
    APIMiddleware middleware;
    @RestClient
    @Inject
    ComplexAPI complexAPI;
    @RestClient
    @Inject
    OrganizationAPI orgAPI;
    @RestClient
    @Inject
    PeopleAPI peopleAPI;
    @RestClient
    @Inject
    ProjectAPI projectsAPI;

    /**
     * On construct, start the loading cache for the full list of member organizations. This is a large call that we want to be highly
     * available, as attempting to call it without a cache would result in timeouts.
     */
    @PostConstruct
    void init() {
        LOGGER.info("Starting init of cached organizations");
        generateHACacheEntries();
    }

    @ActivateRequestContext
    public void generateHACacheEntries() {
        try {
            // handle request-less instantiation of wrapper + params
            InstanceHandle<RequestWrapper> wrapper = Arc.container().instance(RequestWrapper.class);
            RequestWrapper wrap;
            if (wrapper.isAvailable() && Arc.container().requestContext().isActive() && wrapper.get().getURI() != null) {
                wrap = wrapper.get();
            } else {
                // backup for when populating HA cache
                wrap = new FlatRequestWrapper(URI.create("https://membership.eclipse.org/api/organizations"));
            }
            // set limit to 100 for org cache
            wrap.setParam(DefaultUrlParameterNames.PAGESIZE, "100");
            LOGGER.debug("Generating HA cache for organizations data: {}", BaseAPIParameters.buildFromWrapper(wrap));
            generateCache(BaseAPIParameters.buildFromWrapper(wrap));
            Integer maxResults = Integer.valueOf(wrap.getHeader(PaginatedResultsFilter.MAX_RESULTS_SIZE_HEADER));
            Integer maxPageSize = Integer.valueOf(wrap.getHeader(PaginatedResultsFilter.MAX_PAGE_SIZE_HEADER));
            // this should never go beyond max int
            int pageMaxApprox = (int) Math.ceil((double) maxResults / maxPageSize);
            for (int i = SECOND_PAGE; i <= pageMaxApprox; i++) {
                wrap.setParam(DefaultUrlParameterNames.PAGE, Integer.toString(i));
                LOGGER.debug("Generating HA cache for organizations data: {}", BaseAPIParameters.buildFromWrapper(wrap));
                generateCache(BaseAPIParameters.builder().setLimit(maxPageSize).setPage(i).setRequestWrapper(wrap).build());
            }
        } catch (Exception e) {
            LOGGER.error("Error while populating HA cache, performance may be impacted", e);
        }
    }

    /**
     * Used to supply the highly available
     * 
     * @param params
     * @return
     */
    protected List<MemberOrganization> generateCache(BaseAPIParameters params) {
        Objects.requireNonNull(params.getRequestWrapper());
        LOGGER.debug("Creating cached value for {}({})", params, BaseAPIParameters.buildFromWrapper(params.getRequestWrapper()));
        return cache
                .get(ALL_LIST_CACHE_KEY, new MultivaluedMapImpl<>(), MemberOrganization.class, () -> middleware
                        .paginationPassThrough(
                                p -> complexAPI
                                        .getMembers(p, OrganizationRequestParams.populateFromWrap(params.getRequestWrapper()).build()),
                                params.getRequestWrapper(), MemberOrganizationData.class)
                        .stream()
                        .map(this::convertToMemberOrganization)
                        .collect(Collectors.toList()))
                .getData()
                .orElseGet(Collections::emptyList);
    }

    @Override
    public List<MemberOrganization> get(RequestWrapper wrap) {
        OrganizationRequestParams params = OrganizationRequestParams.populateFromWrap(wrap).build();
        // if we are getting base params, try and use HA cache
        if (params.isEmpty()) {
            try {
                BaseAPIParameters key = BaseAPIParameters.buildFromWrapper(wrap);
                LOGGER.debug("Hitting HA cache for organizations data: {}", key);
                return generateCache(key);
            } catch (Exception e) {
                LOGGER.error("Error retrieving member organization from HA cache: ", e);
            }
        }
        LOGGER.debug("Hitting general cache for organizations data");
        // generate the param map for the cache key
        MultivaluedMap<String, String> paramMap = new MultivaluedMapImpl<>();
        paramMap.add(DefaultUrlParameterNames.PAGE.getName(), wrap.getFirstParam(DefaultUrlParameterNames.PAGE).orElseGet(() -> "1"));
        paramMap
                .add(DefaultUrlParameterNames.PAGESIZE.getName(),
                        wrap.getFirstParam(DefaultUrlParameterNames.PAGESIZE).orElseGet(() -> "25"));
        paramMap.add(FoundationDBParameterNames.WORKING_GROUP.getName(), params.getWorkingGroup());
        paramMap.put(FoundationDBParameterNames.LEVELS.getName(), params.getLevels());

        // retrieve the results from the base cache rather than the HA cache
        return cache
                .get(ALL_LIST_CACHE_KEY, paramMap, MemberOrganization.class, () -> middleware
                        .paginationPassThrough(p -> complexAPI.getMembers(p, OrganizationRequestParams.populateFromWrap(wrap).build()),
                                wrap, MemberOrganizationData.class)
                        .stream()
                        .map(this::convertToMemberOrganization)
                        .collect(Collectors.toList()))
                .getData()
                .orElseGet(Collections::emptyList);
    }

    @Override
    public Optional<MemberOrganization> getByID(String id) {
        try {
            return cache.get(id, new MultivaluedMapImpl<>(), MemberOrganization.class, () -> {
                List<MemberOrganizationData> orgs = complexAPI.getMember(id);
                if (orgs.isEmpty()) {
                    return null;
                }
                return convertToMemberOrganization(orgs.get(0));
            }).getData();
        } catch (Exception e) {
            LOGGER.error("Error retrieving member organization: ", e);
        }
        return Optional.empty();
    }

    @Override
    public List<MemberOrganization> getByIDs(List<String> ids) {
        try {
            return cache
                    .get(ids.toString(), new MultivaluedMapImpl<>(), MemberOrganization.class,
                            () -> middleware
                                    .getAll(b -> complexAPI.getMembers(null, OrganizationRequestParams.builder().setIds(ids).build()),
                                            MemberOrganizationData.class)
                                    .stream()
                                    .map(this::convertToMemberOrganization)
                                    .collect(Collectors.toList()))
                    .getData()
                    .orElse(Collections.emptyList());
        } catch (Exception e) {
            LOGGER.error("Error retrieving member organization: ", e);
        }
        return Collections.emptyList();
    }

    @Override
    public Optional<List<OrganizationContactData>> getOrganizationContacts(String orgID, String userName) {
        return cache
                .get(orgID, new MultivaluedMapImpl<>(), OrganizationContactData.class, () -> middleware
                        .getAll(i -> orgAPI
                                .getOrganizationContact(orgID, userName, i,
                                        OrganizationRequestParams.builder().setIsNotExpired(false).build()),
                                OrganizationContactData.class))
                .getData();
    }

    @Override
    public Optional<List<OrganizationContactData>> getOrganizationContacts(String userName) {
        return cache
                .get(userName, new MultivaluedMapImpl<>(), OrganizationContactData.class,
                        () -> middleware
                                .getAll(i -> orgAPI
                                        .getOrganizationContacts(i,
                                                OrganizationRequestParams.builder().setPersonID(userName).setIsNotExpired(false).build()),
                                        OrganizationContactData.class))
                .getData();
    }

    @Override
    public void removeOrganizationContact(String orgID, String userName, String role) {
        clearContactCaches(orgID, userName);
        orgAPI.removeOrganizationContacts(orgID, userName, role);
    }

    @Override
    public OrganizationContactData updateOrganizationContact(String orgID, OrganizationContactData orgContact) {
        clearContactCaches(orgID, orgContact.getPersonID());
        return orgAPI.updateOrganizationContacts(orgID, orgContact);
    }

    @Override
    public Optional<List<PeopleData>> getCommittersForOrganization(String id) {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(FoundationDBParameterNames.RELATION.getName(), "CM");
        params.add(FoundationDBParameterNames.ORGANIZATION_ID.getName(), id);
        return cache
                .get(id + "-cm", params, PeopleData.class, () -> middleware
                        .getAll(i -> peopleAPI.getPeople(i, PeopleRequestParams.builder().setRelation("CM").setOrganizationID(id).build()),
                                PeopleData.class))
                .getData();
    }

    @Override
    public List<OrganizationalUserType> getUserAccessRoles(String orgID, String userName) {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipPortalParameterNames.USER_ID.getName(), userName);
        return cache
                .get(orgID + "-access", params, OrganizationContactData.class, () -> middleware
                        .getAll(i -> orgAPI
                                .getOrganizationContact(orgID, userName, i,
                                        OrganizationRequestParams.builder().setIsNotExpired(false).build()),
                                OrganizationContactData.class))
                .getData()
                .orElse(Collections.emptyList())
                .stream()
                .map(c -> OrganizationalUserType.valueOfChecked(c.getRelation()))
                .collect(Collectors.toList());
    }

    /**
     * Using SysRelation data and the passed organization, creates a member organization object that can be returned which includes multiple
     * tables of data.
     * 
     * @param org base organization of the member organization.
     * @return a member organization object containing additional contextual data about the org
     */
    public MemberOrganization convertToMemberOrganization(MemberOrganizationData org) {
        MemberOrganization.Builder out = MemberOrganization.builder();
        out.setOrganizationID(org.getOrganizationID());
        out.setName(org.getName());
        out.setMemberSince(org.getMemberSince());
        out.setRenewalDate(calculateRenewalDate(org.getOrganizationID(), org.getLatestMembershipStart()));
        out
                .setLevels(org
                        .getRelations()
                        .stream()
                        .map(membership -> MemberOrganizationLevel
                                .builder()
                                .setLevel(membership.getRelation())
                                .setDescription(membership.getDescription())
                                .setSortOrder(membership.getSortOrder())
                                .build())
                        .collect(Collectors.toList()));

        // Get org information from eclipse db
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), Integer.toString(org.getOrganizationID()));
        RDBMSQuery<OrganizationInformation> q = new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                filters.get(OrganizationInformation.class), params);
        q.setRoot(false);
        // retrieve and handle the org info data
        List<OrganizationInformation> r = eDBdao.get(q);
        if (!r.isEmpty()) {
            OrganizationInformation info = r.get(0);
            // retrieve the descriptions of the organization
            out.setDescription(MemberOrganizationDescription.builder().setLongDescription(info.getLongDescription()).build());
            try {
                new URI(info.getCompanyUrl());
                out.setWebsite(info.getCompanyUrl());
            } catch (URISyntaxException e) {
                LOGGER.debug("Error while converting URL for organization '{}', leaving blank", info.getOrganizationID(), e);
            }
        }

        // retrieve the logos of the organization
        MemberOrganizationLogos.Builder logos = MemberOrganizationLogos.builder();
        logos.setWeb(imageService.retrieveImageUrl(Integer.toString(org.getOrganizationID()), Optional.of(ImageStoreFormats.WEB)));
        logos.setPrint(imageService.retrieveImageUrl(Integer.toString(org.getOrganizationID()), Optional.of(ImageStoreFormats.PRINT)));
        out.setLogos(logos.build());

        // get org WGPA documents and convert to model to be returned with extra context
        out.setWgpas(org.getDocuments().stream().map(wgpa -> {
            OrganizationWGPA.Builder owgpa = OrganizationWGPA.builder();
            owgpa.setDocumentID(wgpa.getDocumentID());
            owgpa.setLevel(wgpa.getRelation());

            // find the WG that has a matching ID for current document
            Optional<WorkingGroup> wg = wgService.getByAgreementId(wgpa.getDocumentID());
            // if this is missing then we have a document that wasn't id'd as a WGPA doc
            if (wg.isPresent()) {
                Optional<WorkingGroupParticipationLevel> pl = wg
                        .get()
                        .getLevels()
                        .stream()
                        .filter(l -> l.getRelation().equals(wgpa.getRelation()))
                        .findFirst();
                pl.ifPresent(wgpl -> owgpa.setDescription(wgpl.getDescription()));
                owgpa.setWorkingGroup(wg.get().getAlias());
            } else {
                owgpa.setWorkingGroup("unknown");
            }
            return owgpa.build();
        }).collect(Collectors.toList()));
        return out.build();
    }

    /**
     * Clears caches of contact data for the given org containing the updated user and role.
     * 
     * @param org the org that is being updated
     * @param userName the user whos entry is being updated
     */
    private void clearContactCaches(String org, String userName) {
        // clear the org contacts that contain the removed user entry
        cache.fuzzyRemove(org, OrganizationContactData.class);
        cache.fuzzyRemove(userName, OrganizationContactData.class);
    }

    /**
     * Calculates the member renewal date by comparing the provided membership date with today's date.
     * 
     * @param orgId The org id for which the renewal is calculated
     * @param membershipDate The given membershp date to use in the calculation
     * @return The membership date with the year updated to this year or next year, depending on the day.
     */
    private LocalDate calculateRenewalDate(int orgId, LocalDate membershipDate) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.info("Calculating renewal date for org '{}' with date '{}'", orgId, membershipDate);
        }

        LocalDate now = LocalDate.now();
        int todayDayOfYear = now.getDayOfYear();
        int membershipDayOfYear = membershipDate.getDayOfYear();  

        // If day is after today(or today), renewal is set to this year
        if(membershipDayOfYear >= todayDayOfYear) {
            return membershipDate.withYear(now.getYear());
        }

        // If the day is before today, we set the date to next year
        return membershipDate.withYear(now.getYear() + 1);
    }
}
