/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.helper;

import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.CR;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.CRA;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.DE;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.MA;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import org.eclipsefoundation.core.exception.FinalForbiddenException;
import org.eclipsefoundation.core.exception.UnauthorizedException;
import org.eclipsefoundation.core.helper.TransformationHelper;
import org.eclipsefoundation.membership.portal.config.RoleFilterConfig;
import org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType;
import org.eclipsefoundation.membership.portal.service.OrganizationsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.security.identity.SecurityIdentity;

/**
 * Helper class for validating user access. Provides methods for verifying
 * admin/elevated_user status, as well as denying access.
 */
@ApplicationScoped
public final class UserAccessHelper {
    public static final Logger LOGGER = LoggerFactory.getLogger(UserAccessHelper.class);

    @Inject
    SecurityIdentity identity;

    @Inject
    RoleFilterConfig config;

    @Inject
    OrganizationsService orgService;

    /**
     * Validates whether the currently connected user is an admin user. Returns
     * false if the current user is anonymous.
     * 
     * @return True if admin, false if not
     */
    public boolean userIsAdmin() {
        if (identity.isAnonymous()) {
            return false;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Validating user '{}' is admin", identity.getPrincipal().getName());
        }

        return config.adminUsers()
                .stream()
                .flatMap(Stream::of)
                .anyMatch(uid -> uid.equalsIgnoreCase(identity.getPrincipal().getName()));
    }

    /**
     * Validates whether the currently connected user is an elevated_access user.
     * Returns false if the current user is anonymous.
     * 
     * @return True if elevated_user status, false if not
     */
    public boolean userHasElevatedAccess() {
        if (identity.isAnonymous()) {
            return false;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Validating user '{}' is elevated", identity.getPrincipal().getName());
        }

        return config.elevatedUsers()
                .stream()
                .flatMap(Stream::of)
                .anyMatch(uid -> uid.equalsIgnoreCase(identity.getPrincipal().getName()));
    }

    /**
     * Denies access by returning an appropriate 400-series response based on the
     * client's anonymity. Throws a Response.Status.UNAUTHORIZED.getStatusCode() if
     * the user is anonymous. Throws a Response.Status.FORBIDDEN.getStatusCode() if
     * they are logged in.
     */
    public void denyAccess() {
        if (identity.isAnonymous()) {
            throw new UnauthorizedException("User must be logged in to access this application");
        } else {
            throw new FinalForbiddenException("Current user cannot access this functionality");
        }
    }

    /**
     * Verifies whether the current user is allowed to access the given data
     * by validating they are a contact with one of the provided roles.
     * 
     * @param orgId        The org to check for contacts.
     * @param allowedRoles The allowed roles.
     * @return True if the current user is a valid contact with a matching role.
     *         False otherwise.
     */
    public boolean isAllowedToAccess(String orgId, List<OrganizationalUserType> allowedRoles) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Checking userID '{}' for access of level '{}' in org '{}'",
                    identity.getPrincipal().getName(), allowedRoles, TransformationHelper.formatLog(orgId));
        }
        
        // Check if we have contacts, otherwise assume that user does not have access
        return orgService.getUserAccessRoles(orgId, identity.getPrincipal().getName())
                .stream()
                .anyMatch(allowedRoles::contains);
    }

    /**
     * Verifies whether the current user can view protected fields such as email.
     * Checks first if they are an admin or an elevated user, then defaults to
     * validating if they are an OrgContact with correct access.
     * 
     * @param organizationID The organization to check against the current user.
     * @return True if they can access. False if not.
     */
    public boolean userCanViewProtectedField(String organizationID) {
        // Anonymous users should not be able to see protected fields
        if (identity.isAnonymous()) {
            return false;
        }

        // Admins and elevate users should be able to see protected fields
        if (userIsAdmin() || userHasElevatedAccess()) {
            return true;
        }

        // Validate against the list of allowed roles
        List<OrganizationalUserType> allowedRoles = Arrays.asList(CR, CRA, DE, MA);
        return isAllowedToAccess(organizationID, allowedRoles);
    }
}
