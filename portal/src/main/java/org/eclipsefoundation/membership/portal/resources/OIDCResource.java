/**
 * Copyright (c) 2021, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.resources;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipsefoundation.core.namespace.RequestHeaderNames;
import org.eclipsefoundation.foundationdb.client.model.OrganizationContactData;
import org.eclipsefoundation.membership.portal.config.PortalBaseConfig;
import org.eclipsefoundation.membership.portal.daos.REMPersistenceDAO;
import org.eclipsefoundation.membership.portal.dtos.rem.Sessions;
import org.eclipsefoundation.membership.portal.namespace.MembershipPortalParameterNames;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.security.Authenticated;
import io.smallrye.jwt.auth.principal.DefaultJWTCallerPrincipal;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * Handles OIDC routing for the request.
 *
 * @author Martin Lowe
 */
@Path("")
public class OIDCResource extends AbstractRESTResource {
    public static final Logger LOGGER = LoggerFactory.getLogger(OIDCResource.class);

    @Inject
    PortalBaseConfig baseConfig;

    @Inject
    REMPersistenceDAO dao;

    @GET
    @Authenticated
    @Path("/login")
    public Response routeLogin() throws URISyntaxException {
        logLoginRequest();
        return redirect(baseConfig.baseUrl());
    }

    /**
     * While OIDC plugin takes care of actual logout, a route is needed to properly
     * reroute anon user to home page.
     *
     * @throws URISyntaxException
     */
    @GET
    @Path("/logout")
    public Response routeLogout() throws URISyntaxException {
        return redirect(baseConfig.baseUrl());
    }

    @GET
    @Path("userinfo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserInfo(@HeaderParam(value = RequestHeaderNames.CSRF_TOKEN) String csrf) {
        // require CSRF to protect user info (could contain PII)
        if (!ident.isAnonymous()) {
            // cast the principal to a JWT token (which is the type for OIDC)
            DefaultJWTCallerPrincipal defaultPrin = (DefaultJWTCallerPrincipal) ident.getPrincipal();
            // create wrapper around data for output
            InfoWrapper uiw = new InfoWrapper();
            uiw.setName(defaultPrin.getName());
            uiw.setGivenName(defaultPrin.getClaim("given_name"));
            uiw.setFamilyName(defaultPrin.getClaim("family_name"));
            uiw.setEmail(defaultPrin.getClaim("email"));

            // If user is set as an admin, show in userinfo
            if (accessHelper.userIsAdmin()) {
                LOGGER.debug("{} has the 'admin' role", uiw.name);
                uiw.additionalRoles.add("admin");
            }

            // Add role if user has 'elevated_access'
            Optional<String> orgId = wrap.getFirstParam(MembershipPortalParameterNames.ORGANIZATION_ID);
            if (accessHelper.userHasElevatedAccess()) {
                LOGGER.debug("{} has the 'elevated access' role", uiw.name);
                uiw.additionalRoles.add("eclipsefdn_membership_portal_elevated_access");

                // Add org role if org id is present
                if (orgId.isPresent()) {
                    String role = wrap.getFirstParam(MembershipPortalParameterNames.ROLE).orElse("CR");
                    uiw.organizationalRoles = Map.of(Integer.valueOf(orgId.get()), Arrays.asList(role));
                    LOGGER.debug("Added org roles: {} for user {}", uiw.organizationalRoles, uiw.name);
                }
            }

            // Only member reps have organizational roles. 'elevated_access' is for EF staff
            if (!accessHelper.userHasElevatedAccess()) {
                LOGGER.debug("Checking org roles for user: {}", uiw.name);
                orgService
                        .getOrganizationContacts(defaultPrin.getName())
                        .ifPresent(l -> uiw.organizationalRoles = l
                                .stream()
                                .collect(Collectors.groupingBy(OrganizationContactData::getOrganizationID))
                                .entrySet()
                                .stream()
                                .collect(Collectors
                                        .toMap(Entry<Integer, List<OrganizationContactData>>::getKey,
                                                entry -> entry
                                                        .getValue()
                                                        .stream()
                                                        .map(OrganizationContactData::getRelation)
                                                        .collect(Collectors.toList()))));

                LOGGER.debug("{} has the the following org roles: {}", uiw.name, uiw.organizationalRoles);
            }

            return Response.ok(uiw).build();
        } else {
            return Response.noContent().build();
        }
    }

    @GET
    @Path("csrf")
    public Response generateCSRF() {
        return Response.ok().build();
    }

    /**
     * Logs incoming successful login request to the Sessions table.
     */
    private void logLoginRequest() {
        // retrieve the list of orgs for current user
        Principal p = ident.getPrincipal();
        Optional<List<OrganizationContactData>> cs = orgService.getOrganizationContacts(p.getName());
        // generate the entry for the login event
        Sessions session = new Sessions();
        session.setUsername(p.getName());
        session.setTime(new Date());
        // set the org to the first available org if present
        if (cs.isPresent() && !cs.get().isEmpty()) {
            session.setOrganization(cs.get().get(0).getOrganizationID());
        }
        dao.add(new RDBMSQuery<>(wrap, filters.get(Sessions.class)), Arrays.asList(session));
    }

    private Response redirect(String location) throws URISyntaxException {
        return Response.temporaryRedirect(new URI(location)).build();
    }

    public static class InfoWrapper {
        String name;
        String givenName;
        String familyName;
        String email;
        Map<Integer, List<String>> organizationalRoles = Collections.emptyMap();
        List<String> additionalRoles = new ArrayList<>();

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the givenName
         */
        public String getGivenName() {
            return givenName;
        }

        /**
         * @param givenName the givenName to set
         */
        public void setGivenName(String givenName) {
            this.givenName = givenName;
        }

        /**
         * @return the familyName
         */
        public String getFamilyName() {
            return familyName;
        }

        /**
         * @param familyName the familyName to set
         */
        public void setFamilyName(String familyName) {
            this.familyName = familyName;
        }

        /**
         * @return the email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @param email the email to set
         */
        public void setEmail(String email) {
            this.email = email;
        }

        /**
         * @return the organizationalRoles
         */
        public Map<Integer, List<String>> getOrganizationalRoles() {
            return new HashMap<>(organizationalRoles);
        }

        /**
         * @param organizationalRoles the organizationalRoles to set
         */
        public void setOrganizationalRoles(Map<Integer, List<String>> organizationalRoles) {
            this.organizationalRoles = new HashMap<>(organizationalRoles);
        }

        /**
         * @return the additionalRoles
         */
        public List<String> getAdditionalRoles() {
            return additionalRoles;
        }

        /**
         * @param additionalRoles the additionalRoles to set
         */
        public void setAdditionalRoles(List<String> additionalRoles) {
            this.additionalRoles = additionalRoles;
        }
    }
}
