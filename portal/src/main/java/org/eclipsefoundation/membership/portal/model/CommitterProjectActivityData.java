/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.model;

import jakarta.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

import io.quarkus.runtime.annotations.RegisterForReflection;

@AutoValue
@RegisterForReflection
@JsonDeserialize(builder = AutoValue_CommitterProjectActivityData.Builder.class)
public abstract class CommitterProjectActivityData {
    public abstract String getLogin();

    @Nullable
    public abstract String getProject();

    public abstract String getPeriod();

    public abstract int getCount();

    public static Builder builder() {
        return new AutoValue_CommitterProjectActivityData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setLogin(String login);

        public abstract Builder setProject(@Nullable String project);

        public abstract Builder setPeriod(String period);

        public abstract Builder setCount(int count);

        public abstract CommitterProjectActivityData build();

    }
}
