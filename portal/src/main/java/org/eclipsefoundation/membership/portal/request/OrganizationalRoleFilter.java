/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*       Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.request;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipsefoundation.core.exception.FinalForbiddenException;
import org.eclipsefoundation.membership.portal.config.RoleFilterConfig;
import org.eclipsefoundation.membership.portal.helper.UserAccessHelper;
import org.jboss.resteasy.core.interception.jaxrs.PostMatchContainerRequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.security.identity.SecurityIdentity;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.ext.Provider;

/**
 * Filters requests post-matching to secure PII data for organizations to people associated with said organization.
 * 
 * @author Martin Lowe, Zachary Sabourin
 *
 */
@Provider
public class OrganizationalRoleFilter implements ContainerRequestFilter {
    public static final Logger LOGGER = LoggerFactory.getLogger(OrganizationalRoleFilter.class);

    private static final Pattern ORGANIZATION_ID_URL_PATTERN = Pattern.compile("/organizations/((?!slim)[^/]+).*");

    @Inject
    Instance<RoleFilterConfig> config;
    @Inject
    SecurityIdentity identity;
    @Inject
    UserAccessHelper accessHelper;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        // check if this filter is enabled (may be disabled on development
        if (config.get().enabled()) {
            String principalName = identity.getPrincipal().getName();
            LOGGER.trace("Checking user {} for organization roles", principalName);
            String currentPath = requestContext.getUriInfo().getPath();
            boolean isReadAction = requestContext.getMethod().equals("GET");

            // While otherwise unrestricted, 'admin' is blocked from modifying '/contacts'
            if (accessHelper.userIsAdmin()) {
                LOGGER.trace("User {} is admin, checking if is a blocked action", principalName);
                if (currentPath.contains("/contacts") && !isReadAction) {
                    LOGGER.trace("Admin {} attempted to do a blocked write action, refusing", principalName);
                    accessHelper.denyAccess();
                }
                return;
            }

            // Elevated users are read-only
            boolean isElevated = accessHelper.userHasElevatedAccess();
            if (isElevated && !isReadAction) {
                LOGGER.trace("Elevated user {} attempted to do write action, refusing", principalName);
                throw new FinalForbiddenException("Elevated user is read-only");
            }

            // check if we are in a resolvable resource before reflecting
            Matcher matcher = ORGANIZATION_ID_URL_PATTERN.matcher(currentPath);
            if (orgRoleNeedsValidation(matcher, isElevated)) {
                // check annotation on target endpoint to be sure that user is allowed to access
                // the resources
                Method m = ((PostMatchContainerRequestContext) requestContext).getResourceMethod().getMethod();
                RolesAllowed rolesAnnotation = m.getAnnotation(RolesAllowed.class);

                if (!dataIsAccessible(rolesAnnotation, matcher.group(1))) {
                    LOGGER.debug("User {} does not have the proper role to access current endpoint", principalName);
                    accessHelper.denyAccess();
                }
            }
        }
    }

    /**
     * Checks if the current request needs org contact role validation
     * 
     * @param matcher The matcher used to check if the resource is tied to a specific org
     * @param isElevated Current user's elevated status
     * @return True if an orgId path and user isn't elevated. False otherwise.
     */
    private boolean orgRoleNeedsValidation(Matcher matcher, boolean isElevated) {
        return matcher.matches() && !isElevated;
    }

    /**
     * Checks if the data for the current request is accesible. Either by there being no roles set on the endpoint, or the current user
     * having the proper roles.
     * 
     * @param rolesAnnotation The RolesAllowed on the accessed resource
     * @param orgId The orgId in the path
     * @return True if data is accesible. False if not. Based on the current resource annotation and current user org role
     */
    private boolean dataIsAccessible(RolesAllowed rolesAnnotation, String orgId) {
        return rolesAnnotation == null || accessHelper.isAllowedToAccess(orgId, Arrays.asList(rolesAnnotation.value()));
    }
}
