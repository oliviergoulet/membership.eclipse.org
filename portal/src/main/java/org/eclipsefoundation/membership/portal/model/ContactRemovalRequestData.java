/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.model;

import org.eclipsefoundation.foundationdb.client.model.PeopleData;
import org.eclipsefoundation.membership.portal.request.model.ContactRemovalRequest;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_ContactRemovalRequestData.Builder.class)
public abstract class ContactRemovalRequestData {

    public abstract PeopleData getPerson();
    public abstract MemberOrganization getOrganization();
    public abstract String getReason();
    public abstract ContactRemovalRequest getRawRequest();
    
    public static Builder builder() {
        return new AutoValue_ContactRemovalRequestData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setPerson(PeopleData person);
        public abstract Builder setOrganization(MemberOrganization organization);
        public abstract Builder setReason(String reason);
        public abstract Builder setRawRequest(ContactRemovalRequest rawRequest);
        public abstract ContactRemovalRequestData build();
    }
}
