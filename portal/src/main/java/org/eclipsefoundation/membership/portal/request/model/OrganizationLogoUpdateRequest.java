/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.request.model;

import jakarta.annotation.Nonnull;
import jakarta.ws.rs.FormParam;

public class OrganizationLogoUpdateRequest {

    @FormParam("image")
    @Nonnull
    public byte[] image;
    @FormParam("image_mime")
    @Nonnull
    public String imageMIME;
    @FormParam("image_format")
    @Nonnull
    public String imageFormat;
}
