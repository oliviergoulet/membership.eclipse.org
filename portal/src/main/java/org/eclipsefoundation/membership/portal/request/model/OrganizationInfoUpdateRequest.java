/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.request.model;

import jakarta.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_OrganizationInfoUpdateRequest.Builder.class)
public abstract class OrganizationInfoUpdateRequest {

    public abstract String getDescription();

    @Nullable
    public abstract String getCompanyUrl();

    public static Builder builder() {
        return new AutoValue_OrganizationInfoUpdateRequest.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setDescription(String description);

        public abstract Builder setCompanyUrl(@Nullable String companyUrl);

        public abstract OrganizationInfoUpdateRequest build();
    }

}
