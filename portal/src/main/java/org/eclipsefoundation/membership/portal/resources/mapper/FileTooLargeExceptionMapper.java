/*
 * Copyright (C) 2019, 2024 Eclipse Foundation and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
*/
package org.eclipsefoundation.membership.portal.resources.mapper;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import org.eclipsefoundation.core.model.Error;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.undertow.server.handlers.form.MultiPartParserDefinition.FileTooLargeException;

/**
 * 
 * @author Martin Lowe
 */
@Provider
public class FileTooLargeExceptionMapper implements ExceptionMapper<FileTooLargeException> {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileTooLargeExceptionMapper.class);

    @Override
    public Response toResponse(FileTooLargeException exception) {
        LOGGER.error(exception.getMessage(), exception);
        return new Error(Status.REQUEST_ENTITY_TOO_LARGE,
                "Could not process the given request: " + exception.getMessage()).asResponse();
    }
}
