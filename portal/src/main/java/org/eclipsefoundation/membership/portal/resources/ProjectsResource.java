/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.resources;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.eclipsefoundation.core.helper.TransformationHelper;
import org.eclipsefoundation.membership.portal.service.ProjectsService;

@Path("projects")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProjectsResource extends AbstractRESTResource {

    @Inject
    ProjectsService committerService;

    @GET
    @Path("{projectId}/organizations")
    public Response getOrganizationsForProject(@PathParam("projectId") String projectId) {
        if (!committerService.projectExists(projectId)) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Project with ID '{}' not found in FoundationDB", TransformationHelper.formatLog(projectId));
            }
            return Response.status(Response.Status.NOT_FOUND.getStatusCode()).build();
        }
        return Response.ok(committerService.getOrganizationsForProject(projectId, wrap)).build();
    }
}
