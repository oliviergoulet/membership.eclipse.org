/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.dtos.dashboard;

import java.util.Objects;

import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.model.DtoTable;

import jakarta.persistence.Embeddable;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table
public class CommitterProjectActivity extends BareNode {
    public static final DtoTable TABLE = new DtoTable(CommitterProjectActivity.class, "cpa");

    @EmbeddedId
    private CommitterProjectActivityID compositeID;
    private int count;

    @Override
    public Object getId() {
        return getCompositeID();
    }

    /**
     * @return the compositeID
     */
    public CommitterProjectActivityID getCompositeID() {
        return compositeID;
    }

    /**
     * @param compositeID the compositeID to set
     */
    public void setCompositeID(CommitterProjectActivityID compositeID) {
        this.compositeID = compositeID;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(compositeID, count);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        CommitterProjectActivity other = (CommitterProjectActivity) obj;
        return Objects.equals(compositeID, other.compositeID) && count == other.count;
    }

    @Embeddable
    public static class CommitterProjectActivityID {
        @ManyToOne
        @JoinColumn(name = "login", referencedColumnName = "id")
        private CommitterAffiliation committerAffiliation;
        @ManyToOne
        @JoinColumn(name = "project")
        private Project project;
        private String period;

        /**
         * @return the committer
         */
        public CommitterAffiliation getCommitterAffiliation() {
            return committerAffiliation;
        }

        /**
         * @param committer the committer to set
         */
        public void setCommitterAffiliation(CommitterAffiliation committerAffiliation) {
            this.committerAffiliation = committerAffiliation;
        }

        /**
         * @return the project
         */
        public Project getProject() {
            return project;
        }

        /**
         * @param project the project to set
         */
        public void setProject(Project project) {
            this.project = project;
        }

        /**
         * @return the period
         */
        public String getPeriod() {
            return period;
        }

        /**
         * @param period the period to set
         */
        public void setPeriod(String period) {
            this.period = period;
        }

        @Override
        public int hashCode() {
            return Objects.hash(committerAffiliation, period, project);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            CommitterProjectActivityID other = (CommitterProjectActivityID) obj;
            return Objects.equals(committerAffiliation, other.committerAffiliation) && Objects.equals(period, other.period)
                    && Objects.equals(project, other.project);
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("CommitterProjectActivityID [committerAffiliation=");
            builder.append(committerAffiliation);
            builder.append(", project=");
            builder.append(project);
            builder.append(", period=");
            builder.append(period);
            builder.append("]");
            return builder.toString();
        }
    }
}
