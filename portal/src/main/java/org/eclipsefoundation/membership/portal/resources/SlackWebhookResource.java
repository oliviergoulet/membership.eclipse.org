/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.resources;

import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.CR;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.CRA;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.valueOfChecked;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.params.KeyParameter;
import org.eclipse.microprofile.context.ManagedExecutor;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.annotations.Csrf;
import org.eclipsefoundation.core.exception.FinalForbiddenException;
import org.eclipsefoundation.core.helper.TransformationHelper;
import org.eclipsefoundation.efservices.api.models.WorkingGroup;
import org.eclipsefoundation.efservices.services.WorkingGroupService;
import org.eclipsefoundation.membership.portal.api.SlackAPI;
import org.eclipsefoundation.membership.portal.config.SlackConfig;
import org.eclipsefoundation.membership.portal.model.EnhancedPersonData;
import org.eclipsefoundation.membership.portal.model.MemberOrganization;
import org.eclipsefoundation.membership.portal.model.SlackAttachment;
import org.eclipsefoundation.membership.portal.model.SlackResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/webhook/slack")
@Produces({ MediaType.APPLICATION_JSON })
public class SlackWebhookResource extends AbstractRESTResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(SlackWebhookResource.class);

    private static final String MEMBERSHIP_PAGE_URL = "https://www.eclipse.org/membership/show-member/?member_id=";
    private static final String PROFILE_PAGE_URL = "https://accounts.eclipse.org/users/";
    private static final String TEXT_KEY = "text";
    private static final String RESPONSE_URL_KEY = "response_url";

    @Inject
    SlackConfig config;

    @RestClient
    SlackAPI slackAPI;

    @Inject
    WorkingGroupService wgService;

    @Inject
    ManagedExecutor executor;

    /**
     * POST endpoint with CSRF disabled that accepts incoming 'slash-commands' from slack. Consumes 'application/x-www-form-urlencoded'
     * format. To ensure responsiveness, returns a 200 OK Response to Slack after triggering the member search in a separate thread.
     * 
     * @param body The body is kept as a string to allow for proper validation of content.
     * @return A 200 OK Response containing an error message or a confirmation message.
     */
    @POST
    @Path("m")
    @Csrf(enabled = false)
    @Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
    public Response searchMemberOrg(String body) {

        LOGGER.debug("INCOMING BODY: {}", body);

        // Ensure request came from Slack and is valid
        validateRequestIntegrity(body);

        Map<String, String> requestParams = convertBodyToMap(body);
        String org = requestParams.get(TEXT_KEY);

        // Enforce a minimum character limit on the filter when searching by name
        if (!StringUtils.isNumeric(org) && StringUtils.isBlank(org) || org.length() < 3) {
            // Slack wants a 200 OK
            return Response.ok(buildBadRequestResponse(org)).build();
        }

        String responseUrl = requestParams.get(RESPONSE_URL_KEY);
        if (StringUtils.isBlank(responseUrl)) {
            return Response.ok(buildMissingFieldResponse()).build();
        }

        // Extract dynamic path from response_url param
        String responsePath = StringUtils.substringAfter(responseUrl, "commands");

        // Trigger the search Task and send confirmation to Slack
        executor.execute(() -> performMemberSearch(org, responsePath));
        return Response.ok(buildConfirmationResponse(org)).build();
    }

    /**
     * Performs the search for the desired member org, builds the response object with various member org data points, then posts the
     * results to the SlackAPI at the provided path.
     * 
     * @param org The provided search param. Either an org id or part of an org name
     * @param responsePath The generated path used to respond to Slack.
     */
    private void performMemberSearch(String org, String responsePath) {
        // Do a regular id lookup if value provided is a number
        if (StringUtils.isNumeric(org)) {

            // Return a 'Not Found' message if id is invalid
            Optional<MemberOrganization> result = orgService.getByID(org);
            if (result.isEmpty()) {
                slackAPI.postSlackPayload(responsePath, buildNotFoundResponse(org));
            } else {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Org {} found - Posting message to slack at URL: {}", 
                        TransformationHelper.formatLog(org), responsePath);
                }
                slackAPI.postSlackPayload(responsePath, buildMemberResponse(org, Arrays.asList(result.get())));
            }
        } else {

            // Find any orgs containing the desired string in the name
            List<MemberOrganization> matchingOrgs = orgService
                    .get(wrap)
                    .stream()
                    .filter(o -> StringUtils.containsIgnoreCase(o.getName(), org))
                    .toList();
            if (matchingOrgs == null || matchingOrgs.isEmpty()) {
                slackAPI.postSlackPayload(responsePath, buildNotFoundResponse(org));
            } else {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Org {} found - Posting message to slack at URL: {}", 
                        TransformationHelper.formatLog(org), responsePath);
                }
                slackAPI.postSlackPayload(responsePath, buildMemberResponse(org, matchingOrgs));
            }
        }
    }

    /**
     * Builds a SlackResponse object containing a confirmation message.
     * 
     * @param receivedValue The provided search value
     * @return A SlackResponse object indicating the search is being performed.
     */
    private SlackResponse buildConfirmationResponse(String receivedValue) {
        return new SlackResponse(config.responseType(), String.format("Searching for org: *%s*...", receivedValue),
                Collections.emptyList());
    }

    /**
     * Builds a SlackResponse object indicating the desired member org can't be found.
     * 
     * @param receivedValue The received value
     * @return A SlackResponse object with a "not found" message.
     */
    private SlackResponse buildNotFoundResponse(String receivedValue) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("No matching member orgs for {}", TransformationHelper.formatLog(receivedValue));
        }
        return new SlackResponse(config.responseType(), String.format("No matching member orgs for *%s*", receivedValue),
                Collections.emptyList());
    }

    /**
     * Builds a SlackResponse object indicating the desired search value is too short.
     * 
     * @param receivedValue The received value
     * @return A SlackResponse object with a "bad request" message.
     */
    private SlackResponse buildBadRequestResponse(String receivedValue) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER
                    .debug("Value '{}' is too short. Please add more characters to narrow the search",
                            TransformationHelper.formatLog(receivedValue));
        }
        return new SlackResponse(config.responseType(),
                String.format("Value *%s* is too short. Please add more characters to narrow the search", receivedValue),
                Collections.emptyList());
    }

    /**
     * Builds a SlackResponse object indicating the response_url is missing.
     * 
     * @return A SlackResponse object with a "bad request" message.
     */
    private SlackResponse buildMissingFieldResponse() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("'response_url' field is missing. could not complete the request");
        }
        return new SlackResponse(config.responseType(), "'response_url' field is missing. could not complete the request",
                Collections.emptyList());
    }

    /**
     * Builds a SlackResponse object containing a success message and all desired member org and contact data.
     * 
     * @param receivedValue The received search value.
     * @param matchingOrgs All orgs found matching the received value.
     * @return A SlackResponse object containing the matching member org and contact data.
     */
    private SlackResponse buildMemberResponse(String receivedValue, List<MemberOrganization> matchingOrgs) {
        return new SlackResponse(config.responseType(),
                String.format("*%d* member org(s) found matching *%s*", matchingOrgs.size(), receivedValue),
                buildAttachments(matchingOrgs));
    }

    /**
     * Builds a list of SlackAttachment objects. Each attachment corresponds to a member org matching the desired search param. Each
     * attachment title is the member org name with the company website and the text is the org contact data.
     * 
     * @param matchingOrgs The list of all matching orgs.
     * @return A List of SlackAttachment objects corresponding to each found member org.
     */
    private List<SlackAttachment> buildAttachments(List<MemberOrganization> matchingOrgs) {
        List<SlackAttachment> out = new ArrayList<>(matchingOrgs.size());
        matchingOrgs.forEach(org -> {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Found matching org: {}", org);
            }

            StringBuilder titleBuilder = new StringBuilder();
            titleBuilder.append(org.getName());
            titleBuilder.append(" ");

            // Add all level descriptions to the org title
            if (!org.getLevels().isEmpty()) {
                List<String> memberships = org.getLevels().stream().map(lvl -> lvl.getDescription()).toList();
                titleBuilder.append("(");
                titleBuilder.append(String.join(", ", memberships));
                titleBuilder.append(")");
            }

            String memberUrl = MEMBERSHIP_PAGE_URL + org.getOrganizationID();

            StringBuilder attachmentBody = new StringBuilder();
            attachmentBody.append(buildOrgContactString(org.getOrganizationID()));
            attachmentBody.append(buildAdditionalInfoString(org));

            // Build using the member page as the link, and the contact data as the text
            SlackAttachment attach = new SlackAttachment(titleBuilder.toString(), memberUrl, attachmentBody.toString());
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Built attachment: {}", attach);
            }
            out.add(attach);
        });

        return out;
    }

    /**
     * Builds a string containing org contact data corresponding to a given org. Used as text for a SlackAttachment. The data is limited to
     * first/last name and relation of company reps and alternate company reps.
     * 
     * @param orgId The id of the org used to find company reps
     * @return A StringBuilder containing company rep data.
     */
    private StringBuilder buildOrgContactString(int orgId) {
        StringBuilder repBuilder = new StringBuilder();

        // Only lookup company reps and alternates
        List<EnhancedPersonData> contacts = getEnhancedPeopleDetails(Integer.toString(orgId), Arrays.asList(CR, CRA));
        LOGGER.debug("Found {} contacts for org '{}'", contacts.size(), orgId);
        if (contacts != null && !contacts.isEmpty()) {
            contacts.forEach(contact -> {
                // Extract role if it exists (it should)
                Optional<String> role = contact
                        .getRelations()
                        .stream()
                        .filter(rel -> valueOfChecked(rel).equals(CR) || valueOfChecked(rel).equals(CRA))
                        .map(rel -> valueOfChecked(rel).getLabel())
                        .findFirst();
                if (role.isPresent()) {
                    repBuilder.append("*");
                    repBuilder.append(role.get());
                    repBuilder.append(":* ");
                    repBuilder.append(contact.getFirstName());
                    repBuilder.append(" ");
                    repBuilder.append(contact.getLastName());
                    repBuilder.append("(_");
                    repBuilder.append(contact.getId());
                    repBuilder.append("_) - ");
                    repBuilder.append(PROFILE_PAGE_URL + contact.getId());
                    repBuilder.append("\n");
                }
            });
        }
        return repBuilder;
    }

    private StringBuilder buildAdditionalInfoString(MemberOrganization org) {
        StringBuilder builder = new StringBuilder();
        if (org.getMemberSince() != null) {
            builder.append("*Member Since:* ");
            builder.append(org.getMemberSince());
            builder.append("\n");
        }

        if (org.getRenewalDate() != null) {
            builder.append("*Renewal Date:* ");
            builder.append(org.getRenewalDate());
            builder.append("\n");
        }

        if (StringUtils.isNotBlank(org.getLogos().getWeb())) {
            builder.append("*Logo - web:* ");
            builder.append(org.getLogos().getWeb());
            builder.append("\n");
        }

        if (StringUtils.isNotBlank(org.getLogos().getPrint())) {
            builder.append("*Logo - print:* ");
            builder.append(org.getLogos().getPrint());
            builder.append("\n");
        }

        if (!org.getWgpas().isEmpty()) {
            builder.append("*WG Participation:*\n");
            org.getWgpas().stream().forEach(wgpa -> {
                Optional<WorkingGroup> wg = wgService.getByAgreementId(wgpa.getDocumentID());
                if (wg.isPresent()) {
                    builder.append("- ");
                    builder.append(wg.get().getTitle());
                    builder.append(" - ");
                    builder.append(wgpa.getDescription());
                    builder.append("(_");
                    builder.append(wgpa.getLevel());
                    builder.append("_)\n");
                }
            });
        }

        return builder;
    }

    /**
     * Validates the incoming Slack request body by creating a hash of it's contents and comparing it against the hash sent in the header by
     * Slack. Throws a 403 Forbidden if they do not match.
     * 
     * For reference: https://api.slack.com/authentication/verifying-requests-from-slack
     * 
     * @param body The incoming request body from Slack.
     */
    private void validateRequestIntegrity(String body) {

        // Create the base signature using the same format as Slack
        String signatureBase = "v0:" + wrap.getHeader("x-slack-request-timestamp") + ":" + body;
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Using '{}' as signature base", signatureBase);
        }

        // Initialiaze the HMAC using the signing secret and the base signature
        HMac sha256HMac = new HMac(new SHA256Digest());
        sha256HMac.init(new KeyParameter(config.signingSecret().getBytes()));
        sha256HMac.update(signatureBase.getBytes(), 0, signatureBase.getBytes().length);

        // Compute the final stage into a byte array
        byte[] hmacOut = new byte[sha256HMac.getMacSize()];
        sha256HMac.doFinal(hmacOut, 0);

        // Convert the byte array to it's hex equivalent as a string
        StringBuilder sb = new StringBuilder(hmacOut.length * 2);
        for (byte by : hmacOut) {
            sb.append(String.format("%02x", by));
        }

        // Format the computed hash in the same way as the one sent by Slack
        String computedSignature = "v0=" + sb.toString();
        String slackSignature = wrap.getHeader("x-slack-signature");
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Computed '{}'. Comparing against '{}'", computedSignature, slackSignature);
        }

        if (!computedSignature.equals(slackSignature)) {
            throw new FinalForbiddenException("Invalid signing secret");
        }
    }

    /**
     * Simple util method that converts the params of a 'application/x-www-form-urlencoded' body to a Map.
     * 
     * @param body The param string.
     * @return A reference to a Map ocntaining all params int he body.
     */
    private Map<String, String> convertBodyToMap(String body) {
        Map<String, String> out = new HashMap<>();
        Arrays.asList(body.split("&")).stream().forEach(param -> {
            String[] keyValuePairs = param.split("=");
            if (keyValuePairs.length == 2) {
                out.put(keyValuePairs[0], keyValuePairs[1]);
            }
        });
        return out;
    }
}
