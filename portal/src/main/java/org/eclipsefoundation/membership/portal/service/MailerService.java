/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.service;

import org.eclipsefoundation.membership.portal.model.ContactRemovalRequestData;
import org.eclipsefoundation.membership.portal.request.model.ContactRemovalRequest;

/**
 * Interface defining emails that need to be generated and sent as part of the submission of the membership forms to the
 * membership team. This interface includes emails providing feedback to the user and to the membership team.
 * 
 * @author Martin Lowe
 *
 */
public interface MailerService {
    /**
     * Sends an email message
     * 
     * @param request the information for the user removal request
     */
    void sendContactRemovalRequest(ContactRemovalRequest request);
    /**
     * Sends an email message
     * 
     * @param request the information for the user removal request
     */
    void sendContactRemovalNotification(ContactRemovalRequestData request);
}
