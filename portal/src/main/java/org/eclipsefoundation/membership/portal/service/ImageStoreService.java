/**
 * Copyright (c) 2021, 2022 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.service;

import java.util.Optional;
import java.util.function.Supplier;

import org.eclipsefoundation.membership.portal.namespace.ImageStoreFormat;

import io.undertow.server.handlers.form.MultiPartParserDefinition.FileTooLargeException;

/**
 * Defines writing, retrieval, and deletion of images using byte arrays.
 * 
 * @author Martin Lowe
 */
public interface ImageStoreService {

    /**
     * Retrieves image URL, using previously created image when possible, and recreating using provided supplier to retrieve the data for
     * writing otherwise.
     * 
     * @param organization name of the organization
     * @param format the name of the format to retrieve images for
     * @return absolute path to access the live image
     */
    String retrieveImageUrl(String organization, Optional<ImageStoreFormat> format);

    /**
     * Writes the image to the services providing system (remote, filesystem, etc.). This should overwrite existing files when present.
     * 
     * @param imageBytes supplier of byte arrays for image contents. This is done to leverage lazy-load from Hibernate to save network
     * traffic when not needed
     * @param organization name of the organization
     * @param mimeType file mime-type of the image
     * @param format the name of the format to write the image for
     * @return absolute path to access the live image
     */
    String writeImage(Supplier<byte[]> imageBytes, String organization, String mimeType, Optional<ImageStoreFormat> format)
            throws FileTooLargeException;

    /**
     * Returns the default image location to be used in requests where fallbacks are needed. This isn't always the case, so it will not be
     * folded into the main request for the time being.
     * 
     * @return the location of the default image to use when fallback images are suitable.
     */
    String getDefaultImageLocation();

    /**
     * Remove images associated with the given organization. This should clear all images that exist for the organization to make sure that
     * any '' images/data are cleared.
     * 
     * @param organization the organization name for the logo
     * @param format the name of the format to remove images for
     */
    void removeImages(String organization, Optional<ImageStoreFormat> format);

}
