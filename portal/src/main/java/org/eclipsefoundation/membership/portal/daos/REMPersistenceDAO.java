/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.daos;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.persistence.EntityManager;

import org.eclipsefoundation.persistence.dao.impl.BaseHibernateDao;

/**
 * Allows for Dashboard entities to be retrieved.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class REMPersistenceDAO extends BaseHibernateDao {

    @Named("rem")
    @Inject
    EntityManager em;

    @Override
    public EntityManager getPrimaryEntityManager() {
        return em;
    }
}
