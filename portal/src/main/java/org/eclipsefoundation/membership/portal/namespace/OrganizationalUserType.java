/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.portal.namespace;

public enum OrganizationalUserType {
    CR("Company Representative"), CRA("Company representative - alternate"), MA("Marketing Representative"),
    CM("Committer"), DE("Delegate"), EMPLY("Employee"), PE("Page Editor"), NA("Not found");

    private final String label;

    private OrganizationalUserType(String label) {
        this.label = label;
    }

    /**
     * Gets the label for the user type.
     * 
     * @return
     */
    public String getLabel() {
        return this.label;
    }

    public static OrganizationalUserType valueOfChecked(String name) {
        try {
            return valueOf(name);
        } catch (IllegalArgumentException e) {
            // return na when no relation is found
            return NA;
        }
    }

}
