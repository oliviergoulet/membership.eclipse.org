CREATE TABLE MembershipForm (
  id varchar(255) NOT NULL,
  dateCreated BIGINT DEFAULT NULL,
  dateUpdated BIGINT DEFAULT NULL,
  dateSubmitted BIGINT DEFAULT NULL,
  membershipLevel varchar(255) DEFAULT NULL,
  purchaseOrderRequired varchar(255) DEFAULT NULL,
  registrationCountry varchar(255) DEFAULT NULL,
  signingAuthority boolean NOT NULL,
  userID varchar(255) DEFAULT NULL,
  vatNumber varchar(255) DEFAULT NULL,
  `state` int DEFAULT NULL,
  applicationGroup varchar(255) DEFAULT NULL
);
INSERT INTO MembershipForm(id, dateCreated, dateUpdated, dateSubmitted, membershipLevel, purchaseOrderRequired, registrationCountry, signingAuthority, userID, vatNumber, `state`, applicationGroup)
  VALUES('form-uuid', 1634668281297, 1634668281297, null, 'Something','yes','CA',0,'opearson','0123456789', '1', 'eclipse');
INSERT INTO MembershipForm(id, dateCreated, dateUpdated, dateSubmitted, membershipLevel, purchaseOrderRequired, registrationCountry, signingAuthority, userID, vatNumber, `state`, applicationGroup)
  VALUES('validate-form-uuid', 1634668281297, 1634668281297, null, 'Something','yes','CA',0,'opearson','0123456789', '1', 'eclipse');
INSERT INTO MembershipForm(id, dateCreated, dateUpdated, dateSubmitted, membershipLevel, purchaseOrderRequired, registrationCountry, signingAuthority, userID, vatNumber, `state`, applicationGroup)
  VALUES('delete-test-uuid', 1634668281296, 1634668281296, null, 'Something','yes','CA',0,'opearson','0123456789', '1', 'eclipse');

CREATE TABLE Contact (
  id varchar(255) NOT NULL,
  email varchar(255) DEFAULT NULL,
  fName varchar(255) DEFAULT NULL,
  lName varchar(255) DEFAULT NULL,
  title varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  form_id varchar(255) DEFAULT NULL
);
INSERT INTO Contact(id, email, fName, lName, title, `type`, form_id)
  VALUES('opearson', 'test@test.co', 'opearson', 'mctesterson', 'Tester', 'SIGNING', 'form-uuid');
INSERT INTO Contact(id, email, fName, lName, title, `type`, form_id)
  VALUES('opearson2', 'test@test.co', 'opearson', 'mctesterson', 'Tester', 'SIGNING', 'delete-test-uuid');
INSERT INTO Contact(id, email, fName, lName, title, `type`, form_id)
  VALUES('opearson3', 'test@test.co', 'opearson', 'mctesterson', 'Tester', 'SIGNING', 'validate-form-uuid');
INSERT INTO Contact(id, email, fName, lName, title, `type`, form_id)
  VALUES('optedout', 'fake@address.ca', 'testy', 'testosterone', 'Man', 'ACCOUNTING', 'form-uuid');

CREATE TABLE FormOrganization (
  id varchar(255) NOT NULL,
  legalName varchar(255) DEFAULT NULL,
  twitterHandle varchar(255) DEFAULT NULL,
  aggregateRevenue varchar(255) DEFAULT NULL,
  organizationType varchar(255) DEFAULT NULL,
  employeeCount varchar(255) DEFAULT NULL,
  form_id varchar(255) DEFAULT NULL
);
INSERT INTO FormOrganization(id, legalName, twitterHandle, aggregateRevenue, organizationType, employeeCount, form_id)
  VALUES('15', 'Sample org','@twitterHandle', 'A lot of money', 'NON_PROFIT_OPEN_SOURCE', '1000-10000', 'form-uuid');
INSERT INTO FormOrganization(id, legalName, twitterHandle, aggregateRevenue, organizationType, employeeCount, form_id)
  VALUES('16', 'Sample org','@twitterHandle', 'A lot of money', 'NON_PROFIT_OPEN_SOURCE', '1000-10000', 'validate-form-uuid');

CREATE TABLE FormWorkingGroup (
  id varchar(255) NOT NULL,
  effectiveDate datetime DEFAULT NULL,
  participationLevel varchar(255) DEFAULT NULL,
  workingGroupID varchar(255) DEFAULT NULL,
  contact_id varchar(255) DEFAULT NULL,
  form_id varchar(255) DEFAULT NULL
);
INSERT INTO FormWorkingGroup(id, effectiveDate, participationLevel, workingGroupID, contact_id, form_id)
  VALUES('working_group', CURRENT_TIMESTAMP(), 'Participant', 'working.group', 'opearson', 'form-uuid');

CREATE TABLE Address (
  id varchar(255) NOT NULL,
  locality varchar(255) DEFAULT NULL,
  country varchar(255) DEFAULT NULL,
  postalCode varchar(255) DEFAULT NULL,
  administrativeArea varchar(255) DEFAULT NULL,
  addressLine1 varchar(255) DEFAULT NULL,
  addressLine2 varchar(255) DEFAULT NULL,
  organization_id varchar(255) DEFAULT NULL
);
INSERT INTO Address(id, locality, country, postalCode, administrativeArea, addressLine1, addressLine2, organization_id)
  VALUES('address_id', 'Ottawa', 'Canada', 'k1a 1a1', 'ON', 'Street st.', 'Street st.', '15');
INSERT INTO Address(id, locality, country, postalCode, administrativeArea, addressLine1, addressLine2, organization_id)
  VALUES('address2', 'Ottawa', 'Canada', 'k1a 1a1', 'ON', 'Street st.', 'Street st.', '16');

CREATE TABLE DistributedCSRFToken (
  `token` varchar(100) NOT NULL,
  `ipAddress` varchar(64) NOT NULL,
  `userAgent` varchar(255) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `user` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`token`)
);

CREATE TABLE IF NOT EXISTS `SecureTransaction` (
  `id` SERIAL NOT NULL,
  `token` varchar(255) NOT NULL,
  `associatedEntityId` varchar(255) NOT NULL,
  `associatedEntityType` varchar(63) NOT NULL,
  `is_active` BOOLEAN NOT NULL,
  PRIMARY KEY(`id`)
);
INSERT INTO SecureTransaction(id, token, associatedEntityId, associatedEntityType, is_active)
  VALUES('69', 'token-string', 'form-uuid', 'FORM', '1');
INSERT INTO SecureTransaction(id, token, associatedEntityId, associatedEntityType, is_active)
  VALUES('70', 'token-string', 'validate-form-uuid', 'FORM', '1');
INSERT INTO SecureTransaction(id, token, associatedEntityId, associatedEntityType, is_active)
  VALUES('666', 'other-token', 'opearson', 'OPTOUT', '1');
INSERT INTO SecureTransaction(id, token, associatedEntityId, associatedEntityType, is_active)
  VALUES('17', 'jrr-token', 'optedout', 'OPTOUT', '1');
INSERT INTO SecureTransaction(id, token, associatedEntityId, associatedEntityType, is_active)
  VALUES('50', 'token-string', '9', 'WG_APP', '1');
INSERT INTO SecureTransaction(id, token, associatedEntityId, associatedEntityType, is_active)
  VALUES('51', 'token-string', '10', 'WG_APP', '1');

CREATE TABLE `EmailOptOut` (
  `id` SERIAL NOT NULL,
  `dateReceived` datetime NOT NULL,
  `email` varchar(127) UNIQUE NOT NULL,
  `transaction_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`)
);
INSERT INTO EmailOptOut(id, dateReceived, email, transaction_id)
  VALUES('42', '2022-11-01', 'fake@address.ca', '17');

CREATE TABLE `WorkingGroupApplication` (
  `id` SERIAL NOT NULL,
  `user` varchar(100) NOT NULL,
  `organizationId` int NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `updated` datetime NOT NULL DEFAULT current_timestamp(),
  `state` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
INSERT INTO WorkingGroupApplication(`user`, organizationId,state) VALUES('opearson',15, 'INPROGRESS');
INSERT INTO WorkingGroupApplication(`user`, organizationId,state) VALUES('opearson',15, 'COMPLETE');
INSERT INTO WorkingGroupApplication(`user`, organizationId,state) VALUES('newbie',5, 'INPROGRESS');
INSERT INTO WorkingGroupApplication(`user`, organizationId,state) VALUES('opearson',15, 'INPROGRESS');
INSERT INTO WorkingGroupApplication(`user`, organizationId,state) VALUES('opearson',15, 'SUBMITTED');
INSERT INTO WorkingGroupApplication(`user`, organizationId,state) VALUES('opearson',15, 'INPROGRESS');
INSERT INTO WorkingGroupApplication(`user`, organizationId,state) VALUES('newbie',5, 'INPROGRESS');
INSERT INTO WorkingGroupApplication(`user`, organizationId,state) VALUES('opearson',15, 'INPROGRESS');
INSERT INTO WorkingGroupApplication(`user`, organizationId,state) VALUES('opearson',15, 'INPROGRESS');
INSERT INTO WorkingGroupApplication(`user`, organizationId,state) VALUES('opearson',15, 'SUBMITTED');

CREATE TABLE `WorkingGroupApplicationContact` (
  `id` SERIAL NOT NULL,
  `fName` varchar(100) DEFAULT NULL,
  `lName` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `user` varchar(100) DEFAULT NULL,
  `workingGroupAlias` varchar(100) NOT NULL,
  `workingGroupLevel` varchar(100) NOT NULL,
  `isSigningAuthority` BOOLEAN NOT NULL,
  `application_id` bigint NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO WorkingGroupApplicationContact(email, fName, lName, application_id,workingGroupAlias,workingGroupLevel,isSigningAuthority)
  VALUES('test@test.co', 'pearson', 'mctesterson', 1,'sdv','WGAPS', false);
INSERT INTO WorkingGroupApplicationContact(email,user,application_id,workingGroupAlias,workingGroupLevel,isSigningAuthority)
  VALUES('opearson@sample.co', 'opearson', 2,'sdv','WGAPS', false);
INSERT INTO WorkingGroupApplicationContact(email,user,application_id,workingGroupAlias,workingGroupLevel,isSigningAuthority)
  VALUES('newbie@important.co', 'newbie', 3,'sdv','WGAPS', true);

INSERT INTO WorkingGroupApplicationContact(email, fName, lName, application_id,workingGroupAlias,workingGroupLevel,isSigningAuthority)
  VALUES('test@test.co', 'pearson', 'mctesterson', 4,'sdv','WGAPS', false);
INSERT INTO WorkingGroupApplicationContact(email, user, fName, lName, application_id,workingGroupAlias,workingGroupLevel,isSigningAuthority)
  VALUES('opearson@sample.co', 'opearson', 'oli', 'pearson', 4,'','', true);

INSERT INTO WorkingGroupApplicationContact(email, fName, lName, application_id,workingGroupAlias,workingGroupLevel,isSigningAuthority)
  VALUES('test@test.co', 'pearson', 'mctesterson', 5,'ecri','WGAPS', false);
INSERT INTO WorkingGroupApplicationContact(email, user, fName, lName, application_id,workingGroupAlias,workingGroupLevel,isSigningAuthority)
  VALUES('opearson@sample.co', 'opearson', 'oli', 'pearson', 5,'','', true);

INSERT INTO WorkingGroupApplicationContact(email, fName, lName, application_id,workingGroupAlias,workingGroupLevel,isSigningAuthority)
  VALUES('test@test.co', 'pearson', 'mctesterson', 8,'dataspace','WGSD', false);
INSERT INTO WorkingGroupApplicationContact(email, user, fName, lName, application_id,workingGroupAlias,workingGroupLevel,isSigningAuthority)
  VALUES('opearson@sample.co', 'opearson', 'oli', 'pearson', 8,'','', true);

INSERT INTO WorkingGroupApplicationContact(email, fName, lName, application_id,workingGroupAlias,workingGroupLevel,isSigningAuthority)
  VALUES('test@test.co', 'pearson', 'mctesterson', 9,'ecri','ABCDE', false);
INSERT INTO WorkingGroupApplicationContact(email, user, fName, lName, application_id,workingGroupAlias,workingGroupLevel,isSigningAuthority)
  VALUES('opearson@sample.co', 'opearson', 'oli', 'pearson', 9,'','', true);
  
INSERT INTO WorkingGroupApplicationContact(email, fName, lName, application_id,workingGroupAlias,workingGroupLevel,isSigningAuthority)
  VALUES('test@test.co', 'pearson', 'mctesterson', 10,'ecri','ABCDE', false);
INSERT INTO WorkingGroupApplicationContact(email, user, fName, lName, application_id,workingGroupAlias,workingGroupLevel,isSigningAuthority)
  VALUES('opearson@sample.co', 'opearson', 'oli', 'pearson', 10,'','', true);
  