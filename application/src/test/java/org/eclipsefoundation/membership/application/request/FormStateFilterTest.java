/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.request;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.net.URI;
import java.util.Arrays;
import java.util.Optional;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.membership.application.dto.membership.Contact;
import org.eclipsefoundation.membership.application.dto.membership.FormOrganization;
import org.eclipsefoundation.membership.application.dto.membership.FormWorkingGroup;
import org.eclipsefoundation.membership.application.dto.membership.MembershipForm;
import org.eclipsefoundation.membership.application.model.ContactData;
import org.eclipsefoundation.membership.application.model.FormWorkingGroupData;
import org.eclipsefoundation.membership.application.model.mappers.ContactMapper;
import org.eclipsefoundation.membership.application.model.mappers.FormWorkingGroupMapper;
import org.eclipsefoundation.membership.application.namespace.FormState;
import org.eclipsefoundation.membership.application.test.helper.DtoHelper;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.testing.helpers.AuthHelper;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.ConfigMetadata;
import io.quarkus.test.security.oidc.OidcSecurity;
import io.restassured.http.ContentType;

@QuarkusTest
class FormStateFilterTest {

    @Inject
    DefaultHibernateDao dao;
    @Inject
    FilterService filters;
    @Inject
    ContactMapper cMap;
    @Inject
    FormWorkingGroupMapper wgMapper;

    @Inject
    ObjectMapper json;

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void filterInprogressForms_mutator() {
        // attempting to modify inprogress forms should pass
        testMutatorsWithStatus(FormState.INPROGRESS, Status.OK.getStatusCode());
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void filterSubmittedForms_mutator() {
        // attempting to modify submitted forms should fail
        testMutatorsAssertBadRequest(FormState.SUBMITTED);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void filterCompleteForms_mutator() {
        // attempting to modify complete forms should fail
        testMutatorsAssertBadRequest(FormState.COMPLETE);
    }

    /**
     * Checks to make sure accessors are not affected when state is {@link FormState.SUBMITTED}
     */
    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void filterSubmittedForms_accessor() {
        testAccessors(FormState.SUBMITTED);
    }

    /**
     * Checks to make sure accessors are not affected when state is {@link FormState.COMPLETE}
     */
    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void filterCompletedForms_accessor() {
        testAccessors(FormState.COMPLETE);
    }

    private void testMutatorsAssertBadRequest(FormState state) {
        testMutatorsWithStatus(state, Status.BAD_REQUEST.getStatusCode());
    }

    private void testMutatorsWithStatus(FormState state, int status) {
        MembershipForm form = (MembershipForm) getForm(state);
        String formID = form.getId();

        // get the data for this call (new data to not affect down stream tests)
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("https://membership.eclipse.org"));
        ContactData contact = cMap
                .toModel(dao
                        .add(new RDBMSQuery<Contact>(wrap, filters.get(Contact.class), null),
                                Arrays.asList(DtoHelper.generateContact(form, Optional.empty())))
                        .get(0));
        FormOrganization org = dao
                .add(new RDBMSQuery<FormOrganization>(wrap, filters.get(FormOrganization.class), null),
                        Arrays.asList(DtoHelper.generateOrg(form)))
                .get(0);
        FormWorkingGroupData wg = wgMapper
                .toModel(dao
                        .add(new RDBMSQuery<FormWorkingGroup>(wrap, filters.get(FormWorkingGroup.class), null),
                                Arrays.asList(DtoHelper.generateWorkingGroup(form)))
                        .get(0));
        String formData = null;
        String wgData = null;
        String contactData = null;
        String orgData = null;
        try {
            formData = json.writeValueAsString(form);
            wgData = json.writeValueAsString(wg);
            orgData = json.writeValueAsString(org);
            contactData = json.writeValueAsString(contact);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        assertNotNull(formData);
        assertNotNull(wgData);
        assertNotNull(orgData);
        assertNotNull(contactData);

        // test the PUT, POST, and DELETE calls for all form endpoints
        // DELETE calls have been disabled for now as they cause errors downstream in current state
        AuthHelper
                .getCSRFDefinedResteasyRequest()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(formData)
                .when()
                .put("/form/{id}", formID)
                .then()
                .statusCode(status);
        AuthHelper
                .getCSRFDefinedResteasyRequest()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(contactData)
                .when()
                .post("/form/{id}/contacts", formID)
                .then()
                .statusCode(status);
        AuthHelper
                .getCSRFDefinedResteasyRequest()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(contactData)
                .when()
                .put("/form/{id}/contacts/{contactID}", formID, contact.getId())
                .then()
                .statusCode(status);
        AuthHelper
                .getCSRFDefinedResteasyRequest()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(orgData)
                .when()
                .post("/form/{id}/organizations", formID)
                .then()
                .statusCode(status);
        AuthHelper
                .getCSRFDefinedResteasyRequest()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(orgData)
                .when()
                .put("/form/{id}/organizations/{orgID}", formID, org.getId())
                .then()
                .statusCode(status);
        AuthHelper
                .getCSRFDefinedResteasyRequest()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(wgData)
                .when()
                .post("/form/{id}/working_groups", formID)
                .then()
                .statusCode(status);
        AuthHelper
                .getCSRFDefinedResteasyRequest()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(wgData)
                .when()
                .put("/form/{id}/working_groups/{wgID}", formID, wg.getId())
                .then()
                .statusCode(status);
    }

    private void testAccessors(FormState state) {
        MembershipForm form = (MembershipForm) getForm(state);
        String formID = form.getId();

        // get the data for this call (new data to not affect down stream tests)
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("https://membership.eclipse.org"));
        dao
                .add(new RDBMSQuery<Contact>(wrap, filters.get(Contact.class), null),
                        Arrays.asList(DtoHelper.generateContact(form, Optional.empty())));
        dao
                .add(new RDBMSQuery<FormOrganization>(wrap, filters.get(FormOrganization.class), null),
                        Arrays.asList(DtoHelper.generateOrg(form)));
        dao
                .add(new RDBMSQuery<FormWorkingGroup>(wrap, filters.get(FormWorkingGroup.class), null),
                        Arrays.asList(DtoHelper.generateWorkingGroup(form)));

        AuthHelper.getCSRFDefinedResteasyRequest().when().get("/form/{id}", formID).then().statusCode(Response.Status.OK.getStatusCode());
        AuthHelper
                .getCSRFDefinedResteasyRequest()
                .when()
                .get("/form/{id}/contacts", formID)
                .then()
                .statusCode(Response.Status.OK.getStatusCode());
        AuthHelper
                .getCSRFDefinedResteasyRequest()
                .when()
                .get("/form/{id}/organizations", formID)
                .then()
                .statusCode(Response.Status.OK.getStatusCode());
        AuthHelper
                .getCSRFDefinedResteasyRequest()
                .when()
                .get("/form/{id}/working_groups", formID)
                .then()
                .statusCode(Response.Status.OK.getStatusCode());
    }

    /**
     * Generates a form with an ID and given state.
     * 
     * @param state the state to set into the form
     * @return the new mock form
     */
    private BareNode getForm(FormState state) {
        MembershipForm out = DtoHelper.generateForm(Optional.of(AuthHelper.TEST_USER_NAME));
        out.setState(state);
        return dao
                .add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(MembershipForm.class), null), Arrays.asList(out))
                .get(0);
    }

}
