/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.test.api;

import org.eclipsefoundation.efservices.services.DrupalTokenService;

import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Alternative;

/**
 * Used to bypass oauth token retrieval in testing, as we won't ever be adding credentials to a testing environment.
 */
@Alternative
@Priority(1)
@ApplicationScoped
public class MockDrupalOAuthAPI implements DrupalTokenService {

    @Override
    public String getToken() {
        return "";
    }

}