/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.application.test.api;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.efservices.api.ProfileAPI;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.EfUser.Country;
import org.eclipsefoundation.efservices.api.models.EfUser.Eca;
import org.eclipsefoundation.efservices.api.models.UserSearchParams;

import io.quarkus.logging.Log;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Alternative;

/**
 * @author martin
 *
 */
@Alternative
@Priority(1)
@RestClient
@ApplicationScoped
public class MockProfileApi implements ProfileAPI {

    public List<EfUser> users;

    @PostConstruct
    void init() {
        this.users = new LinkedList<>();
        users
                .add(generateSampleEfUser("barshall_blathers", "slom@eclipse-foundation.org", null)
                        .toBuilder()
                        .setIsCommitter(false)
                        .setUid("0")
                        .setFirstName("Barshall")
                        .setLastName("Blathers")
                        .setEca(Eca.builder().setCanContributeSpecProject(true).setSigned(true).build())
                        .build());
        users
                .add(generateSampleEfUser("da_wizz", "code.wiz@important.co", null)
                        .toBuilder()
                        .setIsCommitter(true)
                        .setUid("1")
                        .setFirstName("Da")
                        .setLastName("Wizz")
                        .setEca(Eca.builder().setCanContributeSpecProject(true).setSigned(true).build())
                        .build());
        users
                .add(generateSampleEfUser("grunt", "grunt@important.co", null)
                        .toBuilder()
                        .setIsCommitter(true)
                        .setUid("3")
                        .setFirstName("Grunt")
                        .setLastName("Grunt")
                        .setEca(Eca.builder().setCanContributeSpecProject(true).setSigned(true).build())
                        .build());
        users
                .add(generateSampleEfUser("opearson", "opearson@sample.co", null)
                        .toBuilder()
                        .setIsCommitter(true)
                        .setUid("2")
                        .setFirstName("Oli")
                        .setLastName("Pearson")
                        .setOrgId(15)
                        .setEca(Eca.builder().setCanContributeSpecProject(true).setSigned(true).build())
                        .build());
        users
                .add(generateSampleEfUser("newbie", "newbie@important.co", null)
                        .toBuilder()
                        .setIsCommitter(true)
                        .setUid("5")
                        .setFirstName("Newbie")
                        .setLastName("Nobody")
                        .setEca(Eca.builder().setCanContributeSpecProject(true).setSigned(true).build())
                        .build());
        users
                .add(generateSampleEfUser("doofenshmirtz", "doofenshmirtz@evil.inc", null)
                        .toBuilder()
                        .setIsCommitter(true)
                        .setUid("6")
                        .setFirstName("Dr")
                        .setLastName("Doofenshmirtz")
                        .setOrgId(15)
                        .setEca(Eca.builder().setCanContributeSpecProject(true).setSigned(true).build())
                        .build());

    }

    @Override
    public List<EfUser> getUsers(String token, UserSearchParams arg1) {
        return users
                .stream()
                .filter(u -> StringUtils.isBlank(arg1.getMail()) || u.getMail().equals(arg1.getMail()))
                .filter(u -> StringUtils.isBlank(arg1.getName()) || u.getName().equals(arg1.getName()))
                .filter(u -> arg1.getUid() == null || u.getUid() == arg1.getUid())
                .collect(Collectors.toList());
    }

    @Override
    public EfUser getUserByEfUsername(String token, String username) {
        if (StringUtils.isBlank(username)) {
            return null;
        }
        return users.stream().filter(u -> username.equals(u.getName())).findFirst().orElse(null);
    }

    @Override
    public EfUser getUserByGithubHandle(String token, String handle) {
        if (StringUtils.isBlank(handle)) {
            return null;
        }
        return users.stream().filter(u -> handle.equals(u.getGithubHandle())).findFirst().orElse(null);
    }

    private EfUser generateSampleEfUser(String username, String email, Integer orgId) {
        return EfUser
                .builder()
                .setFirstName("")
                .setLastName("")
                .setFullName("")
                .setIsBot(false)
                .setIsCommitter(false)
                .setMail(email)
                .setPicture("")
                .setPublisherAgreements(Collections.emptyMap())
                .setTwitterHandle("")
                .setOrg("")
                .setJobTitle("")
                .setWebsite("")
                .setCountry(Country.builder().setCode("CA").setName("Canada").build())
                .setInterests(Collections.emptyList())
                .setName(username)
                .setOrgId(orgId)
                .setUid("0")
                .setEca(EfUser.Eca.builder().setSigned(true).setCanContributeSpecProject(false).build())
                .build();
    }
}
