package org.eclipsefoundation.membership.application.test.api;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.membership.application.api.MembershipAPI;
import org.eclipsefoundation.membership.application.api.models.MemberOrganization;
import org.eclipsefoundation.membership.application.api.models.MemberOrganization.MemberOrganizationDescription;
import org.eclipsefoundation.membership.application.api.models.MemberOrganization.MemberOrganizationLogos;
import org.eclipsefoundation.membership.application.api.models.MemberOrganization.OrganizationWGPA;

import jakarta.annotation.Priority;
import jakarta.enterprise.inject.Alternative;
import jakarta.inject.Singleton;

@Alternative
@Priority(1)
@RestClient
@Singleton
public class MockMembershipAPI implements MembershipAPI {
    public Map<String, MemberOrganization> members;

    public MockMembershipAPI() {
        this.members = new HashMap<>();
        this.members.put("15", generateOrg(15, Arrays.asList(generateWGPA("dataspace", "wgsd"))));
    }

    @Override
    public MemberOrganization getOrganization(int organizationId) {
        return this.members.get(Integer.toString(organizationId));
    }

    private MemberOrganization generateOrg(int orgId, List<OrganizationWGPA> agreements) {
        return MemberOrganization
                .builder()
                .setOrganizationID(orgId)
                .setLogos(MemberOrganizationLogos.builder().setPrint("").setWeb("").build())
                .setMemberSince(LocalDate.now())
                .setName("Sample")
                .setWebsite("")
                .setDescription(MemberOrganizationDescription.builder().setLongDescription("").build())
                .setLevels(Collections.emptyList())
                .setWgpas(agreements)
                .build();
    }

    private OrganizationWGPA generateWGPA(String wg, String level) {
        return OrganizationWGPA.builder().setDescription("").setDocumentID("").setLevel(level).setWorkingGroup(wg).build();
    }
}
