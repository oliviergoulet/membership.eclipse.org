/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*           Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.resources;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;

import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.membership.application.dto.membership.FormWorkingGroup;
import org.eclipsefoundation.membership.application.dto.membership.MembershipForm;
import org.eclipsefoundation.membership.application.model.FormWorkingGroupData;
import org.eclipsefoundation.membership.application.model.mappers.FormWorkingGroupMapper;
import org.eclipsefoundation.membership.application.test.dao.MockHibernateDao;
import org.eclipsefoundation.membership.application.test.helper.DtoHelper;
import org.eclipsefoundation.membership.application.test.helper.SchemaNamespaceHelper;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.testing.helpers.AuthHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.ConfigMetadata;
import io.quarkus.test.security.oidc.OidcSecurity;

// @TestInstance(Lifecycle.PER_CLASS)
@QuarkusTest
class WorkingGroupsResourceTest {
    public static final String FORM_WORKING_GROUPS_BASE_URL = "/form/{id}/working_groups";
    public static final String FORM_WORKING_GROUP_BY_ID_URL = FORM_WORKING_GROUPS_BASE_URL + "/{workingGroupId}";

    public static final String FORM_ID = "form-uuid";
    public static final String FORM_ORG_ID = "15";
    public static final String FORM_WG_ID = "working_group";

    public static final EndpointTestCase GET_FORM_WGS = TestCaseHelper
            .buildSuccessCase(FORM_WORKING_GROUPS_BASE_URL, new String[] { FORM_ID }, SchemaNamespaceHelper.WORKING_GROUPS_SCHEMA_PATH);

    public static final EndpointTestCase GET_FORM_WG_BY_ID = TestCaseHelper
            .buildSuccessCase(FORM_WORKING_GROUP_BY_ID_URL, new String[] { FORM_ID, FORM_WG_ID },
                    SchemaNamespaceHelper.WORKING_GROUP_SCHEMA_PATH);

    @Inject
    ObjectMapper om;
    @Inject
    MockHibernateDao mockDao;
    @Inject
    FilterService filters;
    @Inject
    FormWorkingGroupMapper wgMapper;

    //
    // GET /form/{id}/working_groups
    //
    @Test
    void getFormWorkingGroups_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORM_WORKING_GROUPS_BASE_URL, new String[] { FORM_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getFormWorkingGroups_success() {
        EndpointTestBuilder.from(GET_FORM_WGS).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getFormWorkingGroups_success_validateSchema() {
        EndpointTestBuilder.from(GET_FORM_WGS).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getFormWorkingGroups_success_validatResponseFormat() {
        EndpointTestBuilder.from(GET_FORM_WGS).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getFormWorkingGroups_failure_invalidFormId() {
        EndpointTestBuilder.from(TestCaseHelper.buildBadRequestCase(FORM_WORKING_GROUPS_BASE_URL, new String[] { "nope" }, null)).run();
    }

    //
    // POST /form/{id}/working_groups
    //
    @Test
    void postFormWorkingGroup_failure_requireAuth() {
        FormWorkingGroup wg = persistSampleWGForm().get(0);
        String formId = wg.getForm().getId();

        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORM_WORKING_GROUPS_BASE_URL, new String[] { formId }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doPost(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postFormWorkingGroup_failure_csrfGuard() {
        String formId = persistSampleMembershipform().get(0).getId();

        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORM_WORKING_GROUPS_BASE_URL, new String[] { formId }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .doPost(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postFormWorkingGroup_success() {
        FormWorkingGroup wg = persistSampleWGForm().get(0);
        String formId = wg.getForm().getId();

        EndpointTestBuilder
                .from(TestCaseHelper.buildSuccessCase(FORM_WORKING_GROUPS_BASE_URL, new String[] { formId }, null))
                .doPost(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postFormWorkingGroup_success_validateRequestSchema() {
        String request = generateSample();
        Assertions.assertTrue(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.WORKING_GROUP_SCHEMA_PATH).matches(request));

        FormWorkingGroup wg = persistSampleWGForm().get(0);
        String formId = wg.getForm().getId();

        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildSuccessCase(FORM_WORKING_GROUPS_BASE_URL, new String[] { formId },
                                SchemaNamespaceHelper.WORKING_GROUPS_SCHEMA_PATH))
                .doPost(request)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postFormWorkingGroup_failure_invalidFormId() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildBadRequestCase(FORM_WORKING_GROUPS_BASE_URL, new String[] { "nope" }, null))
                .doPost(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postFormWorkingGroup_success_validateSchema() {
        FormWorkingGroup wg = persistSampleWGForm().get(0);
        String formId = wg.getForm().getId();
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildSuccessCase(FORM_WORKING_GROUPS_BASE_URL, new String[] { formId },
                                SchemaNamespaceHelper.WORKING_GROUPS_SCHEMA_PATH))
                .doPost(generateSample())
                .andCheckSchema()
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postFormWorkingGroup_success_validateResponse() {
        FormWorkingGroup wg = persistSampleWGForm().get(0);
        String formId = wg.getForm().getId();
        EndpointTestBuilder
                .from(TestCaseHelper.buildSuccessCase(FORM_WORKING_GROUPS_BASE_URL, new String[] { formId }, null))
                .doPost(generateSample())
                .andCheckFormat()
                .run();
    }

    //
    // GET /form/{id}/working_groups/{wgID}
    //
    @Test
    void getFormWorkingGroupByID_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORM_WORKING_GROUP_BY_ID_URL, new String[] { FORM_ID, FORM_WG_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getFormWorkingGroupByID_success() {
        EndpointTestBuilder.from(GET_FORM_WG_BY_ID).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getFormWorkingGroupByID_success_validateSchema() {
        EndpointTestBuilder.from(GET_FORM_WG_BY_ID).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getFormWorkingGroupByID_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_FORM_WG_BY_ID).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getFormWorkingGroupByID_failure_wgNotFound() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildNotFoundCase(FORM_WORKING_GROUP_BY_ID_URL, new String[] { FORM_ID, "nope" }, null))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getFormWorkingGroupByID_failure_invalidFormId() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildBadRequestCase(FORM_WORKING_GROUP_BY_ID_URL, new String[] { "nope", FORM_WG_ID }, null))
                .run();
    }

    //
    // PUT /form/{id}/working_groups/{wgID}
    //
    @Test
    void putFormWorkingGroupByID_failure_requireAuth() {
        // create record to update
        FormWorkingGroup wg = persistSampleWGForm().get(0);
        String formId = wg.getForm().getId();
        String wgId = wg.getId();

        // auth is triggered after CSRF for non GET requests
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORM_WORKING_GROUP_BY_ID_URL, new String[] { formId, wgId }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doPut(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormWorkingGroupByID_failure_csrfGuard() {
        // create record to update
        String wgId = persistSampleWGForm().get(0).getId();

        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORM_WORKING_GROUP_BY_ID_URL, new String[] { FORM_ID, wgId }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .doPut(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormWorkingGroupByID_failure_emptyBody() {
        // create record to update
        String wgId = persistSampleWGForm().get(0).getId();

        EndpointTestBuilder
                .from(TestCaseHelper.buildBadRequestCase(FORM_WORKING_GROUP_BY_ID_URL, new String[] { FORM_ID, wgId }, null))
                .doPut(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormWorkingGroupByID_success() {
        // create record to update
        FormWorkingGroup wg = persistSampleWGForm().get(0);
        String formId = wg.getForm().getId();
        String wgId = wg.getId();

        EndpointTestBuilder
                .from(TestCaseHelper.buildSuccessCase(FORM_WORKING_GROUP_BY_ID_URL, new String[] { formId, wgId }, null))
                .doPut(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormWorkingGroupByID_success_validateRequestSchema() {
        String request = generateSample();
        Assertions.assertTrue(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.WORKING_GROUP_SCHEMA_PATH).matches(request));

        FormWorkingGroup wg = persistSampleWGForm().get(0);
        String formId = wg.getForm().getId();
        String wgId = wg.getId();
        EndpointTestBuilder
                .from(TestCaseHelper.buildSuccessCase(FORM_WORKING_GROUP_BY_ID_URL, new String[] { formId, wgId }, null))
                .doPut(request)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormWorkingGroupByID_success_validateSchema() {
        // create record to update
        FormWorkingGroup wg = persistSampleWGForm().get(0);
        String formId = wg.getForm().getId();
        String wgId = wg.getId();
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildSuccessCase(FORM_WORKING_GROUP_BY_ID_URL, new String[] { formId, wgId },
                                SchemaNamespaceHelper.WORKING_GROUPS_SCHEMA_PATH))
                .doPut(generateSample())
                .andCheckSchema()
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormWorkingGroupByID_success_validateResponseFormat() {
        // create record to update
        FormWorkingGroup wg = persistSampleWGForm().get(0);
        String formId = wg.getForm().getId();
        String wgId = wg.getId();

        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildSuccessCase(FORM_WORKING_GROUP_BY_ID_URL, new String[] { formId, wgId },
                                SchemaNamespaceHelper.WORKING_GROUPS_SCHEMA_PATH))
                .doPut(generateSample())
                .andCheckFormat()
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormWorkingGroupByID_failure_invalidFormId() {
        String wgId = persistSampleWGForm().get(0).getId();

        EndpointTestBuilder
                .from(TestCaseHelper.buildBadRequestCase(FORM_WORKING_GROUP_BY_ID_URL, new String[] { "nope", wgId }, null))
                .doPut(generateSample())
                .run();
    }

    //
    // DELETE /form/{id}/working_groups/{wgID}
    //
    @Test
    void deleteFormWorkingGroupByID_failure_requireAuth() {
        // create record to update
        FormWorkingGroup wg = persistSampleWGForm().get(0);
        String formId = wg.getForm().getId();
        String wgId = wg.getId();

        // auth is triggered after CSRF for non GET requests
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORM_WORKING_GROUP_BY_ID_URL, new String[] { formId, wgId }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doDelete(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void deleteFormWorkingGroupByID_failure_csrfGuard() {
        // create record to update
        String wgId = persistSampleWGForm().get(0).getId();

        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORM_WORKING_GROUP_BY_ID_URL, new String[] { FORM_ID, wgId }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .doDelete(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void deleteFormWorkingGroupByID_success() {
        // create record to update
        FormWorkingGroup wg = persistSampleWGForm().get(0);
        String formId = wg.getForm().getId();
        String wgId = wg.getId();

        EndpointTestBuilder
                .from(TestCaseHelper.buildSuccessCase(FORM_WORKING_GROUP_BY_ID_URL, new String[] { formId, wgId }, null))
                .doDelete(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void deleteFormWorkingGroupByID_failure_invalidFormId() {
        String wgId = persistSampleWGForm().get(0).getId();

        EndpointTestBuilder
                .from(TestCaseHelper.buildBadRequestCase(FORM_WORKING_GROUP_BY_ID_URL, new String[] { "nope", wgId }, null))
                .doDelete(null)
                .run();
    }

    private List<MembershipForm> persistSampleMembershipform() {
        return mockDao
                .add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(MembershipForm.class), null), Arrays.asList(generateMemFormRawSample()));
    }

    private MembershipForm generateMemFormRawSample() {
        return DtoHelper.generateForm(Optional.of(AuthHelper.TEST_USER_NAME));
    }

    private FormWorkingGroupData generateFormWGSampleRaw() {
        return wgMapper.toModel(DtoHelper.generateWorkingGroup(persistSampleMembershipform().get(0)));
    }

    private List<FormWorkingGroup> persistSampleWGForm() {
        FormWorkingGroupData wg = generateFormWGSampleRaw();
        return mockDao
                .add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(FormWorkingGroup.class), null), Arrays.asList(wgMapper.toDTO(wg, mockDao)));
    }

    private String generateSample() {
        try {
            return om.writeValueAsString(generateFormWGSampleRaw());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
