/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import jakarta.inject.Inject;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.membership.application.dto.membership.Contact;
import org.eclipsefoundation.membership.application.dto.membership.FormOrganization;
import org.eclipsefoundation.membership.application.dto.membership.FormWorkingGroup;
import org.eclipsefoundation.membership.application.dto.membership.MembershipForm;
import org.eclipsefoundation.membership.application.model.FormMailerData;
import org.eclipsefoundation.membership.application.test.helper.DtoHelper;
import org.eclipsefoundation.testing.helpers.AuthHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.quarkus.mailer.Mail;
import io.quarkus.mailer.MockMailbox;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;

@QuarkusTest
@QuarkusTestResource(OidcWiremockTestResource.class)
class DefaultMailerServiceTest {
    // temp value, will eventually be the email claim of the test user
    public static final String TO_ADDRESS = "webdev@eclipse-foundation.org";

    @ConfigProperty(name = "eclipse.mailer.membership.inbox")
    String membershipMailbox;

    @Inject
    MockMailbox mailbox;

    @Inject
    DefaultMailerService mailerService;

    @BeforeEach
    void init() {
        mailbox.clear();
    }

    // Test disabled temporarily. Quarkus 2.x adds better support for mocked users
    // and should be more easily able to test
    // @Test
    // @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = "viewer")
    void sendToFormAuthor_success() {
        // set up form to submit through mock service
        MembershipForm f = DtoHelper.generateForm(Optional.of(AuthHelper.TEST_USER_NAME));
        FormOrganization org = DtoHelper.generateOrg(f);
        List<FormWorkingGroup> wgs = DtoHelper.generateWorkingGroups(f);
        List<Contact> contacts = DtoHelper.generateContacts(f);

        FormMailerData data = new FormMailerData("Test User", f, org, wgs, contacts, true);

        // perform the action
        mailerService.sendToFormAuthor(data);

        // verify that it was sent
        List<Mail> sent = mailbox.getMessagesSentTo(TO_ADDRESS);
        Assertions.assertEquals(1, sent.size());

        Mail actual = sent.get(0);
        // email should have both Text and HTML contents
        Assertions.assertNotNull(actual.getText());
        Assertions.assertNotNull(actual.getHtml());
        // should only send 1 message out
        Assertions.assertEquals(1, mailbox.getTotalMessagesSent());
    }

    @Test
    void sendToFormAuthor_anon() {
        // set up form to submit through mock service
        MembershipForm f = DtoHelper.generateForm(Optional.of(AuthHelper.TEST_USER_NAME));
        FormOrganization org = DtoHelper.generateOrg(f);
        List<FormWorkingGroup> wgs = DtoHelper.generateWorkingGroups(f);
        List<Contact> contacts = DtoHelper.generateContacts(f);

        FormMailerData data = new FormMailerData("Test User", f, org, wgs, contacts, true);

        // verify that it failed to send due to state exception
        Assertions.assertThrows(IllegalStateException.class, () -> { mailerService.sendToFormAuthor(data); });
        // verify that no messages were sent
        Assertions.assertEquals(0, mailbox.getTotalMessagesSent());
    }

    @Test
    void sendToFormAuthor_missingData() {
        // perform the action
        Assertions.assertThrows(IllegalStateException.class, () -> { mailerService.sendToFormAuthor(null); });
        // verify that no messages were sent
        List<Mail> sent = mailbox.getMessagesSentTo(membershipMailbox);
        Assertions.assertTrue(sent == null || sent.isEmpty());
        Assertions.assertEquals(0, mailbox.getTotalMessagesSent());
    }

    // @Test
    // @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = "viewer")
    void sendToMembershipTeam_success() {
        // set up form to submit through mock service
        MembershipForm f = DtoHelper.generateForm(Optional.of(AuthHelper.TEST_USER_NAME));
        FormOrganization org = DtoHelper.generateOrg(f);
        List<FormWorkingGroup> wgs = DtoHelper.generateWorkingGroups(f);
        List<Contact> contacts = DtoHelper.generateContacts(f);

        FormMailerData data = new FormMailerData("Test User", f, org, wgs, contacts, true);
        // perform the action
        mailerService.sendToMembershipTeam(data);

        // verify that it was sent
        List<Mail> sent = mailbox.getMessagesSentTo(membershipMailbox);
        Assertions.assertEquals(1, sent.size());

        Mail actual = sent.get(0);
        // email should have both Text and HTML contents
        Assertions.assertNotNull(actual.getText());
        Assertions.assertNotNull(actual.getHtml());
        // should only send 1 message out
        Assertions.assertEquals(1, mailbox.getTotalMessagesSent());
    }

    @Test
    void sendToMembershipTeam_missingData() {
        // set up form to submit through mock service
        MembershipForm f = DtoHelper.generateForm(Optional.of(AuthHelper.TEST_USER_NAME));
        FormOrganization org = DtoHelper.generateOrg(f);
        List<FormWorkingGroup> wgs = DtoHelper.generateWorkingGroups(f);
        List<Contact> contacts = DtoHelper.generateContacts(f);

        FormMailerData nullFormMail = new FormMailerData("Test User", null, org, wgs, contacts, true);
        Assertions
                .assertThrows(IllegalStateException.class, () -> { mailerService.sendToMembershipTeam(nullFormMail); },
                        "The form being null should throw an IllegalStateException");

        FormMailerData nullOrgMail = new FormMailerData("Test User", f, null, wgs, contacts, true);
        Assertions
                .assertThrows(IllegalStateException.class, () -> { mailerService.sendToMembershipTeam(nullOrgMail); },
                        "The org being null should throw an IllegalStateException");

        FormMailerData nullContactMail = new FormMailerData("Test User", f, org, wgs, null, true);
        Assertions
                .assertThrows(IllegalStateException.class, () -> { mailerService.sendToMembershipTeam(nullContactMail); },
                        "The contacts being null should throw an IllegalStateException");

        FormMailerData emptyContactMail = new FormMailerData("Test User", f, org, wgs, Collections.emptyList(), true);
        Assertions
                .assertThrows(IllegalStateException.class, () -> { mailerService.sendToMembershipTeam(emptyContactMail); },
                        "An empty contact list should throw an IllegalStateException");
                        
        // verify that no messages were sent
        List<Mail> sent = mailbox.getMessagesSentTo(membershipMailbox);
        Assertions.assertTrue(sent == null || sent.isEmpty());
        Assertions.assertEquals(0, mailbox.getTotalMessagesSent());
    }
}
