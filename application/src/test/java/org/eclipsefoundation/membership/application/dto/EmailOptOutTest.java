/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.dto;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.application.dto.membership.EmailOptOut;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.membership.application.test.helper.DtoHelper;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class EmailOptOutTest {

    @Inject
    DefaultHibernateDao dao;
    @Inject
    FilterService filters;

    private static final RequestWrapper WRAP = new FlatRequestWrapper(URI.create("https://membership.eclipse.org"));

    @Test
    void getById() {
        List<EmailOptOut> addResults = persistOptOut();
        Assertions.assertFalse(addResults.isEmpty());
        EmailOptOut expected = addResults.get(0);

        // Fetch the same record for comparison
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), expected.getId().toString());

        List<EmailOptOut> getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(EmailOptOut.class), params));
        Assertions.assertEquals(1, getResults.size(), "There should be one EmailOptOut record found");
        Assertions.assertEquals(expected, getResults.get(0), "The add and fetch result should be the same");

        // Test again with an invalid id
        params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), "456");

        getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(EmailOptOut.class), params));
        Assertions.assertTrue(getResults.isEmpty(), "There should be 0 EmailOptOut records found");
    }

    @Test
    void getByEmail() {
        List<EmailOptOut> addResults = persistOptOut();
        Assertions.assertFalse(addResults.isEmpty());
        EmailOptOut expected = addResults.get(0);

        // Fetch the same record for comparison
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.EMAIL.getName(), expected.getEmail());

        List<EmailOptOut> getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(EmailOptOut.class), params));
        Assertions.assertEquals(1, getResults.size(), "There should be one EmailOptOut record found");
        Assertions.assertEquals(expected, getResults.get(0), "The add and fetch result should be the same");

        // Test again with an invalid email
        params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.EMAIL.getName(), "nope");

        getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(EmailOptOut.class), params));
        Assertions.assertTrue(getResults.isEmpty(), "There should be 0 EmailOptOut records found");
    }

    @Test
    void getByEmails() {
        List<EmailOptOut> addResults = persistOptOut();
        Assertions.assertFalse(addResults.isEmpty());
        EmailOptOut expected = addResults.get(0);

        // Fetch the same record for comparison
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.put(MembershipFormAPIParameterNames.EMAILS.getName(), Arrays.asList(expected.getEmail(), "test@Email.com"));

        List<EmailOptOut> getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(EmailOptOut.class), params));
        Assertions.assertEquals(1, getResults.size(), "There should be one EmailOptOut record found");
        Assertions.assertEquals(expected, getResults.get(0), "The add and fetch result should be the same");

        // Test again with an invalid email
        params = new MultivaluedMapImpl<>();
        params.put(MembershipFormAPIParameterNames.EMAILS.getName(), Arrays.asList("nope", "wrong"));

        getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(EmailOptOut.class), params));
        Assertions.assertTrue(getResults.isEmpty(), "There should be 0 EmailOptOut records found");
    }

    @Test
    void getByTransactionId() {
        List<EmailOptOut> addResults = persistOptOut();
        Assertions.assertFalse(addResults.isEmpty());
        EmailOptOut expected = addResults.get(0);

        // Fetch the same record for comparison
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.TRANSACTION_ID.getName(), expected.getTransactionId().toString());

        List<EmailOptOut> getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(EmailOptOut.class), params));
        Assertions.assertEquals(1, getResults.size(), "There should be one EmailOptOut record found");
        Assertions.assertEquals(expected, getResults.get(0), "The add and fetch result should be the same");

        // Test again with an invalid email
        params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.TRANSACTION_ID.getName(), "9876");

        getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(EmailOptOut.class), params));
        Assertions.assertTrue(getResults.isEmpty(), "There should be 0 EmailOptOut records found");
    }

    private List<EmailOptOut> persistOptOut() {
        return dao.add(new RDBMSQuery<>(WRAP, filters.get(EmailOptOut.class)), Arrays.asList(DtoHelper.generateOptOut()));
    }
}
