package org.eclipsefoundation.membership.application.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.model.OrganizationContactData;
import org.eclipsefoundation.foundationdb.client.model.OrganizationData;
import org.eclipsefoundation.foundationdb.client.model.PeopleData;
import org.eclipsefoundation.membership.application.api.FoundationAPI;

import jakarta.annotation.Priority;
import jakarta.enterprise.inject.Alternative;
import jakarta.inject.Singleton;
import jakarta.ws.rs.core.Response;

@Alternative
@Priority(1)
@RestClient
@Singleton
public class MockFoundationAPI implements FoundationAPI {
    public List<OrganizationContactData> contacts;
    public Map<String, PeopleData> people;
    public List<OrganizationData> orgs;

    public MockFoundationAPI() {

        // organizations setup
        this.contacts = new ArrayList<>();
        this.contacts.add(generateOrgContact(1, "da_wizz", "EMPLY"));
        this.contacts.add(generateOrgContact(1, "da_wizz", "CC"));
        this.contacts.add(generateOrgContact(2, "barshal_blathers", "EMPLY"));
        this.contacts.add(generateOrgContact(2, "barshal_blathers", "CC"));
        this.contacts.add(generateOrgContact(1, "grunt", "EMPLY"));
        this.contacts.add(generateOrgContact(1, "grunt", "CC"));
        this.contacts.add(generateOrgContact(3, "opearson", "EMPLY"));
        this.contacts.add(generateOrgContact(3, "opearson", "CC"));
        this.contacts.add(generateOrgContact(1, "doofenshmirtz", "EMPLY"));
        this.contacts.add(generateOrgContact(1, "oouilson", "CC"));

        // people setup
        this.people = new HashMap<>();
        this.people.put("opearson", createSamplePerson("opearson", "Oli", "Pearson"));
        this.people.put("da_wizz", createSamplePerson("da_wizz", "Da", "Wizz"));
        this.people.put("doofenshmirtz", createSamplePerson("doofenshmirtz", "Dr", "Doofenshmirtz"));
        this.people.put("barshal_blathers", createSamplePerson("barshal_blathers", "Barshal", "Blathers"));
        this.people.put("grunt", createSamplePerson("grunt", "Grunt", "Grunt"));
        this.people.put("newbie", createSamplePerson("newbie", "New", "Bie"));

        this.orgs = new ArrayList<>();
        this.orgs.add(generateOrganization(15, "sample"));
    }

    @Override
    public Response getAllOrganizations(BaseAPIParameters params) {
        return Response.ok(new ArrayList<>(orgs)).build();
    }

    @Override
    public Response getAllOrganizationContacts(Integer orgId, String personId, BaseAPIParameters params) {
        return Response.ok(contacts).build();
    }

    @Override
    public Optional<PeopleData> getPerson(String personId) {
        return Optional.ofNullable(people.get(personId));
    }

    @Override
    public List<PeopleData> updatePeople(PeopleData person) {
        people.put(person.getPersonID(), person);
        return Arrays.asList(people.get(person.getPersonID()));
    }

    private OrganizationData generateOrganization(int orgId, String name) {
        return OrganizationData
                .builder()
                .setOrganizationID(orgId)
                .setComments("")
                .setFax("")
                .setLatLong("")
                .setMemberSince(new Date())
                .setName1(name)
                .setName2(null)
                .setPhone("")
                .setScrmGUID("")
                .setTimestamp(new Date())
                .build();
    }

    private OrganizationContactData generateOrgContact(Integer organizationId, String username, String relation) {
        return OrganizationContactData
                .builder()
                .setComments("")
                .setDocumentID("")
                .setOrganizationID(organizationId)
                .setPersonID(username)
                .setRelation(relation)
                .setTitle("")
                .build();
    }

    private PeopleData createSamplePerson(String personId, String fname, String lname) {
        return PeopleData
                .builder()
                .setPersonID(personId)
                .setBlog("")
                .setComments("")
                .setEmail("")
                .setFax("")
                .setFname("")
                .setIssuesPending(false)
                .setLatitude("")
                .setLname("")
                .setLongitude("")
                .setMember(false)
                .setMemberPassword("")
                .setMemberSince(new Date(System.currentTimeMillis()))
                .setMobile("")
                .setPhone("")
                .setProvisioning("Sample provisioning string")
                .setScrmGuid("")
                .setType("")
                .setUnixAcctCreated(false)
                .build();
    }
}
