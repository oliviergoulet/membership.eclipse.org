/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.dto;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.application.dto.membership.Address;
import org.eclipsefoundation.membership.application.test.helper.DtoHelper;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class AddressTest {

    @Inject
    DefaultHibernateDao dao;
    @Inject
    FilterService filters;

    private static final RequestWrapper WRAP = new FlatRequestWrapper(URI.create("https://membership.eclipse.org"));

    @Test
    void getById() {
        List<Address> addResults = persistAddress();
        Assertions.assertFalse(addResults.isEmpty());
        Address expected = addResults.get(0);

        // Fetch the same record for comparison
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), expected.getId());

        List<Address> getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(Address.class), params));
        Assertions.assertEquals(1, getResults.size(), "There should be one Address record found");
        Assertions.assertEquals(expected, getResults.get(0), "The add and fetch result should be the same");

        // Test again with an invalid id
        params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), "nope");

        getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(Address.class), params));
        Assertions.assertTrue(getResults.isEmpty(), "There should be 0 Address records found");
    }

    private List<Address> persistAddress() {
        return dao.add(new RDBMSQuery<>(WRAP, filters.get(Address.class)), Arrays.asList(DtoHelper.generateAddress()));
    }
}
