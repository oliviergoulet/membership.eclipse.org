/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.application.test.helper;

import org.eclipsefoundation.membership.application.helper.EncryptionHelper;

import io.quarkus.test.Mock;

/**
 * Quick mock helper to stub the encryption, as we'll need to do some work to get a PEM in CI.
 */
@Mock
public class MockEncryptionHelper extends EncryptionHelper {

    @Override
    public String encryptAndEncodeValue(String src) {
        // just return the source as we won't likely have a PEM here
        return src;
    }
}
