/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*           Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.test.helper;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.eclipsefoundation.membership.application.dto.membership.Address;
import org.eclipsefoundation.membership.application.dto.membership.Contact;
import org.eclipsefoundation.membership.application.dto.membership.EmailOptOut;
import org.eclipsefoundation.membership.application.dto.membership.FormOrganization;
import org.eclipsefoundation.membership.application.dto.membership.FormWorkingGroup;
import org.eclipsefoundation.membership.application.dto.membership.MembershipForm;
import org.eclipsefoundation.membership.application.namespace.ContactTypes;
import org.eclipsefoundation.membership.application.namespace.FormState;
import org.eclipsefoundation.membership.application.namespace.OrganizationTypes;

/**
 * Helper for creating valid random DTO objects for use in testing.
 * 
 * @author Martin Lowe
 *
 */
public class DtoHelper {

    // used for random picking, not cryptographic
    private static final Random r = new Random();

    /**
     * Generates a random valid form for use in tests.
     * 
     * @param userID optional userID to set if needed
     * @return valid membership form data
     */
    public static MembershipForm generateForm(Optional<String> userID) {
        MembershipForm mf = new MembershipForm();
        mf.setUserID(userID.orElseGet(() -> RandomStringUtils.randomAlphabetic(10)));
        mf.setMembershipLevel(RandomStringUtils.randomAlphabetic(10));
        mf.setSigningAuthority(Math.random() > 0.5);
        mf.setRegistrationCountry("CA");
        mf.setVatNumber(RandomStringUtils.randomNumeric(10));
        mf.setPurchaseOrderRequired(Math.random() > 0.5 ? "yes" : "no");
        mf
                .setDateCreated(Math.random() > 0.5 ? System.currentTimeMillis() + r.nextInt(10000)
                        : System.currentTimeMillis() - r.nextInt(10000));
        mf.setApplicationGroup("eclipse");
        mf.setState(FormState.INPROGRESS);
        return mf;
    }

    public static FormOrganization generateOrg(MembershipForm mf) {
        FormOrganization o = new FormOrganization();
        o.setForm(mf);
        o.setLegalName(RandomStringUtils.randomAlphabetic(4, 10));
        o.setOrganizationType(OrganizationTypes.OTHER);
        o.setTwitterHandle("@" + RandomStringUtils.randomAlphabetic(4, 10));
        o.setAggregateRevenue(RandomStringUtils.randomNumeric(5, 10));
        o.setEmployeeCount(RandomStringUtils.randomNumeric(5, 10));
        o.setAddress(generateAddress());
        return o;
    }

    public static FormOrganization generateOrg(MembershipForm mf, Address a) {
        FormOrganization o = new FormOrganization();
        o.setForm(mf);
        o.setLegalName(RandomStringUtils.randomAlphabetic(4, 10));
        o.setOrganizationType(OrganizationTypes.OTHER);
        o.setTwitterHandle("@" + RandomStringUtils.randomAlphabetic(4, 10));
        o.setAggregateRevenue(RandomStringUtils.randomNumeric(5, 10));
        o.setEmployeeCount(RandomStringUtils.randomNumeric(5, 10));
        o.setAddress(a);
        return o;
    }

    public static Address generateAddress() {
        Address a = new Address();
        a.setLocality(RandomStringUtils.randomAlphabetic(4, 10));
        a.setCountry(RandomStringUtils.randomAlphabetic(4, 10));
        a.setPostalCode(RandomStringUtils.randomAlphabetic(4, 10));
        a.setAdministrativeArea(RandomStringUtils.randomAlphabetic(2));
        a.setAddressLine1(RandomStringUtils.randomAlphabetic(4, 10));
        return a;
    }

    public static EmailOptOut generateOptOut() {
        EmailOptOut o = new EmailOptOut();
        o.setId((long) r.nextInt(10000));
        o.setDateReceived(LocalDateTime.now());
        o.setEmail(generateEmail());
        o.setTransactionId((long) r.nextInt(10000));
        return o;
    }

    public static List<Contact> generateContacts(MembershipForm form) {
        List<Contact> out = new ArrayList<>();
        for (int j = 0; j < ContactTypes.values().length; j++) {
            // randomly skip contacts
            if (Math.random() > 0.5 && j > 0) {
                continue;
            }
            out.add(generateContact(form, Optional.of(ContactTypes.values()[j])));
        }
        return out;
    }

    public static Contact generateContact(MembershipForm form, Optional<ContactTypes> type) {
        Contact c = new Contact();
        c.setForm(form);
        c.setTitle("Sample Title");
        c.setfName(RandomStringUtils.randomAlphabetic(4, 10));
        c.setlName(RandomStringUtils.randomAlphabetic(4, 10));
        c.setType(type.orElse(ContactTypes.WORKING_GROUP));
        c.setEmail(generateEmail());
        return c;
    }

    public static List<FormWorkingGroup> generateWorkingGroups(MembershipForm form) {
        List<FormWorkingGroup> wgs = new ArrayList<>();
        // randomly create WG entries
        while (true) {
            wgs.add(generateWorkingGroup(form));
            if (Math.random() > 0.5) {
                break;
            }
        }
        return wgs;
    }

    public static FormWorkingGroup generateWorkingGroup(MembershipForm form) {
        FormWorkingGroup wg = new FormWorkingGroup();
        wg.setWorkingGroupID(RandomStringUtils.randomAlphabetic(4, 10));
        wg.setParticipationLevel(RandomStringUtils.randomAlphabetic(4, 10));
        // get a random instance of time
        wg.setEffectiveDate(OffsetDateTime.now().minus(r.nextInt(1000000), ChronoUnit.SECONDS));
        wg.setContact(generateContact(form, Optional.empty()));
        wg.setForm(form);
        return wg;
    }

    private static String generateEmail() {
        StringBuilder sb = new StringBuilder();
        sb.append(RandomStringUtils.randomAlphabetic(4, 10));
        sb.append("@");
        sb.append(RandomStringUtils.randomAlphabetic(4, 10));
        sb.append(".");
        sb.append(RandomStringUtils.randomAlphabetic(2));
        return sb.toString();
    }

    private DtoHelper() {
    }
}
