/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*           Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.resources;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.application.dao.EclipsePersistenceDAO;
import org.eclipsefoundation.membership.application.dto.eclipse.AccountRequests;
import org.eclipsefoundation.membership.application.dto.membership.MembershipForm;
import org.eclipsefoundation.membership.application.test.dao.MockHibernateDao;
import org.eclipsefoundation.membership.application.test.helper.DtoHelper;
import org.eclipsefoundation.membership.application.test.helper.SchemaNamespaceHelper;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.testing.helpers.AuthHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;

@QuarkusTest
class MembershipFormResourceTest {
    private static final String FORM_UUID = "form-uuid";
    public static final String FORMS_BASE_URL = "/form";
    public static final String FORMS_BY_ID_URL = FORMS_BASE_URL + "/{id}";

    public static final String COMPLETE_URL = FORMS_BY_ID_URL + "/complete";

    public static final String VALIDATE_URL_MISSING_PARAMS = FORMS_BY_ID_URL + "/{transactionId}";
    public static final String VALIDATE_URL_MISSING_TOKEN = FORMS_BY_ID_URL + "/{transactionId}?state={state}";
    public static final String VALIDATE_URL_MISSING_STATE = FORMS_BY_ID_URL + "/{transactionId}?token={token}";
    public static final String VALIDATE_URL = VALIDATE_URL_MISSING_PARAMS + "?token={token}&state={state}";

    public static final EndpointTestCase VALIDATE_NO_TOKEN_CASE = TestCaseHelper
            .buildBadRequestCase(VALIDATE_URL_MISSING_TOKEN, new String[] { FORM_UUID, "69", "CONFIRMED" },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    public static final EndpointTestCase VALIDATE_NO_STATE_CASE = TestCaseHelper
            .buildBadRequestCase(VALIDATE_URL_MISSING_STATE, new String[] { FORM_UUID, "69", "token-string" },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    public static final EndpointTestCase GET_FORMS_CASE = TestCaseHelper
            .buildSuccessCase(FORMS_BASE_URL, new String[] {}, SchemaNamespaceHelper.MEMBERSHIP_FORMS_SCHEMA_PATH);

    public static final EndpointTestCase COMPLETE_FORM_CASE = TestCaseHelper
            .buildSuccessCase(COMPLETE_URL, new String[] { FORM_UUID }, SchemaNamespaceHelper.MEMBERSHIP_FORMS_SCHEMA_PATH);

    public static final EndpointTestCase GET_FORM_BY_ID_CASE = TestCaseHelper
            .buildSuccessCase(FORMS_BY_ID_URL, new String[] { FORM_UUID }, SchemaNamespaceHelper.MEMBERSHIP_FORM_SCHEMA_PATH);

    @Inject
    ObjectMapper om;
    @Inject
    MockHibernateDao mockDao;
    @Inject
    EclipsePersistenceDAO eclipseDB;
    @Inject
    FilterService filters;

    //
    // GET /form
    //
    @Test
    void getForms_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORMS_BASE_URL, new String[] {}, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getForms_success() {
        EndpointTestBuilder.from(GET_FORMS_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getForms_success_validateSchema() {
        EndpointTestBuilder.from(GET_FORMS_CASE).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getForms_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_FORMS_CASE).andCheckFormat().run();
    }

    //
    // GET /form/{id}
    //
    @Test
    void getFormByID_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORMS_BY_ID_URL, new String[] { FORM_UUID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getFormByID_success() {
        EndpointTestBuilder.from(GET_FORM_BY_ID_CASE).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getFormByID_success_validateSchema() {
        EndpointTestBuilder.from(GET_FORM_BY_ID_CASE).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getFormByID_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_FORM_BY_ID_CASE).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getFormByID_failure_invalidFormID() {
        EndpointTestBuilder.from(TestCaseHelper.buildBadRequestCase(FORMS_BY_ID_URL, new String[] { "nope" }, null)).run();
    }

    //
    // POST /form
    //
    @Test
    void postForm_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORMS_BASE_URL, new String[] {}, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doPost(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postForm_failure_csrfGuard() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORMS_BASE_URL, new String[] {}, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .doPost(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postForm_success() {
        EndpointTestBuilder.from(GET_FORMS_CASE).doPost(generateSample()).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postForm_success_validRequestSchema() {
        String request = generateSample();
        Assertions.assertTrue(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.MEMBERSHIP_FORM_SCHEMA_PATH).matches(request));

        EndpointTestBuilder.from(GET_FORMS_CASE).doPost(request).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postForm_success_validateSchema() {
        EndpointTestBuilder.from(GET_FORMS_CASE).doPost(generateSample()).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postForm_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_FORMS_CASE).doPost(generateSample()).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postForm_failure_emptyBody() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORMS_BASE_URL, new String[] {}, null)
                        .setStatusCode(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
                        .build())
                .doPost(null)
                .run();
    }

    //
    // POST /form/{id}/complete
    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postFormComplete_failure_csrfGuard() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(COMPLETE_URL, new String[] { FORM_UUID }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .doPost()
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postFormComplete_success() {
        EndpointTestBuilder.from(COMPLETE_FORM_CASE).doPost().andCheckSchema().andCheckFormat().run();
    }

    //
    // PUT /form/{id}
    //
    @Test
    void putFormByID_failure_requireAuth() {
        String formId = persistSampleMembershipform().get(0).getId();

        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORMS_BY_ID_URL, new String[] { formId }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doPut(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putFormByID_failure_csrfGuard() {
        String formId = persistSampleMembershipform().get(0).getId();

        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORMS_BY_ID_URL, new String[] { formId }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .doPut(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putFormByID_failure_emptyBody() {
        String formId = persistSampleMembershipform().get(0).getId();

        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORMS_BY_ID_URL, new String[] { formId }, null)
                        .setStatusCode(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
                        .build())
                .doPut(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putFormByID_success() {
        String formId = persistSampleMembershipform().get(0).getId();
        EndpointTestBuilder
                .from(TestCaseHelper.buildSuccessCase(FORMS_BY_ID_URL, new String[] { formId }, null))
                .doPut(generateSample())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putFormByID_success_validRequestSchema() {
        String request = generateSample();
        Assertions.assertTrue(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.MEMBERSHIP_FORM_SCHEMA_PATH).matches(request));

        String formId = persistSampleMembershipform().get(0).getId();
        EndpointTestBuilder.from(TestCaseHelper.buildSuccessCase(FORMS_BY_ID_URL, new String[] { formId }, null)).doPut(request).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putFormByID_success_validateSchema() {
        String formId = persistSampleMembershipform().get(0).getId();

        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildSuccessCase(FORMS_BY_ID_URL, new String[] { formId }, SchemaNamespaceHelper.MEMBERSHIP_FORMS_SCHEMA_PATH))
                .doPut(generateSample())
                .andCheckSchema()
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putFormByID_success_validateResponseFormat() {
        String formId = persistSampleMembershipform().get(0).getId();

        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildSuccessCase(FORMS_BY_ID_URL, new String[] { formId }, SchemaNamespaceHelper.MEMBERSHIP_FORMS_SCHEMA_PATH))
                .doPut(generateSample())
                .andCheckFormat()
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void putFormByID_failure_invalidFormId() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildBadRequestCase(FORMS_BY_ID_URL, new String[] { "nope" }, null))
                .doPut(generateSample())
                .run();
    }

    //
    // DELETE /form/{id}
    //
    @Test
    void deleteFormByID_failure_requireAuth() {
        String formId = persistSampleMembershipform().get(0).getId();

        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORMS_BY_ID_URL, new String[] { formId }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doDelete(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void deleteFormByID_failure_csrfGuard() {
        String formId = persistSampleMembershipform().get(0).getId();
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(FORMS_BY_ID_URL, new String[] { formId }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .doDelete(null)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void deleteFormByID_success() {
        String formId = persistSampleMembershipform().get(0).getId();
        EndpointTestBuilder.from(TestCaseHelper.buildSuccessCase(FORMS_BY_ID_URL, new String[] { formId }, null)).doDelete(null).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void deleteFormByID_failure_invalidFormId() {
        EndpointTestBuilder.from(TestCaseHelper.buildBadRequestCase(FORMS_BY_ID_URL, new String[] { "nope" }, null)).doDelete(null).run();
    }

    /*
     * GET form/{id}/{transactionId}
     */
    @Test
    void validateForm_success_confirm() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildSuccessCase(VALIDATE_URL, new String[] { "validate-form-uuid", "70", "token-string", "CONFIRMED" }, null))
                .run();
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), "test@test.co");
        List<AccountRequests> reqs = eclipseDB
                .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://test.eclipse.org/test")),
                        filters.get(AccountRequests.class), params));
        Assertions.assertNotNull(reqs);
        Assertions.assertFalse(reqs.isEmpty());

    }

    @Test
    void validateform_failure_missingToken() {
        EndpointTestBuilder.from(VALIDATE_NO_TOKEN_CASE).run();
    }

    @Test
    void validateform_failure_missingToken_validSchema() {
        EndpointTestBuilder.from(VALIDATE_NO_TOKEN_CASE).andCheckSchema().run();
    }

    @Test
    void validateform_failure_missingState() {
        EndpointTestBuilder.from(VALIDATE_NO_STATE_CASE).run();
    }

    @Test
    void validateform_failure_missingState_validSchema() {
        EndpointTestBuilder.from(VALIDATE_NO_STATE_CASE).andCheckSchema().run();
    }

    @Test
    void validateForm_failure_invalidParams() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildBadRequestCase(VALIDATE_URL, new String[] { "wrong-id", "69", "token-string", "CONFIRMED" }, null))
                .run();
    }

    @Test
    void validateForm_failure_tokenNotFound() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildNotFoundCase(VALIDATE_URL, new String[] { FORM_UUID, "69", "token", "CONFIRMED" }, null))
                .run();
    }

    @Test
    void validateForm_failure_transactionIdNotFound() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildNotFoundCase(VALIDATE_URL, new String[] { FORM_UUID, "42", "token-string", "CONFIRMED" }, null))
                .run();
    }

    private String generateSample() {
        try {
            return om.writeValueAsString(generateRawSample());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private List<MembershipForm> persistSampleMembershipform() {
        return mockDao
                .add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(MembershipForm.class), null), Arrays.asList(generateRawSample()));
    }

    private MembershipForm generateRawSample() {
        return DtoHelper.generateForm(Optional.of(AuthHelper.TEST_USER_NAME));
    }
}
