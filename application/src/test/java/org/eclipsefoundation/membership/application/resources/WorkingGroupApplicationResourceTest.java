package org.eclipsefoundation.membership.application.resources;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.application.dao.EclipsePersistenceDAO;
import org.eclipsefoundation.membership.application.dto.eclipse.AccountRequests;
import org.eclipsefoundation.membership.application.dto.membership.WorkingGroupApplication;
import org.eclipsefoundation.membership.application.dto.membership.WorkingGroupApplicationContact;
import org.eclipsefoundation.membership.application.model.WorkingGroupApplicationData;
import org.eclipsefoundation.membership.application.model.WorkingGroupApplicationData.WorkingGroupContact;
import org.eclipsefoundation.membership.application.namespace.FormState;
import org.eclipsefoundation.membership.application.test.helper.SchemaNamespaceHelper;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.testing.helpers.AuthHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.ConfigMetadata;
import io.quarkus.test.security.oidc.OidcSecurity;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

@QuarkusTest
public class WorkingGroupApplicationResourceTest {
    public static final String WORKING_GROUP_APP_BASE_URL = "/working_groups/application";
    public static final String WORKING_GROUP_APP_BY_ID_URL = WORKING_GROUP_APP_BASE_URL + "/{id}";
    public static final String WORKING_GROUP_APP_COMPLETE_URL = WORKING_GROUP_APP_BY_ID_URL + "/complete";

    public static final String WG_APPLICATION_ID = "1";

    public static final EndpointTestCase WG_APPLICATIONS_LIST = TestCaseHelper
            .buildSuccessCase(WORKING_GROUP_APP_BASE_URL, new String[] {}, SchemaNamespaceHelper.WORKING_GROUP_APPLICATIONS_SCHEMA_PATH);
    public static final EndpointTestCase CREATE_WG_APPLICATION = TestCaseHelper
            .buildSuccessCase(WORKING_GROUP_APP_BASE_URL, new String[] {}, SchemaNamespaceHelper.WORKING_GROUP_APPLICATION_SCHEMA_PATH);

    public static final EndpointTestCase WG_APPLICATION_BY_ID = TestCaseHelper
            .buildSuccessCase(WORKING_GROUP_APP_BY_ID_URL, new String[] { WG_APPLICATION_ID },
                    SchemaNamespaceHelper.WORKING_GROUP_APPLICATION_SCHEMA_PATH);

    public static final EndpointTestCase WG_APPLICATION_COMPLETE = TestCaseHelper
            .buildSuccessCase(WORKING_GROUP_APP_COMPLETE_URL, new String[] { "4" }, null);
    public static final EndpointTestCase WG_APPLICATION_COMPLETE_ALREADY_COMPLETED = TestCaseHelper
            .buildBadRequestCase(WORKING_GROUP_APP_COMPLETE_URL, new String[] { "5" }, null);
    public static final EndpointTestCase WG_APPLICATION_COMPLETE_NO_CONTACTS = TestCaseHelper
            .buildBadRequestCase(WORKING_GROUP_APP_COMPLETE_URL, new String[] { "6" }, null);
    public static final EndpointTestCase WG_APPLICATION_COMPLETE_NON_MEMBER = TestCaseHelper
            .buildBadRequestCase(WORKING_GROUP_APP_COMPLETE_URL, new String[] { "7" }, null);
    public static final EndpointTestCase WG_APPLICATION_COMPLETE_ALREADY_MEMBER = TestCaseHelper
            .buildBadRequestCase(WORKING_GROUP_APP_COMPLETE_URL, new String[] { "8" }, null);
    public static final EndpointTestCase WG_APPLICATION_COMPLETE_INVALID_WG = TestCaseHelper
            .buildBadRequestCase(WORKING_GROUP_APP_COMPLETE_URL, new String[] { "9" }, null);

    public static final String VALIDATE_URL_MISSING_PARAMS = WORKING_GROUP_APP_BY_ID_URL + "/{transactionId}";
    public static final String VALIDATE_URL_MISSING_TOKEN = WORKING_GROUP_APP_BY_ID_URL + "/{transactionId}?state={state}";
    public static final String VALIDATE_URL_MISSING_STATE = WORKING_GROUP_APP_BY_ID_URL + "/{transactionId}?token={token}";
    public static final String VALIDATE_URL = VALIDATE_URL_MISSING_PARAMS + "?token={token}&state={state}";

    public static final EndpointTestCase VALIDATE_NO_TOKEN_CASE = TestCaseHelper
            .buildBadRequestCase(VALIDATE_URL_MISSING_TOKEN, new String[] { "10", "42", "CONFIRMED" },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    public static final EndpointTestCase VALIDATE_NO_STATE_CASE = TestCaseHelper
            .buildBadRequestCase(VALIDATE_URL_MISSING_STATE, new String[] { "10", "42", "token-string" },
                    SchemaNamespaceHelper.ERROR_SCHEMA_PATH);

    @Inject
    ObjectMapper om;
    @Inject
    PersistenceDao dao;
    @Inject
    EclipsePersistenceDAO eclipseDB;
    @Inject
    FilterService filters;

    //
    // GET /working_groups/application
    //
    @Test
    void getWorkingGroupApplications_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(WORKING_GROUP_APP_BASE_URL, new String[] {}, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getWorkingGroupApplications_success() {
        EndpointTestBuilder.from(WG_APPLICATIONS_LIST).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getWorkingGroupApplications_success_validateSchema() {
        EndpointTestBuilder.from(WG_APPLICATIONS_LIST).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getWorkingGroupApplications_success_validatResponseFormat() {
        EndpointTestBuilder.from(WG_APPLICATIONS_LIST).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void getWorkingGroupApplications_success_onlyEntriesForCurrentOrg() {
        // data should contain only entries for the current users org, 15 in this case
        List<WorkingGroupApplicationData> organizationIds = jsonToEntity(
                EndpointTestBuilder.from(WG_APPLICATIONS_LIST).run().extract().asString());
        Assertions.assertTrue(organizationIds.stream().allMatch(app -> app.getOrgId() == 15));

        // this is the source data of the above data set, and should reflect the set of all available data
        List<WorkingGroupApplication> results = dao
                .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(WorkingGroupApplication.class), null));
        // there should be at least one entry where the organization ID isn't 15
        Assertions.assertFalse(results.stream().allMatch(app -> app.getOrganizationId() == 15));
    }

    //
    // POST /working_groups/application
    //
    @Test
    void postWorkingGroupApplication_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(WORKING_GROUP_APP_BASE_URL, new String[] {}, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doPost(generateSampleRequest(null, null))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postWorkingGroupApplication_failure_csrfGuard() {

        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(WORKING_GROUP_APP_BASE_URL, new String[] {}, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .doPost(generateSampleRequest(null, null))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postWorkingGroupApplication_success() {
        EndpointTestBuilder.from(CREATE_WG_APPLICATION).doPost(generateSampleRequest(null, null)).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postWorkingGroupApplication_success_validateRequestSchema() {
        String request = generateSampleRequest(null, null);
        Assertions.assertTrue(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.WORKING_GROUP_SCHEMA_PATH).matches(request));

        EndpointTestBuilder.from(CREATE_WG_APPLICATION).doPost(request).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postWorkingGroupApplication_success_validateSchema() {
        EndpointTestBuilder.from(CREATE_WG_APPLICATION).doPost(generateSampleRequest(null, null)).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    void postWorkingGroupApplication_success_validateResponse() {
        EndpointTestBuilder.from(CREATE_WG_APPLICATION).doPost(generateSampleRequest(null, null)).andCheckFormat().run();
    }

    //
    // GET /working_groups/application/{wgAppId}
    //
    @Test
    void getWorkingGroupApplicationByID_failure_requireAuth() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(WORKING_GROUP_APP_BY_ID_URL, new String[] { WG_APPLICATION_ID }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getWorkingGroupApplicationByID_success() {
        EndpointTestBuilder.from(WG_APPLICATION_BY_ID).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getWorkingGroupApplicationByID_success_validateSchema() {
        EndpointTestBuilder.from(WG_APPLICATION_BY_ID).andCheckSchema().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getWorkingGroupApplicationByID_success_validateResponseFormat() {
        EndpointTestBuilder.from(WG_APPLICATION_BY_ID).andCheckFormat().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getWorkingGroupApplicationByID_failure_noApplicationForID() {
        EndpointTestBuilder.from(TestCaseHelper.buildNotFoundCase(WORKING_GROUP_APP_BY_ID_URL, new String[] { "999" }, null)).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getWorkingGroupApplicationByID_failure_doesNotReturnFormsFromOtherOrgs() {
        EndpointTestBuilder.from(TestCaseHelper.buildNotFoundCase(WORKING_GROUP_APP_BY_ID_URL, new String[] { "3" }, null)).run();
        // this is the source data of the above data set, and should reflect the set of all available data
        WorkingGroupApplication results = dao.getReference(3, WorkingGroupApplication.class);
        // there should be an entry from the test SQL that uses 3 as the ID with an org ID that isn't 15
        Assertions.assertNotNull(results);
        Assertions.assertNotEquals(15, results.getOrganizationId());
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getWorkingGroupApplicationByID_failure_invalidApplicationID() {
        EndpointTestBuilder.from(TestCaseHelper.buildBadRequestCase(WORKING_GROUP_APP_BY_ID_URL, new String[] { "test" }, null)).run();
    }

    //
    // PUT /working_groups/application/{Long.toString(wgAppId)}
    //
    @Test
    void putWorkingGroupApplicationByID_failure_requireAuth() {
        // create record to update
        WorkingGroupApplication wg = persistSampleApplication(Optional.empty(), Optional.empty());

        Long wgAppId = wg.getId();

        // auth is triggered after CSRF for non GET requests
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(WORKING_GROUP_APP_BY_ID_URL, new String[] { Long.toString(wgAppId) }, null)
                        .setStatusCode(Response.Status.UNAUTHORIZED.getStatusCode())
                        .build())
                .doPut(generateSampleRequest(wgAppId, null))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putWorkingGroupApplicationByID_failure_csrfGuard() {
        // create record to update
        Long wgAppId = persistSampleApplication(Optional.empty(), Optional.empty()).getId();

        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(WORKING_GROUP_APP_BY_ID_URL, new String[] { Long.toString(wgAppId) }, null)
                        .setStatusCode(Response.Status.FORBIDDEN.getStatusCode())
                        .setDisableCsrf(true)
                        .build())
                .doPut(generateSampleRequest(wgAppId, null))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putWorkingGroupApplicationByID_failure_emptyBody() {
        // create record to update
        Long wgAppId = persistSampleApplication(Optional.empty(), Optional.empty()).getId();

        EndpointTestBuilder
                .from(TestCaseHelper.buildBadRequestCase(WORKING_GROUP_APP_BY_ID_URL, new String[] { Long.toString(wgAppId) }, null))
                .doPut("")
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putWorkingGroupApplicationByID_success() {
        // create record to update
        WorkingGroupApplication wg = persistSampleApplication(Optional.empty(), Optional.empty());

        Long wgAppId = wg.getId();

        EndpointTestBuilder
                .from(TestCaseHelper.buildSuccessCase(WORKING_GROUP_APP_BY_ID_URL, new String[] { Long.toString(wgAppId) }, null))
                .doPut(generateSampleRequest(wgAppId, null))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putWorkingGroupApplicationByID_success_validateRequestSchema() {
        // set up test data
        WorkingGroupApplication wg = persistSampleApplication(Optional.empty(), Optional.empty());
        Long wgAppId = wg.getId();

        // create request sample data
        String request = generateSampleRequest(wgAppId, null);
        Assertions.assertTrue(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.WORKING_GROUP_APPLICATION_SCHEMA_PATH).matches(request));
        // assert that the post with the validated data works
        EndpointTestBuilder
                .from(TestCaseHelper.buildSuccessCase(WORKING_GROUP_APP_BY_ID_URL, new String[] { Long.toString(wgAppId) }, null))
                .doPut(request)
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putWorkingGroupApplicationByID_success_validateSchema() {
        // create record to update
        WorkingGroupApplication wg = persistSampleApplication(Optional.empty(), Optional.empty());
        Long wgAppId = wg.getId();
        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildSuccessCase(WORKING_GROUP_APP_BY_ID_URL, new String[] { Long.toString(wgAppId) },
                                SchemaNamespaceHelper.WORKING_GROUP_APPLICATION_SCHEMA_PATH))
                .doPut(generateSampleRequest(wgAppId, null))
                .andCheckSchema()
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putWorkingGroupApplicationByID_success_validateResponseFormat() {
        // create record to update
        WorkingGroupApplication wg = persistSampleApplication(Optional.empty(), Optional.empty());

        Long wgAppId = wg.getId();

        EndpointTestBuilder
                .from(TestCaseHelper
                        .buildSuccessCase(WORKING_GROUP_APP_BY_ID_URL, new String[] { Long.toString(wgAppId) },
                                SchemaNamespaceHelper.WORKING_GROUPS_SCHEMA_PATH))
                .doPut(generateSampleRequest(wgAppId, null))
                .andCheckFormat()
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putWorkingGroupApplicationByID_success_sameOrgContactCanMakeUpdates() {
        // create record to update
        WorkingGroupApplication wg = persistSampleApplication(Optional.of("doofenshmirtz"), Optional.empty());

        Long wgAppId = wg.getId();

        EndpointTestBuilder
                .from(TestCaseHelper.buildSuccessCase(WORKING_GROUP_APP_BY_ID_URL, new String[] { Long.toString(wgAppId) }, null))
                .doPut(generateSampleRequest(wgAppId, null))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putWorkingGroupApplicationByID_failure_invalidApplicationId() {
        Long wgAppId = persistSampleApplication(Optional.empty(), Optional.empty()).getId();

        EndpointTestBuilder
                .from(TestCaseHelper.buildBadRequestCase(WORKING_GROUP_APP_BY_ID_URL, new String[] { "test" }, null))
                .doPut(generateSampleRequest(wgAppId, null))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putWorkingGroupApplicationByID_failure_formForDifferentOrg() {
        Long wgAppId = persistSampleApplication(Optional.of("newbie"), Optional.of(5)).getId();

        EndpointTestBuilder
                .from(TestCaseHelper.buildBadRequestCase(WORKING_GROUP_APP_BY_ID_URL, new String[] { "test" }, null))
                .doPut(generateSampleRequest(wgAppId, null))
                .run();
    }


    //
    // POST /working_groups/application/{Long.toString(wgAppId)}/complete
    //
    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void complete_success() {
        // assert that the form is inprogress at test start
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID_PARAMETER_NAME, "4");
        List<WorkingGroupApplication> application = dao
                .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(WorkingGroupApplication.class), params));
        Assertions.assertNotNull(application);
        Assertions.assertEquals(1, application.size());
        Assertions.assertEquals(FormState.INPROGRESS, application.get(0).getState());

        EndpointTestBuilder.from(WG_APPLICATION_COMPLETE).doPost().run();

        // check that the form was transitioned to submitted
        application = dao
                .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(WorkingGroupApplication.class), params));
        Assertions.assertNotNull(application);
        Assertions.assertEquals(1, application.size());
        Assertions.assertEquals(FormState.SUBMITTED, application.get(0).getState());
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void complete_failure_alreadySubmitted() {
        EndpointTestBuilder.from(WG_APPLICATION_COMPLETE_ALREADY_COMPLETED).doPost().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void complete_failure_noContacts() {
        EndpointTestBuilder.from(WG_APPLICATION_COMPLETE_NO_CONTACTS).doPost().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void complete_failure_alreadyMember() {
        EndpointTestBuilder.from(WG_APPLICATION_COMPLETE_ALREADY_MEMBER).doPost().run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void complete_failure_invalidWG() {
        EndpointTestBuilder.from(WG_APPLICATION_COMPLETE_INVALID_WG).doPost().run();
    }
    


    //
    // GET /working_groups/application/{wgAppId}/{transactionID}
    //
    @Test
    void validateForm_success_confirm() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildSuccessCase(VALIDATE_URL, new String[] { "10", "51", "token-string", "CONFIRMED" }, null))
                .run();
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), "test@test.co");
        List<AccountRequests> reqs = eclipseDB
                .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://test.eclipse.org/test")),
                        filters.get(AccountRequests.class), params));
        Assertions.assertNotNull(reqs);
        Assertions.assertFalse(reqs.isEmpty());

    }

    @Test
    void validateform_failure_missingToken() {
        EndpointTestBuilder.from(VALIDATE_NO_TOKEN_CASE).run();
    }

    @Test
    void validateform_failure_missingToken_validSchema() {
        EndpointTestBuilder.from(VALIDATE_NO_TOKEN_CASE).andCheckSchema().run();
    }

    @Test
    void validateform_failure_missingState() {
        EndpointTestBuilder.from(VALIDATE_NO_STATE_CASE).run();
    }

    @Test
    void validateform_failure_missingState_validSchema() {
        EndpointTestBuilder.from(VALIDATE_NO_STATE_CASE).andCheckSchema().run();
    }

    @Test
    void validateForm_failure_tokenNotFound() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildNotFoundCase(VALIDATE_URL, new String[] { "10", "50", "token", "CONFIRMED" }, null))
                .run();
    }

    @Test
    void validateForm_failure_transactionIdNotFound() {
        EndpointTestBuilder
                .from(TestCaseHelper.buildNotFoundCase(VALIDATE_URL, new String[] { "10", "43", "token-string", "CONFIRMED" }, null))
                .run();
    }

    private WorkingGroupApplication persistSampleApplication(Optional<String> author, Optional<Integer> orgId) {

        WorkingGroupApplication data = new WorkingGroupApplication();
        data.setOrganizationId(orgId.orElse(15));
        data.setState(FormState.INPROGRESS);
        data.setUser(author.orElse("opearson"));
        data.setCreated(DateTimeHelper.now());
        data.setUpdated(DateTimeHelper.now());
        List<WorkingGroupApplication> results = dao
                .add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(WorkingGroupApplication.class), null), Arrays.asList(data));
        if (results == null || results.isEmpty()) {
            throw new RuntimeException("Pushing to DB failed, test cannot continue");
        }
        WorkingGroupApplicationContact c = new WorkingGroupApplicationContact();
        c.setEmail("dummy+3@eclipse-foundation.org");
        c.setfName("sample");
        c.setlName("sample");
        c.setForm(results.get(0));
        c.setWorkingGroupAlias("jakarta-ee");
        c.setWorkingGroupLevel("SD");
        dao
                .add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(WorkingGroupApplicationContact.class), null), Arrays.asList(c));

        return results.get(0);
    }

    private String generateSampleRequest(Long id, String state) {
        return entityToString(WorkingGroupApplicationData
                .builder()
                .setId(id)
                .setOrgId(15)
                .setState(StringUtils.isNotBlank(state) ? state : FormState.INPROGRESS.name())
                .setUser("opearson")
                .setUpdated(DateTimeHelper.now())
                .setCreated(DateTimeHelper.now())
                .setContacts(Arrays
                        .asList(WorkingGroupContact
                                .builder()
                                .setFirstName("Sample")
                                .setLastName("Sample")
                                .setMail("dummy+1@eclipse-foundation.org")
                                .setWorkingGroupAlias("jakarta-ee")
                                .setWorkingGroupLevel("WGSD")
                                .setIsSigningAuthority(false)
                                .build()))
                .build());
    }

    private String entityToString(Object o) {
        try {
            return om.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private List<WorkingGroupApplicationData> jsonToEntity(String in) {
        try {
            return om.readerForListOf(WorkingGroupApplicationData.class).readValue(in);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
