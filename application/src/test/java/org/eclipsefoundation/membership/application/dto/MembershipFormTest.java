/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.dto;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.application.dto.membership.MembershipForm;
import org.eclipsefoundation.membership.application.namespace.FormState;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.membership.application.test.helper.DtoHelper;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class MembershipFormTest {

    @Inject
    DefaultHibernateDao dao;
    @Inject
    FilterService filters;

    private static final RequestWrapper WRAP = new FlatRequestWrapper(URI.create("https://membership.eclipse.org"));

    @Test
    void getById() {
        List<MembershipForm> addResults = persistForm();
        Assertions.assertFalse(addResults.isEmpty());
        MembershipForm expected = addResults.get(0);

        // Fetch the same record for comparison
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), expected.getId());

        List<MembershipForm> getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(MembershipForm.class), params));

        Assertions.assertEquals(1, getResults.size(), "There should be one MembershipForm record found");
        Assertions.assertEquals(expected, getResults.get(0), "The add and fetch result should be the same");

        // Test again with an invalid id
        params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), "nope");

        getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(MembershipForm.class), params));
        Assertions.assertTrue(getResults.isEmpty(), "There should be 0 MembershipForm records found");
    }

    @Test
    void getByUserId() {
        List<MembershipForm> addResults = persistForm();
        Assertions.assertFalse(addResults.isEmpty());
        MembershipForm expected = addResults.get(0);

        // Fetch the same record for comparison
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.USER_ID.getName(), expected.getUserID());

        List<MembershipForm> getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(MembershipForm.class), params));

        Assertions.assertEquals(1, getResults.size(), "There should be one MembershipForm record found");
        Assertions.assertEquals(expected, getResults.get(0), "The add and fetch result should be the same");

        // Test again with an invalid user id
        params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.USER_ID.getName(), "nope");

        getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(MembershipForm.class), params));
        Assertions.assertTrue(getResults.isEmpty(), "There should be 0 MembershipForm records found");
    }

    @Test
    void getByState() {
        // Fetch the same record for comparison
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.FORM_STATE.getName(), FormState.INPROGRESS.toString());

        List<MembershipForm> getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(MembershipForm.class), params));
        Assertions.assertTrue(getResults.size() > 1, "There should be multiple results matching this form state");
        Assertions.assertFalse(getResults.isEmpty(), "There should be multiple results matching this form state");

        // Test again with an invalid state
        params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.FORM_STATE.getName(), FormState.CONFIRMED.toString());

        getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(MembershipForm.class), params));
        Assertions.assertTrue(getResults.isEmpty(), "There should be 0 MembershipForm records found");
    }

    @Test
    void getByApplicationGroup() {
        // Fetch the same record for comparison
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.APPLICATION_GROUP.getName(), "eclipse");

        List<MembershipForm> getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(MembershipForm.class), params));
        Assertions.assertTrue(getResults.size() > 1, "There should be multiple results matching this application group");
        Assertions.assertFalse(getResults.isEmpty(), "There should be multiple results matching this application group");

        // Test again with an invalid application group
        params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.APPLICATION_GROUP.getName(), "nope");

        getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(MembershipForm.class), params));
        Assertions.assertTrue(getResults.isEmpty(), "There should be 0 MembershipForm records found");
    }

    @Test
    void getByCreateBefore() {
        // Fetch the same record for comparison
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.BEFORE_DATE_UPDATED_IN_MILLIS.getName(), Long.toString(1634668281297L + 100000));

        List<MembershipForm> getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(MembershipForm.class), params));

        Assertions.assertEquals(3, getResults.size(), "There should be one MembershipForm record created before this date");

        // Test again with an invalid user id
        params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.BEFORE_DATE_UPDATED_IN_MILLIS.getName(), "10000000");

        getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(MembershipForm.class), params));
        Assertions.assertTrue(getResults.isEmpty(), "There should be 0 MembershipForm records found");
    }

    @Test
    void getByCreateAfter() {
        // Fetch the same record for comparison
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.AFTER_DATE_UPDATED_IN_MILLIS.getName(), Long.toString(1634668281297L - 1000000));

        List<MembershipForm> getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(MembershipForm.class), params));

        Assertions.assertEquals(3, getResults.size(), "There should be one MembershipForm record created after this date");

        // Test again with an invalid user id
        params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.AFTER_DATE_UPDATED_IN_MILLIS.getName(), Long.toString(1634668281297L + 100000));

        getResults = dao.get(new RDBMSQuery<>(WRAP, filters.get(MembershipForm.class), params));
        Assertions.assertTrue(getResults.isEmpty(), "There should be 0 MembershipForm records found");
    }

    private List<MembershipForm> persistForm() {
        return dao.add(new RDBMSQuery<>(WRAP, filters.get(MembershipForm.class)), Arrays.asList(DtoHelper.generateForm(Optional.empty())));
    }
}
