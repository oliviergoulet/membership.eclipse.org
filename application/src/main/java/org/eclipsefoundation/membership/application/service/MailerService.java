/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.service;

import java.util.List;

import org.eclipsefoundation.membership.application.api.models.MemberOrganization;
import org.eclipsefoundation.membership.application.dto.membership.SecureTransaction;
import org.eclipsefoundation.membership.application.dto.membership.WorkingGroupApplication;
import org.eclipsefoundation.membership.application.dto.membership.WorkingGroupApplicationContact;
import org.eclipsefoundation.membership.application.model.AccountCreationMailerData;
import org.eclipsefoundation.membership.application.model.FormMailerData;

/**
 * Interface defining emails that need to be generated and sent as part of the submission of the membership forms to the membership team.
 * This interface includes emails providing feedback to the user and to the membership team.
 * 
 * @author Martin Lowe
 *
 */
public interface MailerService {

    /**
     * Sends an EMail message to the author of the form thanking them for their submission and interest. This request is asynchronous and
     * has no return.
     * 
     * @param data the collected form data for the mailer
     */
    void sendToFormAuthor(FormMailerData data);

    /**
     * Sends an email message to the membership team regarding the submitted form. This should list all of the information about the form
     * and the submission for the membership team.
     * 
     * @param data the collected form data for the mailer
     */
    void sendToMembershipTeam(FormMailerData data);

    /**
     * Send notification to the membership team about a new working group application made by an existing member. Will include a link to
     * validate or reject the application.
     * 
     * @param application the application to reference in the message
     * @param contacts the contacts associated with the application
     * @param organization the member organization that this request was done on behalf of
     * @param transaction the transaction object to include in the mail message linked to the application
     */
    void sendWorkingGroupApplicationNotification(WorkingGroupApplication application, List<WorkingGroupApplicationContact> contacts,
            MemberOrganization organization, SecureTransaction transaction);

    /**
     * Sends an email to a member rep that doesn't have an eclipse account. It tells them they've been added as a contact to a membership
     * application.
     * 
     * @param recipient the recipient email
     * @param data the contact data for the mailer
     */
    void sendAccountCreationToReps(String recipient, AccountCreationMailerData data);

}
