/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import jakarta.enterprise.context.Dependent;
import jakarta.inject.Inject;

import org.eclipsefoundation.core.exception.ApplicationException;
import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.core.model.ParameterizedCacheKey;
import org.eclipsefoundation.core.service.LoadingCacheManager;
import org.eclipsefoundation.efservices.api.models.WorkingGroup;
import org.eclipsefoundation.membership.application.api.models.MemberOrganization;
import org.eclipsefoundation.membership.application.config.ApplicationBaseConfig;
import org.eclipsefoundation.membership.application.dto.membership.SecureTransaction;
import org.eclipsefoundation.membership.application.dto.membership.WorkingGroupApplication;
import org.eclipsefoundation.membership.application.dto.membership.WorkingGroupApplicationContact;
import org.eclipsefoundation.membership.application.model.AccountCreationMailerData;
import org.eclipsefoundation.membership.application.model.FormMailerData;
import org.eclipsefoundation.membership.application.model.MailerDataContext;
import org.eclipsefoundation.membership.application.service.MailerService;
import org.eclipsefoundation.membership.application.service.WorkflowService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openhtmltopdf.outputdevice.helper.BaseRendererBuilder.PageSizeUnits;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;

import io.quarkus.mailer.Mail;
import io.quarkus.mailer.Mailer;
import io.quarkus.qute.Location;
import io.quarkus.qute.Template;
import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;
import io.smallrye.jwt.auth.principal.DefaultJWTCallerPrincipal;

/**
 * Default implementation of the mailer service using the Qute templating engine and the mailer extensions built into Quarkus.
 * 
 * @author Martin Lowe
 *
 */
@Dependent
public class DefaultMailerService implements MailerService {
    /**
     * 
     */
    private static final int A4_PAGE_HEIGHT_MM = 297;

    /**
     * 
     */
    private static final int A4_PAGE_WIDTH_MM = 210;

    public static final Logger LOGGER = LoggerFactory.getLogger(DefaultMailerService.class);

    private static final String EMAIL_DATA_VAR = "data";
    private static final String CONTEXT_VAR = "context";

    @Inject
    ApplicationBaseConfig baseConfig;
    @Inject
    EclipseMailerConfig mailerConfig;
    
    @Inject
    LoadingCacheManager lcm;
    @Inject
    WorkflowService wf;

    @Inject
    SecurityIdentity ident;
    @Inject
    Mailer mailer;

    // Qute templates, generates email bodies
    @Location("emails/membership/form_membership_email_web_template")
    Template membershipTemplateWeb;
    @Location("emails/membership/form_membership_email_template")
    Template membershipTemplate;
    @Location("emails/membership/eclipse_account_creation_email_web_template")
    Template accountCreationTemplateWeb;
    @Location("emails/membership/eclipse_account_creation_email_template")
    Template accountCreationTemplate;
    @Location("emails/membership/wg_application_web_template")
    Template wgApplicationWebTemplate;
    @Location("emails/membership/wg_application_template")
    Template wgApplicationTemplate;

    @Override
    public void sendToFormAuthor(FormMailerData data) {
        if (data == null || data.form == null) {
            throw new IllegalStateException("A form is required to submit for mailing");
        } else if (data.org == null || data.wgs == null || data.contacts == null || data.contacts.isEmpty()) {
            throw new IllegalStateException("Could not find a fully complete form for form with ID '" + data.form.getId() + "'");
        } else if (ident.isAnonymous()) {
            // in the future we should fall back to accounts API
            throw new IllegalStateException("A user must be logged in to send the author form message");
        }
        // convert the logged in user into a JWT token to read user claims
        DefaultJWTCallerPrincipal defaultPrin = (DefaultJWTCallerPrincipal) ident.getPrincipal();
        Mail m = getMembershipFormMail(defaultPrin.getClaim("email"), "Thank you for completing the member enrollment form", data, true);

        // add reply to and bcc if set
        mailerConfig.membership().authorMessage().bcc().ifPresent(m::setBcc);
        mailerConfig.membership().authorMessage().replyTo().ifPresent(m::setReplyTo);
        mailer.send(m);
    }

    @Override
    public void sendToMembershipTeam(FormMailerData data) {
        if (data == null || data.form == null) {
            throw new IllegalStateException("A form is required to submit for mailing");
        } else if (data.org == null || data.wgs == null || data.contacts == null || data.contacts.isEmpty()) {
            throw new IllegalStateException("Could not find a fully complete form for form with ID '" + data.form.getId() + "'");
        }

        Mail m = getMembershipFormMail(mailerConfig.membership().inbox(), "New Request to join working group(s) - " + data.name, data,
                false);

        // add BCC + reply to fields if set
        mailerConfig.membership().membershipMessage().bcc().ifPresent(m::setBcc);
        mailerConfig.membership().membershipMessage().replyTo().ifPresent(m::setReplyTo);
        // add the PDF attachment
        m
                .addAttachment("membership-enrollment-" + data.name.toLowerCase().replace(" ", "-") + ".pdf",
                        renderHTMLPDF(membershipTemplateWeb
                                .data(EMAIL_DATA_VAR, data, CONTEXT_VAR,
                                        new MailerDataContext(baseConfig.baseUrl(), baseConfig.applicationGroup(), DateTimeHelper.now(),
                                                true, false))
                                .render()),
                        "application/pdf");
        mailer.send(m);
    }

    @Override
    public void sendAccountCreationToReps(String recipient, AccountCreationMailerData data) {
        Mail mail = Mail
                .withHtml(recipient, "You've been listed as rep in an application to join Eclipse Foundation",
                        accountCreationTemplateWeb
                                .data(EMAIL_DATA_VAR, data, CONTEXT_VAR,
                                        new MailerDataContext(baseConfig.baseUrl(), baseConfig.applicationGroup(), DateTimeHelper.now(),
                                                false, false))
                                .render());

        mail
                .setText(accountCreationTemplate
                        .data(EMAIL_DATA_VAR, data, CONTEXT_VAR,
                                new MailerDataContext(baseConfig.baseUrl(), baseConfig.applicationGroup(), DateTimeHelper.now(), false,
                                        false))
                        .render());

        // add reply to and bcc if set
        mailerConfig.membership().authorMessage().bcc().ifPresent(mail::setBcc);
        mailerConfig.membership().authorMessage().replyTo().ifPresent(mail::setReplyTo);
        mailer.send(mail);
    }

    @Override
    public void sendWorkingGroupApplicationNotification(WorkingGroupApplication application, List<WorkingGroupApplicationContact> contacts,
            MemberOrganization organization, SecureTransaction transaction) {
        LOGGER.info("Sending mail notification for submission of WG application {}", application.getId());
        // get the reusable working groups to include with the email render
        List<WorkingGroup> wgs = lcm.getList(ParameterizedCacheKey.builder().setId("all").setClazz(WorkingGroup.class).build());
        // generate the mail body
        Mail mail = Mail
                .withHtml(mailerConfig.membership().inbox(), organization.getName() + " - Request to join Working Groups",
                        wgApplicationWebTemplate
                                .data("application", application)
                                .data("contacts", contacts)
                                .data("prospectiveUsers", wf.getProspectiveUsers(contacts))
                                .data("organization", organization)
                                .data("transaction", transaction)
                                .data("baseUrl", baseConfig.baseUrl())
                                .data("working_groups", wgs)
                                .render());
        mail
                .setText(wgApplicationTemplate
                        .data("application", application)
                        .data("contacts", contacts)
                        .data("prospectiveUsers", wf.getProspectiveUsers(contacts))
                        .data("organization", organization)
                        .data("transaction", transaction)
                        .data("baseUrl", baseConfig.baseUrl())
                        .data("working_groups", wgs)
                        .render());

        // set replyto and send the message
        mailerConfig.membership().authorMessage().replyTo().ifPresent(mail::setReplyTo);
        mailer.send(mail);
    }

    /**
     * Centralize the creation of the mail object to reduce repetition. A preamble may be included at the top of the message based on the
     * includePreamble argument.
     * 
     * @param recipient the recipient of the mail message
     * @param subject the subject line for the message
     * @param data the collected form data that is the base of the email.
     * @param includePreamble whether or not to include the preamble in the email.
     * @return the mail message with text and HTML versions set.
     */
    private Mail getMembershipFormMail(String recipient, String subject, FormMailerData data, boolean includePreamble) {
        // generate the mail message, sending the messsage to the membershipMailbox
        Mail m = Mail
                .withHtml(recipient, subject,
                        membershipTemplateWeb
                                .data(EMAIL_DATA_VAR, data, CONTEXT_VAR,
                                        new MailerDataContext(baseConfig.baseUrl(), baseConfig.applicationGroup(), DateTimeHelper.now(),
                                                false, includePreamble))
                                .render());
        m
                .setText(membershipTemplate
                        .data(EMAIL_DATA_VAR, data, CONTEXT_VAR,
                                new MailerDataContext(baseConfig.baseUrl(), baseConfig.applicationGroup(), DateTimeHelper.now(), false,
                                        includePreamble))
                        .render());
        return m;
    }

    /**
     * Render the PDF document using HTML generated by the Qute template engine.
     * 
     * @param html the HTML for the PDF document.
     * @return the file that represents the document. This should be in a temporary directory so that it gets cleaned on occasion.
     */
    private File renderHTMLPDF(String html) {
        // create a unique file name for temporary storage
        File f = new File(mailerConfig.membership().docStorageRoot() + '/' + UUID.randomUUID().toString() + ".pdf");
        try (OutputStream os = new FileOutputStream(f)) {
            PdfRendererBuilder builder = new PdfRendererBuilder();
            builder.withHtmlContent(html, mailerConfig.membership().docStorageRoot());
            // using a4 measurements
            builder.useDefaultPageSize(A4_PAGE_WIDTH_MM, A4_PAGE_HEIGHT_MM, PageSizeUnits.MM);
            builder.toStream(os);
            builder.run();
        } catch (IOException e) {
            throw new ApplicationException("Could not build PDF document", e);
        }
        return f;
    }

    /**
     * Represents configuration for the default mailer service.
     * 
     * @author Martin Lowe
     *
     */
    @ConfigMapping(prefix = "eclipse.mailer")
    public interface EclipseMailerConfig {
        public Membership membership();

        /**
         * Represents configurations for the membership form messages.
         */
        public interface Membership {
            public String inbox();

            public MessageConfiguration authorMessage();

            public MessageConfiguration membershipMessage();

            @WithDefault("/tmp")
            public String docStorageRoot();
        }

        public interface MessageConfiguration {
            public Optional<String> replyTo();

            public Optional<List<String>> bcc();
        }
    }
}
