/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.tasks;

import java.net.URI;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.services.AccountService;
import org.eclipsefoundation.membership.application.config.ApplicationScheduledTaskConfig;
import org.eclipsefoundation.membership.application.dto.membership.Contact;
import org.eclipsefoundation.membership.application.dto.membership.FormOrganization;
import org.eclipsefoundation.membership.application.dto.membership.FormWorkingGroup;
import org.eclipsefoundation.membership.application.dto.membership.MembershipForm;
import org.eclipsefoundation.membership.application.dto.membership.SecureTransaction;
import org.eclipsefoundation.membership.application.model.FormMailerData;
import org.eclipsefoundation.membership.application.namespace.FormState;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.membership.application.namespace.TransactionType;
import org.eclipsefoundation.membership.application.service.WorkflowService;
import org.eclipsefoundation.membership.application.service.MailerService;
import org.eclipsefoundation.membership.application.service.SecureTransactionService;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.arc.Arc;
import io.quarkus.arc.InstanceHandle;
import io.quarkus.scheduler.Scheduled;

@ApplicationScoped
public class ScheduledEmailReminderTask {
    public static final Logger LOGGER = LoggerFactory.getLogger(ScheduledEmailReminderTask.class);

    @ConfigProperty(name = "eclipse.membership.base-url")
    String baseUrl;

    @Inject
    Instance<ApplicationScheduledTaskConfig> config;

    @Inject
    CachingService cache;
    @Inject
    WorkflowService wflow;

    @Inject
    AccountService accounts;

    // Scheduled for every 7 days, delayed by 5 minutes
    @Scheduled(every = "P7D", delay = 5, identity = "email-reminder-submitted-apps")
    void sendReminder() {
        if (Boolean.FALSE.equals(config.get().emailReminder().enabled())) {
            LOGGER.warn("Email reminder task did not run. It has been disabled through configuration.");
        } else {
            InstanceHandle<DefaultHibernateDao> daoHandle = Arc.container().instance(DefaultHibernateDao.class);
            DefaultHibernateDao dao = daoHandle.get();

            InstanceHandle<FilterService> filtersHandle = Arc.container().instance(FilterService.class);
            FilterService filters = filtersHandle.get();

            Optional<List<MembershipForm>> forms = getFormsSinceLastRun(dao, filters);

            forms.ifPresent(list -> processMembershipForms(list, dao, filters));
        }
    }

    /**
     * Retrieves the list of forms submitted since the last run of the scheduled task.
     * 
     * @param dao The dao service
     * @param filters The filters service
     * @return An Optional containing a list of submitted forms
     */
    private Optional<List<MembershipForm>> getFormsSinceLastRun(DefaultHibernateDao dao, FilterService filters) {

        ZonedDateTime timeSinceLastRun = DateTimeHelper.now().minus(config.get().emailReminder().interval());
        LOGGER.info("Checking database for entries since {}", timeSinceLastRun);

        // Check for forms listed as SUBMITTED since last run
        MultivaluedMap<String, String> formParams = new MultivaluedMapImpl<>();
        formParams.add(MembershipFormAPIParameterNames.FORM_STATE.getName(), FormState.SUBMITTED.name());
        formParams
                .add(MembershipFormAPIParameterNames.AFTER_DATE_UPDATED_IN_MILLIS.getName(),
                        Long.toString(DateTimeHelper.getMillis(timeSinceLastRun)));

        return Optional.of(dao.get(generateQuery(formParams, MembershipForm.class, filters)));
    }

    /**
     * Iterates through the list of given forms, gathers all relevant data, and sends a reminder email to the membership team containg the
     * valid form data and a SecureTransaction entity.
     * 
     * @param forms The list of submitted membership forms
     * @param daoThe dao service
     * @param filters The filters service
     */
    private void processMembershipForms(List<MembershipForm> forms, DefaultHibernateDao dao, FilterService filters) {

        InstanceHandle<MailerService> mailerHandle = Arc.container().instance(MailerService.class);
        MailerService mailer = mailerHandle.get();

        InstanceHandle<WorkflowService> contactHandle = Arc.container().instance(WorkflowService.class);
        WorkflowService contactService = contactHandle.get();

        LOGGER.info("Unvalidated forms found: {}", forms.size());

        forms.forEach(form -> {

            // Fetch existing or generate new
            SecureTransaction transaction = generateValidTransaction(form.getId());

            // retrieve all of the info needed to post the form email
            MultivaluedMap<String, String> extraParams = new MultivaluedMapImpl<>();
            extraParams.add(MembershipFormAPIParameterNames.FORM_ID.getName(), form.getId());

            List<FormOrganization> orgs = dao.get(generateQuery(extraParams, FormOrganization.class, filters));
            List<FormWorkingGroup> wgs = dao.get(generateQuery(extraParams, FormWorkingGroup.class, filters));
            List<Contact> contacts = dao.get(generateQuery(extraParams, Contact.class, filters));

            Optional<EfUser> applicant = accounts.fetchUserByUsername(form.getUserID(), false);

            if (!applicant.isPresent()) {
                LOGGER.warn("Form with id {} in bad state. No applicant found.", form.getId());
            } else {
                FormOrganization org = !orgs.isEmpty() ? orgs.get(0) : null;
                FormMailerData data = new FormMailerData(applicant.get().getFullName(), form, org, wgs, contacts,
                        wflow.doesOrganizationExist(org));

                dao
                        .add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create(baseUrl)), filters.get(SecureTransaction.class)),
                                Arrays.asList(transaction));

                // Send form with transaction
                mailer.sendToMembershipTeam(new FormMailerData(data, transaction, contactService.getProspectiveUsers(contacts)));
            }
        });
    }

    /**
     * Checks whether there exists a valid transaction ofr the current form using the form id. Returns the existing transaction entity.
     * Creates a new entity if none exist.
     * 
     * @param formId The given form id
     * @return An existing or new SecureTransaction entity
     */
    private SecureTransaction generateValidTransaction(String formId) {

        InstanceHandle<SecureTransactionService> transctionServicehandle = Arc.container().instance(SecureTransactionService.class);
        SecureTransactionService transactionService = transctionServicehandle.get();

        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.ASSOCIATED_ENTITY_ID.getName(), formId);
        params.add(MembershipFormAPIParameterNames.ASSOCIATED_ENTITY_TYPE.getName(), TransactionType.FORM.name());
        params.add(MembershipFormAPIParameterNames.ACTIVE.getName(), Boolean.toString(true));

        SecureTransaction formTransaction = transactionService
                .getActiveTrackedTransaction(params, new FlatRequestWrapper(URI.create(baseUrl)));

        if (formTransaction == null) {
            formTransaction = transactionService.generateUntrackedTransaction(formId, TransactionType.FORM);
        }

        return formTransaction;
    }

    private <T extends BareNode> RDBMSQuery<T> generateQuery(MultivaluedMap<String, String> params, Class<T> type, FilterService filters) {
        RDBMSQuery<T> out = new RDBMSQuery<>(new FlatRequestWrapper(URI.create(baseUrl)), filters.get(type), params);
        out.setRoot(false);
        return out;
    }
}
