/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.helper;

import java.io.FileReader;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Optional;

import javax.crypto.Cipher;

import org.bouncycastle.util.io.pem.PemReader;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.service.CachingService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

/**
 * Helper class used to perform encryption operations.
 */
@ApplicationScoped
public class EncryptionHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(EncryptionHelper.class);

    @ConfigProperty(name = "eclipse.security.encryption.public-key-filepath", defaultValue = "config/application/public_key.pem")
    String publicKeyPath;

    @Inject
    CachingService cache;

    /**
     * Encrypts an string using the public key in the application configs and returns the result in a Base64 encoded string. Returns the
     * original string if there was an error.
     * 
     * @param src The given string to encrypt and encode.
     * @return The encrypted and encoded string
     * @throws RuntimeException if there was an error that stops the encryption of the string
     */
    public String encryptAndEncodeValue(String src) {
        try {
            LOGGER.debug("Encrypting and encoding value: {}", src);

            // Strip key headers and parse content. Caching results
            Optional<byte[]> decodedKey = cache.get(publicKeyPath, new MultivaluedMapImpl<>(), byte.class, this::readAndParsePublicKey).getData();
            if (decodedKey.isEmpty() || decodedKey.get().length == 0) {
                LOGGER.error("Error decoding public key while encrypting: {}", src);
                throw new RuntimeException("Could not encrypt value, key could not be loaded");
            }

            // Generate an RSA public key for encryption
            RSAPublicKey publicKey = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(decodedKey.get()));

            // Initialize a cipher using our public key and encrypt the email
            Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-1AndMGF1Padding");
            cipher.init(Cipher.PUBLIC_KEY, publicKey);
            byte[] encyptedEmail = cipher.doFinal(src.getBytes());

            // Base64 encode before returning the results
            return Base64.getEncoder().encodeToString(encyptedEmail);
        } catch (Exception e) {
            throw new RuntimeException("Could not encrypt passed value", e);
        }
    }

    /**
     * Reads the public key file, strips the headers, and decodes the public key value, returning the result in an array of bytes.
     * 
     * @return The decoded public key
     */
    private byte[] readAndParsePublicKey() {
        try (PemReader reader = new PemReader(new FileReader(publicKeyPath))) {
            return reader.readPemObject().getContent();
        } catch (Exception e) {
            LOGGER.error("Error while reading public key", e);
            return new byte[] {};
        }
    }
}
