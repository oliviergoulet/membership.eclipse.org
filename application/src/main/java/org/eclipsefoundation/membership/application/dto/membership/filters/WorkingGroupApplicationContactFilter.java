/*******************************************************************************
 * Copyright (c) 2024 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipsefoundation.membership.application.dto.membership.filters;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.application.dto.membership.WorkingGroupApplicationContact;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.ws.rs.core.MultivaluedMap;

@Singleton
public class WorkingGroupApplicationContactFilter implements DtoFilter<WorkingGroupApplicationContact> {
    @Inject
    ParameterizedSQLStatementBuilder builder;

    @Override
    public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
        ParameterizedSQLStatement stmt = builder.build(WorkingGroupApplicationContact.TABLE);
        if (isRoot) {
            // ID check
            String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
            if (id != null) {
                stmt
                        .addClause(new ParameterizedSQLStatement.Clause(WorkingGroupApplicationContact.TABLE.getAlias() + ".id = ?",
                                new Object[] { id }));
            }
        }
        // form ID check
        String formId = params.getFirst(MembershipFormAPIParameterNames.FORM_ID.getName());
        if (StringUtils.isNumeric(formId)) {
            stmt
                    .addClause(new ParameterizedSQLStatement.Clause(WorkingGroupApplicationContact.TABLE.getAlias() + ".form.id = ?",
                            new Object[] { Long.valueOf(formId) }));
        }
        return stmt;
    }

    @Override
    public Class<WorkingGroupApplicationContact> getType() {
        return WorkingGroupApplicationContact.class;
    }

}
