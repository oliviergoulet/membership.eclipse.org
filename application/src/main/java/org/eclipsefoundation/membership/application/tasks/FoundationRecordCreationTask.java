/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.application.tasks;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.UserSearchParams;
import org.eclipsefoundation.efservices.services.AccountService;
import org.eclipsefoundation.foundationdb.client.model.PeopleData;
import org.eclipsefoundation.membership.application.api.FoundationAPI;
import org.eclipsefoundation.membership.application.config.ApplicationScheduledTaskConfig;
import org.eclipsefoundation.membership.application.dto.membership.BaseContact;
import org.eclipsefoundation.membership.application.dto.membership.Contact;
import org.eclipsefoundation.membership.application.dto.membership.MembershipForm;
import org.eclipsefoundation.membership.application.dto.membership.WorkingGroupApplication;
import org.eclipsefoundation.membership.application.namespace.FormState;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.membership.application.namespace.TransactionType;
import org.eclipsefoundation.membership.application.service.WorkflowService;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.scheduler.Scheduled;
import jakarta.enterprise.context.Dependent;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Regular scheduled task that will check for CONFIRMED forms, and check if their user accounts have been created. If all user accounts are
 * ready, the Person records for the users will be created.
 */
@Dependent
public class FoundationRecordCreationTask {
    public static final Logger LOGGER = LoggerFactory.getLogger(FoundationRecordCreationTask.class);

    @Inject
    Instance<ApplicationScheduledTaskConfig> config;

    @Inject
    FilterService filters;
    @Inject
    DefaultHibernateDao dao;
    @Inject
    AccountService accounts;
    @Inject
    WorkflowService workflowSvc;

    @RestClient
    FoundationAPI fdn;

    /**
     * Optional scheduled task that will scan for confirmed membership and working group applications to check if user accounts exist for
     * all contacts. If all contacts have Eclipse accounts, this task will generate Foundation records for the users.
     * 
     * If not all users are created yet, a failsafe runs to make sure that all of the account request records exist for the stated users and
     * the processing ends for that application to be checked on the next run.
     */
    @Scheduled(every = "PT5M", identity = "foundation-people-record-creation")
    void schedule() {
        if (Boolean.TRUE.equals(config.get().orgCreation().enabled())) {

            // create params for looking up confirmed forms (validated by Membership team)
            MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
            params.add(MembershipFormAPIParameterNames.FORM_STATE.getName(), FormState.CONFIRMED.name());

            RequestWrapper wrap = new FlatRequestWrapper(URI.create("https://membership.eclipse.org"));
            // get the forms that are completed
            List<MembershipForm> forms = dao.get(new RDBMSQuery<>(wrap, filters.get(MembershipForm.class), params));
            if (forms != null && forms.isEmpty()) {
                // for each of the forms, submit them for processing
                forms.forEach(form -> {
                    MultivaluedMap<String, String> extraparams = new MultivaluedMapImpl<>();
                    extraparams.add(MembershipFormAPIParameterNames.FORM_ID.getName(), form.getId());

                    // retrieve all of the form contacts so we can lookup if they are ready
                    boolean readyForFoundationRecords = processMembershipForm(dao
                            .get(new RDBMSQuery<>(wrap, filters.get(Contact.class), extraparams))
                            .stream()
                            .map(c -> (BaseContact) c)
                            .toList(), form.getUserID(), TransactionType.FORM, form.getId());
                    if (readyForFoundationRecords) {
                        // transition the form to the next stage
                        form.setState(FormState.READY_FOR_DOCUMENTS);
                        dao.add(new RDBMSQuery<>(wrap, filters.get(MembershipForm.class)), Arrays.asList(form));
                    }
                });
            }

            // handle the wg applications as well
            List<WorkingGroupApplication> wgApplications = dao
                    .get(new RDBMSQuery<>(wrap, filters.get(WorkingGroupApplication.class), params));
            if (wgApplications != null && wgApplications.isEmpty()) {
                // for each of the forms, submit them for processing
                wgApplications.forEach(application -> {
                    MultivaluedMap<String, String> extraparams = new MultivaluedMapImpl<>();
                    extraparams.add(MembershipFormAPIParameterNames.FORM_ID.getName(), Long.toString(application.getId()));

                    // retrieve all of the form contacts so we can lookup if they are ready
                    boolean readyForFoundationRecords = processMembershipForm(dao
                            .get(new RDBMSQuery<>(wrap, filters.get(Contact.class), extraparams))
                            .stream()
                            .map(c -> (BaseContact) c)
                            .toList(), application.getUser(), TransactionType.WG_APP, Long.toString(application.getId()));
                    if (readyForFoundationRecords) {
                        // transition the form to the next stage
                        application.setState(FormState.READY_FOR_DOCUMENTS);
                        dao.add(new RDBMSQuery<>(wrap, filters.get(WorkingGroupApplication.class)), Arrays.asList(application));
                    }
                });
            }
        }
    }

    /**
     * Process a membership form, retrieving its associated users and attempting to create foundation records for them. If there are members
     * still missing, this process will request for the missing users to be created and return.
     * 
     * @param contacts the list of base contact records for the given form
     * @param username the user that was the originator the request
     * @param type FORM for member applications, and WG_APP for working group applications, used for context in mailing
     * @param formIdentifier id for the form being processed (either a wg or member application)
     * @return true if all users associated with the membership form have an Eclipse Account, false otherwise
     */
    private boolean processMembershipForm(List<BaseContact> contacts, String username, TransactionType type, String formIdentifier) {
        // for each of the contacts, look up user accounts by mail
        List<EfUser> users = contacts
                .parallelStream()
                .map(c -> accounts.performUserSearch(UserSearchParams.builder().setMail(c.getEmail()).build()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();
        if (users.size() == contacts.size()) {
            // if the list matches length, then we can create foundation records
            generateFoundationRecords(users);
            return true;
        } else {
            // if there is a mismatch in contacts, make sure any missing contacts have forms and return
            workflowSvc.createMissingUserAccounts(contacts, username, type, formIdentifier);
        }
        return false;
    }

    /**
     * Creates Foundation DB People records for specified users if they don't already exist.
     * 
     * @param users the contacts to be added to the Foundation DB as people records.
     */
    private void generateFoundationRecords(List<EfUser> users) {
        // create the people records for the given users
        List<PeopleData> createdContacts = users.parallelStream().map(this::createOrRetrievePersonRecord).toList();
        // log the users that were generated
        LOGGER.info("Created records for the following users: {}", createdContacts.stream().map(PeopleData::getPersonID).toList());
    }

    /**
     * Checks Foundation DB if the person record already exists for the given user, and if it doesn't, creates the initial record and
     * returns the new record.
     * 
     * @param u the user to check for personal records for
     * @return the person record once created.
     */
    private PeopleData createOrRetrievePersonRecord(EfUser u) {
        try {
            // check if the record already exists before creating
            Optional<PeopleData> personRecord = fdn.getPerson(u.getName());
            if (personRecord.isPresent()) {
                return personRecord.get();
            }
        } catch (WebApplicationException e) {
            // split stack into debug as it isn't needed here in most cases
            LOGGER.info("Cannot find Person record for {}, attempting to create", u.getName());
            LOGGER.debug("Stack trace for Person record fetch for {}:", u.getName(), e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Creating personal record for user {}", u.getName());
        }
        return fdn
                .updatePeople(PeopleData
                        .builder()
                        .setPersonID(u.getName())
                        .setEmail(u.getMail())
                        .setFname(u.getFirstName())
                        .setLname(u.getLastName())
                        .setFax("")
                        .setType("XX")
                        .setMember(false)
                        .setIssuesPending(false)
                        .setUnixAcctCreated(false)
                        .build())
                .get(0);
    }
}
