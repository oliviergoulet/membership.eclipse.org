/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.namespace;

/**
 * Represents the different types of organizations that may be part of the memebership application process.
 * 
 * @author Martin Lowe
 *
 */
public enum OrganizationTypes {
    NON_PROFIT_OPEN_SOURCE, ACADEMIC, STANDARDS, GOVERNMENT_ORGANIZATION_AGENCY_NGO, MEDIA_ORGANIZATION, RESEARCH,
    OTHER;
}
