/**
 * Copyright (c) 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.application.dto.membership;

import java.util.Objects;

import org.eclipsefoundation.persistence.dto.BareNode;

import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

/**
 * Base form contact to be used with membership, working group, and other future application types.
 */
@MappedSuperclass
public abstract class BaseContact extends BareNode {

    @NotBlank(message = "First name cannot be blank")
    private String fName;
    @NotBlank(message = "Last name cannot be blank")
    private String lName;
    @Email(message = "Email address is not valid")
    private String email;

    /** @return the fName */
    public String getfName() {
        return fName;
    }

    /** @param fName the fName to set */
    public void setfName(String fName) {
        this.fName = fName;
    }

    /** @return the lName */
    public String getlName() {
        return lName;
    }

    /** @param lName the lName to set */
    public void setlName(String lName) {
        this.lName = lName;
    }

    /** @return the email */
    public String getEmail() {
        return email;
    }

    /** @param email the email to set */
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(email, fName, lName);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        BaseContact other = (BaseContact) obj;
        return Objects.equals(email, other.email) && Objects.equals(fName, other.fName) && Objects.equals(lName, other.lName);
    }

}
