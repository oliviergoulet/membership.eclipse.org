/**
 * Copyright (c) 2021, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.application.resources;

import java.util.Arrays;
import java.util.List;

import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.application.dto.membership.Contact;
import org.eclipsefoundation.membership.application.dto.membership.MembershipForm;
import org.eclipsefoundation.membership.application.model.ContactData;
import org.eclipsefoundation.membership.application.model.mappers.ContactMapper;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;

import io.quarkus.security.Authenticated;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

/**
 * Handles contact CRUD requests.
 *
 * @author Martin Lowe
 */
@Authenticated
@Path("form/{id}/contacts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ContactsResource extends AbstractRESTResource {

    @Inject
    ContactMapper cMap;

    @GET
    public Response getAll(@PathParam("id") String formID) {
        // check if user is allowed to modify these resources
        Response r = checkAccess(formID);
        if (r != null) {
            return r;
        }
        // create parameter map
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.FORM_ID.getName(), formID);

        // retrieve the possible object
        List<Contact> results = dao.get(new RDBMSQuery<>(wrap, filters.get(Contact.class), params));
        if (results == null) {
            return Response.serverError().build();
        }
        // return the results as a response
        return Response.ok(results.stream().map(cMap::toModel)).build();
    }

    @POST
    public Response create(@PathParam("id") String formID, ContactData contact) {
        // check if user is allowed to modify these resources
        Response r = checkAccess(formID);
        if (r != null) {
            return r;
        }
        // convert and update the DTO
        Contact ref = cMap.toDTO(contact, dao);
        ref.setForm(dao.getReference(formID, MembershipForm.class));
        List<Contact> contacts = dao.add(new RDBMSQuery<>(wrap, filters.get(Contact.class)), Arrays.asList(ref));
        // convert back to outward facing model
        return Response.ok(cMap.toModel(contacts.get(0))).build();
    }

    @GET
    @Path("{contactID}")
    public Response get(@PathParam("id") String formID, @PathParam("contactID") String contactID) {
        // check if user is allowed to modify these resources
        Response r = checkAccess(formID);
        if (r != null) {
            return r;
        }
        // create parameter map
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.FORM_ID.getName(), formID);
        params.add(DefaultUrlParameterNames.ID.getName(), contactID);

        // retrieve the possible object
        List<Contact> results = dao.get(new RDBMSQuery<>(wrap, filters.get(Contact.class), params));
        if (results == null) {
            return Response.serverError().build();
        } else if (results.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND.getStatusCode()).build();
        }
        // return the results as a response
        return Response.ok(cMap.toModel(results.get(0))).build();
    }

    @PUT
    @Path("{contactID}")
    public Response update(@PathParam("id") String formID, @PathParam("contactID") String id, ContactData contact) {
        // check if user is allowed to modify these resources
        Response r = checkAccess(formID);
        if (r != null) {
            return r;
        }
        // need to fetch ref to use attached entity
        Contact ref = cMap.toDTO(contact, dao).cloneTo(dao.getReference(id, Contact.class));
        ref.setForm(dao.getReference(formID, MembershipForm.class));
        return Response
                .ok(dao.add(new RDBMSQuery<>(wrap, filters.get(Contact.class)), Arrays.asList(ref)).stream().map(cMap::toModel))
                .build();
    }

    @DELETE
    @Path("{contactID}")
    public Response delete(@PathParam("id") String formID, @PathParam("contactID") String id) {
        // check if user is allowed to modify these resources
        Response r = checkAccess(formID);
        if (r != null) {
            return r;
        }
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), id);
        params.add(MembershipFormAPIParameterNames.FORM_ID.getName(), formID);

        dao.delete(new RDBMSQuery<>(wrap, filters.get(Contact.class), params));
        return Response.ok().build();
    }
}
