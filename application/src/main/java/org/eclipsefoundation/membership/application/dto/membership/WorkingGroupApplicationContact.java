/*******************************************************************************
 * Copyright (c) 2024 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipsefoundation.membership.application.dto.membership;

import java.util.Objects;

import org.eclipsefoundation.persistence.model.DtoTable;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Table
@Entity
public class WorkingGroupApplicationContact extends BaseContact {
    public static final DtoTable TABLE = new DtoTable(WorkingGroupApplicationContact.class, "wgac");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String user;
    private String workingGroupAlias;
    private String workingGroupLevel;
    private boolean isSigningAuthority;

    // form entity for FK relation
    @ManyToOne(targetEntity = WorkingGroupApplication.class)
    @JoinColumn(name = "application_id")
    private WorkingGroupApplication form;

    @Override
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the workingGroupAlias
     */
    public String getWorkingGroupAlias() {
        return workingGroupAlias;
    }

    /**
     * @param workingGroupAlias the workingGroupAlias to set
     */
    public void setWorkingGroupAlias(String workingGroupAlias) {
        this.workingGroupAlias = workingGroupAlias;
    }

    /**
     * @return the workingGroupLevel
     */
    public String getWorkingGroupLevel() {
        return workingGroupLevel;
    }

    /**
     * @param workingGroupLevel the workingGroupLevel to set
     */
    public void setWorkingGroupLevel(String workingGroupLevel) {
        this.workingGroupLevel = workingGroupLevel;
    }

    /**
     * @return the isSigningAuthority
     */
    public boolean isSigningAuthority() {
        return isSigningAuthority;
    }

    /**
     * @param isSigningAuthority the isSigningAuthority to set
     */
    public void setSigningAuthority(boolean isSigningAuthority) {
        this.isSigningAuthority = isSigningAuthority;
    }

    /**
     * @return the form
     */
    public WorkingGroupApplication getForm() {
        return form;
    }

    /**
     * @param form the form to set
     */
    public void setForm(WorkingGroupApplication form) {
        this.form = form;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(form, id, isSigningAuthority, user, workingGroupAlias, workingGroupLevel);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        WorkingGroupApplicationContact other = (WorkingGroupApplicationContact) obj;
        return Objects.equals(form, other.form) && id == other.id && isSigningAuthority == other.isSigningAuthority
                && Objects.equals(user, other.user) && Objects.equals(workingGroupAlias, other.workingGroupAlias)
                && Objects.equals(workingGroupLevel, other.workingGroupLevel);
    }

    @Override
    public String toString() {
        return "WorkingGroupApplicationContact [id=" + id + ", user=" + user + ", workingGroupAlias=" + workingGroupAlias
                + ", workingGroupLevel=" + workingGroupLevel + ", isSigningAuthority=" + isSigningAuthority + ", form=" + form + "]";
    }

}
