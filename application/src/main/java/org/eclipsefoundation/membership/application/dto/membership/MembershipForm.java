/**
 * Copyright (c) 2021, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.application.dto.membership;

import java.util.Objects;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.application.dto.membership.ValidationGroups.Completion;
import org.eclipsefoundation.membership.application.namespace.FormState;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;
import org.eclipsefoundation.persistence.model.SortableField;
import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Table
@Entity
public class MembershipForm extends BareNode implements TargetedClone<MembershipForm> {
    public static final DtoTable TABLE = new DtoTable(MembershipForm.class, "m");

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;
    private String userID;
    @NotBlank(message = "Membership level is required for submission", groups = Completion.class)
    private String membershipLevel;
    private boolean signingAuthority;
    @NotBlank(message = "Purchase order state cannot be blank", groups = Completion.class)
    private String purchaseOrderRequired;
    private String vatNumber;
    private String registrationCountry;
    @SortableField
    private Long dateCreated;
    @SortableField
    private Long dateUpdated;
    @SortableField
    private Long dateSubmitted;
    private String applicationGroup;
    @NotNull(message = "The form state cannot be null")
    private FormState state;

    /** @return the id */
    @Override
    public String getId() {
        return id;
    }

    /** @param id the id to set */
    public void setId(String id) {
        this.id = id;
    }

    /** @return the userId */
    public String getUserID() {
        return userID;
    }

    /** @param userID the userId to set */
    public void setUserID(String userID) {
        this.userID = userID;
    }

    /** @return the membershipLevel */
    public String getMembershipLevel() {
        return membershipLevel;
    }

    /** @param membershipLevel the membershipLevel to set */
    public void setMembershipLevel(String membershipLevel) {
        this.membershipLevel = membershipLevel;
    }

    /** @return the signingAuthority */
    public boolean isSigningAuthority() {
        return signingAuthority;
    }

    /** @param signingAuthority the signingAuthority to set */
    public void setSigningAuthority(boolean signingAuthority) {
        this.signingAuthority = signingAuthority;
    }

    public String getPurchaseOrderRequired() {
        return this.purchaseOrderRequired;
    }

    public void setPurchaseOrderRequired(String purchaseOrderRequired) {
        this.purchaseOrderRequired = purchaseOrderRequired;
    }

    public String getVatNumber() {
        return this.vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getRegistrationCountry() {
        return this.registrationCountry;
    }

    public void setRegistrationCountry(String registrationCountry) {
        this.registrationCountry = registrationCountry;
    }

    public Long getDateCreated() {
        return this.dateCreated;
    }

    @JsonIgnore
    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the dateUpdated
     */
    public Long getDateUpdated() {
        return dateUpdated;
    }

    /**
     * @param dateUpdated the dateUpdated to set
     */
    public void setDateUpdated(Long dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public Long getDateSubmitted() {
        return this.dateSubmitted;
    }

    @JsonIgnore
    public void setDateSubmitted(Long dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
    }

    /**
     * @return the applicationGroup
     */
    public String getApplicationGroup() {
        return applicationGroup;
    }

    /**
     * @param applicationGroup the applicationGroup to set
     */
    public void setApplicationGroup(String applicationGroup) {
        this.applicationGroup = applicationGroup;
    }

    public FormState getState() {
        return this.state;
    }

    public void setState(FormState state) {
        this.state = state;
    }

    @Override
    public MembershipForm cloneTo(MembershipForm target) {
        target.setUserID(getUserID());
        target.setMembershipLevel(getMembershipLevel());
        target.setSigningAuthority(isSigningAuthority());
        target.setPurchaseOrderRequired(getPurchaseOrderRequired());
        target.setRegistrationCountry(getRegistrationCountry());
        target.setVatNumber(getVatNumber());
        target.setApplicationGroup(getApplicationGroup());
        target.setState(getState());
        if (getDateCreated() != null) {
            target.setDateCreated(getDateCreated());
        }
        if (getDateSubmitted() != null) {
            target.setDateSubmitted(getDateSubmitted());
        }
        return target;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + Objects.hash(id, membershipLevel, signingAuthority, userID, vatNumber, registrationCountry,
                        purchaseOrderRequired, dateCreated, dateUpdated, dateSubmitted, applicationGroup, state);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        MembershipForm other = (MembershipForm) obj;
        return Objects.equals(id, other.id) && Objects.equals(membershipLevel, other.membershipLevel)
                && signingAuthority == other.signingAuthority && Objects.equals(userID, other.userID)
                && Objects.equals(vatNumber, other.vatNumber) && Objects.equals(dateCreated, other.dateCreated)
                && Objects.equals(registrationCountry, other.registrationCountry)
                && Objects.equals(purchaseOrderRequired, other.purchaseOrderRequired)
                && Objects.equals(state, other.state) && Objects.equals(dateSubmitted, other.dateSubmitted)
                && Objects.equals(dateUpdated, other.dateUpdated)
                && Objects.equals(applicationGroup, other.applicationGroup);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MembershipForm [id=");
        builder.append(id);
        builder.append(", userID=");
        builder.append(userID);
        builder.append(", membershipLevel=");
        builder.append(membershipLevel);
        builder.append(", signingAuthority=");
        builder.append(signingAuthority);
        builder.append(", purchaseOrderRequired=");
        builder.append(purchaseOrderRequired);
        builder.append(", registrationCountry=");
        builder.append(registrationCountry);
        builder.append(", vatNumber=");
        builder.append(vatNumber);
        builder.append(", dateCreated=");
        builder.append(dateCreated);
        builder.append(", dateSubmitted=");
        builder.append(dateSubmitted);
        builder.append(", applicationGroup=");
        builder.append(applicationGroup);
        builder.append(", state=");
        builder.append(state);
        builder.append("]");
        return builder.toString();
    }

    @Singleton
    public static class MembershipFormFilter implements DtoFilter<MembershipForm> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            if (isRoot) {
                // ID check
                String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
                if (id != null) {
                    stmt.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".id = ?", new Object[] { id }));
                }
            }
            // user ID check
            String userId = params.getFirst(MembershipFormAPIParameterNames.USER_ID.getName());
            if (userId != null) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".userID = ?",
                        new Object[] { userId }));
            }
            // form state check
            String formState = params.getFirst(MembershipFormAPIParameterNames.FORM_STATE.getName());
            if (formState != null) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".state = ?",
                        new Object[] { FormState.valueOf(formState.toUpperCase()) }));
            }
            // form group check
            String formGroup = params.getFirst(MembershipFormAPIParameterNames.APPLICATION_GROUP.getName());
            if (formGroup != null) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".applicationGroup = ?",
                        new Object[] { formGroup }));
            }

            String createdBefore = params
                    .getFirst(MembershipFormAPIParameterNames.BEFORE_DATE_UPDATED_IN_MILLIS.getName());
            if (createdBefore != null && StringUtils.isNumeric(createdBefore)) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".dateUpdated < ?",
                        new Object[] { Long.parseLong(createdBefore) }));
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".dateUpdated IS NOT NULL",
                        new Object[] {}));
            }

            String createdAfter = params
                    .getFirst(MembershipFormAPIParameterNames.AFTER_DATE_UPDATED_IN_MILLIS.getName());
            if (createdAfter != null && StringUtils.isNumeric(createdAfter)) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".dateUpdated > ?",
                        new Object[] { Long.parseLong(createdAfter) }));
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".dateUpdated IS NOT NULL",
                        new Object[] {}));
            }
            return stmt;
        }

        @Override
        public Class<MembershipForm> getType() {
            return MembershipForm.class;
        }
    }
}
