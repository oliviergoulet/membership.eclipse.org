/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.resources.mappers;

import org.eclipsefoundation.core.model.Error;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.persistence.EntityNotFoundException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * Creates human legible error responses in the case of specific reference lookups in the persistence layer. This is triggered by calling
 * Dao.getReference, which is typically used for relations between DTO instances. If one of these instances are missing, this more often
 * than not indicates that there is bad data set into the origin request.
 * 
 * @author Martin Lowe
 */
@Provider
public class EntityNotFoundMapper implements ExceptionMapper<EntityNotFoundException> {
    private static final Logger LOGGER = LoggerFactory.getLogger(EntityNotFoundMapper.class);

    @Override
    public Response toResponse(EntityNotFoundException exception) {
        LOGGER.error(exception.getMessage(), exception);
        return new Error(Status.BAD_REQUEST, exception.getMessage()).asResponse();
    }
}
