/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.namespace;

/**
 * Defined types of contacts that can be added within a form.
 * 
 * @author Martin Lowe
 *
 */
public enum ContactTypes {
    WORKING_GROUP("Working Group Representative"), COMPANY("Member Representative"), MARKETING("Marketing Contact"),
    ACCOUNTING("Accounting Contact"), SIGNING("Signing Contact");

    private String displayName;

    private ContactTypes(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return this.displayName;
    }
}
