/**
 * Copyright (c) 2021, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.application.dto.membership;

import java.util.List;
import java.util.Objects;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.membership.application.namespace.OrganizationTypes;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;
import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Table
@Entity
public class FormOrganization extends BareNode implements TargetedClone<FormOrganization> {
    public static final DtoTable TABLE = new DtoTable(FormOrganization.class, "o");

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;
    @NotBlank(message = "Legal name cannot be blank")
    private String legalName;
    @JsonProperty("twitter")
    private String twitterHandle;
    private String employeeCount;
    private String aggregateRevenue;
    @Enumerated(EnumType.STRING)
    @NotNull(message = "An organization requires a defined type")
    private OrganizationTypes organizationType;

    // form entity
    @OneToOne(targetEntity = MembershipForm.class)
    @JoinColumn(name = "form_id", unique = true)
    private MembershipForm form;
    @NotNull(message = "Organization Address must be set")
    @OneToOne(cascade = { CascadeType.ALL }, mappedBy = "organization")
    private Address address;

    @Override
    public String getId() {
        return id;
    }

    /** @param id the id to set */
    @JsonIgnore
    public void setId(String id) {
        this.id = id;
    }

    /** @return the formID */
    @JsonIgnore
    public MembershipForm getForm() {
        return form;
    }

    /** @param form the form to set */
    @JsonIgnore
    public void setForm(MembershipForm form) {
        this.form = form;
    }

    /** @return the legalName */
    public String getLegalName() {
        return legalName;
    }

    /** @param legalName the legalName to set */
    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    /**
     * @return the twitterHandle
     */
    public String getTwitterHandle() {
        return twitterHandle;
    }

    /**
     * @param twitterHandle the twitterHandle to set
     */
    public void setTwitterHandle(String twitterHandle) {
        this.twitterHandle = twitterHandle;
    }

    /**
     * @return the employeeCount
     */
    public String getEmployeeCount() {
        return employeeCount;
    }

    /**
     * @param employeeCount the employeeCount to set
     */
    public void setEmployeeCount(String employeeCount) {
        this.employeeCount = employeeCount;
    }

    /**
     * @return the aggregateRevenue
     */
    public String getAggregateRevenue() {
        return aggregateRevenue;
    }

    /**
     * @param aggregateRevenue the aggregateRevenue to set
     */
    public void setAggregateRevenue(String aggregateRevenue) {
        this.aggregateRevenue = aggregateRevenue;
    }

    /**
     * @return the organizationType
     */
    public OrganizationTypes getOrganizationType() {
        return organizationType;
    }

    /**
     * @param organizationType the organizationType to set
     */
    public void setOrganizationType(OrganizationTypes organizationType) {
        this.organizationType = organizationType;
    }

    /** @return the address */
    public Address getAddress() {
        return address;
    }

    /** @param address the address to set */
    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public FormOrganization cloneTo(FormOrganization target) {
        target.setLegalName(getLegalName());
        target.setTwitterHandle(getTwitterHandle());
        target.setAggregateRevenue(getAggregateRevenue());
        target.setEmployeeCount(getEmployeeCount());
        target.setOrganizationType(getOrganizationType());
        return target;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(address, form, id, legalName, twitterHandle);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        FormOrganization other = (FormOrganization) obj;
        return Objects.equals(address, other.address) && Objects.equals(form, other.form)
                && Objects.equals(id, other.id) && Objects.equals(legalName, other.legalName)
                && Objects.equals(twitterHandle, other.twitterHandle)
                && Objects.equals(aggregateRevenue, other.aggregateRevenue)
                && Objects.equals(employeeCount, other.employeeCount)
                && Objects.equals(organizationType, other.organizationType);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Organization [id=");
        builder.append(id);
        builder.append(", legalName=");
        builder.append(legalName);
        builder.append(", twitterHandle=");
        builder.append(twitterHandle);
        builder.append(", aggregateRevenue=");
        builder.append(aggregateRevenue);
        builder.append(", employeeCount=");
        builder.append(employeeCount);
        builder.append(", organizationType=");
        builder.append(organizationType);
        builder.append(", form=");
        builder.append(form);
        builder.append(", address=");
        builder.append(address);
        builder.append("]");
        return builder.toString();
    }

    @Singleton
    public static class FormOrganizationFilter implements DtoFilter<FormOrganization> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            if (isRoot) {
                // ID check
                String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
                if (id != null) {
                    stmt.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".id = ?", new Object[] { id }));
                }
            }
            // form ID check
            String formId = params.getFirst(MembershipFormAPIParameterNames.FORM_ID.getName());
            if (formId != null) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".form.id = ?",
                        new Object[] { formId }));
            }
            // form IDs check
            List<String> formIds = params.get(MembershipFormAPIParameterNames.FORM_IDS.getName());
            if (formIds != null) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".form.id IN ?",
                        new Object[] { formIds }));
            }
            return stmt;
        }

        @Override
        public Class<FormOrganization> getType() {
            return FormOrganization.class;
        }
    }
}
