/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.service;

import java.util.List;

import org.eclipsefoundation.membership.application.dto.membership.BaseContact;
import org.eclipsefoundation.membership.application.dto.membership.FormOrganization;
import org.eclipsefoundation.membership.application.namespace.TransactionType;

/**
 * Interface defining application workflow activities taken during the completion of the submissions.
 * 
 */
public interface WorkflowService {

    /**
     * Generate account request records for contacts that are associated with the given form. These records will then be converted into
     * actual user objects by an external process.
     * 
     * @param form the membership form to create user accounts for contacts.
     * @param uname user that created the base form triggering the account creation
     * @param formIdentifier an identifier used for what type of form and which form is the origin of this request
     */
    void createMissingUserAccounts(List<? extends BaseContact> contacts, String uname, TransactionType type, String formIdentifier);

    /**
     * Filters through the list of form contacts and returns a filtered list of contacts that will receive an email to create an Eclipse
     * account.
     * 
     * @param formContacts The form contacts
     * @return A List of emailable form contacts
     */
    List<BaseContact> getProspectiveUsers(List<? extends BaseContact> contacts);

    /**
     * Checks the foundation records to see if there is an exact match on the form organization for legal name.
     * 
     * @param formOrg the organization for the form being processed
     * @return true if there is a match on legal name, false otherwise.
     */
    boolean doesOrganizationExist(FormOrganization formOrg);
}
