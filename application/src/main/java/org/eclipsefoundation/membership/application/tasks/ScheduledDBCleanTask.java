/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.tasks;

import java.net.URI;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import jakarta.enterprise.context.Dependent;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.application.config.ApplicationScheduledTaskConfig;
import org.eclipsefoundation.membership.application.dto.membership.Contact;
import org.eclipsefoundation.membership.application.dto.membership.FormOrganization;
import org.eclipsefoundation.membership.application.dto.membership.FormWorkingGroup;
import org.eclipsefoundation.membership.application.dto.membership.MembershipForm;
import org.eclipsefoundation.membership.application.namespace.FormState;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.arc.Arc;
import io.quarkus.arc.InstanceHandle;
import io.quarkus.scheduler.Scheduled;

/**
 * Schedules a task everyday to batch cleanup documents that are older than the given maxage duration.
 * 
 * @author Martin Lowe
 *
 */
@Dependent
public class ScheduledDBCleanTask {
    public static final Logger LOGGER = LoggerFactory.getLogger(ScheduledDBCleanTask.class);

    @Inject
    Instance<ApplicationScheduledTaskConfig> config;

    /**
     * Schedule the task every day from start up to clean up unused form entries.
     */
    @Scheduled(every = "P1D", delay = 5, identity = "application-stale-cleanup")
    void schedule() {
        if (Boolean.TRUE.equals(config.get().dbClean().enabled())) {
            InstanceHandle<DefaultHibernateDao> daoHandle = Arc.container().instance(DefaultHibernateDao.class);
            DefaultHibernateDao dao = daoHandle.get();
            InstanceHandle<FilterService> filtersHandle = Arc.container().instance(FilterService.class);
            FilterService filters = filtersHandle.get();

            ZonedDateTime maxAge = DateTimeHelper.now().minus(config.get().dbClean().maxAge());
            LOGGER.info("Checking for database entries updated before {}", maxAge);
            // create parameter map for inprogress documents older than the configured period
            MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
            params
                    .add(MembershipFormAPIParameterNames.BEFORE_DATE_UPDATED_IN_MILLIS.getName(),
                            Long.toString(DateTimeHelper.getMillis(maxAge)));
            params.add(MembershipFormAPIParameterNames.FORM_STATE.getName(), FormState.INPROGRESS.name());
            URI baseUri = URI.create("https://membership.eclipse.org");
            // generate the query to get expired documents
            RDBMSQuery<MembershipForm> initialQuery = new RDBMSQuery<>(new FlatRequestWrapper(baseUri), filters.get(MembershipForm.class),
                    params);
            initialQuery.setRoot(false);
            // get the expired form objects
            long size = dao.count(initialQuery);
            LOGGER.info("Getting {} forms to remove dependant records", size);
            List<MembershipForm> forms = new ArrayList<>();
            int count = 0;
            while (forms.size() < size) {
                // update the query to get the next page
                params.add(DefaultUrlParameterNames.PAGE.getName(), Integer.toString(++count));
                RDBMSQuery<MembershipForm> q = new RDBMSQuery<>(new FlatRequestWrapper(baseUri), filters.get(MembershipForm.class), params);
                q.setRoot(false);
                forms.addAll(dao.get(q));
                LOGGER.info("Retrieved {} out of {} records", forms.size(), size);
            }

            // build batch parameters to delete old documents
            MultivaluedMap<String, String> formFKParams = new MultivaluedMapImpl<>();
            formFKParams
                    .addAll(MembershipFormAPIParameterNames.FORM_IDS.getName(),
                            forms.stream().map(MembershipForm::getId).collect(Collectors.toList()));

            // log useful information about removed entries
            LOGGER.info("Removing {} form entries from the database", size);
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Entries being removed {}", formFKParams.get(MembershipFormAPIParameterNames.FORM_IDS.getName()));
            }
            // delete the downstream entities in bulk
            dao.delete(generateQuery(formFKParams, FormWorkingGroup.class, filters));
            dao.delete(generateQuery(formFKParams, Contact.class, filters));
            dao.delete(generateQuery(formFKParams, FormOrganization.class, filters));
            // delete the forms last
            dao.delete(initialQuery);
        } else {
            LOGGER.warn("DB clean scheduled task not run as task has been disabled through configuration");
        }
    }

    private <T extends BareNode> RDBMSQuery<T> generateQuery(MultivaluedMap<String, String> params, Class<T> type, FilterService filters) {
        RDBMSQuery<T> out = new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")), filters.get(type),
                params);
        out.setRoot(false);
        return out;
    }
}
