/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.model;

import java.time.ZonedDateTime;

import io.quarkus.qute.TemplateData;

@TemplateData
public class MailerDataContext {
    public final String baseUrl;
    public final String group;
    public final ZonedDateTime now;
    public final boolean isPDF;
    public final boolean includePreamble;

    public MailerDataContext(String baseUrl, String group, ZonedDateTime now, boolean isPDF, boolean includePreamble) {
        this.baseUrl = baseUrl;
        this.group = group;
        this.now = now;
        this.isPDF = isPDF;
        this.includePreamble = includePreamble;
    }
}
