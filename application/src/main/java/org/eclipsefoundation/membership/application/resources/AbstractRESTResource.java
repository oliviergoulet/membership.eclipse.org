/**
 * Copyright (c) 2021, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.application.resources;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.foundationdb.client.model.OrganizationContactData;
import org.eclipsefoundation.membership.application.api.FoundationAPI;
import org.eclipsefoundation.membership.application.dto.membership.MembershipForm;
import org.eclipsefoundation.membership.application.model.WorkingGroupApplicationData;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.security.ForbiddenException;
import io.quarkus.security.UnauthorizedException;
import io.quarkus.security.identity.SecurityIdentity;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

/**
 * Provides access to commonly required services and containers for REST request serving.
 *
 * @author Martin Lowe
 */
public abstract class AbstractRESTResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRESTResource.class);

    private static final List<String> ALLOWED_WG_APP_ROLES = Collections.unmodifiableList(Arrays.asList("CR", "CRA", "DE"));

    public static final String ALL_CACHE_PLACEHOLDER = "all";

    @Inject
    DefaultHibernateDao dao;
    @Inject
    FilterService filters;
    @Inject
    CachingService cache;
    @Inject
    APIMiddleware middleware;

    @Inject
    RequestWrapper wrap;
    @Inject
    SecurityIdentity ident;

    @RestClient
    FoundationAPI api;

    /**
     * Check if the current user has access to the current resource, based on owner of form and current logged in user.
     * 
     * @param formId the form ID that is being loaded, or has assets that are to be loaded/modified.
     * @return a response if there is an access error, or null if there is no access error.
     */
    protected Response checkAccess(String formId) {
        // check if there is a logged in user
        if (ident.isAnonymous()) {
            return Response.status(Response.Status.UNAUTHORIZED.getStatusCode()).build();
        }
        // check if user is allowed to get these resources
        MembershipForm form = dao.getReference(formId, MembershipForm.class);
        if (form == null) {
            return Response.status(Response.Status.NOT_FOUND.getStatusCode()).build();
        }
        // check that the logged in user is the creator of the form
        if (!form.getUserID().equalsIgnoreCase(ident.getPrincipal().getName())) {
            LOGGER
                    .warn("User with name '{}' attempted to access form data for user '{}'", ident.getPrincipal().getName(),
                            form.getUserID());
            return Response.status(Response.Status.FORBIDDEN.getStatusCode()).build();
        }
        // if there is no issue, return no response
        return null;
    }

    protected void checkAccessForWorkingGroupApplication(WorkingGroupApplicationData form) {
        // check if there is a logged in user, should always be a logged in user if used correctly
        if (ident.isAnonymous()) {
            throw new UnauthorizedException("User must be logged in to use this endpoint");
        }

        // check that the logged in user is the creator of the form or in same organization
        if (!form.getUser().equalsIgnoreCase(ident.getPrincipal().getName()) && !canAccessData(form.getOrgId())) {
            LOGGER
                    .warn("User with name '{}' attempted to access form {} but did not have permissions to access data",
                            ident.getPrincipal().getName(), form.getId());
            throw new ForbiddenException("User must either be the author or within the same organization as author of form to access");
        }
    }

    protected boolean canAccessData(Integer orgId) {
        String username = ident.getPrincipal().getName();

        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.USER_ID.getName(), username);
        List<OrganizationContactData> contacts = cache
                .get(orgId.toString() + "-access", params, OrganizationContactData.class,
                        () -> middleware.getAll(i -> api.getAllOrganizationContacts(orgId, username, i), OrganizationContactData.class))
                .getData()
                .orElse(Collections.emptyList());
        LOGGER.trace("Found {} contact records for user {} in organization {}", contacts.size(), username, orgId);

        // check if any of the relations for the user in the org match the allowed list of roles
        return contacts.stream().anyMatch(c -> ALLOWED_WG_APP_ROLES.contains(c.getRelation()));
    }
}
