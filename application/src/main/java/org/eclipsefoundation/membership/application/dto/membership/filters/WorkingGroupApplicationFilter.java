/*******************************************************************************
 * Copyright (c) 2024 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipsefoundation.membership.application.dto.membership.filters;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.application.dto.membership.WorkingGroupApplication;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.ws.rs.core.MultivaluedMap;

@Singleton
public class WorkingGroupApplicationFilter implements DtoFilter<WorkingGroupApplication> {
    @Inject
    ParameterizedSQLStatementBuilder builder;

    @Override
    public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
        ParameterizedSQLStatement stmt = builder.build(WorkingGroupApplication.TABLE);
        // ID check
        String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
        if (StringUtils.isNumeric(id)) {
            stmt
                    .addClause(new ParameterizedSQLStatement.Clause(WorkingGroupApplication.TABLE.getAlias() + ".id = ?",
                            new Object[] { Long.parseLong(id) }));
        }
        // handle Username + org ID filters. When both present they are used in conjunction
        String userId = params.getFirst(MembershipFormAPIParameterNames.USER_ID.getName());
        String orgId = params.getFirst(MembershipFormAPIParameterNames.ORGANIZATION_ID.getName());
        if (StringUtils.isNumeric(orgId) && StringUtils.isNotBlank(userId)) {
            stmt
                    .addClause(new ParameterizedSQLStatement.Clause('(' + WorkingGroupApplication.TABLE.getAlias() + ".user = ? OR "
                            + WorkingGroupApplication.TABLE.getAlias() + ".organizationId = ?)", new Object[] { userId, Integer.parseInt(orgId) }));
        } else if (userId != null) {
            stmt
                    .addClause(new ParameterizedSQLStatement.Clause(WorkingGroupApplication.TABLE.getAlias() + ".user = ?",
                            new Object[] { userId }));
        } else if (StringUtils.isNumeric(orgId)) {
            stmt
                    .addClause(new ParameterizedSQLStatement.Clause(WorkingGroupApplication.TABLE.getAlias() + ".organizationId = ?",
                            new Object[] { Integer.parseInt(orgId) }));
        }
        // form state check
        String formState = params.getFirst(MembershipFormAPIParameterNames.FORM_STATE.getName());
        if (formState != null) {
            stmt
                    .addClause(new ParameterizedSQLStatement.Clause(WorkingGroupApplication.TABLE.getAlias() + ".state = ?",
                            new Object[] { formState.toUpperCase() }));
        }
        return stmt;
    }

    @Override
    public Class<WorkingGroupApplication> getType() {
        return WorkingGroupApplication.class;
    }
}
