/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.service;

import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.membership.application.dto.membership.SecureTransaction;
import org.eclipsefoundation.membership.application.namespace.TransactionType;

/**
 * Validates and inactivates SecureTransaction entities.
 */
public interface SecureTransactionService {

        /**
         * Retrieves a valid SecureTransaction from the DB using the given params.
         * Returns null if none exist
         * 
         * @param transaction The transaction to check
         * @param wrap        The request wrapper
         * @return A SecureTransaction entity or null
         */
        public SecureTransaction getActiveTrackedTransaction(MultivaluedMap<String, String> params,
                        RequestWrapper wrap);

        /**
         * Invalidates a secure transaction in the DB.
         * 
         * @param transaction The transaction to inactivate
         * @param wrap        The request wrapper
         */
        public void deactivateTrackedTransaction(SecureTransaction transaction, RequestWrapper wrap);

        /**
         * Creates a new SecureTransaction using the associated entity id and
         * transaction type. The id is ommited since it is auto generated upon creation
         * in the DB.
         * 
         * @param associatedEntityId the associated entity id
         * @param type               the transaction type
         * @return a SecureTransaction with the given fields.
         */
        public SecureTransaction generateUntrackedTransaction(String associatedEntityId, TransactionType type);

        /**
         * Using the given parameters, creates a MultivaluedMap for querying a
         * SecureTransaction entity with type FORM.
         * 
         * @param transactionId the transaction id for querying
         * @param token         the token for querying
         * @param formId        the form id for querying
         * @return A MultivaluedMap containing the desired query parameters
         */
        public MultivaluedMap<String, String> createTransactionQueryParams(Long transactionId, String token,
                        String formId, TransactionType type);
}
