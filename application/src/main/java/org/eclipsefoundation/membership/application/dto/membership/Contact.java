/**
 * Copyright (c) 2021, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.application.dto.membership;

import java.util.List;
import java.util.Objects;

import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.application.namespace.ContactTypes;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;
import org.hibernate.annotations.GenericGenerator;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * A contact entity, representing a contact for an organization or working group.
 * 
 * @author Martin Lowe
 */
@Table
@Entity
public class Contact extends BaseContact implements TargetedClone<Contact> {
    public static final DtoTable TABLE = new DtoTable(Contact.class, "c");

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    // form entity for FK relation
    @ManyToOne(targetEntity = MembershipForm.class)
    @JoinColumn(name = "form_id")
    private MembershipForm form;
    @NotBlank(message = "Job title cannot be blank")
    private String title;
    @NotNull(message = "Contact type cannot be empty")
    @Enumerated(EnumType.STRING)
    private ContactTypes type;

    @Override
    public String getId() {
        return id;
    }

    /** @param id the id to set */
    public void setId(String id) {
        this.id = id;
    }

    /** @return the form */
    public MembershipForm getForm() {
        return form;
    }

    /** @param form the form to set */
    public void setForm(MembershipForm form) {
        this.form = form;
    }

    /** @return the title */
    public String getTitle() {
        return title;
    }

    /** @param title the title to set */
    public void setTitle(String title) {
        this.title = title;
    }

    /** @return the type */
    public ContactTypes getType() {
        return type;
    }

    /** @param type the type to set */
    public void setType(ContactTypes type) {
        this.type = type;
    }

    @Override
    public Contact cloneTo(Contact target) {
        target.setEmail(getEmail());
        target.setfName(getfName());
        target.setlName(getlName());
        target.setTitle(getTitle());
        target.setType(getType());
        return target;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(form, id, title, type);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Contact other = (Contact) obj;
        return Objects.equals(form, other.form) && Objects.equals(id, other.id) && Objects.equals(title, other.title) && type == other.type;
    }

    @Singleton
    public static class ContactFilter implements DtoFilter<Contact> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            if (isRoot) {
                // ID check
                String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
                if (id != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".id = ?", new Object[] { id }));
                }
            }
            // form ID check
            String formId = params.getFirst(MembershipFormAPIParameterNames.FORM_ID.getName());
            if (formId != null) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".form.id = ?", new Object[] { formId }));
            }
            // form IDs check
            List<String> formIds = params.get(MembershipFormAPIParameterNames.FORM_IDS.getName());
            if (formIds != null) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".form.id IN ?", new Object[] { formIds }));
            }
            return stmt;
        }

        @Override
        public Class<Contact> getType() {
            return Contact.class;
        }
    }
}
