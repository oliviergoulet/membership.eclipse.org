/**
 * Copyright (c) 2021, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.application.dto.membership;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Objects;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;
import org.hibernate.annotations.GenericGenerator;

/**
 * Represents a prospective Working Group relationship with the current organization (based on the form)
 * 
 * @author Martin Lowe
 */
@Table
@Entity
public class FormWorkingGroup extends BareNode implements TargetedClone<FormWorkingGroup> {
    public static final DtoTable TABLE = new DtoTable(FormWorkingGroup.class, "wg");

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;
    @NotBlank(message = "Working Group ID cannot be blank")
    private String workingGroupID;
    @NotBlank(message = "Participation level cannot be blank")
    private String participationLevel;
    @NotNull(message = "Effective date cannot be blank")
    private OffsetDateTime effectiveDate;

    // form entity
    @OneToOne(targetEntity = MembershipForm.class)
    @JoinColumn(name = "form_id")
    private MembershipForm form;

    @NotNull(message = "Organization Contact cannot be null")
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(referencedColumnName = "id", unique = true)
    private Contact contact;

    /**
     * @return the id
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the form
     */
    public MembershipForm getForm() {
        return form;
    }

    /**
     * @param form the form to set
     */
    public void setForm(MembershipForm form) {
        this.form = form;
    }

    /**
     * @return the workingGroupID
     */
    public String getWorkingGroupID() {
        return workingGroupID;
    }

    /**
     * @param workingGroupID the workingGroupID to set
     */
    public void setWorkingGroupID(String workingGroupID) {
        this.workingGroupID = workingGroupID;
    }

    /**
     * @return the participationLevel
     */
    public String getParticipationLevel() {
        return participationLevel;
    }

    /**
     * @param participationLevel the participationLevel to set
     */
    public void setParticipationLevel(String participationLevel) {
        this.participationLevel = participationLevel;
    }

    /**
     * @return the effectiveDate
     */
    public OffsetDateTime getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * @param effectiveDate the effectiveDate to set
     */
    public void setEffectiveDate(OffsetDateTime effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    /**
     * @return the contact
     */
    public Contact getContact() {
        return contact;
    }

    /**
     * @param contact the contact to set
     */
    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Override
    public FormWorkingGroup cloneTo(FormWorkingGroup target) {
        target.setEffectiveDate(getEffectiveDate());
        target.setParticipationLevel(getParticipationLevel());
        target.setWorkingGroupID(getWorkingGroupID());
        return target;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(contact, effectiveDate, form, id, participationLevel, workingGroupID);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        FormWorkingGroup other = (FormWorkingGroup) obj;
        return Objects.equals(contact, other.contact) && Objects.equals(effectiveDate, other.effectiveDate)
                && Objects.equals(form, other.form) && Objects.equals(id, other.id)
                && Objects.equals(participationLevel, other.participationLevel)
                && Objects.equals(workingGroupID, other.workingGroupID);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("WorkingGroup [id=");
        builder.append(id);
        builder.append(", workingGroupID=");
        builder.append(workingGroupID);
        builder.append(", participationLevel=");
        builder.append(participationLevel);
        builder.append(", effectiveDate=");
        builder.append(effectiveDate);
        builder.append(", form=");
        builder.append(form);
        builder.append(", contact=");
        builder.append(contact);
        builder.append("]");
        return builder.toString();
    }

    @Singleton
    public static class FormWorkingGroupFilter implements DtoFilter<FormWorkingGroup> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            if (isRoot) {
                // ID check
                String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
                if (id != null) {
                    stmt.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".id = ?", new Object[] { id }));
                }
            }
            // form ID check
            String formId = params.getFirst(MembershipFormAPIParameterNames.FORM_ID.getName());
            if (formId != null) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".form.id = ?",
                        new Object[] { formId }));
            }
            // form IDs check
            List<String> formIds = params.get(MembershipFormAPIParameterNames.FORM_IDS.getName());
            if (formIds != null) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".form.id IN ?",
                        new Object[] { formIds }));
            }
            return stmt;
        }

        @Override
        public Class<FormWorkingGroup> getType() {
            return FormWorkingGroup.class;
        }
    }
}
