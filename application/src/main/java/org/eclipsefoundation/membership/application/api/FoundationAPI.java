/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.api;

import java.util.List;
import java.util.Optional;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.model.PeopleData;

import io.quarkus.oidc.client.filter.OidcClientFilter;
import jakarta.annotation.security.RolesAllowed;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * REST client for FoundationDB API access. Requires an OIDC client with the following associated roles:
 * 
 * <ul>
 * <li>fdb_read_organization
 * <li>fdb_write_organization_employment
 * <li>fdb_write_organization
 * <li>fdb_write_people
 * </ul>
 */
@OidcClientFilter
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "fdndb-api")
public interface FoundationAPI {

    /**
     * Retrieves a page of results for the organizations in the Foundation DB.
     * 
     * @param params pagination params for request
     * @return return from FoundationDB containing the results for the page, or an error if there was an issue fetching the information
     */
    @GET
    @Path("organizations")
    @RolesAllowed("fdb_read_organization")
    Response getAllOrganizations(@BeanParam BaseAPIParameters params);

    /**
     * Retrieves a page of results for the organization contacts in the Foundation DB for the given user.
     * 
     * @param orgId the organization that the user is associated with
     * @param personId the username of the user to retrieve organization contact records for
     * @param params pagination params for request
     * @return return from FoundationDB containing the results for the page, or an error if there was an issue fetching the information
     */
    @GET
    @Path("organizations/{id}/contacts/{user}")
    @RolesAllowed("fdb_read_organization")
    Response getAllOrganizationContacts(@PathParam("id") Integer orgId, @PathParam("user") String personId,
            @BeanParam BaseAPIParameters params);

    /**
     * Retrieves a person entry with the given ID if it exists
     * 
     * @param personId id of the person to retrieve
     * @return the person record if it exists or empty
     */
    @GET
    @Path("people/{id}")
    @RolesAllowed("fdb_read_people")
    Optional<PeopleData> getPerson(@PathParam("id") String personId);

    /**
     * Updates or creates an Person record in the foundation database using the passed data.
     * 
     * @param person personal data to persist to the Foundation database.
     * @return the persisted record from the Foundation DB.
     */
    @PUT
    @Path("people")
    @RolesAllowed("fdb_write_people")
    List<PeopleData> updatePeople(PeopleData person);
}
