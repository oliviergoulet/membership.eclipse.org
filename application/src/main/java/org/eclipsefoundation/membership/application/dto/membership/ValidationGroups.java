/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.dto.membership;

import jakarta.validation.groups.Default;

/**
 * Used to differentiate some validation annotations from default to allow partial states.
 * 
 * @author Martin Lowe
 *
 */
public interface ValidationGroups {
    interface Completion extends Default {
    }
}
