/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.model;

import java.time.ZonedDateTime;

import jakarta.annotation.Nullable;

import org.eclipsefoundation.membership.application.namespace.FormState;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonPropertyOrder(value = { "userId", "userEmail", "fullName", "organizationName", "formCompletionStage", "formId",
        "membershipStage", "workingGroupCount", "dateUpdated", "dateCreated", "dateSubmitted", "registrationCountry" })
@JsonDeserialize(builder = AutoValue_FormStatusReportItem.Builder.class)
public abstract class FormStatusReportItem {
    public abstract String getUserId();

    public abstract String getUserEmail();

    public abstract String getFullName();

    public abstract String getFormId();

    @Nullable
    public abstract String getMembershipLevel();

    @Nullable
    public abstract String getOrganizationName();

    @Nullable
    public abstract String getRegistrationCountry();

    public abstract ZonedDateTime getDateCreated();

    @Nullable
    public abstract ZonedDateTime getDateUpdated();

    @Nullable
    public abstract ZonedDateTime getDateSubmitted();

    public abstract FormState getState();

    @Nullable
    public abstract Integer getWorkingGroupCount();

    public abstract Integer getFormCompletionStage();

    public static Builder builder() {
        return new AutoValue_FormStatusReportItem.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setUserId(String userId);

        public abstract Builder setUserEmail(String userEmail);

        public abstract Builder setFullName(String fullName);

        public abstract Builder setFormId(String formId);

        public abstract Builder setMembershipLevel(@Nullable String membershipLevel);

        public abstract Builder setOrganizationName(@Nullable String organizationName);

        public abstract Builder setRegistrationCountry(@Nullable String registrationCountry);

        public abstract Builder setDateCreated(ZonedDateTime dateCreated);

        public abstract Builder setDateUpdated(@Nullable ZonedDateTime dateUpdated);

        public abstract Builder setDateSubmitted(@Nullable ZonedDateTime dateSubmitted);

        public abstract Builder setState(FormState state);

        public abstract Builder setWorkingGroupCount(@Nullable Integer workingGroupCount);

        public abstract Builder setFormCompletionStage(Integer formCompletionStage);

        public abstract FormStatusReportItem build();
    }
}
