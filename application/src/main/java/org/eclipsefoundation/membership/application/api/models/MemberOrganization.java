/**
 * Copyright (c) 2021, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.application.api.models;

import java.time.LocalDate;
import java.util.List;

import jakarta.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_MemberOrganization.Builder.class)
public abstract class MemberOrganization {
    public abstract int getOrganizationID();

    public abstract String getName();

    @Nullable 
    public abstract LocalDate getMemberSince();

    @Nullable
    public abstract MemberOrganizationDescription getDescription();

    @Nullable
    public abstract MemberOrganizationLogos getLogos();

    @Nullable
    public abstract String getWebsite();

    @Nullable
    public abstract List<MemberOrganizationLevel> getLevels();

    public abstract List<OrganizationWGPA> getWgpas();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_MemberOrganization.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setOrganizationID(int organizationID);

        public abstract Builder setName(String name);

        public abstract Builder setMemberSince(@Nullable LocalDate memberSince);

        public abstract Builder setDescription(MemberOrganizationDescription description);

        public abstract Builder setLogos(@Nullable MemberOrganizationLogos logos);

        public abstract Builder setWebsite(@Nullable String website);

        public abstract Builder setLevels(@Nullable List<MemberOrganizationLevel> levels);

        public abstract Builder setWgpas(List<OrganizationWGPA> wgpas);

        public abstract MemberOrganization build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_MemberOrganization_MemberOrganizationDescription.Builder.class)
    public abstract static class MemberOrganizationDescription {

        @Nullable
        @JsonProperty("long")
        public abstract String getLongDescription();

        public static Builder builder() {
            return new AutoValue_MemberOrganization_MemberOrganizationDescription.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            @Nullable
            @JsonProperty("long")
            public abstract Builder setLongDescription(String longDescription);

            public abstract MemberOrganizationDescription build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_MemberOrganization_MemberOrganizationLogos.Builder.class)
    public abstract static class MemberOrganizationLogos {
        @Nullable
        public abstract String getPrint();

        @Nullable
        public abstract String getWeb();

        public static Builder builder() {
            return new AutoValue_MemberOrganization_MemberOrganizationLogos.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setPrint(@Nullable String print);

            public abstract Builder setWeb(@Nullable String web);

            public abstract MemberOrganizationLogos build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_MemberOrganization_MemberOrganizationLevel.Builder.class)
    public abstract static class MemberOrganizationLevel {
        @Nullable
        public abstract String getLevel();

        @Nullable
        public abstract String getDescription();

        @Nullable
        public abstract String getSortOrder();

        public static Builder builder() {
            return new AutoValue_MemberOrganization_MemberOrganizationLevel.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setLevel(@Nullable String level);

            public abstract Builder setDescription(@Nullable String description);

            public abstract Builder setSortOrder(@Nullable String sortOrder);

            public abstract MemberOrganizationLevel build();
        }
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_MemberOrganization_OrganizationWGPA.Builder.class)
    public abstract static class OrganizationWGPA {
        public abstract String getDocumentID();

        @Nullable
        public abstract String getDescription();

        @Nullable
        public abstract String getLevel();

        public abstract String getWorkingGroup();

        public static Builder builder() {
            return new AutoValue_MemberOrganization_OrganizationWGPA.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setDocumentID(String documentID);

            public abstract Builder setDescription(@Nullable String description);

            public abstract Builder setLevel(@Nullable String level);

            public abstract Builder setWorkingGroup(String workingGroup);

            public abstract OrganizationWGPA build();
        }
    }
}
