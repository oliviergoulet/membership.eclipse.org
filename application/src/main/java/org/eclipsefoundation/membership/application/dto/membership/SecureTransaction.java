/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.dto.membership;

import java.util.Objects;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.membership.application.namespace.TransactionType;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

@Table
@Entity
public class SecureTransaction extends BareNode {
    public static final DtoTable TABLE = new DtoTable(SecureTransaction.class, "st");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String token;
    private String associatedEntityId;
    @Enumerated(EnumType.STRING)
    private TransactionType associatedEntityType;
    @Column(name = "is_active")
    private boolean active;

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAssociatedEntityId() {
        return this.associatedEntityId;
    }

    public void setAssociatedEntityId(String associatedEntityId) {
        this.associatedEntityId = associatedEntityId;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public TransactionType getAssociatedEntityType() {
        return this.associatedEntityType;
    }

    public void setAssociatedEntityType(TransactionType type) {
        this.associatedEntityType = type;
    }

    public boolean getActive() {
        return this.active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(id, associatedEntityId, token);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        SecureTransaction other = (SecureTransaction) obj;
        return Objects.equals(this.id, other.id) && Objects.equals(this.associatedEntityId, other.associatedEntityId)
                && Objects.equals(this.token, other.token)
                && Objects.equals(this.associatedEntityType, other.associatedEntityType)
                && Objects.equals(this.active, other.active);
    }

    @Singleton
    public static class SecureTransactionFilter implements DtoFilter<SecureTransaction> {

        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement statement = builder.build(TABLE);

            if (isRoot) {
                String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
                if (StringUtils.isNumeric(id)) {
                    statement.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".id = ?",
                                    new Object[] { Long.valueOf(id) }));
                }

                String token = params.getFirst(MembershipFormAPIParameterNames.TOKEN.getName());
                if (StringUtils.isNotBlank(token)) {
                    statement.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".token = ?",
                                    new Object[] { token }));
                }

                String entityId = params.getFirst(MembershipFormAPIParameterNames.ASSOCIATED_ENTITY_ID.getName());
                if (StringUtils.isNotBlank(entityId)) {
                    statement.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".associatedEntityId = ?",
                                    new Object[] { entityId }));
                }

                String entityType = params.getFirst(MembershipFormAPIParameterNames.ASSOCIATED_ENTITY_TYPE.getName());
                if (StringUtils.isNotBlank(entityType)) {
                    statement.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".associatedEntityType = ?",
                                    new Object[] { TransactionType.valueOf(entityType.toUpperCase()) }));
                }

                String active = params.getFirst(MembershipFormAPIParameterNames.ACTIVE.getName());
                if (StringUtils.isNotBlank(active)) {
                    statement.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".active = ?",
                                    new Object[] { Boolean.valueOf(active) }));
                }
            }

            return statement;
        }

        @Override
        public Class<SecureTransaction> getType() {
            return SecureTransaction.class;
        }
    }
}
