/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.config;

import java.util.List;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/**
 * A ConfgMapper used to capture all configs related to the FormStateFilter. Includes an enabled flag, as well as the list of allowed admin
 * roles.
 */
@ConfigMapping(prefix = "eclipse.membership-application.form-state")
public interface FormStateConfig {

    @WithDefault("true")
    boolean enabled();

    @WithDefault("eclipsefdn_membership_portal_admin")
    List<String> allowedAdminRoles();
}
