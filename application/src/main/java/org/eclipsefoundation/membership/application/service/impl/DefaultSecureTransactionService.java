/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.application.dto.membership.SecureTransaction;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.membership.application.namespace.TransactionType;
import org.eclipsefoundation.membership.application.service.SecureTransactionService;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;

@ApplicationScoped
public class DefaultSecureTransactionService implements SecureTransactionService {

    @Inject
    DefaultHibernateDao dao;
    @Inject
    FilterService filters;

    @Override
    public SecureTransaction getActiveTrackedTransaction(MultivaluedMap<String, String> params, RequestWrapper wrap) {
        List<SecureTransaction> results = dao
                .get(new RDBMSQuery<>(wrap, filters.get(SecureTransaction.class), params));

        if (results == null || results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }

    @Override
    public void deactivateTrackedTransaction(SecureTransaction transaction, RequestWrapper wrap) {
        transaction.setActive(false);
        dao.add(new RDBMSQuery<>(wrap, filters.get(SecureTransaction.class)), Arrays.asList(transaction));
    }

    @Override
    public SecureTransaction generateUntrackedTransaction(String associatedEntityId, TransactionType type) {
        SecureTransaction transaction = new SecureTransaction();
        transaction.setToken(UUID.randomUUID().toString());
        transaction.setAssociatedEntityId(associatedEntityId);
        transaction.setAssociatedEntityType(type);
        transaction.setActive(true);
        return transaction;
    }

    @Override
    public MultivaluedMap<String, String> createTransactionQueryParams(Long transactionId, String token,
            String formId, TransactionType type) {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), Long.toString(transactionId));
        params.add(MembershipFormAPIParameterNames.TOKEN.getName(), token);
        params.add(MembershipFormAPIParameterNames.ASSOCIATED_ENTITY_ID.getName(), formId);
        params.add(MembershipFormAPIParameterNames.ASSOCIATED_ENTITY_TYPE.getName(), type.name());
        params.add(MembershipFormAPIParameterNames.ACTIVE.getName(), Boolean.toString(true));
        return params;
    }
}
