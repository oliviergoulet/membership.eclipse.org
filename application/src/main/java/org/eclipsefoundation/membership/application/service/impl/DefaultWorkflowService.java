/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.service.impl;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentHashMap.KeySetView;
import java.util.function.Function;
import java.util.function.Predicate;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.helper.TransformationHelper;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.UserSearchParams;
import org.eclipsefoundation.efservices.services.AccountService;
import org.eclipsefoundation.foundationdb.client.model.OrganizationData;
import org.eclipsefoundation.membership.application.api.FoundationAPI;
import org.eclipsefoundation.membership.application.dao.EclipsePersistenceDAO;
import org.eclipsefoundation.membership.application.dto.eclipse.AccountRequests;
import org.eclipsefoundation.membership.application.dto.eclipse.AccountRequests.AccountRequestsCompositeId;
import org.eclipsefoundation.membership.application.dto.membership.BaseContact;
import org.eclipsefoundation.membership.application.dto.membership.FormOrganization;
import org.eclipsefoundation.membership.application.helper.EncryptionHelper;
import org.eclipsefoundation.membership.application.model.AccountCreationMailerData;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.membership.application.namespace.TransactionType;
import org.eclipsefoundation.membership.application.service.MailerService;
import org.eclipsefoundation.membership.application.service.WorkflowService;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Default implementation of the ContactService. Gets emailable contacts, checks if they have opted out, and if they have an Eclipse
 * account.
 */
@ApplicationScoped
public class DefaultWorkflowService implements WorkflowService {
    public static final Logger LOGGER = LoggerFactory.getLogger(DefaultWorkflowService.class);

    private static final String ACCOUNT_CREATION_TOKEN = "CONFIRM_SUCCESS";
    private static final String VALID_PASSWORD_CHARACTERS = "0123456789abcdefghijklmnopqrstuvwxyz-_ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*.";

    @Inject
    MailerService mailer;
    @Inject
    AccountService account;

    @Inject
    DefaultHibernateDao dao;
    @Inject
    EclipsePersistenceDAO eclipseDB;
    @Inject
    FilterService filters;
    @Inject
    RequestWrapper wrap;
    @Inject
    CachingService cache;
    @Inject
    APIMiddleware middleware;

    @Inject
    EncryptionHelper crypt;

    @RestClient
    FoundationAPI fdn;

    private SecureRandom secureRandom;

    @Override
    public void createMissingUserAccounts(List<? extends BaseContact> contacts, String uname, TransactionType type, String formIdentifier) {

        // check that we don't have a form from a deleted user
        Optional<EfUser> applicant = account.fetchUserByUsername(uname, false);
        if (applicant.isEmpty()) {
            LOGGER.warn("Form with id {} in bad state. No applicant found.", TransformationHelper.formatLog(formIdentifier));
        } else {
            // force cache to dump any existing user search results. While overkill, this ensures we won't have empty cache entries
            cache.fuzzyRemove("search", EfUser.class);
            // get prospective users, and get unique emails
            getProspectiveUsers(contacts).stream().filter(distinctByKey(BaseContact::getEmail)).forEach(c -> {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Creating account request for {} {} - {}", c.getfName(), c.getlName(), c.getEmail());
                }
                // if the user has an active pending account request, don't create another one
                if (userHasActiveAccountRequest(c)) {
                    LOGGER.info("There is currently an active account request for mail '{}', skipping", c.getEmail());
                    return;
                }

                // generate the account request and save it
                AccountRequests ar = generateNewAccountRequest(c);
                eclipseDB.add(new RDBMSQuery<>(wrap, filters.get(AccountRequests.class)), Arrays.asList(ar));

                // Get recipient before encoding. Encoding for optout URLs in email
                String recipient = c.getEmail();
                c.setEmail(c.getEmail().replaceAll("[+]", "%2B"));

                // Create mailer data and send
                AccountCreationMailerData data = new AccountCreationMailerData(applicant.get().getFullName(), c, type);
                mailer.sendAccountCreationToReps(recipient, data);
            });
        }
    }

    @Override
    public List<BaseContact> getProspectiveUsers(List<? extends BaseContact> formContacts) {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.put(MembershipFormAPIParameterNames.EMAILS.getName(), formContacts.stream().map(BaseContact::getEmail).toList());
        // filter contacts and then cast to parent for more generic usage
        return formContacts.stream().filter(c -> !hasEclipseAccount(c.getEmail())).map(c -> (BaseContact) c).toList();
    }

    @Override
    public boolean doesOrganizationExist(FormOrganization formOrg) {
        // Fetch organizations to check if applying organization already exists
        Optional<List<OrganizationData>> orgResponse = cache
                .get("all", null, OrganizationData.class, () -> middleware.getAll(fdn::getAllOrganizations, OrganizationData.class))
                .getData();
        return formOrg == null || orgResponse
                .orElseGet(Collections::emptyList)
                .stream()
                .noneMatch(o -> o.getName1() != null && o.getName1().equalsIgnoreCase(formOrg.getLegalName()));
    }

    /**
     * Using the contacts email, check if there is a pending account creation token for the given user.
     * 
     * @param c form contact to check for active account requests for
     * @return true if the user has an active request, false otherwise.
     */
    private boolean userHasActiveAccountRequest(BaseContact c) {
        // set up params to check for account requests
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), c.getEmail());
        params.add(MembershipFormAPIParameterNames.TOKEN.getName(), ACCOUNT_CREATION_TOKEN);
        // check if there are any active account creation requests for mail
        return eclipseDB.count(new RDBMSQuery<>(wrap, filters.get(AccountRequests.class), params)) > 0;
    }

    /**
     * Generates a new account request for the given user, using a random password and the contact information gathered in the form to
     * generate the initial request.
     * 
     * @param c the form contact that needs a new Eclipse account
     * @return the new accounts record. This record is not persisted, and needs to be submitted separately.
     */
    private AccountRequests generateNewAccountRequest(BaseContact c) {
        AccountRequests ar = new AccountRequests();
        AccountRequestsCompositeId id = new AccountRequestsCompositeId();
        id.setEmail(c.getEmail());
        id.setReqWhen(LocalDateTime.now());
        ar.setId(id);
        ar.setFname(c.getfName());
        ar.setLname(c.getlName());
        ar.setPassword(crypt.encryptAndEncodeValue(generateSecureRandomPassword()));
        ar.setToken(ACCOUNT_CREATION_TOKEN);
        ar.setIp("0.0.0.0");
        return ar;
    }

    /**
     * Generates a cryprographically secure string to be used as a temporary password. The password by default is 32 characters long to give
     * it decently strong .
     * 
     * @return a 32 character long secure random passoword.
     */
    private String generateSecureRandomPassword() {
        return getSecureRandom()
                .ints(32, 0, VALID_PASSWORD_CHARACTERS.length())
                .mapToObj(VALID_PASSWORD_CHARACTERS::charAt)
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                .toString();
    }

    /**
     * Thread safe generator of secure randoms to be used for generating temporary passwords. A fallback to a standard securerandom was
     * provided, but it shouldn't be needed as secure algorithms should always be available.
     * 
     * @return a cryptographically secure random instance to generate temporary passwords
     */
    private SecureRandom getSecureRandom() {
        synchronized (this) {
            if (secureRandom == null) {
                try {
                    secureRandom = SecureRandom.getInstanceStrong();
                } catch (NoSuchAlgorithmException e) {
                    LOGGER.warn("Cannot create best secure random, using standard secure random", e);
                    // while not perfect, this is "good enough" to still securely generate strings
                    secureRandom = new SecureRandom();
                }
            }
            return secureRandom;
        }
    }

    /**
     * Performs a lookup using the user email to determine whether they have an eclipse account.
     * 
     * @param userEmail The email to lookup
     * @return true if account exists, false if not
     */
    private boolean hasEclipseAccount(String userEmail) {
        return account.performUserSearch(UserSearchParams.builder().setMail(userEmail).build()).isPresent();
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        KeySetView<Object, Boolean> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }
}
