/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.namespace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import jakarta.inject.Singleton;

import org.eclipsefoundation.core.namespace.UrlParameterNamespace;

@Singleton
public final class MembershipFormAPIParameterNames implements UrlParameterNamespace {
    public static final UrlParameter USER_ID = new UrlParameter("userID");
    public static final UrlParameter FORM_ID = new UrlParameter("formID");
    public static final UrlParameter FORM_IDS = new UrlParameter("formIDs");
    public static final UrlParameter TRANSACTION_ID = new UrlParameter("transactionID");
    public static final UrlParameter FORM_STATE = new UrlParameter("state");
    public static final UrlParameter APPLICATION_GROUP = new UrlParameter("application_group");
    public static final UrlParameter BEFORE_DATE_UPDATED_IN_MILLIS = new UrlParameter("beforeDateUpdatedMillis");
    public static final UrlParameter AFTER_DATE_UPDATED_IN_MILLIS = new UrlParameter("afterDateUpdatedMillis");
    public static final UrlParameter TOKEN = new UrlParameter("token");
    public static final UrlParameter EMAIL = new UrlParameter("email");
    public static final UrlParameter EMAILS = new UrlParameter("emails");
    public static final UrlParameter ASSOCIATED_ENTITY_ID = new UrlParameter("associated_entity_id");
    public static final UrlParameter ASSOCIATED_ENTITY_TYPE = new UrlParameter("associated_entity_type");
    public static final UrlParameter ACTIVE = new UrlParameter("active");
    public static final UrlParameter ORGANIZATION_ID = new UrlParameter("org_id");

    private static final List<UrlParameter> params = Collections
            .unmodifiableList(Arrays
                    .asList(USER_ID, FORM_ID, FORM_IDS, FORM_STATE, BEFORE_DATE_UPDATED_IN_MILLIS, AFTER_DATE_UPDATED_IN_MILLIS,
                            APPLICATION_GROUP, TOKEN, TRANSACTION_ID, EMAIL, EMAILS, ASSOCIATED_ENTITY_ID, ASSOCIATED_ENTITY_TYPE, ACTIVE,
                            ORGANIZATION_ID));

    @Override
    public List<UrlParameter> getParameters() {
        return new ArrayList<>(params);
    }
}
