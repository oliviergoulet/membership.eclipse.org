/*******************************************************************************
 * Copyright (c) 2024 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipsefoundation.membership.application.dto.membership;

import java.time.ZonedDateTime;
import java.util.Objects;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;

import org.eclipsefoundation.membership.application.namespace.FormState;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.model.DtoTable;

/**
 * Working group application record for existing organizations. Contains base information needed to track the working group request and its
 * current state.
 */
@Table
@Entity
public class WorkingGroupApplication extends BareNode {
    public static final DtoTable TABLE = new DtoTable(WorkingGroupApplication.class, "wga");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String user;
    private Integer organizationId;
    private ZonedDateTime created;
    private ZonedDateTime updated;

    @NotNull(message = "Form state must be set")
    @Enumerated(EnumType.STRING)
    private FormState state;

    @Override
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the organizationId
     */
    public Integer getOrganizationId() {
        return organizationId;
    }

    /**
     * @param organizationId the organizationId to set
     */
    public void setOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * @return the created
     */
    public ZonedDateTime getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    /**
     * @return the updated
     */
    public ZonedDateTime getUpdated() {
        return updated;
    }

    /**
     * @param updated the updated to set
     */
    public void setUpdated(ZonedDateTime updated) {
        this.updated = updated;
    }

    /**
     * @return the state
     */
    public FormState getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(FormState state) {
        this.state = state;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(created, id, organizationId, state, updated, user);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        WorkingGroupApplication other = (WorkingGroupApplication) obj;
        return Objects.equals(created, other.created) && id == other.id && Objects.equals(organizationId, other.organizationId)
                && state == other.state && Objects.equals(updated, other.updated) && Objects.equals(user, other.user);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder
                .append("WorkingGroupApplication [id=")
                .append(id)
                .append(", user=")
                .append(user)
                .append(", organizationId=")
                .append(organizationId)
                .append(", created=")
                .append(created)
                .append(", updated=")
                .append(updated)
                .append(", state=")
                .append(state)
                .append("]");
        return builder.toString();
    }

}
