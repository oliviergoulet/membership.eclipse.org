/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.dto.membership;

import java.util.Objects;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.application.namespace.FormState;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

/**
 * Represents a view in the database for better query performance when creating report output
 * 
 * @author Martin Lowe
 *
 */
@Table(name = "v_FormStatusReport")
@Entity
public class FormStatusReport extends BareNode {
    public static final DtoTable TABLE = new DtoTable(FormStatusReport.class, "fsr");

    private String userId;
    @Id
    private String formId;
    private String membershipLevel;
    private String organizationName;
    private String registrationCountry;
    private Long dateCreated;
    private Long dateUpdated;
    private Long dateSubmitted;
    private FormState formState;
    private Integer workingGroupCount;
    private Integer formCompletionStage;

    @Override
    public Object getId() {
        return getFormId();
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the formId
     */
    public String getFormId() {
        return formId;
    }

    /**
     * @param formId the formId to set
     */
    public void setFormId(String formId) {
        this.formId = formId;
    }

    /**
     * @return the membershipLevel
     */
    public String getMembershipLevel() {
        return membershipLevel;
    }

    /**
     * @param membershipLevel the membershipLevel to set
     */
    public void setMembershipLevel(String membershipLevel) {
        this.membershipLevel = membershipLevel;
    }

    /**
     * @return the organizationName
     */
    public String getOrganizationName() {
        return organizationName;
    }

    /**
     * @param organizationName the organizationName to set
     */
    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    /**
     * @return the registrationCountry
     */
    public String getRegistrationCountry() {
        return registrationCountry;
    }

    /**
     * @param registrationCountry the registrationCountry to set
     */
    public void setRegistrationCountry(String registrationCountry) {
        this.registrationCountry = registrationCountry;
    }

    /**
     * @return the dateCreated
     */
    public Long getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the dateUpdated
     */
    public Long getDateUpdated() {
        return dateUpdated;
    }

    /**
     * @param dateUpdated the dateUpdated to set
     */
    public void setDateUpdated(Long dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    /**
     * @return the dateSubmitted
     */
    public Long getDateSubmitted() {
        return dateSubmitted;
    }

    /**
     * @param dateSubmitted the dateSubmitted to set
     */
    public void setDateSubmitted(Long dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
    }

    /**
     * @return the state
     */
    public FormState getFormState() {
        return formState;
    }

    /**
     * @param formState the state to set
     */
    public void setFormState(FormState formState) {
        this.formState = formState;
    }

    /**
     * @return the workingGroupCount
     */
    public Integer getWorkingGroupCount() {
        return workingGroupCount;
    }

    /**
     * @param workingGroupCount the workingGroupCount to set
     */
    public void setWorkingGroupCount(Integer workingGroupCount) {
        this.workingGroupCount = workingGroupCount;
    }

    /**
     * @return the formCompletionStage
     */
    public Integer getFormCompletionStage() {
        return formCompletionStage;
    }

    /**
     * @param formCompletionStage the formCompletionStage to set
     */
    public void setFormCompletionStage(Integer formCompletionStage) {
        this.formCompletionStage = formCompletionStage;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects
                .hash(userId, formId, membershipLevel, organizationName, registrationCountry, dateCreated, dateUpdated, dateSubmitted,
                        formState, workingGroupCount, formCompletionStage);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        FormStatusReport other = (FormStatusReport) obj;
        return Objects.equals(userId, other.userId) && Objects.equals(formId, other.formId)
                && Objects.equals(membershipLevel, other.membershipLevel) && Objects.equals(organizationName, other.organizationName)
                && Objects.equals(registrationCountry, other.registrationCountry) && Objects.equals(dateCreated, other.dateCreated)
                && Objects.equals(dateUpdated, other.dateUpdated) && Objects.equals(dateSubmitted, other.dateSubmitted)
                && Objects.equals(formState, other.formState) && Objects.equals(workingGroupCount, other.workingGroupCount)
                && Objects.equals(formCompletionStage, other.formCompletionStage);
    }

    @Singleton
    public static class FormStatusReportFilter implements DtoFilter<FormStatusReport> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            if (isRoot) {
                // ID check
                String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
                if (id != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".id = ?", new Object[] { id }));
                }
            }
            // form state check
            String formState = params.getFirst(MembershipFormAPIParameterNames.FORM_STATE.getName());
            if (formState != null) {
                stmt
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".formState = ?",
                                new Object[] { FormState.valueOf(formState.toUpperCase()) }));
            }
            return stmt;
        }

        @Override
        public Class<FormStatusReport> getType() {
            return FormStatusReport.class;
        }
    }
}
