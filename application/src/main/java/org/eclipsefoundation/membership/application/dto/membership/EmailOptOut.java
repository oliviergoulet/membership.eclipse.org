/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.dto.membership;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

@Table
@Entity
public class EmailOptOut extends BareNode {
    public static final DtoTable TABLE = new DtoTable(EmailOptOut.class, "eoo");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDateTime dateReceived;
    private String email;
    @Column(name = "transaction_id")
    private Long transactionId;

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateReceived() {
        return this.dateReceived;
    }

    public void setDateReceived(LocalDateTime dateReceived) {
        this.dateReceived = dateReceived;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getTransactionId() {
        return this.transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(id, dateReceived, email, transactionId);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        EmailOptOut other = (EmailOptOut) obj;
        return Objects.equals(this.id, other.id) && Objects.equals(this.email, other.email)
                && DateTimeHelper.looseCompare(this.dateReceived.atZone(ZoneId.of("UTC")), other.dateReceived.atZone(ZoneId.of("UTC")))
                && Objects.equals(this.transactionId, other.transactionId);
    }

    @Singleton
    public static class EmailOptOutFilter implements DtoFilter<EmailOptOut> {

        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement statement = builder.build(TABLE);

            if (isRoot) {
                String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
                if (StringUtils.isNumeric(id)) {
                    statement.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".id = ?",
                                    new Object[] { Long.valueOf(id) }));
                }

                String email = params.getFirst(MembershipFormAPIParameterNames.EMAIL.getName());
                if (StringUtils.isNotBlank(email)) {
                    statement.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".email = ?",
                                    new Object[] { email }));
                }

                List<String> emails = params.get(MembershipFormAPIParameterNames.EMAILS.getName());
                if (emails != null) {
                    statement.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".email IN ?",
                                    new Object[] { emails }));
                }

                String transactionId = params.getFirst(MembershipFormAPIParameterNames.TRANSACTION_ID.getName());
                if (StringUtils.isNumeric(transactionId)) {
                    statement.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".transactionId = ?",
                                    new Object[] { Long.valueOf(transactionId) }));
                }
            }

            return statement;
        }

        @Override
        public Class<EmailOptOut> getType() {
            return EmailOptOut.class;
        }
    }
}
