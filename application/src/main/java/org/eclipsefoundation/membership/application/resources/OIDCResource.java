/**
 * Copyright (c) 2021, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.application.resources;

import java.net.URI;
import java.net.URISyntaxException;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.namespace.RequestHeaderNames;

import io.quarkus.security.Authenticated;
import io.smallrye.jwt.auth.principal.DefaultJWTCallerPrincipal;

/**
 * Handles OIDC routing for the request.
 *
 * @author Martin Lowe
 */
@Path("")
public class OIDCResource extends AbstractRESTResource {

    @ConfigProperty(name = "eclipse.membership.base-url", defaultValue = "/")
    String baseUrl;

    @GET
    @Authenticated
    @Path("/login")
    public Response routeLogin() throws URISyntaxException {
        return redirect(baseUrl);
    }

    /**
     * While OIDC plugin takes care of actual logout, a route is needed to properly reroute anon user to home page.
     *
     * @throws URISyntaxException
     */
    @GET
    @Path("/logout")
    public Response routeLogout() throws URISyntaxException {
        return redirect(baseUrl);
    }

    @GET
    @Path("userinfo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserInfo(@HeaderParam(value = RequestHeaderNames.CSRF_TOKEN) String csrf) {

        if (!ident.isAnonymous()) {
            // cast the principal to a JWT token (which is the type for OIDC)
            DefaultJWTCallerPrincipal defaultPrin = (DefaultJWTCallerPrincipal) ident.getPrincipal();
            // create wrapper around data for output
            InfoWrapper uiw = new InfoWrapper();
            uiw.name = defaultPrin.getName();
            uiw.givenName = defaultPrin.getClaim("given_name");
            uiw.familyName = defaultPrin.getClaim("family_name");
            uiw.email = defaultPrin.getClaim("email");

            return Response.ok(uiw).build();
        } else {
            return Response.noContent().build();
        }
    }

    @GET
    @Path("csrf")
    public Response generateCSRF() {
        return Response.ok().build();
    }

    private Response redirect(String location) throws URISyntaxException {
        return Response.temporaryRedirect(new URI(location)).build();
    }

    public static class InfoWrapper {
        String name;
        String givenName;
        String familyName;
        String email;

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the givenName
         */
        public String getGivenName() {
            return givenName;
        }

        /**
         * @param givenName the givenName to set
         */
        public void setGivenName(String givenName) {
            this.givenName = givenName;
        }

        /**
         * @return the familyName
         */
        public String getFamilyName() {
            return familyName;
        }

        /**
         * @param familyName the familyName to set
         */
        public void setFamilyName(String familyName) {
            this.familyName = familyName;
        }

        /**
         * @return the email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @param email the email to set
         */
        public void setEmail(String email) {
            this.email = email;
        }
    }
}
