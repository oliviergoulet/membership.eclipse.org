/*******************************************************************************
 * Copyright (c) 2024 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipsefoundation.membership.application.model;

import java.time.ZonedDateTime;
import java.util.List;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_WorkingGroupApplicationData.Builder.class)
public abstract class WorkingGroupApplicationData {

    @Nullable
    public abstract Long getId();

    public abstract String getUser();

    public abstract Integer getOrgId();

    public abstract String getState();

    @Nullable
    public abstract List<WorkingGroupContact> getContacts();

    @Nullable
    public abstract ZonedDateTime getCreated();

    @Nullable
    public abstract ZonedDateTime getUpdated();

    public static Builder builder() {
        return new AutoValue_WorkingGroupApplicationData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setId(@Nullable Long id);

        public abstract Builder setUser(String user);

        public abstract Builder setOrgId(Integer orgId);

        public abstract Builder setCreated(@Nullable ZonedDateTime created);

        public abstract Builder setUpdated(@Nullable ZonedDateTime updated);

        public abstract Builder setState(String state);

        public abstract Builder setContacts(@Nullable List<WorkingGroupContact> contacts);

        public abstract WorkingGroupApplicationData build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_WorkingGroupApplicationData_WorkingGroupContact.Builder.class)
    public abstract static class WorkingGroupContact {
        @Nullable
        public abstract String getUser();

        @Nullable
        public abstract String getFirstName();

        @Nullable
        public abstract String getLastName();

        @Nullable
        public abstract String getMail();

        public abstract String getWorkingGroupAlias();

        public abstract String getWorkingGroupLevel();

        public abstract boolean getIsSigningAuthority();

        public static Builder builder() {
            return new AutoValue_WorkingGroupApplicationData_WorkingGroupContact.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            public abstract Builder setUser(@Nullable String user);

            public abstract Builder setFirstName(@Nullable String firstName);

            public abstract Builder setLastName(@Nullable String lastName);

            public abstract Builder setMail(@Nullable String mail);

            public abstract Builder setWorkingGroupAlias(String workingGroupAlias);

            public abstract Builder setWorkingGroupLevel(String workingGroupLevel);

            public abstract Builder setIsSigningAuthority(boolean isSigningAuthority);

            public abstract WorkingGroupContact build();
        }
    }
}
