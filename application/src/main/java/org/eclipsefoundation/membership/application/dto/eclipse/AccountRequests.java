/**
 * Copyright (c) 2023, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.application.dto.eclipse;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

/**
 * @author Martin Lowe
 *
 */
@Table(name = "account_requests")
@Entity
public class AccountRequests extends BareNode {
    public static final DtoTable TABLE = new DtoTable(AccountRequests.class, "ar");

    @EmbeddedId
    private AccountRequestsCompositeId id;
    @Column(name = "new_email")
    private String newEmail;
    private String fname;
    private String lname;
    private String password;
    private String ip;
    private String token;

    /**
     * @return the id
     */
    @Override
    public AccountRequestsCompositeId getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(AccountRequestsCompositeId id) {
        this.id = id;
    }

    /**
     * @return the newEmail
     */
    public String getNewEmail() {
        return newEmail;
    }

    /**
     * @param newEmail the newEmail to set
     */
    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

    /**
     * @return the fname
     */
    public String getFname() {
        return fname;
    }

    /**
     * @param fname the fname to set
     */
    public void setFname(String fname) {
        this.fname = fname;
    }

    /**
     * @return the lname
     */
    public String getLname() {
        return lname;
    }

    /**
     * @param lname the lname to set
     */
    public void setLname(String lname) {
        this.lname = lname;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(fname, id, ip, lname, newEmail, password, token);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AccountRequests other = (AccountRequests) obj;
        return Objects.equals(fname, other.fname) && Objects.equals(id, other.id) && Objects.equals(ip, other.ip)
                && Objects.equals(lname, other.lname) && Objects.equals(newEmail, other.newEmail)
                && Objects.equals(password, other.password) && Objects.equals(token, other.token);
    }

    @Embeddable
    public static class AccountRequestsCompositeId implements Serializable {
        private static final long serialVersionUID = -6301337509368582680L;

        private String email;
        @Column(name = "req_when")
        private LocalDateTime reqWhen;

        /**
         * @return the email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @param email the email to set
         */
        public void setEmail(String email) {
            this.email = email;
        }

        /**
         * @return the reqWhen
         */
        public LocalDateTime getReqWhen() {
            return reqWhen;
        }

        /**
         * @param reqWhen the reqWhen to set
         */
        public void setReqWhen(LocalDateTime reqWhen) {
            this.reqWhen = reqWhen;
        }

        @Override
        public int hashCode() {
            return Objects.hash(email, reqWhen);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            AccountRequestsCompositeId other = (AccountRequestsCompositeId) obj;
            return Objects.equals(email, other.email) && Objects.equals(reqWhen, other.reqWhen);
        }

    }

    @Singleton
    public static class AccountRequestsFilter implements DtoFilter<AccountRequests> {

        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
            if (StringUtils.isNotBlank(id)) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".id.email = ?", new Object[] { id }));
            }
            String token = params.getFirst(MembershipFormAPIParameterNames.TOKEN.getName());
            if (StringUtils.isNotBlank(token)) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".token = ?", new Object[] { token }));
            }
            return stmt;
        }

        @Override
        public Class<AccountRequests> getType() {
            return AccountRequests.class;
        }
    }
}
