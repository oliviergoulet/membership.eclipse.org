/*******************************************************************************
 * Copyright (c) 2024 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipsefoundation.membership.application.resources;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.exception.ApplicationException;
import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.core.helper.TransformationHelper;
import org.eclipsefoundation.core.model.CacheWrapper;
import org.eclipsefoundation.core.model.ParameterizedCacheKey;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.core.service.LoadingCacheManager;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.WorkingGroup;
import org.eclipsefoundation.efservices.services.AccountService;
import org.eclipsefoundation.membership.application.api.MembershipAPI;
import org.eclipsefoundation.membership.application.api.models.MemberOrganization;
import org.eclipsefoundation.membership.application.dto.membership.SecureTransaction;
import org.eclipsefoundation.membership.application.dto.membership.WorkingGroupApplication;
import org.eclipsefoundation.membership.application.dto.membership.WorkingGroupApplicationContact;
import org.eclipsefoundation.membership.application.model.WorkingGroupApplicationData;
import org.eclipsefoundation.membership.application.model.WorkingGroupApplicationData.WorkingGroupContact;
import org.eclipsefoundation.membership.application.namespace.FormState;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.membership.application.namespace.TransactionType;
import org.eclipsefoundation.membership.application.service.MailerService;
import org.eclipsefoundation.membership.application.service.SecureTransactionService;
import org.eclipsefoundation.membership.application.service.WorkflowService;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.security.Authenticated;
import jakarta.annotation.security.PermitAll;
import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.ServerErrorException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * REST resources for managing working group applications for existing organizations. This was kept separate from the standard application
 * flow for new memebrs to keep the logic less complicated in both use cases.
 * 
 * @author Martin Lowe
 */
@Authenticated
@Path("working_groups/application")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class WorkingGroupApplicationResources extends AbstractRESTResource {
    public static final Logger LOGGER = LoggerFactory.getLogger(WorkingGroupApplicationResources.class);

    @Inject
    LoadingCacheManager lcm;
    @Inject
    AccountService dus;

    @Inject
    SecureTransactionService transactionService;
    @Inject
    WorkflowService wfs;
    @Inject
    MailerService mailer;

    @RestClient
    MembershipAPI memberApi;

    /**
     * Retrieves all applications and returns them to the user.
     * 
     * @return list of applications for the current user
     */
    @GET
    public List<WorkingGroupApplicationData> getApplications() {
        return getApplicationData(null);
    }

    /**
     * Create a new working group application for the passed request.
     * 
     * @param req request containing required information to generate a new request
     * @return the request that was registered within the system
     */
    @POST
    public WorkingGroupApplicationData createApplication(WorkingGroupApplicationData req) {
        if (req == null) {
            throw new BadRequestException("An application is required to generate a new record");
        }
        if (req.getContacts() == null) {
            throw new BadRequestException("Contact should be included for new requests");
        }

        // persist the base application
        WorkingGroupApplication wga = new WorkingGroupApplication();
        wga.setOrganizationId(req.getOrgId());
        wga.setState(FormState.INPROGRESS);
        wga.setUser(req.getUser());
        wga.setCreated(req.getCreated() != null ? req.getCreated() : DateTimeHelper.now());
        wga.setUpdated(req.getUpdated() != null ? req.getUpdated() : DateTimeHelper.now());
        List<WorkingGroupApplication> persistedApplication = dao
                .add(new RDBMSQuery<>(wrap, filters.get(WorkingGroupApplication.class)), Arrays.asList(wga));
        if (persistedApplication == null || persistedApplication.isEmpty()) {
            throw new ServerErrorException("Error while saving the application for the new form", Status.INTERNAL_SERVER_ERROR);
        }

        // update contacts for application
        updateAssociatedContactsForApplication(req, persistedApplication.get(0));

        // recalculate the cache for the list entry
        cache.fuzzyRemove(ALL_CACHE_PLACEHOLDER, WorkingGroupApplicationData.class);
        // convert the data back into a data format and return
        return convertToData(persistedApplication.get(0));
    }

    /**
     * Retrieve a single working group entry and return it.
     * 
     * @param id the ID of the working group form to retrieve
     * @return the form if it exists.
     */
    @GET
    @Path("{id}")
    public Response getSingleApplication(@PathParam("id") String id) {
        // check ID is in proper format
        if (!StringUtils.isNumeric(id)) {
            throw new BadRequestException("Application ID must be numeric");
        }

        // attempt to retrieve cached application data
        List<WorkingGroupApplicationData> results = getApplicationData(Long.valueOf(id));
        if (results == null || results.isEmpty()) {
            throw new NotFoundException("No working group applications found for id: " + id);
        }
        // check that the user owns the form before allowing access
        checkAccessForWorkingGroupApplication(results.get(0));
        // return the checked data
        return Response.ok(results.get(0)).build();
    }

    /**
     * Update an existing working group request matched on the ID. Will update the request and return the newly saved entity back to the
     * requester.
     * 
     * @param id the ID of the form being updated
     * @param req the payload containing the updated form data
     * @return the updated and saved form data
     */
    @PUT
    @Path("{id}")
    public WorkingGroupApplicationData updateApplication(@PathParam("id") String id, WorkingGroupApplicationData req) {
        // checks that an application exists for params and is in a valid state
        WorkingGroupApplication wga = checkAndRetrieveApplication(id, req);

        // update + persist the base application
        wga.setOrganizationId(req.getOrgId());
        wga.setState(FormState.getByName(req.getState()));
        wga.setUser(req.getUser());
        wga.setCreated(req.getCreated() != null ? req.getCreated() : DateTimeHelper.now());
        wga.setUpdated(req.getUpdated() != null ? req.getUpdated() : DateTimeHelper.now());
        List<WorkingGroupApplication> persistedApplication = dao
                .add(new RDBMSQuery<>(wrap, filters.get(WorkingGroupApplication.class)), Arrays.asList(wga));
        if (persistedApplication == null || persistedApplication.isEmpty()) {
            throw new ServerErrorException("Error while saving the application for the updated form", Status.INTERNAL_SERVER_ERROR);
        }

        // update contacts for application
        updateAssociatedContactsForApplication(req, persistedApplication.get(0));

        // recalculate the cache for the list entry + associated entries
        cache.fuzzyRemove(ALL_CACHE_PLACEHOLDER, WorkingGroupApplicationData.class);
        cache.fuzzyRemove(id, WorkingGroupApplicationContact.class);
        // convert the data back into a data format and return
        return convertToData(persistedApplication.get(0));
    }

    @POST
    @Path("{id}/complete")
    public Response complete(@PathParam("id") Long id) {
        // checks that an application exists for params and is in a valid state
        List<WorkingGroupApplication> wgas = getApplications(null);
        if (wgas == null || wgas.isEmpty()) {
            throw new NotFoundException("No form exists for the current user with the given ID or their organization");
        }
        // get the current form being completed
        Optional<WorkingGroupApplication> wgaOpt = wgas.stream().filter(app -> app.getId().equals(id)).findFirst();
        if (wgaOpt.isEmpty()) {
            throw new NotFoundException("No form exists for the current user with the given ID");
        }
        WorkingGroupApplication wga = wgaOpt.get();
        if (wga.getState() != FormState.INPROGRESS) {
            throw new BadRequestException("Form must be in progress to submit");
        }

        // get the associated contacts with the request
        List<WorkingGroupApplicationContact> contacts = getApplicationContacts(wga);
        if (contacts.isEmpty()) {
            throw new BadRequestException("Contacts are required to submit the form");
        }
        // check that there is a signing authority
        if (contacts.stream().noneMatch(WorkingGroupApplicationContact::isSigningAuthority)) {
            throw new BadRequestException("No signing authority is present for the current form");
        }

        // retrieve organization information from the membership portal to check for currently active WG memberships
        String idStr = Integer.toString(wga.getOrganizationId());
        CacheWrapper<MemberOrganization> orgWrapper = cache
                .get(idStr, null, MemberOrganization.class, () -> memberApi.getOrganization(wga.getOrganizationId()));
        if (orgWrapper.getErrorType().isPresent()) {
            throw new ApplicationException("Could not load the member organization data for organization: " + idStr);
        }
        Optional<MemberOrganization> org = orgWrapper.getData();
        if (org.isEmpty()) {
            throw new NotFoundException("No active member found for passed organization ID: " + idStr);
        }

        // check currently active and submitted applications for overlap
        checkApplicationForConstraintViolations(id, wgas, contacts, org.get());

        // Create new transaction and add to DB
        SecureTransaction transaction = transactionService.generateUntrackedTransaction(Long.toString(id), TransactionType.WG_APP);
        dao.add(new RDBMSQuery<>(wrap, filters.get(SecureTransaction.class)), Arrays.asList(transaction));

        // send email to membership
        mailer.sendWorkingGroupApplicationNotification(wga, contacts, org.get(), transaction);

        // update the form
        wga.setState(FormState.SUBMITTED);
        // persist the submitted form
        List<WorkingGroupApplication> updatedApplication = dao
                .add(new RDBMSQuery<>(wrap, filters.get(WorkingGroupApplication.class)), Arrays.asList(wga));
        if (updatedApplication == null || updatedApplication.isEmpty()) {
            throw new ApplicationException("Error while updating the application in data set");
        }
        return Response.ok().build();
    }

    @GET
    @PermitAll
    @Path("{id}/{transactionId}")
    public Response validateForm(@PathParam("id") String formId, @PathParam("transactionId") Long transactionId,
            @QueryParam("token") String token, @QueryParam("state") FormState state) {
        if (StringUtils.isBlank(token)) {
            throw new BadRequestException("Param 'token' is required to use this endpoint");
        }
        if (state == null) {
            throw new BadRequestException("Param 'state' is required to use this endpoint");
        }

        // Verify transaction is valid and active
        MultivaluedMap<String, String> params = transactionService
                .createTransactionQueryParams(transactionId, token, formId, TransactionType.WG_APP);
        SecureTransaction formTransaction = transactionService.getActiveTrackedTransaction(params, wrap);
        if (formTransaction == null) {
            return Response.status(Response.Status.NOT_FOUND.getStatusCode()).build();
        }

        // Must set form state to null otherwise it will include the state from the query params
        params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), formId);
        params.add(MembershipFormAPIParameterNames.FORM_STATE.getName(), null);
        List<WorkingGroupApplication> formResults = dao.get(new RDBMSQuery<>(wrap, filters.get(WorkingGroupApplication.class), params));
        if (formResults == null) {
            return Response.serverError().build();
        } else if (formResults.isEmpty()) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Form ID: {} - No form found", TransformationHelper.formatLog(formId));
            }
            return Response.status(Response.Status.NOT_FOUND.getStatusCode()).build();
        }
        // retrieve contacts that will be passed to user creation calls
        MultivaluedMap<String, String> formParams = new MultivaluedMapImpl<>();
        formParams.add(MembershipFormAPIParameterNames.FORM_ID.getName(), formId);
        List<WorkingGroupApplicationContact> contacts = dao
                .get(new RDBMSQuery<>(wrap, filters.get(WorkingGroupApplicationContact.class), formParams));

        // Update form with new state
        formResults.get(0).setState(state);
        List<WorkingGroupApplication> updatedFormResults = dao
                .add(new RDBMSQuery<>(wrap, filters.get(WorkingGroupApplication.class), params), formResults);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Form with id {} set to {}", TransformationHelper.formatLog(formId), state);
        }

        // Set transaction to inactive
        transactionService.deactivateTrackedTransaction(formTransaction, wrap);

        // If form is rejected, exit
        if (state == FormState.REJECTED) {
            return Response.ok().build();
        }
        wfs.createMissingUserAccounts(contacts, updatedFormResults.get(0).getUser(), TransactionType.WG_APP, formId);

        return Response.ok().build();
    }

    /**
     * Checks the application and contacts for constraint violations, including the following:
     * 
     * <ul>
     * <li>Application contains requests for a group and level that is already active
     * <li>Application contains requests for a group that has a pending application
     * <li>That the requested group and level combinations are valid
     * </ul>
     * 
     * @param id the unique ID for the application being validated
     * @param wgas all current applications for the given organization
     * @param contacts the contacts associated with the current request
     * @param org the organization record associated with the request
     */
    private void checkApplicationForConstraintViolations(Long id, List<WorkingGroupApplication> wgas,
            List<WorkingGroupApplicationContact> contacts, MemberOrganization org) {
        // if there is already a working group participation agreement that matches one of the contacts, reject
        // this will allow for a different level in an existing group to allow people to "down/upgrade" their membership
        List<String> existingWGViolations = org
                .getWgpas()
                .stream()
                .flatMap(wgpa -> contacts
                        .stream()
                        .filter(c -> !c.isSigningAuthority() && wgpa.getWorkingGroup().equalsIgnoreCase(c.getWorkingGroupAlias())
                                && wgpa.getLevel().equalsIgnoreCase(c.getWorkingGroupLevel()))
                        .map(WorkingGroupApplicationContact::getWorkingGroupAlias))
                .distinct()
                .toList();
        if (!existingWGViolations.isEmpty()) {
            throw new BadRequestException(
                    "The organization already has a matching working group agreement with the following requested groups: "
                            + String.join(",", existingWGViolations));
        }

        // check that there isn't another application already submitted for the given WG
        List<String> groupsAppliedTo = contacts
                .stream()
                .filter(c -> !c.isSigningAuthority())
                .map(WorkingGroupApplicationContact::getWorkingGroupAlias)
                .toList();
        /**
         * Using all applications for the current user, filters to just those that aren't the current form and are submitted. Each of those
         * forms then have their contacts fetched to check which Working Groups have active applications. If any of the applied to working
         * groups match one there is a submitted application for, the application completion is rejected.
         */
        List<String> activeApplicationViolations = wgas
                .stream()
                .filter(app -> !app.getId().equals(id) && app.getState() == FormState.SUBMITTED)
                .flatMap(app -> getApplicationContacts(app).stream())
                .filter(c -> groupsAppliedTo.contains(c.getWorkingGroupAlias()))
                .map(WorkingGroupApplicationContact::getWorkingGroupAlias)
                .distinct()
                .toList();
        if (!activeApplicationViolations.isEmpty()) {
            throw new BadRequestException("The organization already has a matching working group application for the requested groups: "
                    + String.join(",", activeApplicationViolations));
        }

        // check that each of the referenced working groups are valid
        List<WorkingGroup> wgs = lcm.getList(ParameterizedCacheKey.builder().setId("all").setClazz(WorkingGroup.class).build());
        List<String> invalidContactWorkingGroups = contacts.stream().filter(c -> {
            // don't process signing authorities w/o WG stated
            if (c.isSigningAuthority() && StringUtils.isBlank(c.getWorkingGroupAlias())) {
                return false;
            }
            // check that the working groups exist
            Optional<WorkingGroup> group = wgs.stream().filter(wg -> wg.getAlias().equalsIgnoreCase(c.getWorkingGroupAlias())).findFirst();
            if (group.isEmpty()) {
                return true;
            }
            // check that the levels correspond to a real level
            return group.get().getLevels().stream().noneMatch(l -> l.getRelation().equalsIgnoreCase(c.getWorkingGroupLevel()));
        }).map(WorkingGroupApplicationContact::getWorkingGroupAlias).toList();
        if (!invalidContactWorkingGroups.isEmpty()) {
            throw new BadRequestException(
                    "Contacts with the following Working Groups have an invalid state: " + invalidContactWorkingGroups);
        }
    }

    /**
     * Updates contacts for the passed application, loading or creating the contact if it does not yet exist.
     * 
     * @param req the current update request
     * @param persistedApplication the persisted application to join a working group
     */
    private void updateAssociatedContactsForApplication(WorkingGroupApplicationData req, WorkingGroupApplication persistedApplication) {
        // grab a copy of the contacts or an empty list for use in the lookup below
        List<WorkingGroupApplicationContact> cs = getApplicationContacts(persistedApplication);
        // convert the contacts and persist them to the DB
        List<WorkingGroupApplicationContact> persistedContacts = dao
                .add(new RDBMSQuery<>(wrap, filters.get(WorkingGroupApplicationContact.class)),
                        req.getContacts().stream().map(reqContact -> {
                            // attempt to find an existing value, using it if present
                            WorkingGroupApplicationContact out = cs
                                    .stream()
                                    .filter(c -> c.getWorkingGroupAlias().equalsIgnoreCase(reqContact.getWorkingGroupAlias()))
                                    .findFirst()
                                    .orElseGet(WorkingGroupApplicationContact::new);
                            // set all fields back to the output
                            out.setEmail(reqContact.getMail());
                            out.setfName(reqContact.getFirstName());
                            out.setlName(reqContact.getLastName());
                            out.setSigningAuthority(reqContact.getIsSigningAuthority());
                            out.setWorkingGroupLevel(reqContact.getWorkingGroupLevel());
                            out.setWorkingGroupAlias(reqContact.getWorkingGroupAlias());
                            // set the form back to the contact to create the FK rel
                            out.setForm(persistedApplication);
                            return out;
                        }).toList());
        if (persistedContacts == null || persistedContacts.isEmpty()) {
            throw new ServerErrorException("Error while saving the contact for the updated form", Status.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Checks the passed request and ID before retrieving the associated application.
     * 
     * @param id unique serial ID of the application to retrieve
     * @param req the update request containing additional data/context
     * @return the appliation associated with the current request if present
     * @throws BadRequestException if the request is in an invalid state and cannot be actioned
     * @throws NotFoundException if there is no application associated with the given ID
     */
    private WorkingGroupApplication checkAndRetrieveApplication(String id, WorkingGroupApplicationData req) {
        // check that the current user has access to this form
        if (!StringUtils.isNumeric(id)) {
            throw new BadRequestException("Application ID must be numeric");
        }
        if (req == null) {
            throw new BadRequestException("An application is required to generate a new record");
        }
        if (!id.equals(req.getId().toString())) {
            throw new BadRequestException("Application IDs passed in the URL and request body do not match");
        }
        // check that the current user has access to this data
        checkAccessForWorkingGroupApplication(req);

        // check that each of the referenced working groups are valid
        List<WorkingGroup> wgs = lcm.getList(ParameterizedCacheKey.builder().setId("all").setClazz(WorkingGroup.class).build());
        List<String> invalidContactWorkingGroups = req.getContacts().stream().filter(c -> {
            // check that
            Optional<WorkingGroup> group = wgs.stream().filter(wg -> wg.getAlias().equalsIgnoreCase(c.getWorkingGroupAlias())).findFirst();
            if (group.isEmpty()) {
                return true;
            }
            return group.get().getLevels().stream().noneMatch(l -> l.getRelation().equalsIgnoreCase(c.getWorkingGroupLevel()));
        }).map(WorkingGroupContact::getWorkingGroupAlias).toList();
        if (!invalidContactWorkingGroups.isEmpty()) {
            throw new BadRequestException(
                    "Contacts with the following Working Groups have an invalid state: " + invalidContactWorkingGroups);
        }

        // get the base entities and check that we have results
        List<WorkingGroupApplication> wgas = dao
                .get(new RDBMSQuery<>(wrap, filters.get(WorkingGroupApplication.class), getParamMapIfSet(Long.parseLong(id))));
        if (wgas == null || wgas.isEmpty()) {
            throw new NotFoundException("No working group applications found for id: " + id);
        }
        // grab the entry that will be updated as part of this request
        WorkingGroupApplication wga = wgas.get(0);

        // check that the application isn't completed, as we shouldn't be updating completed applications
        if (!wga.getState().equals(FormState.INPROGRESS)) {
            throw new BadRequestException("Forms should not be updated after being submitted");
        }
        return wga;
    }

    /**
     * Retrieves a cached list of applications, fetching all applications if an ID is not provided, and convert them to the full data
     * format.
     * 
     * @param id the unique ID of the form to retrieve and return, or null if all applications should be retrieved instead.
     * @return the list of applications for the request, all applications if the ID is null, or the single application if an ID was passed.
     */
    private List<WorkingGroupApplicationData> getApplicationData(Long id) {
        // check the cache for applications
        CacheWrapper<List<WorkingGroupApplicationData>> data = cache
                .get(id != null ? Long.toString(id) : ALL_CACHE_PLACEHOLDER, getParamMapIfSet(id), WorkingGroupApplicationData.class,
                        () -> {
                            if (LOGGER.isDebugEnabled()) {
                                LOGGER.debug("Attempting to lookup up application data for {}", getParamMapIfSet(id));
                            }
                            // get the base entities and check that we have results
                            List<WorkingGroupApplication> wgas = getApplications(id);
                            if (wgas == null || wgas.isEmpty()) {
                                return Collections.emptyList();
                            }
                            if (LOGGER.isDebugEnabled()) {
                                LOGGER.debug("Found {} applications to convert", wgas.size());
                            }
                            return wgas.stream().map(this::convertToData).toList();
                        });
        return data.getData().orElse(null);
    }

    /**
     * Retrieves a cached list of applications, fetching all applications if an ID is not provided.
     * 
     * @param id the unique ID of the form to retrieve and return, or null if all applications should be retrieved instead.
     * @return the list of applications for the request, all applications if the ID is null, or the single application if an ID was passed.
     */
    private List<WorkingGroupApplication> getApplications(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Attempting to lookup up applications for {}", getParamMapIfSet(id));
        }
        // check the cache for applications
        CacheWrapper<List<WorkingGroupApplication>> data = cache
                .get(id != null ? Long.toString(id) : ALL_CACHE_PLACEHOLDER, getParamMapIfSet(id), WorkingGroupApplication.class, () -> {
                    RDBMSQuery<WorkingGroupApplication> q = new RDBMSQuery<>(wrap, filters.get(WorkingGroupApplication.class),
                            getParamMapIfSet(id));
                    q.setUseLimit(false);

                    // get the base entities and check that we have results
                    return dao.get(q);
                });
        return data.getData().orElse(null);
    }

    /**
     * Retrieves the application contacts for the given application.
     * 
     * @param wga the application for the current context
     * @return the list of contacts for the current application, or an empty list if there are none.
     */
    private List<WorkingGroupApplicationContact> getApplicationContacts(WorkingGroupApplication wga) {
        MultivaluedMap<String, String> params = getContactsParamMap(wga.getId());
        params.remove(DefaultUrlParameterNames.ID_PARAMETER_NAME);
        // get cached contacts for current application
        return cache.get(Long.toString(wga.getId()), params, WorkingGroupApplicationContact.class, () -> {
            RDBMSQuery<WorkingGroupApplicationContact> q = new RDBMSQuery<>(wrap, filters.get(WorkingGroupApplicationContact.class),
                    params);
            q.setUseLimit(false);
            q.setRoot(false);
            return dao.get(q);
        }).getData().orElse(Collections.emptyList());
    }

    /**
     * Converts DTO objects to model to be returned to the user.
     * 
     * @param wga the persisted application to convert to a data model.
     * @return the completed data model for the application.
     */
    private WorkingGroupApplicationData convertToData(WorkingGroupApplication wga) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Retrieving application contacts for form {}", wga.getId());
        }
        // get the contacts for the mentioned application
        List<WorkingGroupApplicationContact> contacts = getApplicationContacts(wga);
        // while this should not be possible, we don't want to fail to load entities when ones in a bad state
        List<WorkingGroupContact> outputContacts = null;
        if (contacts != null && !contacts.isEmpty()) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Converting {} application contacts to JSON format", contacts.size());
            }
            outputContacts = contacts
                    .stream()
                    .map(wgac -> WorkingGroupContact
                            .builder()
                            .setFirstName(wgac.getfName())
                            .setLastName(wgac.getlName())
                            .setMail(wgac.getEmail())
                            .setUser(wgac.getUser())
                            .setWorkingGroupAlias(wgac.getWorkingGroupAlias())
                            .setWorkingGroupLevel(wgac.getWorkingGroupLevel())
                            .setIsSigningAuthority(wgac.isSigningAuthority())
                            .build())
                    .toList();
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Converting application {} to JSON format", wga.getId());
        }
        // finish compiling and return
        return WorkingGroupApplicationData
                .builder()
                .setId(wga.getId())
                .setUpdated(wga.getUpdated())
                .setCreated(wga.getCreated())
                .setOrgId(wga.getOrganizationId())
                .setUser(wga.getUser())
                .setState(wga.getState().name())
                .setCreated(wga.getCreated())
                .setContacts(outputContacts)
                .build();
    }

    /**
     * Simple helper to generate a param inline for operations for the base form.
     * 
     * @param id the ID to filter by if required, or null
     * @return a map with the param set if the ID is set, or null if missing
     */
    private MultivaluedMap<String, String> getParamMapIfSet(Long id) {
        MultivaluedMap<String, String> params = getCommonParamMap();

        // if there is an ID set, add that to the param map
        if (id != null && id > 0) {
            params.add(DefaultUrlParameterNames.ID_PARAMETER_NAME, Long.toString(id));
        }
        return params;
    }

    private MultivaluedMap<String, String> getContactsParamMap(Long formId) {
        MultivaluedMap<String, String> params = getCommonParamMap();

        // if there is an ID set, add that to the param map
        if (formId != null && formId > 0) {
            params.add(MembershipFormAPIParameterNames.FORM_ID.getName(), Long.toString(formId));
        }
        return params;
    }

    private MultivaluedMap<String, String> getCommonParamMap() {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        // get the logged in user. The user must be logged in to retrieve this data, and should always have an assoc. account
        Optional<EfUser> user = dus.fetchUserByUsername(ident.getPrincipal().getName(), false);
        if (user.isEmpty()) {
            throw new IllegalStateException("Logged in user does not have a discoverable Eclipse account, cannot continue");
        }
        // if the user has organization ID, allow filtering by that, otherwise just use the username
        if (user.get().getOrgId() != null) {
            params.add(MembershipFormAPIParameterNames.ORGANIZATION_ID.getName(), Integer.toString(user.get().getOrgId()));
        }
        params.add(MembershipFormAPIParameterNames.USER_ID.getName(), user.get().getName());
        return params;
    }

}
