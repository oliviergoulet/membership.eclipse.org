/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipsefoundation.membership.application.dto.membership.BaseContact;
import org.eclipsefoundation.membership.application.dto.membership.Contact;
import org.eclipsefoundation.membership.application.dto.membership.FormOrganization;
import org.eclipsefoundation.membership.application.dto.membership.FormWorkingGroup;
import org.eclipsefoundation.membership.application.dto.membership.MembershipForm;
import org.eclipsefoundation.membership.application.dto.membership.SecureTransaction;

import io.quarkus.qute.TemplateData;

@TemplateData
public class FormMailerData {
    public final String name;
    public final MembershipForm form;
    public final FormOrganization org;
    public final List<FormWorkingGroup> wgs;
    public final List<Contact> contacts;
    public final SecureTransaction transaction;
    public final List<? extends BaseContact> prospectiveUsers;
    public final boolean isNewOrganization;

    public FormMailerData(String name, MembershipForm form, FormOrganization org, List<FormWorkingGroup> wgs, List<Contact> contacts,
            boolean isNewOrganization) {
        this.name = name;
        this.form = form;
        this.org = org;
        this.wgs = Collections.unmodifiableList(wgs != null ? wgs : new ArrayList<>());
        this.contacts = Collections.unmodifiableList(contacts != null ? contacts : new ArrayList<>());
        this.transaction = null;
        this.prospectiveUsers = new ArrayList<>();
        this.isNewOrganization = isNewOrganization;
    }

    public FormMailerData(FormMailerData data, SecureTransaction transaction, List<? extends BaseContact> prospectiveUsers) {
        this.name = data.name;
        this.form = data.form;
        this.org = data.org;
        this.wgs = Collections.unmodifiableList(data.wgs != null ? data.wgs : new ArrayList<>());
        this.contacts = Collections.unmodifiableList(data.contacts != null ? data.contacts : new ArrayList<>());
        this.transaction = transaction;
        this.prospectiveUsers = Collections.unmodifiableList(prospectiveUsers != null ? prospectiveUsers : new ArrayList<>());
        this.isNewOrganization = data.isNewOrganization;
    }
}
