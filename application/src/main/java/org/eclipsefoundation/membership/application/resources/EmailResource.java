/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.resources;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.membership.application.dto.membership.EmailOptOut;
import org.eclipsefoundation.membership.application.dto.membership.SecureTransaction;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.membership.application.namespace.TransactionType;
import org.eclipsefoundation.membership.application.service.SecureTransactionService;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;

@Path("email")
public class EmailResource extends AbstractRESTResource {

    @Inject
    SecureTransactionService transactionService;

    @GET
    @Path("optout/{id}/{transactionId}")
    public Response optOutOfEmails(@PathParam("id") String contactId, @PathParam("transactionId") Long transactionId,
            @QueryParam("token") String token, @QueryParam("email") String email) {

        if (StringUtils.isBlank(token)) {
            throw new BadRequestException("Param 'token' is required to use this endpoint");
        }
        if (StringUtils.isBlank(email)) {
            throw new BadRequestException("Param 'email' is required to use this endpoint");
        }

        // Verify transaction is valid and active
        MultivaluedMap<String, String> params = transactionService.createTransactionQueryParams(transactionId, token,
                contactId, TransactionType.OPTOUT);
        SecureTransaction optOutTransaction = transactionService.getActiveTrackedTransaction(params, wrap);

        if (optOutTransaction == null) {
            return Response.status(Response.Status.NOT_FOUND.getStatusCode()).build();
        }

        params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.EMAIL.getName(), email);

        // Check if email already opted out
        List<EmailOptOut> emailsOptedOut = dao.get(new RDBMSQuery<>(wrap, filters.get(EmailOptOut.class), params));

        if (emailsOptedOut == null) {
            return Response.serverError().build();
        } else if (!emailsOptedOut.isEmpty()) {
            transactionService.deactivateTrackedTransaction(optOutTransaction, wrap);
            return Response.ok().build();
        }

        dao.add(new RDBMSQuery<>(wrap, filters.get(EmailOptOut.class)),
                Arrays.asList(createEmailOptOut(email, transactionId)));

        // Deactivate transaction
        transactionService.deactivateTrackedTransaction(optOutTransaction, wrap);

        return Response.ok().build();
    }

    /**
     * Creates an EmailOptOut DTO using the given email and transactio id.
     * 
     * @param email         The entity's email
     * @param transactionId the entity's transaction id
     * @return An EmailOptOut DTO
     */
    private EmailOptOut createEmailOptOut(String email, long transactionId) {
        EmailOptOut dto = new EmailOptOut();
        dto.setEmail(email);
        dto.setTransactionId(transactionId);
        dto.setDateReceived(LocalDateTime.now());
        return dto;
    }
}
