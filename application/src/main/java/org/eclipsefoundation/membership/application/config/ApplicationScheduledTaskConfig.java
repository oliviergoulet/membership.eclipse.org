/*********************************************************************
* Copyright (c) 2023, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.config;

import java.time.Duration;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/**
 * Captures all configs related to the membership application scheduled tasks. Contains the ScheduledDBCleanTask configs and the
 * ScheduledEmailReminderTask configs.
 */
@ConfigMapping(prefix = "eclipse.scheduled")
public interface ApplicationScheduledTaskConfig {

    EmailReminderTaskDefinition emailReminder();

    DBCleanTaskDefinition dbClean();

    OrgCreationTaskDefinition orgCreation();

    /**
     * Captures all configs related to the ScheduledEmailReminderTask. Contains an enabled flag as well as a task interval.
     */
    interface EmailReminderTaskDefinition {

        @WithDefault("true")
        boolean enabled();

        @WithDefault("P7D")
        Duration interval();
    }

    /**
     * Captures all configs related to the ScheduledDBCleanTask. Contains an enabled flag, as well as an entity max age.
     */
    interface DBCleanTaskDefinition {

        @WithDefault("true")
        boolean enabled();

        @WithDefault("P180D")
        Duration maxAge();
    }
    /**
     * Captures all configs related to the ScheduledDBCleanTask. Contains an enabled flag, as well as an entity max age.
     */
    interface OrgCreationTaskDefinition {

        @WithDefault("true")
        boolean enabled();
    }
}
