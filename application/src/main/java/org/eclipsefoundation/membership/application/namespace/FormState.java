/*********************************************************************
* Copyright (c) 2022, 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.membership.application.namespace;

import java.util.stream.Stream;

/**
 * Defined membership form states
 * 
 * @author Martin Lowe
 *
 */
public enum FormState {
    /** Submitted by the user, ready for validation */
    SUBMITTED,
    /** Form is in initial steps, not yet submitted */
    INPROGRESS,
    /** Form has been fulfilled and the organization has been onboarded */
    COMPLETE,
    /** Marked as valid post user submission by EF as ready for processing */
    CONFIRMED,
    /** Form has been marked as invalid or cancelled by the foundation */
    REJECTED,
    /** Initial post-processing of validated form completed, organization + initial contact entries in foundation exist */
    READY_FOR_DOCUMENTS;

    public static FormState getByName(String name) {
        return Stream.of(values()).filter(s -> s.name().equalsIgnoreCase(name)).findFirst().orElse(null);
    }
}
