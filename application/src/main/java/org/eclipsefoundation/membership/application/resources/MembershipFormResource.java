/**
 * Copyright (c) 2021, 2024 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.application.resources;

import java.io.IOException;
import java.io.StringWriter;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.helper.DateTimeHelper;
import org.eclipsefoundation.core.helper.TransformationHelper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.core.namespace.RequestHeaderNames;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.services.AccountService;
import org.eclipsefoundation.membership.application.config.ApplicationBaseConfig;
import org.eclipsefoundation.membership.application.dto.membership.Contact;
import org.eclipsefoundation.membership.application.dto.membership.FormOrganization;
import org.eclipsefoundation.membership.application.dto.membership.FormStatusReport;
import org.eclipsefoundation.membership.application.dto.membership.FormWorkingGroup;
import org.eclipsefoundation.membership.application.dto.membership.MembershipForm;
import org.eclipsefoundation.membership.application.dto.membership.SecureTransaction;
import org.eclipsefoundation.membership.application.dto.membership.ValidationGroups.Completion;
import org.eclipsefoundation.membership.application.model.ConstraintViolationWrapFactory;
import org.eclipsefoundation.membership.application.model.ConstraintViolationWrapFactory.ConstraintViolationWrap;
import org.eclipsefoundation.membership.application.model.FormMailerData;
import org.eclipsefoundation.membership.application.model.FormStatusReportItem;
import org.eclipsefoundation.membership.application.namespace.FormState;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.membership.application.namespace.TransactionType;
import org.eclipsefoundation.membership.application.service.MailerService;
import org.eclipsefoundation.membership.application.service.SecureTransactionService;
import org.eclipsefoundation.membership.application.service.WorkflowService;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.SequenceWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import io.quarkus.security.Authenticated;
import io.smallrye.jwt.auth.principal.DefaultJWTCallerPrincipal;
import jakarta.annotation.security.PermitAll;
import jakarta.inject.Inject;
import jakarta.validation.Validator;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * Handles membership form CRUD requests.
 *
 * @author Martin Lowe
 */
@Authenticated
@Path("form")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MembershipFormResource extends AbstractRESTResource {
    public static final Logger LOGGER = LoggerFactory.getLogger(MembershipFormResource.class);

    @ConfigProperty(name = "eclipse.membership-application.csv-view.allowed-users")
    List<String> allowedUsers;

    @Inject
    ApplicationBaseConfig baseConfig;

    @Inject
    Validator validator;
    @Inject
    MailerService mailer;
    @Inject
    SecureTransactionService transactionService;
    @Inject
    WorkflowService contactService;
    @Inject
    AccountService accounts;
    @Inject
    WorkflowService wflow;

    static final CsvMapper CSV_MAPPER = (CsvMapper) new CsvMapper()
            .registerModule(new JavaTimeModule())
            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    @GET
    public Response getAll(@HeaderParam(value = RequestHeaderNames.CSRF_TOKEN) String csrf) {

        // create parameter map
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.USER_ID.getName(), ident.getPrincipal().getName());
        // retrieve the possible cached object
        List<MembershipForm> results = dao.get(new RDBMSQuery<>(wrap, filters.get(MembershipForm.class), params));
        if (results == null) {
            return Response.serverError().build();
        }
        // return the results as a response
        return Response.ok(results).build();
    }

    @GET
    @Path("{id}")
    public Response get(@PathParam("id") String formID, @HeaderParam(value = RequestHeaderNames.CSRF_TOKEN) String csrf) {
        // check if user is allowed to modify these resources
        Response r = checkAccess(formID);
        if (r != null) {
            return r;
        }

        // create parameter map
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), formID);

        // retrieve the possible cached object
        List<MembershipForm> results = dao.get(new RDBMSQuery<>(wrap, filters.get(MembershipForm.class), params));
        if (results == null) {
            return Response.serverError().build();
        } else if (results.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND.getStatusCode()).build();
        }
        // return the results as a response
        return Response.ok(results.get(0)).build();
    }

    @POST
    public List<MembershipForm> create(MembershipForm mem) {
        mem.setUserID(ident.getPrincipal().getName());
        mem.setDateCreated(DateTimeHelper.getMillis());
        mem.setDateUpdated(mem.getDateCreated());
        mem.setApplicationGroup(baseConfig.applicationGroup());
        return dao.add(new RDBMSQuery<>(wrap, filters.get(MembershipForm.class)), Arrays.asList(mem));
    }

    @PUT
    @Path("{id}")
    public Response update(@PathParam("id") String formID, MembershipForm mem) {
        // make sure we have something to put
        if (mem == null) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()).build();
        }
        // check if user is allowed to modify these resources
        Response r = checkAccess(formID);
        if (r != null) {
            return r;
        }
        mem.setUserID(ident.getPrincipal().getName());
        mem.setDateUpdated(DateTimeHelper.getMillis());
        mem.setApplicationGroup(baseConfig.applicationGroup());
        // need to fetch ref to use attached entity
        MembershipForm ref = mem.cloneTo(dao.getReference(formID, MembershipForm.class));
        return Response.ok(dao.add(new RDBMSQuery<>(wrap, filters.get(MembershipForm.class)), Arrays.asList(ref))).build();
    }

    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("id") String formID) {
        // check if user is allowed to modify these resources
        Response r = checkAccess(formID);
        if (r != null) {
            return r;
        }
        // standard form params
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), formID);

        // FK dependents params
        MultivaluedMap<String, String> depParams = new MultivaluedMapImpl<>();
        depParams.add(MembershipFormAPIParameterNames.FORM_ID.getName(), formID);
        dao.delete(new RDBMSQuery<>(wrap, filters.get(FormWorkingGroup.class), depParams));
        dao.delete(new RDBMSQuery<>(wrap, filters.get(Contact.class), depParams));
        dao.delete(new RDBMSQuery<>(wrap, filters.get(FormOrganization.class), depParams));
        dao.delete(new RDBMSQuery<>(wrap, filters.get(MembershipForm.class), params));
        return Response.ok().build();
    }

    @POST
    @Path("{id}/complete")
    public Response completeForm(@PathParam("id") String formID, @QueryParam("force") boolean force) {
        // check if user is allowed to modify these resources
        Response r = checkAccess(formID);
        if (r != null) {
            return r;
        }

        // create parameter map
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), formID);

        // retrieve the membership form for the current post
        List<MembershipForm> results = dao.get(new RDBMSQuery<>(wrap, filters.get(MembershipForm.class), params));
        if (results == null) {
            return Response.serverError().build();
        } else if (results.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND.getStatusCode()).build();
        } else if (!force
                && (FormState.SUBMITTED.equals(results.get(0).getState()) || FormState.COMPLETE.equals(results.get(0).getState()))) {
            // dont send email if force param is not true and form already submitted
            return Response.status(Response.Status.NO_CONTENT.getStatusCode()).build();
        }

        // retrieve all of the info needed to post the form email
        MultivaluedMap<String, String> extraparams = new MultivaluedMapImpl<>();
        extraparams.add(MembershipFormAPIParameterNames.FORM_ID.getName(), formID);
        List<FormOrganization> orgs = dao.get(new RDBMSQuery<>(wrap, filters.get(FormOrganization.class), extraparams));
        List<FormWorkingGroup> wgs = dao.get(new RDBMSQuery<>(wrap, filters.get(FormWorkingGroup.class), extraparams));
        List<Contact> contacts = dao.get(new RDBMSQuery<>(wrap, filters.get(Contact.class), extraparams));

        // check that there are no validation violations for form elements
        List<ConstraintViolationWrap> violations = findFormViolations(results, orgs, wgs, contacts);
        if (!violations.isEmpty()) {
            return Response.status(Status.BAD_REQUEST.getStatusCode()).entity(violations).build();
        }

        MembershipForm mf = results.get(0);
        // Create new transaction and add to DB
        SecureTransaction transaction = transactionService.generateUntrackedTransaction(mf.getId(), TransactionType.FORM);
        dao.add(new RDBMSQuery<>(wrap, filters.get(SecureTransaction.class)), Arrays.asList(transaction));

        // send the forms to the mailing service
        FormOrganization org = !orgs.isEmpty() ? orgs.get(0) : null;
        FormMailerData data = new FormMailerData(generateName((DefaultJWTCallerPrincipal) ident.getPrincipal()), mf, org, wgs, contacts,
                wflow.doesOrganizationExist(org));
        mailer.sendToFormAuthor(data);
        mailer.sendToMembershipTeam(new FormMailerData(data, transaction, contactService.getProspectiveUsers(contacts)));

        // update the state and push the update
        mf.setDateSubmitted(DateTimeHelper.getMillis());
        mf.setDateUpdated(mf.getDateSubmitted());
        mf.setState(FormState.SUBMITTED);
        return Response.ok(dao.add(new RDBMSQuery<>(wrap, filters.get(MembershipForm.class)), Arrays.asList(mf))).build();
    }

    @GET
    @Path("state/{state}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getActive(@PathParam("state") FormState state) throws IOException {
        // check if allow listed user is requesting data
        if (!allowedUsers.contains(ident.getPrincipal().getName())) {
            return Response.status(Response.Status.FORBIDDEN.getStatusCode()).build();
        }
        // create parameter map for our form state
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.FORM_STATE.getName(), state.name());
        // generate CSV output
        try (StringWriter sw = new StringWriter()) {
            // create the CSV writer for membership forms
            SequenceWriter writer = CSV_MAPPER.writer(CSV_MAPPER.schemaFor(FormStatusReportItem.class).withHeader()).writeValues(sw);
            // create query to get all forms (no limit)
            RDBMSQuery<FormStatusReport> formQuery = new RDBMSQuery<>(wrap, filters.get(FormStatusReport.class), params);
            formQuery.setUseLimit(false);
            // retrieve the membership form for the current state and write to output
            writer.writeAll(generateReportItems(dao.get(formQuery)));
            return Response.ok(sw.toString()).build();
        }
    }

    @GET
    @PermitAll
    @Path("{id}/{transactionId}")
    public Response validateForm(@PathParam("id") String formId, @PathParam("transactionId") Long transactionId,
            @QueryParam("token") String token, @QueryParam("state") FormState state) {

        if (StringUtils.isBlank(token)) {
            throw new BadRequestException("Param 'token' is required to use this endpoint");
        }
        if (state == null) {
            throw new BadRequestException("Param 'state' is required to use this endpoint");
        }

        // Verify transaction is valid and active
        MultivaluedMap<String, String> params = transactionService
                .createTransactionQueryParams(transactionId, token, formId, TransactionType.FORM);
        SecureTransaction formTransaction = transactionService.getActiveTrackedTransaction(params, wrap);

        if (formTransaction == null) {
            return Response.status(Response.Status.NOT_FOUND.getStatusCode()).build();
        }

        // Must set form state to null otherwise it will include the state from the
        // query params
        params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), formId);
        params.add(MembershipFormAPIParameterNames.FORM_STATE.getName(), null);
        List<MembershipForm> formResults = dao.get(new RDBMSQuery<>(wrap, filters.get(MembershipForm.class), params));
        if (formResults == null) {
            return Response.serverError().build();
        } else if (formResults.isEmpty()) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Form ID: {} - No form found", TransformationHelper.formatLog(formId));
            }
            return Response.status(Response.Status.NOT_FOUND.getStatusCode()).build();
        }
        // retrieve contacts that will be passed to user creation calls
        MultivaluedMap<String, String> formParams = new MultivaluedMapImpl<>();
        formParams.add(MembershipFormAPIParameterNames.FORM_ID.getName(), formId);
        List<Contact> contacts = dao.get(new RDBMSQuery<>(wrap, filters.get(Contact.class), formParams));

        // Update form with new state
        formResults.get(0).setState(state);
        List<MembershipForm> updatedFormResults = dao.add(new RDBMSQuery<>(wrap, filters.get(MembershipForm.class), params), formResults);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Form with id {} set to {}", TransformationHelper.formatLog(formId), state);
        }

        // Set transaction to inactive
        transactionService.deactivateTrackedTransaction(formTransaction, wrap);

        // If form is rejected, exit
        if (state == FormState.REJECTED) {
            return Response.ok().build();
        }
        contactService.createMissingUserAccounts(contacts, updatedFormResults.get(0).getUserID(), TransactionType.FORM, formId);

        return Response.ok().build();
    }

    /**
     * Validates the form fields and returns a set containing all form violations if they exist
     *
     * @param results the membership forms to validate
     * @param orgs the organization forms to validate
     * @param wgs the working gorup forms to validate
     * @param contacts the contact info to validate
     * @return A set of form violations
     */
    private List<ConstraintViolationWrap> findFormViolations(List<MembershipForm> results, List<FormOrganization> orgs,
            List<FormWorkingGroup> wgs, List<Contact> contacts) {
        List<ConstraintViolationWrap> violations = new ArrayList<>();
        violations.addAll(recordViolations(results));
        violations.addAll(recordViolations(orgs));
        violations.addAll(recordViolations(wgs));
        violations.addAll(recordViolations(contacts));

        return violations;
    }

    private List<FormStatusReportItem> generateReportItems(List<FormStatusReport> formItems) {
        return formItems.stream().map(this::generateReportItem).collect(Collectors.toList());
    }

    private FormStatusReportItem generateReportItem(FormStatusReport statusReport) {
        // get the user from cache for current form
        Optional<EfUser> user = accounts.fetchUserByUsername(statusReport.getUserId(), true);

        return FormStatusReportItem
                .builder()
                .setWorkingGroupCount(statusReport.getWorkingGroupCount())
                .setUserEmail(user.isPresent() ? user.get().getMail() : "")
                .setFullName(user.isPresent() ? user.get().getFullName() : "")
                .setDateCreated(ZonedDateTime.ofInstant(Instant.ofEpochMilli(statusReport.getDateCreated()), ZoneId.systemDefault()))
                .setDateUpdated(statusReport.getDateUpdated() != null
                        ? ZonedDateTime.ofInstant(Instant.ofEpochMilli(statusReport.getDateUpdated()), ZoneId.systemDefault())
                        : null)
                .setDateSubmitted(statusReport.getDateSubmitted() != null
                        ? ZonedDateTime.ofInstant(Instant.ofEpochMilli(statusReport.getDateSubmitted()), ZoneId.systemDefault())
                        : null)
                .setFormId(statusReport.getFormId())
                .setUserId(statusReport.getUserId())
                .setMembershipLevel(statusReport.getMembershipLevel())
                .setRegistrationCountry(statusReport.getRegistrationCountry())
                .setState(statusReport.getFormState())
                .setOrganizationName(statusReport.getOrganizationName())
                .setFormCompletionStage(statusReport.getFormCompletionStage())
                .build();
    }

    private <T extends BareNode> Set<ConstraintViolationWrap> recordViolations(List<T> items) {
        ConstraintViolationWrapFactory factory = new ConstraintViolationWrapFactory();
        return items
                .stream()
                .flatMap(item -> factory.build(validator.validate(item, Completion.class)).stream())
                .collect(Collectors.toSet());
    }

    private String generateName(DefaultJWTCallerPrincipal defaultPrin) {
        return new StringBuilder()
                .append((String) defaultPrin.getClaim("given_name"))
                .append(" ")
                .append((String) defaultPrin.getClaim("family_name"))
                .toString();
    }
}
