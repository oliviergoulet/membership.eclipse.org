  @Library('common-shared') _

pipeline {

  agent {
    kubernetes {
      label 'buildenv-agent'
      yaml '''
      apiVersion: v1
      kind: Pod
      spec:
        containers:
        - name: buildcontainer
          image: eclipsefdn/stack-build-agent:h111.3-n18.17-jdk17
          imagePullPolicy: Always
          command:
          - cat
          tty: true
          resources:
            requests:
              cpu: "1"
              memory: "4Gi"
            limits:
              cpu: "2"
              memory: "4Gi"
          env:
          - name: "HOME"
            value: "/home/jenkins"
          - name: "MAVEN_OPTS"
            value: "-Duser.home=/home/jenkins"
          volumeMounts:
          - name: m2-repo
            mountPath: /home/jenkins/.m2/repository
          - name: m2-secret-dir
            mountPath: /home/jenkins/.m2/settings.xml
            subPath: settings.xml
            readOnly: true
          - mountPath: "/home/jenkins/.m2/settings-security.xml"
            name: "m2-secret-dir"
            readOnly: true
            subPath: "settings-security.xml"
          - mountPath: "/home/jenkins/.mavenrc"
            name: "m2-dir"
            readOnly: true
            subPath: ".mavenrc"
          - mountPath: "/home/jenkins/.m2/wrapper"
            name: "m2-wrapper"
            readOnly: false
          - mountPath: "/home/jenkins/.cache"
            name: "yarn-cache"
            readOnly: false
          - mountPath: "/home/jenkins/.yarn"
            name: "secondary-yarn-cache"
            readOnly: false
          - mountPath: "/home/jenkins/.sonar"
            name: "sonar-cache"
            readOnly: false
        - name: jnlp
          resources:
            requests:
              memory: "1024Mi"
              cpu: "500m"
            limits:
              memory: "1024Mi"
              cpu: "1000m"
        volumes:
        - name: "m2-dir"
          configMap:
            name: "m2-dir"
        - name: m2-secret-dir
          secret:
            secretName: m2-secret-dir
        - name: m2-repo
          emptyDir: {}
        - name: m2-wrapper
          emptyDir: {}
        - name: yarn-cache
          emptyDir: {}
        - name: secondary-yarn-cache
          emptyDir: {}
        - name: sonar-cache
          emptyDir: {}
      '''
    }
  }

  environment {
    APP_NAME = 'eclipsefdn-react-membership'
    NAMESPACE = 'foundation-internal-webdev-apps'
    WEB_IMAGE_NAME = 'eclipsefdn/eclipsefdn-react-membership-www'
    APPLICATION_IMAGE_NAME = 'eclipsefdn/eclipsefdn-membership-application'
    APPLICATION_CONTAINER_NAME = 'application-api'
    PORTAL_IMAGE_NAME = 'eclipsefdn/eclipsefdn-membership-portal'
    PORTAL_CONTAINER_NAME = 'portal-api'
    ENVIRONMENT = sh(
      script: """
        printf "${env.BRANCH_NAME}"
      """,
      returnStdout: true
    )
    TAG_NAME = sh(
      script: """
        GIT_COMMIT_SHORT=\$(git rev-parse --short ${env.GIT_COMMIT})
        if [ "${env.ENVIRONMENT}" = "" ]; then
          printf \${GIT_COMMIT_SHORT}-${env.BUILD_NUMBER}
        else
          printf ${env.ENVIRONMENT}-\${GIT_COMMIT_SHORT}-${env.BUILD_NUMBER}
        fi
      """,
      returnStdout: true
    )
  }

  options {
    buildDiscarder(logRotator(numToKeepStr: '10'))
    timeout(time: 30, unit: 'MINUTES') 
  }

  stages {
    stage('Build with Sonarcloud scan') {
      when {
          branch 'main'
        }

      steps {
        container('buildcontainer') {
          readTrusted 'Makefile'
          readTrusted 'pom.xml'
          readTrusted 'application/pom.xml'
          readTrusted 'portal/pom.xml'
          
          readTrusted 'application/package.json'
          readTrusted 'application/yarn.lock'
          readTrusted 'portal/package.json'
          readTrusted 'portal/yarn.lock'

          sh 'make compile-test-resources'
          withCredentials([string(credentialsId: 'sonarcloud-membership-eclipse-org', variable: 'SONAR_TOKEN')]) {
              sh 'mvn clean verify -B org.sonarsource.scanner.maven:sonar-maven-plugin:sonar -Dsonar.login=${SONAR_TOKEN}'
            }

          stash name: "target", includes: "**/target/**/*"
          stash name: "build", includes: "src/main/www/build/**/*"
        }
      }
    }

    stage('Build without Sonarcloud scan') {
      when {
          not {
           branch 'main'
          }
        }

      steps {
        container('buildcontainer') {
          readTrusted 'Makefile'
          readTrusted 'pom.xml'
          readTrusted 'application/pom.xml'
          readTrusted 'portal/pom.xml'
          
          readTrusted 'application/package.json'
          readTrusted 'application/yarn.lock'
          readTrusted 'portal/package.json'
          readTrusted 'portal/yarn.lock'
          
          sh 'make compile'
          stash name: "target", includes: "**/target/**/*"
          stash name: "build", includes: "src/main/www/build/**/*"
        }
      }
    }

    stage('Build docker image') {
      agent {
        label 'docker-build'
      }
      steps {
        readTrusted 'portal/src/main/docker/Dockerfile.jvm'
        readTrusted 'application/src/main/docker/Dockerfile.jvm'
        readTrusted 'src/main/docker/Dockerfile.www'

        unstash 'target'
        unstash 'build'

        sh '''
          docker build -f portal/src/main/docker/Dockerfile.jvm --no-cache -t ${PORTAL_IMAGE_NAME}:${TAG_NAME} -t ${PORTAL_IMAGE_NAME}:latest ./portal
          docker build -f application/src/main/docker/Dockerfile.jvm --no-cache -t ${APPLICATION_IMAGE_NAME}:${TAG_NAME} -t ${APPLICATION_IMAGE_NAME}:latest ./application
          docker build -f src/main/docker/Dockerfile.www --no-cache -t ${WEB_IMAGE_NAME}:${TAG_NAME} -t ${WEB_IMAGE_NAME}:latest .
        '''
      }
    }

    stage('Push docker image') {
      agent {
        label 'docker-build'
      }
      when {
        anyOf {
          environment name: 'ENVIRONMENT', value: 'production'
          environment name: 'ENVIRONMENT', value: 'staging'
        }
      }
      steps {
        withDockerRegistry([credentialsId: '04264967-fea0-40c2-bf60-09af5aeba60f', url: 'https://index.docker.io/v1/']) {
          sh '''
            docker push ${PORTAL_IMAGE_NAME}:${TAG_NAME}
            docker push ${PORTAL_IMAGE_NAME}:latest
            docker push ${APPLICATION_IMAGE_NAME}:${TAG_NAME}
            docker push ${APPLICATION_IMAGE_NAME}:latest
            docker push ${WEB_IMAGE_NAME}:${TAG_NAME}
            docker push ${WEB_IMAGE_NAME}:latest
          '''
        }
      }
    }

    stage('Deploy to cluster') {
      agent {
        kubernetes {
          label 'kubedeploy-agent'
          yaml '''
          apiVersion: v1
          kind: Pod
          spec:
            containers:
            - name: kubectl
              image: eclipsefdn/kubectl:okd-c1
              command:
              - cat
              tty: true
              resources:
                limits:
                  cpu: 1
                  memory: 1Gi
              volumeMounts:
              - mountPath: "/home/default/.kube"
                name: "dot-kube"
                readOnly: false
            - name: jnlp
              resources:
                limits:
                  cpu: 1
                  memory: 1Gi
            volumes:
            - name: "dot-kube"
              emptyDir: {}
          '''
        }
      }

      when {
        anyOf {
          environment name: 'ENVIRONMENT', value: 'production'
          environment name: 'ENVIRONMENT', value: 'staging'
        }
      }
      steps {
        container('kubectl') {
          updateContainerImage([
            namespace: "${env.NAMESPACE}",
            selector: "app=${env.APP_NAME},environment=${env.ENVIRONMENT}",
            containerName: "${env.PORTAL_CONTAINER_NAME}",
            newImageRef: "${env.PORTAL_IMAGE_NAME}:${env.TAG_NAME}"
          ])
          updateContainerImage([
            namespace: "${env.NAMESPACE}",
            selector: "app=${env.APP_NAME},environment=${env.ENVIRONMENT}",
            containerName: "${env.APPLICATION_CONTAINER_NAME}",
            newImageRef: "${env.APPLICATION_IMAGE_NAME}:${env.TAG_NAME}"
          ])
          updateContainerImage([
            namespace: "${env.NAMESPACE}",
            selector: "app=${env.APP_NAME},environment=${env.ENVIRONMENT}",
            containerName: "nginx",
            newImageRef: "${env.WEB_IMAGE_NAME}:${env.TAG_NAME}"
          ])
        }
      }
    }
  }

  post {
    always {
      deleteDir() /* clean up workspace */
      sendNotifications currentBuild
    }
  }
}
