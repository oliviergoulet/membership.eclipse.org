#!/usr/bin/env bash
# ===========================================================================
# Copyright (c) 2021 Eclipse Foundation and others.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Christopher Guindon (Eclipse Foundation)
# ==========================================================================

set -euo pipefail
yarn --cwd src/main/www install --frozen-lockfile